-- MySQL dump 10.17  Distrib 10.3.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: neosoft.shopimind.com    Database: apporteurdaffaires
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB-10.2.13+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apporteur`
--

DROP TABLE IF EXISTS `apporteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apporteur` (
  `IdApporteur` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nom` text CHARACTER SET utf8 NOT NULL,
  `Prenom` text CHARACTER SET utf8 NOT NULL,
  `Societe` varchar(150) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `MotDePasse` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Adresse` varchar(100) CHARACTER SET utf8 NOT NULL,
  `SuiteAdresse` varchar(100) CHARACTER SET utf8 NOT NULL,
  `CodePostal` int(11) NOT NULL,
  `Ville` text CHARACTER SET utf8 NOT NULL,
  `Pays` text CHARACTER SET utf8 NOT NULL,
  `telephone` text NOT NULL,
  `codepromo` text NOT NULL,
  `NumSIRET` varchar(30) CHARACTER SET utf8 NOT NULL,
  `NumTVA` varchar(30) CHARACTER SET utf8 NOT NULL,
  `PrcentageRemuneration` tinyint(2) NOT NULL DEFAULT 15,
  `DateDajout` datetime NOT NULL,
  `DateSignature` datetime NOT NULL DEFAULT current_timestamp(),
  `DateDeModif` datetime NOT NULL,
  `IBAN` varchar(34) CHARACTER SET utf8 NOT NULL,
  `BIC` varchar(12) CHARACTER SET utf8 NOT NULL,
  `NomBanque` text CHARACTER SET utf8 NOT NULL,
  `NomTitulaire` text CHARACTER SET utf8 NOT NULL,
  `service` tinyint(1) unsigned zerofill NOT NULL DEFAULT 1,
  `IdSalesService` int(10) unsigned NOT NULL DEFAULT 0,
  `IdParrain` int(10) unsigned NOT NULL DEFAULT 0,
  `PourcentageParrainage` tinyint(2) unsigned NOT NULL DEFAULT 5,
  PRIMARY KEY (`IdApporteur`),
  KEY `IdSalesService` (`IdSalesService`),
  KEY `Email_MotDePasse` (`Email`,`MotDePasse`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apporteur`
--

LOCK TABLES `apporteur` WRITE;
/*!40000 ALTER TABLE `apporteur` DISABLE KEYS */;
/*!40000 ALTER TABLE `apporteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codepromo`
--

DROP TABLE IF EXISTS `codepromo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codepromo` (
  `IdCode` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdApporteur` int(10) unsigned NOT NULL DEFAULT 0,
  `IdBundle` int(10) unsigned NOT NULL DEFAULT 0,
  `Code` varchar(32) NOT NULL DEFAULT '',
  `Bundle` varchar(45) NOT NULL DEFAULT '',
  `tracking_code` text DEFAULT NULL,
  PRIMARY KEY (`IdCode`),
  UNIQUE KEY `Code` (`Code`),
  KEY `IdApporteur` (`IdApporteur`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codepromo`
--

LOCK TABLES `codepromo` WRITE;
/*!40000 ALTER TABLE `codepromo` DISABLE KEYS */;
/*!40000 ALTER TABLE `codepromo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commission`
--

DROP TABLE IF EXISTS `commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commission` (
  `IdCommission` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdFilleul` int(10) unsigned NOT NULL,
  `MontComSurAbo` float(6,2) NOT NULL,
  `MontComSurHorsForfait` float(6,2) NOT NULL,
  `Payee` tinyint(1) DEFAULT NULL COMMENT 'Payee=1/NonPaye=0',
  PRIMARY KEY (`IdCommission`),
  KEY `IdFilleul` (`IdFilleul`)
) ENGINE=InnoDB AUTO_INCREMENT=1296 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commission`
--

LOCK TABLES `commission` WRITE;
/*!40000 ALTER TABLE `commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commission_parrainage`
--

DROP TABLE IF EXISTS `commission_parrainage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commission_parrainage` (
  `IdCommission` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdFilleul` int(10) unsigned NOT NULL,
  `MontComSurAbo` float(6,2) NOT NULL,
  `MontComSurHorsForfait` float(6,2) NOT NULL,
  `Payee` tinyint(1) DEFAULT NULL COMMENT 'Payee=1/NonPaye=0',
  PRIMARY KEY (`IdCommission`),
  KEY `IdFilleul` (`IdFilleul`),
  CONSTRAINT `commission_parrainage_ibfk_1` FOREIGN KEY (`IdFilleul`) REFERENCES `apporteur` (`IdApporteur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commission_parrainage`
--

LOCK TABLES `commission_parrainage` WRITE;
/*!40000 ALTER TABLE `commission_parrainage` DISABLE KEYS */;
/*!40000 ALTER TABLE `commission_parrainage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filleul`
--

DROP TABLE IF EXISTS `filleul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filleul` (
  `IdFilleul` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdShop` int(10) unsigned NOT NULL DEFAULT 0,
  `IdPsCustomer` int(50) unsigned NOT NULL,
  `IdApporteur` int(10) unsigned NOT NULL,
  `Nom` text CHARACTER SET utf8 NOT NULL,
  `URL` varchar(200) CHARACTER SET utf8 NOT NULL,
  `Pourcentage` tinyint(2) NOT NULL,
  `Statut` tinyint(1) DEFAULT NULL COMMENT 'Innactif=0/Actif=1',
  `DteDajout` datetime NOT NULL DEFAULT current_timestamp(),
  `DteDeModif` datetime NOT NULL DEFAULT current_timestamp(),
  `logAsShop` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`IdFilleul`),
  KEY `idApporteur` (`IdApporteur`),
  CONSTRAINT `filleul_ibfk_1` FOREIGN KEY (`IdApporteur`) REFERENCES `apporteur` (`IdApporteur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filleul`
--

LOCK TABLES `filleul` WRITE;
/*!40000 ALTER TABLE `filleul` DISABLE KEYS */;
/*!40000 ALTER TABLE `filleul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formulairedecontact`
--

DROP TABLE IF EXISTS `formulairedecontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulairedecontact` (
  `IdFormulaire` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TypeDemande` text CHARACTER SET utf8 NOT NULL,
  `Commentaire` longtext CHARACTER SET utf8 NOT NULL,
  `IdApporteur` int(10) unsigned NOT NULL,
  PRIMARY KEY (`IdFormulaire`),
  KEY `IdApporteur` (`IdApporteur`),
  KEY `IdApporteur_2` (`IdApporteur`),
  CONSTRAINT `FK_formulairedecontact_apporteur` FOREIGN KEY (`IdApporteur`) REFERENCES `apporteur` (`IdApporteur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formulairedecontact`
--

LOCK TABLES `formulairedecontact` WRITE;
/*!40000 ALTER TABLE `formulairedecontact` DISABLE KEYS */;
/*!40000 ALTER TABLE `formulairedecontact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paiement`
--

DROP TABLE IF EXISTS `paiement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paiement` (
  `IdPaiement` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdApporteur` int(10) unsigned NOT NULL,
  `NomPdfFacturePaiement` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Date` datetime NOT NULL,
  `MontantCommission` float(6,2) NOT NULL,
  `Etat` tinyint(1) DEFAULT NULL COMMENT 'Non effectue =0/Effectue=1 ',
  PRIMARY KEY (`IdPaiement`),
  KEY `IdApporteur` (`IdApporteur`),
  CONSTRAINT `fk_Id_Apporteur` FOREIGN KEY (`IdApporteur`) REFERENCES `apporteur` (`IdApporteur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paiement`
--

LOCK TABLES `paiement` WRITE;
/*!40000 ALTER TABLE `paiement` DISABLE KEYS */;
/*!40000 ALTER TABLE `paiement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parrainages`
--

DROP TABLE IF EXISTS `parrainages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parrainages` (
  `IdParrainage` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Prenom` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Email` varchar(255) NOT NULL DEFAULT '',
  `IdApporteur` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`IdParrainage`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parrainages`
--

LOCK TABLES `parrainages` WRITE;
/*!40000 ALTER TABLE `parrainages` DISABLE KEYS */;
/*!40000 ALTER TABLE `parrainages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-11  8:56:42
