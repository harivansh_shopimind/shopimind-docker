-- MySQL dump 10.17  Distrib 10.3.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: neosoft.shopimind.com    Database: shopyminddatabyshop
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB-10.2.13+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emails_rejects_shops_1`
--

DROP TABLE IF EXISTS `emails_rejects_shops_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_rejects_shops_1` (
  `id_email_reject` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `email` char(255) NOT NULL,
  `reject_type` char(25) NOT NULL DEFAULT 'soft-bounce' COMMENT 'hard-bounce, soft-bounce, spam, unsub',
  `reject_from` char(25) NOT NULL DEFAULT '',
  `reject_message` text DEFAULT NULL,
  `ems_category` char(50) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_email_reject`,`id_shop_customer`,`id_shop`,`email`,`reject_type`,`date`),
  KEY `fk_id_shop_customer` (`email`),
  KEY `fk_nl_reject` (`reject_type`),
  KEY `fk_date` (`date`),
  KEY `reject_type_email` (`reject_type`,`email`),
  KEY `ems_category` (`ems_category`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`,`reject_type`),
  KEY `reject_from` (`reject_from`)
) ENGINE=InnoDB AUTO_INCREMENT=2660199 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_rejects_shops_1`
--

LOCK TABLES `emails_rejects_shops_1` WRITE;
/*!40000 ALTER TABLE `emails_rejects_shops_1` DISABLE KEYS */;
INSERT INTO `emails_rejects_shops_1` VALUES (2660198,26637692,1,'soufianeben94@gmail.com','soft-bounce','fifko','bjhobhj',NULL,'2018-08-08 14:10:08');
/*!40000 ALTER TABLE `emails_rejects_shops_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_rejects_shops_10`
--

DROP TABLE IF EXISTS `emails_rejects_shops_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_rejects_shops_10` (
  `id_email_reject` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `email` char(255) NOT NULL,
  `reject_type` char(25) NOT NULL DEFAULT 'soft-bounce' COMMENT 'hard-bounce, soft-bounce, spam, unsub',
  `reject_from` char(25) NOT NULL DEFAULT '',
  `reject_message` text DEFAULT NULL,
  `ems_category` char(50) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_email_reject`,`id_shop_customer`,`id_shop`,`email`,`reject_type`,`date`),
  KEY `fk_id_shop_customer` (`email`),
  KEY `fk_nl_reject` (`reject_type`),
  KEY `fk_date` (`date`),
  KEY `reject_type_email` (`reject_type`,`email`),
  KEY `ems_category` (`ems_category`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`,`reject_type`),
  KEY `reject_from` (`reject_from`)
) ENGINE=InnoDB AUTO_INCREMENT=2947869 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_rejects_shops_10`
--

LOCK TABLES `emails_rejects_shops_10` WRITE;
/*!40000 ALTER TABLE `emails_rejects_shops_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_rejects_shops_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_rejects_shops_8`
--

DROP TABLE IF EXISTS `emails_rejects_shops_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_rejects_shops_8` (
  `id_email_reject` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `email` char(255) NOT NULL,
  `reject_type` char(25) NOT NULL DEFAULT 'soft-bounce' COMMENT 'hard-bounce, soft-bounce, spam, unsub',
  `reject_from` char(25) NOT NULL DEFAULT '',
  `reject_message` text DEFAULT NULL,
  `ems_category` char(50) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_email_reject`,`id_shop_customer`,`id_shop`,`email`,`reject_type`,`date`),
  KEY `fk_id_shop_customer` (`email`),
  KEY `fk_nl_reject` (`reject_type`),
  KEY `fk_date` (`date`),
  KEY `reject_type_email` (`reject_type`,`email`),
  KEY `ems_category` (`ems_category`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`,`reject_type`),
  KEY `reject_from` (`reject_from`)
) ENGINE=InnoDB AUTO_INCREMENT=2947869 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_rejects_shops_8`
--

LOCK TABLES `emails_rejects_shops_8` WRITE;
/*!40000 ALTER TABLE `emails_rejects_shops_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_rejects_shops_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_rejects_shops_9`
--

DROP TABLE IF EXISTS `emails_rejects_shops_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_rejects_shops_9` (
  `id_email_reject` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `email` char(255) NOT NULL,
  `reject_type` char(25) NOT NULL DEFAULT 'soft-bounce' COMMENT 'hard-bounce, soft-bounce, spam, unsub',
  `reject_from` char(25) NOT NULL DEFAULT '',
  `reject_message` text DEFAULT NULL,
  `ems_category` char(50) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_email_reject`,`id_shop_customer`,`id_shop`,`email`,`reject_type`,`date`),
  KEY `fk_id_shop_customer` (`email`),
  KEY `fk_nl_reject` (`reject_type`),
  KEY `fk_date` (`date`),
  KEY `reject_type_email` (`reject_type`,`email`),
  KEY `ems_category` (`ems_category`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`,`reject_type`),
  KEY `reject_from` (`reject_from`)
) ENGINE=InnoDB AUTO_INCREMENT=2947869 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_rejects_shops_9`
--

LOCK TABLES `emails_rejects_shops_9` WRITE;
/*!40000 ALTER TABLE `emails_rejects_shops_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_rejects_shops_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_carriers_1`
--

DROP TABLE IF EXISTS `shops_carriers_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_carriers_1` (
  `id_shop_carrier` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_carrier` varchar(45) NOT NULL,
  `name` varchar(128) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`id_shop_carrier`),
  UNIQUE KEY `id_shop_id_carrier` (`id_shop`,`id_carrier`),
  KEY `name` (`name`),
  KEY `date` (`date`),
  KEY `date_modification` (`date_modification`),
  KEY `date_creation` (`date_creation`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_carriers_1`
--

LOCK TABLES `shops_carriers_1` WRITE;
/*!40000 ALTER TABLE `shops_carriers_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_carriers_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_carriers_10`
--

DROP TABLE IF EXISTS `shops_carriers_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_carriers_10` (
  `id_shop_carrier` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_carrier` varchar(45) NOT NULL,
  `name` varchar(128) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`id_shop_carrier`),
  UNIQUE KEY `id_shop_id_carrier` (`id_shop`,`id_carrier`),
  KEY `name` (`name`),
  KEY `date` (`date`),
  KEY `date_modification` (`date_modification`),
  KEY `date_creation` (`date_creation`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_carriers_10`
--

LOCK TABLES `shops_carriers_10` WRITE;
/*!40000 ALTER TABLE `shops_carriers_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_carriers_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_1`
--

DROP TABLE IF EXISTS `shops_customers_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_1` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `perfect_timing` time DEFAULT NULL,
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26637739 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_1`
--

LOCK TABLES `shops_customers_1` WRITE;
/*!40000 ALTER TABLE `shops_customers_1` DISABLE KEYS */;
INSERT INTO `shops_customers_1` VALUES (26637692,'shpm-1001@yopmail.com',1,'','','shpm-1001@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:18',0,'2018-07-24 12:48:00'),(26637702,'shpm-1002@mailinator.com',1,'','','shpm-1002@mailinator.com','0','2018-07-24','0','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:48:00'),(26637677,'shpm-10000@mailinator.com',1,'test1','test1','shpm-10000@mailinator.com','0','1994-07-25','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:11',0,'2018-07-24 12:50:00'),(26637678,'shpm-10002@mailinator.com',1,'','','shpm-10002@mailinator.com','0','1994-07-25','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:12',0,'2018-07-24 12:50:00'),(26637679,'shpm-10003@yopmail.com',1,'test1','test1','shpm-10003@yopmail.com','0','1994-07-25','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:12',0,'2018-07-24 12:50:00'),(26637680,'shpm-10006@mailinator.com',1,'','','shpm-10006@mailinator.com','0','1914-12-16','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:13',0,'2018-07-24 12:50:00'),(26637681,'shpm-10007@yopmail.com',1,'','','shpm-10007@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:13',0,'2018-07-24 12:50:00'),(26637682,'shpm-10010@mailinator.com',1,'','','shpm-10010@mailinator.com','0','0000-00-00','0','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:14',0,'2018-07-24 12:50:00'),(26637683,'shpm-10011@yopmail.com',1,'','','shpm-10011@yopmail.com','0','0000-00-00','0','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:14',0,'2018-07-24 12:50:00'),(26637684,'shpm-10012@mailinator.com',1,'','','shpm-10012@mailinator.com','0','0000-00-00','0','1','','qd',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:15',0,'2018-07-24 12:50:00'),(26637686,'shpm-10014@mailinator.com',1,'','','shpm-10014@mailinator.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:16',0,'2018-07-24 12:50:00'),(26637689,'shpm-10017@yopmail.com',1,'','','shpm-10017@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:17',0,'2018-07-24 12:50:00'),(26637690,'shpm-10018@mailinator.com',1,'','','shpm-10018@mailinator.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:17',0,'2018-07-24 12:50:00'),(26637691,'shpm-10019@yopmail.com',1,'','','shpm-10019@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:18',0,'2018-07-24 12:50:00'),(26637693,'shpm-10020@mailinator.com',1,'','','shpm-10020@mailinator.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:19',0,'2018-07-24 12:50:00'),(26637694,'shpm-10021@yopmail.com',1,'','test1','shpm-10021@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:19',0,'2018-07-24 12:50:00'),(26637695,'shpm-10023@yopmail.com',1,'','','shpm-10023@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:20',0,'2018-07-24 12:50:00'),(26637696,'shpm-10024@mailinator.com',1,'','','shpm-10024@mailinator.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:20',0,'2018-07-24 12:50:00'),(26637697,'shpm-10025@yopmail.com',1,'','','shpm-10025@yopmail.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,83,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:20',0,'2018-07-24 12:50:00'),(26637698,'shpm-10026@mailinator.com',1,'','','shpm-10026@mailinator.com','0','2018-07-24','0','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,83,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:21',0,'2018-07-24 12:50:00'),(26637699,'shpm-10027@yopmail.com',1,'','','shpm-10027@yopmail.com','0','2018-07-24','0','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637700,'shpm-10028@mailinator.com',1,'','','shpm-10028@mailinator.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637701,'shpm-10029@yopmail.com',1,'','','shpm-10029@yopmail.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637703,'shpm-10030@mailinator.com',1,'','','shpm-10030@mailinator.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637704,'shpm-10031@yopmail.com',1,'','','shpm-10031@yopmail.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637705,'shpm-10032@mailinator.com',1,'','','shpm-10032@mailinator.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637706,'shpm-10033@yopmail.com',1,'','','shpm-10033@yopmail.com','0','2018-07-24','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-07 10:21:10','2018-08-16 15:46:19',0,'2018-07-24 12:50:00'),(26637707,'shpm-10034@mailinator.com',1,'','','shpm-10034@mailinator.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:26',0,'2018-07-24 12:50:00'),(26635716,'4',1,'Souf','Ben','soufianeben94@gmail.com','1','2011-08-07','0','0','00','fr',NULL,NULL,'1',1,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,88,45,0,'2018-08-01 09:23:11','2018-08-01 09:23:11',0,'2018-07-27 17:30:32'),(26634526,'me@me.com',1,'','','me@me.com','0','1994-07-25','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-07 10:21:10',0,'2018-07-31 17:07:10'),(26634527,'me2@me.com',1,'','test2','me2@me.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634528,'tutu@toto.com',1,'','test3','tutu@toto.com','0','0000-00-00','1','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634529,'tutu2@toto.com',1,'','test4','tutu2@toto.com','0','1994-09-25','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634530,'tutu3@toto.com',1,'','test5','tutu3@toto.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634531,'tutu4@toto.com',1,'','test6','tutu4@toto.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634532,'tutu5@toto.com',1,'','','tutu5@toto.com','0','0000-00-00','1','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634533,'tutu6@toto.com',1,'','','tutu6@toto.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-05 01:00:22',0,'2018-07-31 17:07:10'),(26634534,'marylinejeu@aol.com',1,'','','marylinejeu@aol.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634535,'chappe-claude@hotmail.fr',1,'','','chappe-claude@hotmail.fr','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634536,'pierrette.verger@live.frt',1,'','','pierrette.verger@live.frt','0','0000-00-00','1','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634537,'lilisucrette@free.fr',1,'','','lilisucrette@free.fr','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634538,'christinebertrandjacquet@hotmail.com',1,'','','christinebertrandjacquet@hotmail.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634539,'pommier63250@gmail.com',1,'','','pommier63250@gmail.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-05 01:00:22',0,'2018-07-31 17:07:10'),(26634540,'truffin.emy@hotmail.fr',1,'','','truffin.emy@hotmail.fr','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634541,'nielsthomsen@yahoo.com',1,'','','nielsthomsen@yahoo.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-05 01:00:22',0,'2018-07-31 17:07:10'),(26634542,'julien.guinand@wanadoo.fr',1,'','','julien.guinand@wanadoo.fr','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634543,'veroniquelopinto@gmail.com',1,'','','veroniquelopinto@gmail.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634544,'elodiechretien1@gmail.com',1,'','','elodiechretien1@gmail.com','0','0000-00-00','1','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-07-31 17:07:10','2018-08-06 15:24:53',0,'2018-07-31 17:07:10'),(26634545,'benjamin@avenuedesvins.fr',1,'','','benjamin@avenuedesvins.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:32',0,'2018-07-31 17:07:10'),(26634546,'claude.sanitas@gmail.com',1,'','','claude.sanitas@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:33',0,'2018-07-31 17:07:10'),(26634547,'andy.klom@ec.europa.eu',1,'','','andy.klom@ec.europa.eu','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:33',0,'2018-07-31 17:07:10'),(26634548,'ghfh@gmail.com',1,'','','ghfh@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:33',0,'2018-07-31 17:07:10'),(26634549,'yg.internet@gmail.com',1,'','','yg.internet@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:33',0,'2018-07-31 17:07:10'),(26634550,'fransdanneels@tiscalinet.be',1,'','','fransdanneels@tiscalinet.be','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:33',0,'2018-07-31 17:07:10'),(26634551,'jennifer@avenuedesvins.fr',1,'','','jennifer@avenuedesvins.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:34',0,'2018-07-31 17:07:10'),(26634552,'bendeschamps@msn.com',1,'','','bendeschamps@msn.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:34',0,'2018-07-31 17:07:10'),(26634553,'0682393762@orange.fr',1,'','','0682393762@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:34',0,'2018-07-31 17:07:10'),(26634554,'chatelainsamuel@aol.com',1,'','','chatelainsamuel@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:34',0,'2018-07-31 17:07:10'),(26634555,'lecoat.patricia@orange.fr',1,'','','lecoat.patricia@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:35',0,'2018-07-31 17:07:10'),(26634556,'hugodelatour3@gmail.com',1,'','','hugodelatour3@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:35',0,'2018-07-31 17:07:10'),(26634557,'thoang@groupelexpress.fr',1,'','','thoang@groupelexpress.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:35',0,'2018-07-31 17:07:10'),(26634558,'sarahfrht@hotmail.fr',1,'','','sarahfrht@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:35',0,'2018-07-31 17:07:10'),(26634559,'colombemoinet@gmail.fr',1,'','','colombemoinet@gmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:36',0,'2018-07-31 17:07:10'),(26634560,'natthjul2016@gmail.com',1,'','','natthjul2016@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:36',0,'2018-07-31 17:07:10'),(26634561,'marcbregeon@yahoo.fr',1,'','','marcbregeon@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:36',0,'2018-07-31 17:07:10'),(26634562,'ch.stgregoire@gmail.com',1,'','','ch.stgregoire@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:36',0,'2018-07-31 17:07:10'),(26634563,'benoit.pepin@hotmail.fr',1,'','','benoit.pepin@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:36',0,'2018-07-31 17:07:10'),(26634564,'s_clara_890@hotmail.com',1,'','','s_clara_890@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:37',0,'2018-07-31 17:07:10'),(26634565,'jean-paul.beggiato@neuf.fr',1,'','','jean-paul.beggiato@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:37',0,'2018-07-31 17:07:10'),(26634566,'chang28@orange.fr',1,'','','chang28@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:37',0,'2018-07-31 17:07:10'),(26634567,'jacques.lafouge@wanadoo.fr',1,'','','jacques.lafouge@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:37',0,'2018-07-31 17:07:10'),(26634568,'sylvie.bernard@univ-amu.fr',1,'','','sylvie.bernard@univ-amu.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:38',0,'2018-07-31 17:07:10'),(26634569,'micheltressens@gmail.com',1,'','','micheltressens@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:38',0,'2018-07-31 17:07:10'),(26634570,'emmanuelle.saillard@agencearcange.fr',1,'','','emmanuelle.saillard@agencearcange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:38',0,'2018-07-31 17:07:10'),(26634571,'gourvennec.dominique@orange.fr',1,'','','gourvennec.dominique@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:38',0,'2018-07-31 17:07:10'),(26634572,'jacques.courau33@orange.fr',1,'','','jacques.courau33@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:39',0,'2018-07-31 17:07:10'),(26634573,'tollelege@hotmail.fr',1,'','','tollelege@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:39',0,'2018-07-31 17:07:10'),(26634574,'rolande.allene1957@gmail.com',1,'','','rolande.allene1957@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:39',0,'2018-07-31 17:07:10'),(26634575,'familleferaud@yahoo.fr',1,'','','familleferaud@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:39',0,'2018-07-31 17:07:10'),(26634576,'aubry.avocat@wanadoo.fr',1,'','','aubry.avocat@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:39',0,'2018-07-31 17:07:10'),(26634577,'janine.buche-blanc@orange.fr',1,'','','janine.buche-blanc@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:40',0,'2018-07-31 17:07:10'),(26634578,'jp_gouin@yahoo.fr',1,'','','jp_gouin@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:40',0,'2018-07-31 17:07:10'),(26634579,'ocatherine2@orange.fr',1,'','','ocatherine2@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:40',0,'2018-07-31 17:07:10'),(26634580,'septimesevere13@gmail.com',1,'','','septimesevere13@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:41',0,'2018-07-31 17:07:10'),(26634581,'andrecombes.direct@outlook.fr',1,'','','andrecombes.direct@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:41',0,'2018-07-31 17:07:10'),(26634582,'mangondamariefrance@yahoo.fr',1,'','','mangondamariefrance@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:41',0,'2018-07-31 17:07:10'),(26634583,'belzmedia@gmail.com',1,'','','belzmedia@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:41',0,'2018-07-31 17:07:10'),(26634584,'raphael@ouiflash.com',1,'','','raphael@ouiflash.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:41',0,'2018-07-31 17:07:10'),(26634585,'rene.delcormminette@gmail.com',1,'','','rene.delcormminette@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:42',0,'2018-07-31 17:07:10'),(26634586,'lyndon@southwestbrains.co.uk',1,'','','lyndon@southwestbrains.co.uk','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:42',0,'2018-07-31 17:07:10'),(26634587,'vigliarolomaria@wanadoo.fr',1,'','','vigliarolomaria@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:42',0,'2018-07-31 17:07:10'),(26634588,'robert.jabaud@wanadoo.fr',1,'','','robert.jabaud@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:42',0,'2018-07-31 17:07:10'),(26634589,'rmorino@hotmail.com',1,'','','rmorino@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:43',0,'2018-07-31 17:07:10'),(26634590,'lorenzo97400@orange.fr',1,'','','lorenzo97400@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:43',0,'2018-07-31 17:07:10'),(26634591,'jean.egu@orange.fr',1,'','','jean.egu@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:43',0,'2018-07-31 17:07:10'),(26634592,'j.guinand@terreetvigne.com',1,'','','j.guinand@terreetvigne.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:43',0,'2018-07-31 17:07:10'),(26634593,'rara@hotmail.fr',1,'','','rara@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:44',0,'2018-07-31 17:07:10'),(26634594,'edouard.margain@gmail.com',1,'','','edouard.margain@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:44',0,'2018-07-31 17:07:10'),(26634595,'rschmitt@bbox.fr',1,'','','rschmitt@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:44',0,'2018-07-31 17:07:10'),(26634596,'michel.deschamps030@gmail.com',1,'','','michel.deschamps030@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:44',0,'2018-07-31 17:07:10'),(26634597,'rvialettes@yahoo.fr',1,'','','rvialettes@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:44',0,'2018-07-31 17:07:10'),(26634598,'cforget@live.fr',1,'','','cforget@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:45',0,'2018-07-31 17:07:10'),(26634599,'dailleaumyriam@gmail.com',1,'','','dailleaumyriam@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:45',0,'2018-07-31 17:07:10'),(26634600,'eve.2862@hotmail.fr',1,'','','eve.2862@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:45',0,'2018-07-31 17:07:10'),(26634601,'florent-testut@orange.fr',1,'','','florent-testut@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:45',0,'2018-07-31 17:07:10'),(26634602,'chloe.jacquet1@orange.fr',1,'','','chloe.jacquet1@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:46',0,'2018-07-31 17:07:10'),(26634603,'gicquel.f@outlook.fr',1,'','','gicquel.f@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:46',0,'2018-07-31 17:07:10'),(26634604,'delmon@club-internet.fr',1,'','','delmon@club-internet.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:46',0,'2018-07-31 17:07:10'),(26634605,'aissata.simon@laposte.net',1,'','','aissata.simon@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:47',0,'2018-07-31 17:07:10'),(26634606,'b.waterlot37@gmail.com',1,'','','b.waterlot37@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:47',0,'2018-07-31 17:07:10'),(26634607,'rmerini300@gmail.com',1,'','','rmerini300@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:47',0,'2018-07-31 17:07:10'),(26634608,'deschampsgilles@hotmail.ca',1,'','','deschampsgilles@hotmail.ca','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:47',0,'2018-07-31 17:07:10'),(26634609,'morganedupiech@gmail.com',1,'','','morganedupiech@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:47',0,'2018-07-31 17:07:10'),(26634610,'francoismoscard@wanadoo.fr',1,'','','francoismoscard@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:48',0,'2018-07-31 17:07:10'),(26634611,'arnaud.sanquer@yahoo.fr',1,'','','arnaud.sanquer@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:48',0,'2018-07-31 17:07:10'),(26634612,'jacques.bigotte@free.fr',1,'','','jacques.bigotte@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:48',0,'2018-07-31 17:07:10'),(26634613,'roland.bourquin@gmail.com',1,'','','roland.bourquin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:48',0,'2018-07-31 17:07:10'),(26634614,'jeanclaude.talut@neuf.fr',1,'','','jeanclaude.talut@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:49',0,'2018-07-31 17:07:10'),(26634615,'karinehallot71@gmail.com',1,'','','karinehallot71@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:49',0,'2018-07-31 17:07:10'),(26634616,'eyes12@hotmail.com',1,'','','eyes12@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:49',0,'2018-07-31 17:07:10'),(26634617,'hoenicke.michel@orange.fr',1,'','','hoenicke.michel@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:49',0,'2018-07-31 17:07:10'),(26634618,'juliemaire876@gmail.com',1,'','','juliemaire876@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:49',0,'2018-07-31 17:07:10'),(26634619,'court.lea33@gmail.com',1,'','','court.lea33@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:50',0,'2018-07-31 17:07:10'),(26634620,'guy59640@neuf.fr',1,'','','guy59640@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:50',0,'2018-07-31 17:07:10'),(26634621,'lfjeux@gmail.com',1,'','','lfjeux@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:50',0,'2018-07-31 17:07:10'),(26634622,'salimette_bouclette@yahoo.fr',1,'','','salimette_bouclette@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:50',0,'2018-07-31 17:07:10'),(26634623,'balyjoe@aol.com',1,'','','balyjoe@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:51',0,'2018-07-31 17:07:10'),(26634624,'raulin.liliane76370@gmail.com',1,'','','raulin.liliane76370@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:51',0,'2018-07-31 17:07:10'),(26634625,'vinsouze@gmail.com',1,'','','vinsouze@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:51',0,'2018-07-31 17:07:10'),(26634626,'laville.jp@hotmail.fr',1,'','','laville.jp@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:51',0,'2018-07-31 17:07:10'),(26634627,'sabrina.lebournot@laposte.net',1,'','','sabrina.lebournot@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:52',0,'2018-07-31 17:07:10'),(26634628,'alexiakraft@outlook.fr',1,'','','alexiakraft@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:52',0,'2018-07-31 17:07:10'),(26634629,'ponton.martine@neuf.fr',1,'','','ponton.martine@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:52',0,'2018-07-31 17:07:10'),(26634630,'daniel.levyfve0507@orange.fr',1,'','','daniel.levyfve0507@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:52',0,'2018-07-31 17:07:10'),(26634631,'leoit31@hotmail.fr',1,'','','leoit31@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:53',0,'2018-07-31 17:07:10'),(26634632,'marielleconcours@laposte.net',1,'','','marielleconcours@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:53',0,'2018-07-31 17:07:10'),(26634633,'mathieu.picaud@aliceadsl.fr',1,'','','mathieu.picaud@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:53',0,'2018-07-31 17:07:10'),(26634634,'jinco12@hotmail.fr',1,'','','jinco12@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:53',0,'2018-07-31 17:07:10'),(26634635,'sophie.kasseh@sfr.fr',1,'','','sophie.kasseh@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:53',0,'2018-07-31 17:07:10'),(26634636,'mathildejacob@hotmail.com',1,'','','mathildejacob@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:54',0,'2018-07-31 17:07:10'),(26634637,'nathalie.cagnard@sfr.fr',1,'','','nathalie.cagnard@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:54',0,'2018-07-31 17:07:10'),(26634638,'pancholito65@yahoo.fr',1,'','','pancholito65@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:54',0,'2018-07-31 17:07:10'),(26634639,'sylvia.andamaye@hotmail.fr',1,'','','sylvia.andamaye@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:55',0,'2018-07-31 17:07:10'),(26634640,'theba.l@wanadoo.fr',1,'','','theba.l@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:55',0,'2018-07-31 17:07:10'),(26634641,'sandla29@yahoo.com',1,'','','sandla29@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:55',0,'2018-07-31 17:07:10'),(26634642,'guy.thibaut@sfr.fr',1,'','','guy.thibaut@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:55',0,'2018-07-31 17:07:10'),(26634643,'michel.crochemore@9online.fr',1,'','','michel.crochemore@9online.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:56',0,'2018-07-31 17:07:10'),(26634644,'albertsaelen@laposte.net',1,'','','albertsaelen@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:56',0,'2018-07-31 17:07:10'),(26634645,'paulgaillardc@wanadoo.fr',1,'','','paulgaillardc@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:56',0,'2018-07-31 17:07:10'),(26634646,'quessada0914@orange.fr',1,'','','quessada0914@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:57',0,'2018-07-31 17:07:10'),(26634647,'karenne.motard@gmail.com',1,'','','karenne.motard@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:57',0,'2018-07-31 17:07:10'),(26634648,'cocolove@hotmail.fr',1,'','','cocolove@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:57',0,'2018-07-31 17:07:10'),(26634649,'tcadoret65@gmail.com',1,'','','tcadoret65@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:57',0,'2018-07-31 17:07:10'),(26634650,'n.brigitte2304@orange.fr',1,'','','n.brigitte2304@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:58',0,'2018-07-31 17:07:10'),(26634651,'petitprince94@hotmail.com',1,'','','petitprince94@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:58',0,'2018-07-31 17:07:10'),(26634652,'lepoemien.2000@free.fr',1,'','','lepoemien.2000@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:58',0,'2018-07-31 17:07:10'),(26634653,'tverger1@gmail.com',1,'','','tverger1@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:59',0,'2018-07-31 17:07:10'),(26634654,'jgabriel24@orange.fr',1,'','','jgabriel24@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:59',0,'2018-07-31 17:07:10'),(26634655,'jhilpipre@orange.fr',1,'','','jhilpipre@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:59',0,'2018-07-31 17:07:10'),(26634656,'pat.chevallier@yahoo.fr',1,'','','pat.chevallier@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:24:59',0,'2018-07-31 17:07:10'),(26634657,'shanicenat@hotmail.fr',1,'','','shanicenat@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:00',0,'2018-07-31 17:07:10'),(26634658,'st-germain@hotmail.fr',1,'','','st-germain@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:00',0,'2018-07-31 17:07:10'),(26634659,'angy78@hotmail.fr',1,'','','angy78@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:00',0,'2018-07-31 17:07:10'),(26634660,'pascale.berger23@gmail.com',1,'','','pascale.berger23@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:00',0,'2018-07-31 17:07:10'),(26634661,'roger.pittet@wanadoo.fr',1,'','','roger.pittet@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:00',0,'2018-07-31 17:07:10'),(26634662,'jennypic@msn.com',1,'','','jennypic@msn.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:01',0,'2018-07-31 17:07:10'),(26634663,'Piquet.Colette@bbox.fr',1,'','','Piquet.Colette@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:01',0,'2018-07-31 17:07:10'),(26634664,'therese52300@hotmail.fr',1,'','','therese52300@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:01',0,'2018-07-31 17:07:10'),(26634665,'stella.kieffer@cegetel.net',1,'','','stella.kieffer@cegetel.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:02',0,'2018-07-31 17:07:10'),(26634666,'pierredecarpentry@gmail.com',1,'','','pierredecarpentry@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:02',0,'2018-07-31 17:07:10'),(26634667,'aquemener@ymail.com',1,'','','aquemener@ymail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:02',0,'2018-07-31 17:07:10'),(26634668,'siciliendu66@hotmail.fr',1,'','','siciliendu66@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:02',0,'2018-07-31 17:07:10'),(26634669,'romaincoulaud52100@gmail.com',1,'','','romaincoulaud52100@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:03',0,'2018-07-31 17:07:10'),(26634670,'verolagagne@gmail.com',1,'','','verolagagne@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:03',0,'2018-07-31 17:07:10'),(26634671,'jeanclaudeprot@sfr.fr',1,'','','jeanclaudeprot@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:03',0,'2018-07-31 17:07:10'),(26634672,'mcbergeon53@gmail.com',1,'','','mcbergeon53@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:03',0,'2018-07-31 17:07:10'),(26634673,'alan.raude.p@wanadoo.fr',1,'','','alan.raude.p@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:04',0,'2018-07-31 17:07:10'),(26634674,'mariefernandeetienne@gmail.com',1,'','','mariefernandeetienne@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:04',0,'2018-07-31 17:07:10'),(26634675,'cliclac85@yahoo.fr',1,'','','cliclac85@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:04',0,'2018-07-31 17:07:10'),(26634676,'pmoiret@hotmail.com',1,'','','pmoiret@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:04',0,'2018-07-31 17:07:10'),(26634677,'10troyens@gmail.com',1,'','','10troyens@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:05',0,'2018-07-31 17:07:10'),(26634678,'karine56320@hotmail.fr',1,'','','karine56320@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:05',0,'2018-07-31 17:07:10'),(26634679,'oliviaosouf@orange.fr',1,'','','oliviaosouf@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:05',0,'2018-07-31 17:07:10'),(26634680,'jmharisboure@yahoo.fr',1,'','','jmharisboure@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:05',0,'2018-07-31 17:07:10'),(26634681,'ludovic.eliot.concours@gmail.com',1,'','','ludovic.eliot.concours@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:06',0,'2018-07-31 17:07:10'),(26634682,'katounet59@hotmail.fr',1,'','','katounet59@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:06',0,'2018-07-31 17:07:10'),(26634683,'isabelle.meuli@orange.fr',1,'','','isabelle.meuli@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:06',0,'2018-07-31 17:07:10'),(26634684,'mifachatin@wanadoo.fr',1,'','','mifachatin@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:06',0,'2018-07-31 17:07:10'),(26634685,'robfamilly@free.fr',1,'','','robfamilly@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:06',0,'2018-07-31 17:07:10'),(26634686,'nicolasdahirel@orange.fr',1,'','','nicolasdahirel@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:07',0,'2018-07-31 17:07:10'),(26634687,'aragones.laurent@orange.fr',1,'','','aragones.laurent@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:07',0,'2018-07-31 17:07:10'),(26634688,'csap22.yop@outlook.fr',1,'','','csap22.yop@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:07',0,'2018-07-31 17:07:10'),(26634689,'lacaille.dominique@yahoo.fr',1,'','','lacaille.dominique@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:07',0,'2018-07-31 17:07:10'),(26634690,'celine.billerey@free.fr',1,'','','celine.billerey@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:08',0,'2018-07-31 17:07:10'),(26634691,'gpellieu1@laposte.net',1,'','','gpellieu1@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:08',0,'2018-07-31 17:07:10'),(26634692,'kasame52100@gmail.com',1,'','','kasame52100@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:08',0,'2018-07-31 17:07:10'),(26634693,'damien.calvino@hotmail.fr',1,'','','damien.calvino@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:08',0,'2018-07-31 17:07:10'),(26634694,'diable_de_tazmanie@hotmail.fr',1,'','','diable_de_tazmanie@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:09',0,'2018-07-31 17:07:10'),(26634695,'anny.dantin@wanadoo.fr',1,'','','anny.dantin@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:09',0,'2018-07-31 17:07:10'),(26634696,'mindyna@hotmail.com',1,'','','mindyna@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:09',0,'2018-07-31 17:07:10'),(26634697,'maurelie.winzblum@gmail.com',1,'','','maurelie.winzblum@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:09',0,'2018-07-31 17:07:10'),(26634698,'agnes_lea@hotmail.fr',1,'','','agnes_lea@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:10',0,'2018-07-31 17:07:10'),(26634699,'jeanpol.dubois@gmail.com',1,'','','jeanpol.dubois@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:10',0,'2018-07-31 17:07:10'),(26634700,'s.audic@laposte.net',1,'','','s.audic@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:10',0,'2018-07-31 17:07:10'),(26634701,'brunopontonnier@gmail.com',1,'','','brunopontonnier@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:10',0,'2018-07-31 17:07:10'),(26634702,'munch.muckensturm@orange.fr',1,'','','munch.muckensturm@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:11',0,'2018-07-31 17:07:10'),(26634703,'marierom@live.fr',1,'','','marierom@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:11',0,'2018-07-31 17:07:10'),(26634704,'acadilhac@yahoo.fr',1,'','','acadilhac@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:11',0,'2018-07-31 17:07:10'),(26634705,'claudettes51@hotmail.fr',1,'','','claudettes51@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:11',0,'2018-07-31 17:07:10'),(26634706,'adrenss@free.fr',1,'','','adrenss@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:12',0,'2018-07-31 17:07:10'),(26634707,'eugene.bardi@orange.fr',1,'','','eugene.bardi@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:12',0,'2018-07-31 17:07:10'),(26634708,'sardinade@gmail.com',1,'','','sardinade@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:13',0,'2018-07-31 17:07:10'),(26634709,'gaugain_dominique@yahoo.fr',1,'','','gaugain_dominique@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:13',0,'2018-07-31 17:07:10'),(26634710,'paty40@laposte.net',1,'','','paty40@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:13',0,'2018-07-31 17:07:10'),(26634711,'anthony_dhers@hotmail.com',1,'','','anthony_dhers@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:14',0,'2018-07-31 17:07:10'),(26634712,'ntifashion@yahoo.fr',1,'','','ntifashion@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:14',0,'2018-07-31 17:07:10'),(26634713,'claude.siellet@wanadoo.fr',1,'','','claude.siellet@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:14',0,'2018-07-31 17:07:10'),(26634714,'laurence.marie47@sfr.fr',1,'','','laurence.marie47@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:15',0,'2018-07-31 17:07:10'),(26634715,'tatal@numericable.fr',1,'','','tatal@numericable.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:15',0,'2018-07-31 17:07:10'),(26634716,'nadinepadou@yahoo.fr',1,'','','nadinepadou@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:15',0,'2018-07-31 17:07:10'),(26634717,'bflahaut62@aol.com',1,'','','bflahaut62@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:16',0,'2018-07-31 17:07:10'),(26634718,'sandrineelfideremy@gmail.com',1,'','','sandrineelfideremy@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:16',0,'2018-07-31 17:07:10'),(26634719,'iprat73@gmail.com',1,'','','iprat73@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:16',0,'2018-07-31 17:07:10'),(26634720,'quiz@orange.fr',1,'','','quiz@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:17',0,'2018-07-31 17:07:10'),(26634721,'michel.dunet@laposte.net',1,'','','michel.dunet@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:17',0,'2018-07-31 17:07:10'),(26634722,'pappalardo.patrick@laposte.net',1,'','','pappalardo.patrick@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:17',0,'2018-07-31 17:07:10'),(26634723,'sylvie_2501@hotmail.fr',1,'','','sylvie_2501@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:17',0,'2018-07-31 17:07:10'),(26634724,'pitpeg@outlook.fr',1,'','','pitpeg@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:18',0,'2018-07-31 17:07:10'),(26634725,'verim10@laposte.net',1,'','','verim10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:18',0,'2018-07-31 17:07:10'),(26634726,'eugenia.jambut@orange.fr',1,'','','eugenia.jambut@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:18',0,'2018-07-31 17:07:10'),(26634727,'bonifacech@aol.com',1,'','','bonifacech@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:18',0,'2018-07-31 17:07:10'),(26634728,'gilles42100@laposte.net',1,'','','gilles42100@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:19',0,'2018-07-31 17:07:10'),(26634729,'ocjama@outlook.com',1,'','','ocjama@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:19',0,'2018-07-31 17:07:10'),(26634730,'grandepoucinette@hotmail.com',1,'','','grandepoucinette@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:19',0,'2018-07-31 17:07:10'),(26634731,'dufour29@gmail.com',1,'','','dufour29@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:20',0,'2018-07-31 17:07:10'),(26634732,'delphine3v@gmail.com',1,'','','delphine3v@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:20',0,'2018-07-31 17:07:10'),(26634733,'legrosnain74@gmail.com',1,'','','legrosnain74@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:20',0,'2018-07-31 17:07:10'),(26634734,'annefer@laposte.net',1,'','','annefer@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:20',0,'2018-07-31 17:07:10'),(26634735,'love291@hotmail.fr',1,'','','love291@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:21',0,'2018-07-31 17:07:10'),(26634736,'audrey20a@hotmail.com',1,'','','audrey20a@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:21',0,'2018-07-31 17:07:10'),(26634737,'tsanica@laposte.net',1,'','','tsanica@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:21',0,'2018-07-31 17:07:10'),(26634738,'julie.poudevigne@gmail.com',1,'','','julie.poudevigne@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:21',0,'2018-07-31 17:07:10'),(26634739,'claudepancia@aol.com',1,'','','claudepancia@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:22',0,'2018-07-31 17:07:10'),(26634740,'richard.jacques31@sfr.fr',1,'','','richard.jacques31@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:22',0,'2018-07-31 17:07:10'),(26634741,'emilie.charmot@sfr.fr',1,'','','emilie.charmot@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:22',0,'2018-07-31 17:07:10'),(26634742,'stefandsand@hotmail.fr',1,'','','stefandsand@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:22',0,'2018-07-31 17:07:10'),(26634743,'xav59920@live.fr',1,'','','xav59920@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:22',0,'2018-07-31 17:07:10'),(26634744,'maryse.le-dily@laposte.net',1,'','','maryse.le-dily@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:23',0,'2018-07-31 17:07:10'),(26634745,'poupettematthieu@hotmail.fr',1,'','','poupettematthieu@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:23',0,'2018-07-31 17:07:10'),(26634746,'bb.brouckaert@gmail.com',1,'','','bb.brouckaert@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:23',0,'2018-07-31 17:07:10'),(26634747,'caroledeffaugt@yahoo.fr',1,'','','caroledeffaugt@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:23',0,'2018-07-31 17:07:10'),(26634748,'l.mottet1@orange.fr',1,'','','l.mottet1@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:24',0,'2018-07-31 17:07:10'),(26634749,'rogergruchot@sfr.fr',1,'','','rogergruchot@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:24',0,'2018-07-31 17:07:10'),(26634750,'tornade80100@msn.com',1,'','','tornade80100@msn.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:24',0,'2018-07-31 17:07:10'),(26634751,'phil211164@gmail.com',1,'','','phil211164@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:24',0,'2018-07-31 17:07:10'),(26634752,'karima.allard4@hotmail.fr',1,'','','karima.allard4@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:25',0,'2018-07-31 17:07:10'),(26634753,'dynah5751@aol.fr',1,'','','dynah5751@aol.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:25',0,'2018-07-31 17:07:10'),(26634754,'orchidee59nd@free.fr',1,'','','orchidee59nd@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:25',0,'2018-07-31 17:07:10'),(26634755,'marchal.correspondances@orange.fr',1,'','','marchal.correspondances@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:26',0,'2018-07-31 17:07:10'),(26634756,'belny.jeanclaude@bbox.fr',1,'','','belny.jeanclaude@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:26',0,'2018-07-31 17:07:10'),(26634757,'tosie10@laposte.net',1,'','','tosie10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:26',0,'2018-07-31 17:07:10'),(26634758,'michelejaegle@free.fr',1,'','','michelejaegle@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:26',0,'2018-07-31 17:07:10'),(26634759,'patrick.records@laposte.net',1,'','','patrick.records@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:26',0,'2018-07-31 17:07:10'),(26634760,'franpou20@orange.fr',1,'','','franpou20@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:27',0,'2018-07-31 17:07:10'),(26634761,'sebastien.rabet16@sfr.fr',1,'','','sebastien.rabet16@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:27',0,'2018-07-31 17:07:10'),(26634762,'masselin.christine@neuf.fr',1,'','','masselin.christine@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:27',0,'2018-07-31 17:07:10'),(26634763,'fab76600@hotmail.fr',1,'','','fab76600@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:27',0,'2018-07-31 17:07:10'),(26634764,'sunny444@live.fr',1,'','','sunny444@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:28',0,'2018-07-31 17:07:10'),(26634765,'olivierkergall1@aol.com',1,'','','olivierkergall1@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:28',0,'2018-07-31 17:07:10'),(26634766,'sebastien.lorgnier@me.com',1,'','','sebastien.lorgnier@me.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:28',0,'2018-07-31 17:07:10'),(26634767,'daniel.gaudel@laposte.net',1,'','','daniel.gaudel@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:28',0,'2018-07-31 17:07:10'),(26634768,'deejayrobles@aol.com',1,'','','deejayrobles@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:29',0,'2018-07-31 17:07:10'),(26634769,'papytetard@yahoo.fr',1,'','','papytetard@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:29',0,'2018-07-31 17:07:10'),(26634770,'jodiestopin@yahoo.fr',1,'','','jodiestopin@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:29',0,'2018-07-31 17:07:10'),(26634771,'nellyfabreguettes@yahoo.fr',1,'','','nellyfabreguettes@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:30',0,'2018-07-31 17:07:10'),(26634772,'gbuttazzi@laposte.net',1,'','','gbuttazzi@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:30',0,'2018-07-31 17:07:10'),(26634773,'lorenzi.denise@orange.fr',1,'','','lorenzi.denise@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:30',0,'2018-07-31 17:07:10'),(26634774,'sylvie.sepan@orange.fr',1,'','','sylvie.sepan@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:30',0,'2018-07-31 17:07:10'),(26634775,'85colette@orange.fr',1,'','','85colette@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:30',0,'2018-07-31 17:07:10'),(26634776,'karineleclercq2000@yahoo.fr',1,'','','karineleclercq2000@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:31',0,'2018-07-31 17:07:10'),(26634777,'danielle.gandar@sfr.fr',1,'','','danielle.gandar@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:31',0,'2018-07-31 17:07:10'),(26634778,'sophiebertin@hotmail.fr',1,'','','sophiebertin@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:31',0,'2018-07-31 17:07:10'),(26634779,'Sandrine.daassuncao@laposte.net',1,'','','Sandrine.daassuncao@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:31',0,'2018-07-31 17:07:10'),(26634780,'g.wanaelle@live.fr',1,'','','g.wanaelle@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:32',0,'2018-07-31 17:07:10'),(26634781,'melanie.durau@club-internet.fr',1,'','','melanie.durau@club-internet.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:32',0,'2018-07-31 17:07:10'),(26634782,'sandrinedrouin1@live.fr',1,'','','sandrinedrouin1@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:32',0,'2018-07-31 17:07:10'),(26634783,'christine348@hotmail.fr',1,'','','christine348@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:32',0,'2018-07-31 17:07:10'),(26634784,'mathevet.ingrid@gmail.com',1,'','','mathevet.ingrid@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:33',0,'2018-07-31 17:07:10'),(26634785,'sophiebandon103@gmail.com',1,'','','sophiebandon103@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:33',0,'2018-07-31 17:07:10'),(26634786,'brigittemary45@hotmail.fr',1,'','','brigittemary45@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:33',0,'2018-07-31 17:07:10'),(26634787,'peggy.cresson@gmail.com',1,'','','peggy.cresson@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:33',0,'2018-07-31 17:07:10'),(26634788,'desseaux.patrice@orange.fr',1,'','','desseaux.patrice@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:34',0,'2018-07-31 17:07:10'),(26634789,'nelly.saulnier249@gmail.com',1,'','','nelly.saulnier249@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:34',0,'2018-07-31 17:07:10'),(26634790,'aiglehouxmarc@wanadoo.fr',1,'','','aiglehouxmarc@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:34',0,'2018-07-31 17:07:10'),(26634791,'aurore3434@hotmail.fr',1,'','','aurore3434@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:34',0,'2018-07-31 17:07:10'),(26634792,'joseph.westrelin@free.fr',1,'','','joseph.westrelin@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:35',0,'2018-07-31 17:07:10'),(26634793,'acviano@orange.fr',1,'','','acviano@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:35',0,'2018-07-31 17:07:10'),(26634794,'fille-900@hotmail.fr',1,'','','fille-900@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:35',0,'2018-07-31 17:07:10'),(26634795,'annick.berruyer@orange.fr',1,'','','annick.berruyer@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:35',0,'2018-07-31 17:07:10'),(26634796,'jane3191@free.fr',1,'','','jane3191@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:36',0,'2018-07-31 17:07:10'),(26634797,'angy07@sfr.fr',1,'','','angy07@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:36',0,'2018-07-31 17:07:10'),(26634798,'christine@extremly.net',1,'','','christine@extremly.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:36',0,'2018-07-31 17:07:10'),(26634799,'monafifi@outlook.fr',1,'','','monafifi@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:36',0,'2018-07-31 17:07:10'),(26634800,'bertrand.francoise@sfr.fr',1,'','','bertrand.francoise@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:37',0,'2018-07-31 17:07:10'),(26634801,'chmonard@yahoo.fr',1,'','','chmonard@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:37',0,'2018-07-31 17:07:10'),(26634802,'cnous61@outlook.fr',1,'','','cnous61@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:37',0,'2018-07-31 17:07:10'),(26634803,'naomie9234@hotmail.com',1,'','','naomie9234@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:37',0,'2018-07-31 17:07:10'),(26634804,'albertini.audrey@orange.fr',1,'','','albertini.audrey@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:37',0,'2018-07-31 17:07:10'),(26634805,'brigittemenu57@orange.fr',1,'','','brigittemenu57@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:38',0,'2018-07-31 17:07:10'),(26634806,'fanfanjouan@yahoo.fr',1,'','','fanfanjouan@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:38',0,'2018-07-31 17:07:10'),(26634807,'rodrigues.win@live.fr',1,'','','rodrigues.win@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:38',0,'2018-07-31 17:07:10'),(26634808,'murielle.demouiller@libertysurf.fr',1,'','','murielle.demouiller@libertysurf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:38',0,'2018-07-31 17:07:10'),(26634809,'garnier_alain@hotmail.com',1,'','','garnier_alain@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:39',0,'2018-07-31 17:07:10'),(26634810,'perrine1989@gmail.com',1,'','','perrine1989@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:39',0,'2018-07-31 17:07:10'),(26634811,'patsmora@laposte.net',1,'','','patsmora@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:39',0,'2018-07-31 17:07:10'),(26634812,'capucine1945@laposte.net',1,'','','capucine1945@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:39',0,'2018-07-31 17:07:10'),(26634813,'gerard.vinatier8@orange.fr',1,'','','gerard.vinatier8@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:40',0,'2018-07-31 17:07:10'),(26634814,'e.guerin816@laposte.net',1,'','','e.guerin816@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:40',0,'2018-07-31 17:07:10'),(26634815,'philmarques@yahoo.fr',1,'','','philmarques@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:40',0,'2018-07-31 17:07:10'),(26634816,'bea.perbost@orange.fr',1,'','','bea.perbost@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:40',0,'2018-07-31 17:07:10'),(26634817,'roches.alex@orange.fr',1,'','','roches.alex@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:41',0,'2018-07-31 17:07:10'),(26634818,'cholletjennifer@live.fr',1,'','','cholletjennifer@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:41',0,'2018-07-31 17:07:10'),(26634819,'edevienne@gmail.com',1,'','','edevienne@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:41',0,'2018-07-31 17:07:10'),(26634820,'reitermagali@neuf.fr',1,'','','reitermagali@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:41',0,'2018-07-31 17:07:10'),(26634821,'gilles.demouiller1@libertysurf.fr',1,'','','gilles.demouiller1@libertysurf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:42',0,'2018-07-31 17:07:10'),(26634822,'murielle.demouiller@laposte.net',1,'','','murielle.demouiller@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:42',0,'2018-07-31 17:07:10'),(26634823,'jessco56@gmail.com',1,'','','jessco56@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:42',0,'2018-07-31 17:07:10'),(26634824,'elvira-val1@live.fr',1,'','','elvira-val1@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:42',0,'2018-07-31 17:07:10'),(26634825,'ludovic.fournerie@gmail.com',1,'','','ludovic.fournerie@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:43',0,'2018-07-31 17:07:10'),(26634826,'etienne.schotte@wanadoo.fr',1,'','','etienne.schotte@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:43',0,'2018-07-31 17:07:10'),(26634827,'clemasr@free.fr',1,'','','clemasr@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:43',0,'2018-07-31 17:07:10'),(26634828,'bruno.cassecuel@laposte.net',1,'','','bruno.cassecuel@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:43',0,'2018-07-31 17:07:10'),(26634829,'therese.jolivel@orange.fr',1,'','','therese.jolivel@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:43',0,'2018-07-31 17:07:10'),(26634830,'laetitiagaq@gmail.com',1,'','','laetitiagaq@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:44',0,'2018-07-31 17:07:10'),(26634831,'gnon_gnon_51@hotmail.com',1,'','','gnon_gnon_51@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:44',0,'2018-07-31 17:07:10'),(26634832,'mauricetteh6437@gmail.com',1,'','','mauricetteh6437@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:44',0,'2018-07-31 17:07:10'),(26634833,'vanefredo@gmail.com',1,'','','vanefredo@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:44',0,'2018-07-31 17:07:10'),(26634834,'reginemombrun@yahoo.fr',1,'','','reginemombrun@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:45',0,'2018-07-31 17:07:10'),(26634835,'baguecalais@aol.com',1,'','','baguecalais@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:45',0,'2018-07-31 17:07:10'),(26634836,'sebastien.lecoq2112@gmail.com',1,'','','sebastien.lecoq2112@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:45',0,'2018-07-31 17:07:10'),(26634837,'szalia18@gmail.com',1,'','','szalia18@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:45',0,'2018-07-31 17:07:10'),(26634838,'rapido45@orange.fr',1,'','','rapido45@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:46',0,'2018-07-31 17:07:10'),(26634839,'melaniasaragea@yahoo.com',1,'','','melaniasaragea@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:46',0,'2018-07-31 17:07:10'),(26634840,'arum@modulonet.fr',1,'','','arum@modulonet.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:46',0,'2018-07-31 17:07:10'),(26634841,'kbccc@hotmail.fr',1,'','','kbccc@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:46',0,'2018-07-31 17:07:10'),(26634842,'godetorg@free.fr',1,'','','godetorg@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:47',0,'2018-07-31 17:07:10'),(26634843,'lafhaj@hotmail.com',1,'','','lafhaj@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:47',0,'2018-07-31 17:07:10'),(26634844,'s.deleau@yahoo.fr',1,'','','s.deleau@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:47',0,'2018-07-31 17:07:10'),(26634845,'metiss1975@hotmail.fr',1,'','','metiss1975@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:47',0,'2018-07-31 17:07:10'),(26634846,'jeanclaude.corrollerbis@sfr.fr',1,'','','jeanclaude.corrollerbis@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:48',0,'2018-07-31 17:07:10'),(26634847,'stephane.lecouls@laposte.net',1,'','','stephane.lecouls@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:48',0,'2018-07-31 17:07:10'),(26634848,'jacquelinebard@live.fr',1,'','','jacquelinebard@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:48',0,'2018-07-31 17:07:10'),(26634849,'idelorme@aliceadsl.fr',1,'','','idelorme@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:48',0,'2018-07-31 17:07:10'),(26634850,'nat1980@orange.fr',1,'','','nat1980@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:48',0,'2018-07-31 17:07:10'),(26634851,'jeschek.magdaleina@hotmail.fr',1,'','','jeschek.magdaleina@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:49',0,'2018-07-31 17:07:10'),(26634852,'game5962@hotmail.com',1,'','','game5962@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:49',0,'2018-07-31 17:07:10'),(26634853,'soulier.patrizia@bbox.fr',1,'','','soulier.patrizia@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:49',0,'2018-07-31 17:07:10'),(26634854,'vertessen@free.fr',1,'','','vertessen@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:49',0,'2018-07-31 17:07:10'),(26634855,'pascal.noury76@orange.fr',1,'','','pascal.noury76@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:50',0,'2018-07-31 17:07:10'),(26634856,'yannickmispelaere@gmail.com',1,'','','yannickmispelaere@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:50',0,'2018-07-31 17:07:10'),(26634857,'moustakira@gmail.com',1,'','','moustakira@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:50',0,'2018-07-31 17:07:10'),(26634858,'v_prudhon@hotmail.com',1,'','','v_prudhon@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:50',0,'2018-07-31 17:07:10'),(26634859,'turlututu24100@outlook.fr',1,'','','turlututu24100@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:51',0,'2018-07-31 17:07:10'),(26634860,'chaoijui24@outlook.com',1,'','','chaoijui24@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:51',0,'2018-07-31 17:07:10'),(26634861,'lmarie68240@gmail.com',1,'','','lmarie68240@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:51',0,'2018-07-31 17:07:10'),(26634862,'lolypop59@live.fr',1,'','','lolypop59@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:51',0,'2018-07-31 17:07:10'),(26634863,'marilyne.pain@sfr.fr',1,'','','marilyne.pain@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:52',0,'2018-07-31 17:07:10'),(26634864,'kenan-aleyna@hotmail.fr',1,'','','kenan-aleyna@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:52',0,'2018-07-31 17:07:10'),(26634865,'zarian61@aol.com',1,'','','zarian61@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:52',0,'2018-07-31 17:07:10'),(26634866,'sabrii.du.16@gmail.com',1,'','','sabrii.du.16@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:52',0,'2018-07-31 17:07:10'),(26634867,'aj3eg1973@gmail.com',1,'','','aj3eg1973@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:53',0,'2018-07-31 17:07:10'),(26634868,'zizou74@numericable.fr',1,'','','zizou74@numericable.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:53',0,'2018-07-31 17:07:10'),(26634869,'ivan.foutel@orange.fr',1,'','','ivan.foutel@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:53',0,'2018-07-31 17:07:10'),(26634870,'r_eon@orange.fr',1,'','','r_eon@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:53',0,'2018-07-31 17:07:10'),(26634871,'ana_p@hotmail.fr',1,'','','ana_p@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:54',0,'2018-07-31 17:07:10'),(26634872,'stephanie.colart@hotmail.com',1,'','','stephanie.colart@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:54',0,'2018-07-31 17:07:10'),(26634873,'lina.k@hotmail.fr',1,'','','lina.k@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:54',0,'2018-07-31 17:07:10'),(26634874,'fety10@laposte.net',1,'','','fety10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:55',0,'2018-07-31 17:07:10'),(26634875,'poupou5767@orange.fr',1,'','','poupou5767@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:55',0,'2018-07-31 17:07:10'),(26634876,'dannimic@laposte.net',1,'','','dannimic@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:55',0,'2018-07-31 17:07:10'),(26634877,'hellolidia@icloud.com',1,'','','hellolidia@icloud.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:55',0,'2018-07-31 17:07:10'),(26634878,'serge.vangrevelinghe@wanadoo.fr',1,'','','serge.vangrevelinghe@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:55',0,'2018-07-31 17:07:10'),(26634879,'herve.lavergne@free.fr',1,'','','herve.lavergne@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:56',0,'2018-07-31 17:07:10'),(26634880,'miniferreira@outlook.fr',1,'','','miniferreira@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:56',0,'2018-07-31 17:07:10'),(26634881,'marjo.gonzalez@wanadoo.fr',1,'','','marjo.gonzalez@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:56',0,'2018-07-31 17:07:10'),(26634882,'hamelin.yc@orange.fr',1,'','','hamelin.yc@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:56',0,'2018-07-31 17:07:10'),(26634883,'mollesylvie2504@hotmail.fr',1,'','','mollesylvie2504@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:57',0,'2018-07-31 17:07:10'),(26634884,'roger.pattou@free.fr',1,'','','roger.pattou@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:57',0,'2018-07-31 17:07:10'),(26634885,'pkohrmann@yahoo.fr',1,'','','pkohrmann@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:57',0,'2018-07-31 17:07:10'),(26634886,'bernadette.rivette@gmail.com',1,'','','bernadette.rivette@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:57',0,'2018-07-31 17:07:10'),(26634887,'mona.mecheri@hotmail.fr',1,'','','mona.mecheri@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:58',0,'2018-07-31 17:07:10'),(26634888,'chatpeluche@yahoo.fr',1,'','','chatpeluche@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:58',0,'2018-07-31 17:07:10'),(26634889,'nathystm@yahoo.fr',1,'','','nathystm@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:58',0,'2018-07-31 17:07:10'),(26634890,'djeseuss@orange.fr',1,'','','djeseuss@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:58',0,'2018-07-31 17:07:10'),(26634891,'jojolbernard@orange.fr',1,'','','jojolbernard@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:59',0,'2018-07-31 17:07:10'),(26634892,'patrickbesnard51@gmail.com',1,'','','patrickbesnard51@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:59',0,'2018-07-31 17:07:10'),(26634893,'jean-yves.mahieu@orange.fr',1,'','','jean-yves.mahieu@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:59',0,'2018-07-31 17:07:10'),(26634894,'ribau.stephanie57@gmail.com',1,'','','ribau.stephanie57@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:59',0,'2018-07-31 17:07:10'),(26634895,'florencebravo@msn.com',1,'','','florencebravo@msn.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:25:59',0,'2018-07-31 17:07:10'),(26634896,'natilan@hotmail.fr',1,'','','natilan@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:00',0,'2018-07-31 17:07:10'),(26634897,'mailaumi@yahoo.fr',1,'','','mailaumi@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:00',0,'2018-07-31 17:07:10'),(26634898,'sebasrfoot@gmail.com',1,'','','sebasrfoot@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:00',0,'2018-07-31 17:07:10'),(26634899,'ungrendale@outlook.fr',1,'','','ungrendale@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:00',0,'2018-07-31 17:07:10'),(26634900,'93lloic@gmail.com',1,'','','93lloic@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:01',0,'2018-07-31 17:07:10'),(26634901,'myriam.modicom@wanadoo.fr',1,'','','myriam.modicom@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:01',0,'2018-07-31 17:07:10'),(26634902,'melanie.massard89@gmail.com',1,'','','melanie.massard89@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:01',0,'2018-07-31 17:07:10'),(26634903,'britney24100@hotmail.fr',1,'','','britney24100@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:01',0,'2018-07-31 17:07:10'),(26634904,'alexia.xerner@gmail.com',1,'','','alexia.xerner@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:02',0,'2018-07-31 17:07:10'),(26634905,'cali_2@live.fr',1,'','','cali_2@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:02',0,'2018-07-31 17:07:10'),(26634906,'a-m.cogo@laposte.net',1,'','','a-m.cogo@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:02',0,'2018-07-31 17:07:10'),(26634907,'morgane@avenuedesvins.fr',1,'','','morgane@avenuedesvins.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:02',0,'2018-07-31 17:07:10'),(26634908,'rogermarie02200@gmail.com',1,'','','rogermarie02200@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:03',0,'2018-07-31 17:07:10'),(26634909,'presentaousse@hotmail.fr',1,'','','presentaousse@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:03',0,'2018-07-31 17:07:10'),(26634910,'frederique.garnier@aliceadsl.fr',1,'','','frederique.garnier@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:03',0,'2018-07-31 17:07:10'),(26634911,'julieanne.hantraye@yahoo.fr',1,'','','julieanne.hantraye@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:03',0,'2018-07-31 17:07:10'),(26634912,'belleaventure90@gmail.com',1,'','','belleaventure90@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:03',0,'2018-07-31 17:07:10'),(26634913,'nath63.chev@gmail.com',1,'','','nath63.chev@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:04',0,'2018-07-31 17:07:10'),(26634914,'pgvgggjg@gmail.com',1,'','','pgvgggjg@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:04',0,'2018-07-31 17:07:10'),(26634915,'franck30perrin@gmail.com',1,'','','franck30perrin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:04',0,'2018-07-31 17:07:10'),(26634916,'kar12@laposte.net',1,'','','kar12@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:04',0,'2018-07-31 17:07:10'),(26634917,'laurence.cavellat@gmail.com',1,'','','laurence.cavellat@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:05',0,'2018-07-31 17:07:10'),(26634918,'christine33.thomelin@gmx.com',1,'','','christine33.thomelin@gmx.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:05',0,'2018-07-31 17:07:10'),(26634919,'renckly.nathalie@aliceadsl.fr',1,'','','renckly.nathalie@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:05',0,'2018-07-31 17:07:10'),(26634920,'patricktissot90@yahoo.com',1,'','','patricktissot90@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:05',0,'2018-07-31 17:07:10'),(26634921,'melanie.schmitt40@outlook.com',1,'','','melanie.schmitt40@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:06',0,'2018-07-31 17:07:10'),(26634922,'patrix.s713@orange.fr',1,'','','patrix.s713@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:06',0,'2018-07-31 17:07:10'),(26634923,'martinlauro.anais@hotmail.fr',1,'','','martinlauro.anais@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:06',0,'2018-07-31 17:07:10'),(26634924,'nounoucricri@magicmanu.com',1,'','','nounoucricri@magicmanu.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:06',0,'2018-07-31 17:07:10'),(26634925,'michel.wimmer@wanadoo.fr',1,'','','michel.wimmer@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:06',0,'2018-07-31 17:07:10'),(26634926,'claire.lamy86@gmail.com',1,'','','claire.lamy86@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:07',0,'2018-07-31 17:07:10'),(26634927,'evelyne.debay@bbox.fr',1,'','','evelyne.debay@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:07',0,'2018-07-31 17:07:10'),(26634928,'jenyfer89300@hotmail.fr',1,'','','jenyfer89300@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:07',0,'2018-07-31 17:07:10'),(26634929,'carmenitou@hotmail.fr',1,'','','carmenitou@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:07',0,'2018-07-31 17:07:10'),(26634930,'talice.89k@gmail.com',1,'','','talice.89k@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:08',0,'2018-07-31 17:07:10'),(26634931,'jacquot.mimine@free.fr',1,'','','jacquot.mimine@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:08',0,'2018-07-31 17:07:10'),(26634932,'demarelle.cyrille@gmail.com',1,'','','demarelle.cyrille@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:08',0,'2018-07-31 17:07:10'),(26634933,'al03061991@gmail.com',1,'','','al03061991@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:08',0,'2018-07-31 17:07:10'),(26634934,'jjp.clau51@hotmail.com',1,'','','jjp.clau51@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:08',0,'2018-07-31 17:07:10'),(26634935,'bulim10@laposte.net',1,'','','bulim10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:09',0,'2018-07-31 17:07:10'),(26634936,'mariji22@free.fr',1,'','','mariji22@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:09',0,'2018-07-31 17:07:10'),(26634937,'creamymagic@wanadoo.fr',1,'','','creamymagic@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:09',0,'2018-07-31 17:07:10'),(26634938,'stephanie.gros6@gmail.com',1,'','','stephanie.gros6@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:09',0,'2018-07-31 17:07:10'),(26634939,'frederic591@free.fr',1,'','','frederic591@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:10',0,'2018-07-31 17:07:10'),(26634940,'lm8creation@gmail.com',1,'','','lm8creation@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:10',0,'2018-07-31 17:07:10'),(26634941,'badgoll00@gmail.com',1,'','','badgoll00@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:10',0,'2018-07-31 17:07:10'),(26634942,'gilbert.lenglet@sfr.fr',1,'','','gilbert.lenglet@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:10',0,'2018-07-31 17:07:10'),(26634943,'jolyines33270@gmail.com',1,'','','jolyines33270@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:11',0,'2018-07-31 17:07:10'),(26634944,'petitprezjeanluc@gmail.com',1,'','','petitprezjeanluc@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:11',0,'2018-07-31 17:07:10'),(26634945,'gmgeraldine39@gmail.com',1,'','','gmgeraldine39@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:11',0,'2018-07-31 17:07:10'),(26634946,'cecilia21300@hotmail.fr',1,'','','cecilia21300@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:11',0,'2018-07-31 17:07:10'),(26634947,'jcvpascal@orange.fr',1,'','','jcvpascal@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:12',0,'2018-07-31 17:07:10'),(26634948,'luvaalle.ros@laposte.net',1,'','','luvaalle.ros@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:12',0,'2018-07-31 17:07:10'),(26634949,'charmotlea@gmail.com',1,'','','charmotlea@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:12',0,'2018-07-31 17:07:10'),(26634950,'vanessadebouys52100@gmail.com',1,'','','vanessadebouys52100@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:12',0,'2018-07-31 17:07:10'),(26634951,'jeanpaulpycke@yahoo.fr',1,'','','jeanpaulpycke@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:12',0,'2018-07-31 17:07:10'),(26634952,'ri54er@yahoo.fr',1,'','','ri54er@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:13',0,'2018-07-31 17:07:10'),(26634953,'steph.liger@yahoo.com',1,'','','steph.liger@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:13',0,'2018-07-31 17:07:10'),(26634954,'s.benard360@laposte.net',1,'','','s.benard360@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:13',0,'2018-07-31 17:07:10'),(26634955,'patrickanzelin@yahoo.fr',1,'','','patrickanzelin@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:13',0,'2018-07-31 17:07:10'),(26634956,'flofloetcoco@live.fr',1,'','','flofloetcoco@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:14',0,'2018-07-31 17:07:10'),(26634957,'pierre.vivet@neuf.fr',1,'','','pierre.vivet@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:14',0,'2018-07-31 17:07:10'),(26634958,'syl_leger@yahoo.fr',1,'','','syl_leger@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:14',0,'2018-07-31 17:07:10'),(26634959,'titenat@aliceadsl.fr',1,'','','titenat@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:15',0,'2018-07-31 17:07:10'),(26634960,'marisunjo3@gmail.com',1,'','','marisunjo3@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:15',0,'2018-07-31 17:07:10'),(26634961,'gerard.lurion@gmail.com',1,'','','gerard.lurion@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:15',0,'2018-07-31 17:07:10'),(26634962,'sylvie.blanckaert@free.fr',1,'','','sylvie.blanckaert@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:15',0,'2018-07-31 17:07:10'),(26634963,'collasmichele@orange.fr',1,'','','collasmichele@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:15',0,'2018-07-31 17:07:10'),(26634964,'christelle.pesty@sfr.fr',1,'','','christelle.pesty@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:16',0,'2018-07-31 17:07:10'),(26634965,'vinilouh3@gmail.com',1,'','','vinilouh3@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:16',0,'2018-07-31 17:07:10'),(26634966,'mcvandenbrouck@gmail.com',1,'','','mcvandenbrouck@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:16',0,'2018-07-31 17:07:10'),(26634967,'caroline.garledo@outlook.fr',1,'','','caroline.garledo@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:16',0,'2018-07-31 17:07:10'),(26634968,'ophelieromero@orange.fr',1,'','','ophelieromero@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:17',0,'2018-07-31 17:07:10'),(26634969,'olivier.bedelet@gmail.com',1,'','','olivier.bedelet@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:17',0,'2018-07-31 17:07:10'),(26634970,'angel8@netcourrier.com',1,'','','angel8@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:17',0,'2018-07-31 17:07:10'),(26634971,'tjptorto@aol.com',1,'','','tjptorto@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:17',0,'2018-07-31 17:07:10'),(26634972,'micheleg21@free.fr',1,'','','micheleg21@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:18',0,'2018-07-31 17:07:10'),(26634973,'jlmcama@orange.fr',1,'','','jlmcama@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:18',0,'2018-07-31 17:07:10'),(26634974,'arno37700@gmail.com',1,'','','arno37700@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:18',0,'2018-07-31 17:07:10'),(26634975,'serge.pizzirulli@orange.fr',1,'','','serge.pizzirulli@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:18',0,'2018-07-31 17:07:10'),(26634976,'kinder47257@gmail.com',1,'','','kinder47257@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:18',0,'2018-07-31 17:07:10'),(26634977,'muza772@orange.fr',1,'','','muza772@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:19',0,'2018-07-31 17:07:10'),(26634978,'chbleroijui24@outlook.com',1,'','','chbleroijui24@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:19',0,'2018-07-31 17:07:10'),(26634979,'l-sabrina@live.fr',1,'','','l-sabrina@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:19',0,'2018-07-31 17:07:10'),(26634980,'st.cottet@laposte.net',1,'','','st.cottet@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:19',0,'2018-07-31 17:07:10'),(26634981,'debolydie@hotmail.fr',1,'','','debolydie@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:20',0,'2018-07-31 17:07:10'),(26634982,'mireillemaisse@laposte.net',1,'','','mireillemaisse@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:20',0,'2018-07-31 17:07:10'),(26634983,'ilona41@live.fr',1,'','','ilona41@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:20',0,'2018-07-31 17:07:10'),(26634984,'ptiange-ptitefleur@hotmail.fr',1,'','','ptiange-ptitefleur@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:20',0,'2018-07-31 17:07:10'),(26634985,'simone.rohr@sfr.fr',1,'','','simone.rohr@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:20',0,'2018-07-31 17:07:10'),(26634986,'conducteur1966@gmail.com',1,'','','conducteur1966@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:21',0,'2018-07-31 17:07:10'),(26634987,'delvallee73@gmail.com',1,'','','delvallee73@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:21',0,'2018-07-31 17:07:10'),(26634988,'eraslan68@hotmail.fr',1,'','','eraslan68@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:21',0,'2018-07-31 17:07:10'),(26634989,'cecile.oullie@yahoo.fr',1,'','','cecile.oullie@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:21',0,'2018-07-31 17:07:10'),(26634990,'karine.galasso@laposte.net',1,'','','karine.galasso@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:22',0,'2018-07-31 17:07:10'),(26634991,'irela10@laposte.net',1,'','','irela10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:22',0,'2018-07-31 17:07:10'),(26634992,'jmblagny@gmail.com',1,'','','jmblagny@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:22',0,'2018-07-31 17:07:10'),(26634993,'annietranchard@orange.fr',1,'','','annietranchard@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:22',0,'2018-07-31 17:07:10'),(26634994,'loutreuilski@gmail.com',1,'','','loutreuilski@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:23',0,'2018-07-31 17:07:10'),(26634995,'legent.rosanna@orange.fr',1,'','','legent.rosanna@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:23',0,'2018-07-31 17:07:10'),(26634996,'scarabee30@live.fr',1,'','','scarabee30@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:23',0,'2018-07-31 17:07:10'),(26634997,'fabienne.lafee@orange.fr',1,'','','fabienne.lafee@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:23',0,'2018-07-31 17:07:10'),(26634998,'aurore.ouazene@hotmail.fr',1,'','','aurore.ouazene@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:23',0,'2018-07-31 17:07:10'),(26634999,'francoise.robert19@wanadoo.fr',1,'','','francoise.robert19@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:24',0,'2018-07-31 17:07:10'),(26635000,'amanda_bc2000@yahoo.com',1,'','','amanda_bc2000@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:24',0,'2018-07-31 17:07:10'),(26635001,'celinebrens@hotmail.fr',1,'','','celinebrens@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:24',0,'2018-07-31 17:07:10'),(26635002,'stephanno.francoise@gmail.com',1,'','','stephanno.francoise@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:24',0,'2018-07-31 17:07:10'),(26635003,'alinemalinesa@gmail.com',1,'','','alinemalinesa@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:25',0,'2018-07-31 17:07:10'),(26635004,'nestrieno23@gmail.com',1,'','','nestrieno23@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:25',0,'2018-07-31 17:07:10'),(26635005,'danielle.mariolle@orange.fr',1,'','','danielle.mariolle@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:25',0,'2018-07-31 17:07:10'),(26635006,'florencedemont85@gmail.com',1,'','','florencedemont85@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:25',0,'2018-07-31 17:07:10'),(26635007,'bcn@gmx.fr',1,'','','bcn@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:26',0,'2018-07-31 17:07:10'),(26635008,'beignet.maison@gmail.com',1,'','','beignet.maison@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:26',0,'2018-07-31 17:07:10'),(26635009,'patrick.demotier@neuf.fr',1,'','','patrick.demotier@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:26',0,'2018-07-31 17:07:10'),(26635010,'nicohind@gmail.com',1,'','','nicohind@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:26',0,'2018-07-31 17:07:10'),(26635011,'yamninick14@gmail.com',1,'','','yamninick14@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:27',0,'2018-07-31 17:07:10'),(26635012,'hare.nathalie@hotmail.fr',1,'','','hare.nathalie@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:27',0,'2018-07-31 17:07:10'),(26635013,'myssy54@hotmail.fr',1,'','','myssy54@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:27',0,'2018-07-31 17:07:10'),(26635014,'pillonsebastien@orange.fr',1,'','','pillonsebastien@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:27',0,'2018-07-31 17:07:10'),(26635015,'daura@laposte.net',1,'','','daura@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:27',0,'2018-07-31 17:07:10'),(26635016,'julie.menez29@laposte.net',1,'','','julie.menez29@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:28',0,'2018-07-31 17:07:10'),(26635017,'iyodatho@yahoo.fr',1,'','','iyodatho@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:28',0,'2018-07-31 17:07:10'),(26635018,'silouene@hotmail.fr',1,'','','silouene@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:28',0,'2018-07-31 17:07:10'),(26635019,'marion2974@hotmail.com',1,'','','marion2974@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:29',0,'2018-07-31 17:07:10'),(26635020,'espithierry@hotmail.com',1,'','','espithierry@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:29',0,'2018-07-31 17:07:10'),(26635021,'car.vg@wanadoo.fr',1,'','','car.vg@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:29',0,'2018-07-31 17:07:10'),(26635022,'stefdu32@outlook.fr',1,'','','stefdu32@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:29',0,'2018-07-31 17:07:10'),(26635023,'emelinesterckeman@gmail.com',1,'','','emelinesterckeman@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:29',0,'2018-07-31 17:07:10'),(26635024,'charenton75@yahoo.fr',1,'','','charenton75@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:30',0,'2018-07-31 17:07:10'),(26635025,'a.audouze@yahoo.fr',1,'','','a.audouze@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:30',0,'2018-07-31 17:07:10'),(26635026,'yayadu35@hotmail.fr',1,'','','yayadu35@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:30',0,'2018-07-31 17:07:10'),(26635027,'chrisber76550@laposte.net',1,'','','chrisber76550@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:30',0,'2018-07-31 17:07:10'),(26635028,'nicod.rosine@gmail.com',1,'','','nicod.rosine@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:31',0,'2018-07-31 17:07:10'),(26635029,'beatrice.patot@gmail.com',1,'','','beatrice.patot@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:31',0,'2018-07-31 17:07:10'),(26635030,'culliganan@gmail.com',1,'','','culliganan@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:31',0,'2018-07-31 17:07:10'),(26635031,'am.chomel@gmail.com',1,'','','am.chomel@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:31',0,'2018-07-31 17:07:10'),(26635032,'anthonybertrand07100@gmail.com',1,'','','anthonybertrand07100@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:32',0,'2018-07-31 17:07:10'),(26635033,'anne.barboux@gmail.com',1,'','','anne.barboux@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:32',0,'2018-07-31 17:07:10'),(26635034,'sebastiendu58130@hotmail.com',1,'','','sebastiendu58130@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:32',0,'2018-07-31 17:07:10'),(26635035,'sandrinevacherot@hotmail.fr',1,'','','sandrinevacherot@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:32',0,'2018-07-31 17:07:10'),(26635036,'fabenoman@live.fr',1,'','','fabenoman@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:32',0,'2018-07-31 17:07:10'),(26635037,'gilson.annie@orange.fr',1,'','','gilson.annie@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:33',0,'2018-07-31 17:07:10'),(26635038,'benedicte-heiligenstein@orange.fr',1,'','','benedicte-heiligenstein@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:33',0,'2018-07-31 17:07:10'),(26635039,'coliope@gmail.com',1,'','','coliope@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:33',0,'2018-07-31 17:07:10'),(26635040,'agridomi@hotmail.fr',1,'','','agridomi@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:33',0,'2018-07-31 17:07:10'),(26635041,'maryberthen@wanadoo.fr',1,'','','maryberthen@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:34',0,'2018-07-31 17:07:10'),(26635042,'greynald@orange.fr',1,'','','greynald@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:34',0,'2018-07-31 17:07:10'),(26635043,'oraclaries@hotmail.com',1,'','','oraclaries@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:34',0,'2018-07-31 17:07:10'),(26635044,'didiervialles@orange.fr',1,'','','didiervialles@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:35',0,'2018-07-31 17:07:10'),(26635045,'momodu59310@laposte.net',1,'','','momodu59310@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:35',0,'2018-07-31 17:07:10'),(26635046,'andre.pain1@laposte.net',1,'','','andre.pain1@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:35',0,'2018-07-31 17:07:10'),(26635047,'bernard.rabin@orange.fr',1,'','','bernard.rabin@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:35',0,'2018-07-31 17:07:10'),(26635048,'passiontricot52@yahoo.com',1,'','','passiontricot52@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:35',0,'2018-07-31 17:07:10'),(26635049,'patrickmurat85@orange.fr',1,'','','patrickmurat85@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:36',0,'2018-07-31 17:07:10'),(26635050,'boosta200@hotmail.fr',1,'','','boosta200@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:36',0,'2018-07-31 17:07:10'),(26635051,'mmedhi007@hotmail.com',1,'','','mmedhi007@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:36',0,'2018-07-31 17:07:10'),(26635052,'RACHELE.DONSIMONI@ORANGE.FR',1,'','','RACHELE.DONSIMONI@ORANGE.FR','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:36',0,'2018-07-31 17:07:10'),(26635053,'atlasdiva@gmail.com',1,'','','atlasdiva@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:37',0,'2018-07-31 17:07:10'),(26635054,'floeva03@hotmail.fr',1,'','','floeva03@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:37',0,'2018-07-31 17:07:10'),(26635055,'helen.allain@gmail.com',1,'','','helen.allain@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:37',0,'2018-07-31 17:07:10'),(26635056,'lavilco@orange.fr',1,'','','lavilco@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:37',0,'2018-07-31 17:07:10'),(26635057,'earl.vilnius27@gmail.com',1,'','','earl.vilnius27@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:38',0,'2018-07-31 17:07:10'),(26635058,'slyanneso@gmail.com',1,'','','slyanneso@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:38',0,'2018-07-31 17:07:10'),(26635059,'vienne.sonia4@gmail.com',1,'','','vienne.sonia4@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:38',0,'2018-07-31 17:07:10'),(26635060,'malou84100@hotmail.fr',1,'','','malou84100@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:38',0,'2018-07-31 17:07:10'),(26635061,'skarette@hotmail.fr',1,'','','skarette@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:38',0,'2018-07-31 17:07:10'),(26635062,'mi.melo@hotmail.fr',1,'','','mi.melo@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:39',0,'2018-07-31 17:07:10'),(26635063,'tissotmelania@yahoo.fr',1,'','','tissotmelania@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:39',0,'2018-07-31 17:07:10'),(26635064,'regis.boizumeau@orange.fr',1,'','','regis.boizumeau@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:40',0,'2018-07-31 17:07:10'),(26635065,'aurelle21@aol.com',1,'','','aurelle21@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:40',0,'2018-07-31 17:07:10'),(26635066,'bea62800@outlook.fr',1,'','','bea62800@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:40',0,'2018-07-31 17:07:10'),(26635067,'sebastiendarques@yahoo.fr',1,'','','sebastiendarques@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:41',0,'2018-07-31 17:07:10'),(26635068,'edoublet@laposte.net',1,'','','edoublet@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:41',0,'2018-07-31 17:07:10'),(26635069,'roger.lesueur@gmail.com',1,'','','roger.lesueur@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:41',0,'2018-07-31 17:07:10'),(26635070,'pat.kow@hotmail.fr',1,'','','pat.kow@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:41',0,'2018-07-31 17:07:10'),(26635071,'perrier.evelyne@orange.fr',1,'','','perrier.evelyne@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:42',0,'2018-07-31 17:07:10'),(26635072,'myriam.feuillois@aliceadsl.fr',1,'','','myriam.feuillois@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:42',0,'2018-07-31 17:07:10'),(26635073,'michele.daimez@gmail.com',1,'','','michele.daimez@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:42',0,'2018-07-31 17:07:10'),(26635074,'ccolot@hotmail.com',1,'','','ccolot@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:43',0,'2018-07-31 17:07:10'),(26635075,'remy.renaudat@gmail.com',1,'','','remy.renaudat@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:43',0,'2018-07-31 17:07:10'),(26635076,'cglebourdon@gmail.com',1,'','','cglebourdon@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:43',0,'2018-07-31 17:07:10'),(26635077,'annik.benyahia@orange.fr',1,'','','annik.benyahia@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:44',0,'2018-07-31 17:07:10'),(26635078,'chevalletmaud@free.fr',1,'','','chevalletmaud@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:44',0,'2018-07-31 17:07:10'),(26635079,'hervethivollet@yahoo.fr',1,'','','hervethivollet@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:45',0,'2018-07-31 17:07:10'),(26635080,'irenecassy@hotmail.fr',1,'','','irenecassy@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:45',0,'2018-07-31 17:07:10'),(26635081,'mayachrist77@hotmail.fr',1,'','','mayachrist77@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:46',0,'2018-07-31 17:07:10'),(26635082,'julien.fanchette@laposte.net',1,'','','julien.fanchette@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:46',0,'2018-07-31 17:07:10'),(26635083,'angeba7@hotmail.com',1,'','','angeba7@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:46',0,'2018-07-31 17:07:10'),(26635084,'j.fornes@laposte.net',1,'','','j.fornes@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:46',0,'2018-07-31 17:07:10'),(26635085,'elviremonchanin@gmail.com',1,'','','elviremonchanin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:47',0,'2018-07-31 17:07:10'),(26635086,'kiveubien@gmail.com',1,'','','kiveubien@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:47',0,'2018-07-31 17:07:10'),(26635087,'karinechrist@free.fr',1,'','','karinechrist@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:47',0,'2018-07-31 17:07:10'),(26635088,'dayounete@hotmail.com',1,'','','dayounete@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:48',0,'2018-07-31 17:07:10'),(26635089,'cecile.bialoux.kfc@gmail.com',1,'','','cecile.bialoux.kfc@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:48',0,'2018-07-31 17:07:10'),(26635090,'celia.schehrer@gmail.com',1,'','','celia.schehrer@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:49',0,'2018-07-31 17:07:10'),(26635091,'mollyjoylyn@gmail.com',1,'','','mollyjoylyn@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:49',0,'2018-07-31 17:07:10'),(26635092,'vanessa.coppin@bbox.fr',1,'','','vanessa.coppin@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:49',0,'2018-07-31 17:07:10'),(26635093,'pascoulaines2100@outlook.fr',1,'','','pascoulaines2100@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:50',0,'2018-07-31 17:07:10'),(26635094,'p.chardes@cegetel.net',1,'','','p.chardes@cegetel.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:50',0,'2018-07-31 17:07:10'),(26635095,'jusalain@hotmail.fr',1,'','','jusalain@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:50',0,'2018-07-31 17:07:10'),(26635096,'gillet.maelly@orange.fr',1,'','','gillet.maelly@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:50',0,'2018-07-31 17:07:10'),(26635097,'tofibelle@hotmail.fr',1,'','','tofibelle@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:51',0,'2018-07-31 17:07:10'),(26635098,'val147@orange.fr',1,'','','val147@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:51',0,'2018-07-31 17:07:10'),(26635099,'lounette62260@hotmail.fr',1,'','','lounette62260@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:51',0,'2018-07-31 17:07:10'),(26635100,'bso12366@gmail.com',1,'','','bso12366@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:52',0,'2018-07-31 17:07:10'),(26635101,'corinne.bv@hotmail.fr',1,'','','corinne.bv@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:52',0,'2018-07-31 17:07:10'),(26635102,'emiliadavid1991@gmail.com',1,'','','emiliadavid1991@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:52',0,'2018-07-31 17:07:10'),(26635103,'brigitte_jeanne@outlook.fr',1,'','','brigitte_jeanne@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:52',0,'2018-07-31 17:07:10'),(26635104,'alaingobert@laposte.net',1,'','','alaingobert@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:53',0,'2018-07-31 17:07:10'),(26635105,'line.44@wanadoo.fr',1,'','','line.44@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:53',0,'2018-07-31 17:07:10'),(26635106,'laurence.bouteleux@outlook.fr',1,'','','laurence.bouteleux@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:53',0,'2018-07-31 17:07:10'),(26635107,'pegtaburet@gmail.com',1,'','','pegtaburet@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:53',0,'2018-07-31 17:07:10'),(26635108,'auguste.vialade@orange.fr',1,'','','auguste.vialade@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:54',0,'2018-07-31 17:07:10'),(26635109,'nina7552@live.fr',1,'','','nina7552@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:54',0,'2018-07-31 17:07:10'),(26635110,'chrisambre@free.fr',1,'','','chrisambre@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:54',0,'2018-07-31 17:07:10'),(26635111,'clementine41300@gmail.com',1,'','','clementine41300@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:54',0,'2018-07-31 17:07:10'),(26635112,'rachalex@sfr.fr',1,'','','rachalex@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:55',0,'2018-07-31 17:07:10'),(26635113,'anaismech@outlook.fr',1,'','','anaismech@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:55',0,'2018-07-31 17:07:10'),(26635114,'melaniatissot@outlook.fr',1,'','','melaniatissot@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:55',0,'2018-07-31 17:07:10'),(26635115,'mona24100@hotmail.fr',1,'','','mona24100@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:55',0,'2018-07-31 17:07:10'),(26635116,'sochadu62430@hotmail.fr',1,'','','sochadu62430@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:56',0,'2018-07-31 17:07:10'),(26635117,'chapeaupoijui24@outlook.com',1,'','','chapeaupoijui24@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:56',0,'2018-07-31 17:07:10'),(26635118,'maelysconcouriste@outlook.fr',1,'','','maelysconcouriste@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:56',0,'2018-07-31 17:07:10'),(26635119,'lisatacje@gmail.com',1,'','','lisatacje@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:57',0,'2018-07-31 17:07:10'),(26635120,'lilychery24@outlook.fr',1,'','','lilychery24@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:57',0,'2018-07-31 17:07:10'),(26635121,'esteb-an@live.fr',1,'','','esteb-an@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:57',0,'2018-07-31 17:07:10'),(26635122,'m.guillot70@laposte.net',1,'','','m.guillot70@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:57',0,'2018-07-31 17:07:10'),(26635123,'elosigu@hotmail.com',1,'','','elosigu@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:57',0,'2018-07-31 17:07:10'),(26635124,'myriam.cocqueret@sfr.fr',1,'','','myriam.cocqueret@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:58',0,'2018-07-31 17:07:10'),(26635125,'jean-luc.thouron@laposte.net',1,'','','jean-luc.thouron@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:58',0,'2018-07-31 17:07:10'),(26635126,'bernardbrumain@orange.fr',1,'','','bernardbrumain@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:58',0,'2018-07-31 17:07:10'),(26635127,'bnoit-marilou@hotmail.fr',1,'','','bnoit-marilou@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:58',0,'2018-07-31 17:07:10'),(26635128,'nathalie.pillon25@orange.fr',1,'','','nathalie.pillon25@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:59',0,'2018-07-31 17:07:10'),(26635129,'58olympe@live.fr',1,'','','58olympe@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:59',0,'2018-07-31 17:07:10'),(26635130,'fluttaz.claude@gmail.com',1,'','','fluttaz.claude@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:59',0,'2018-07-31 17:07:10'),(26635131,'rene.reynaert1648@gmail.com',1,'','','rene.reynaert1648@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:26:59',0,'2018-07-31 17:07:10'),(26635132,'verosabine@free.fr',1,'','','verosabine@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:00',0,'2018-07-31 17:07:10'),(26635133,'him59229@hotmail.com',1,'','','him59229@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:00',0,'2018-07-31 17:07:10'),(26635134,'genevieve.amiez@orange.fr',1,'','','genevieve.amiez@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:00',0,'2018-07-31 17:07:10'),(26635135,'bbhardouin@hotmail.com',1,'','','bbhardouin@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:00',0,'2018-07-31 17:07:10'),(26635136,'cathye3320@outlook.fr',1,'','','cathye3320@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:01',0,'2018-07-31 17:07:10'),(26635137,'fleurdepicardie@wanadoo.fr',1,'','','fleurdepicardie@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:01',0,'2018-07-31 17:07:10'),(26635138,'bennassar.nicole@gmail.com',1,'','','bennassar.nicole@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:01',0,'2018-07-31 17:07:10'),(26635139,'amandinemech@outlook.fr',1,'','','amandinemech@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:01',0,'2018-07-31 17:07:10'),(26635140,'cojain@outlook.com',1,'','','cojain@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:01',0,'2018-07-31 17:07:10'),(26635141,'patrick.lucchesi3@orange.fr',1,'','','patrick.lucchesi3@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:02',0,'2018-07-31 17:07:10'),(26635142,'broussewilys@hotmail.com',1,'','','broussewilys@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:02',0,'2018-07-31 17:07:10'),(26635143,'herrando.h@wanadoo.fr',1,'','','herrando.h@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:02',0,'2018-07-31 17:07:10'),(26635144,'helene.darmo@yahoo.fr',1,'','','helene.darmo@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:02',0,'2018-07-31 17:07:10'),(26635145,'elwood44@free.fr',1,'','','elwood44@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:03',0,'2018-07-31 17:07:10'),(26635146,'nadege.lahmar@laposte.net',1,'','','nadege.lahmar@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:03',0,'2018-07-31 17:07:10'),(26635147,'mariolle.aurore@orange.fr',1,'','','mariolle.aurore@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:03',0,'2018-07-31 17:07:10'),(26635148,'francois.rameaux@yahoo.fr',1,'','','francois.rameaux@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:03',0,'2018-07-31 17:07:10'),(26635149,'mulderfox62@gmail.com',1,'','','mulderfox62@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:03',0,'2018-07-31 17:07:10'),(26635150,'martinec20@hotmail.com',1,'','','martinec20@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:04',0,'2018-07-31 17:07:10'),(26635151,'yanickschwartz@outlook.fr',1,'','','yanickschwartz@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:04',0,'2018-07-31 17:07:10'),(26635152,'gbassoleil@yahoo.fr',1,'','','gbassoleil@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:04',0,'2018-07-31 17:07:10'),(26635153,'verti@netcourrier.com',1,'','','verti@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:04',0,'2018-07-31 17:07:10'),(26635154,'mireille.flohic@gmail.com',1,'','','mireille.flohic@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:05',0,'2018-07-31 17:07:10'),(26635155,'hennu.celine@orange.fr',1,'','','hennu.celine@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:05',0,'2018-07-31 17:07:10'),(26635156,'goinfretteperso@gmail.com',1,'','','goinfretteperso@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:05',0,'2018-07-31 17:07:10'),(26635157,'awalt77@numericable.fr',1,'','','awalt77@numericable.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:05',0,'2018-07-31 17:07:10'),(26635158,'thierry.knauf0606@orange.fr',1,'','','thierry.knauf0606@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:06',0,'2018-07-31 17:07:10'),(26635159,'lahoussinechristophe@gmail.com',1,'','','lahoussinechristophe@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:06',0,'2018-07-31 17:07:10'),(26635160,'jawad59330@hotmail.fr',1,'','','jawad59330@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:06',0,'2018-07-31 17:07:10'),(26635161,'moutonne@outlook.fr',1,'','','moutonne@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:06',0,'2018-07-31 17:07:10'),(26635162,'jocle.verse@free.fr',1,'','','jocle.verse@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:06',0,'2018-07-31 17:07:10'),(26635163,'kimaurelien@hotmail.fr',1,'','','kimaurelien@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:07',0,'2018-07-31 17:07:10'),(26635164,'sebastien.gillet54400@orange.fr',1,'','','sebastien.gillet54400@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:07',0,'2018-07-31 17:07:10'),(26635165,'sebmosphere@hotmail.fr',1,'','','sebmosphere@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:07',0,'2018-07-31 17:07:10'),(26635166,'tatiana2424@outlook.fr',1,'','','tatiana2424@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:07',0,'2018-07-31 17:07:10'),(26635167,'damienbonneau94@gmail.com',1,'','','damienbonneau94@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:08',0,'2018-07-31 17:07:10'),(26635168,'myriam.mimy@laposte.net',1,'','','myriam.mimy@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:08',0,'2018-07-31 17:07:10'),(26635169,'chapeaupointu24@outlook.fr',1,'','','chapeaupointu24@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:08',0,'2018-07-31 17:07:10'),(26635170,'djeblijeux80620@gmail.com',1,'','','djeblijeux80620@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:09',0,'2018-07-31 17:07:10'),(26635171,'loganorlane@outlook.fr',1,'','','loganorlane@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:09',0,'2018-07-31 17:07:10'),(26635172,'apocalyptica44@hotmail.fr',1,'','','apocalyptica44@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:09',0,'2018-07-31 17:07:10'),(26635173,'uriol10@laposte.net',1,'','','uriol10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:10',0,'2018-07-31 17:07:10'),(26635174,'celine.moreau83@orange.fr',1,'','','celine.moreau83@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:10',0,'2018-07-31 17:07:10'),(26635175,'fgaydier@gmail.com',1,'','','fgaydier@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:10',0,'2018-07-31 17:07:10'),(26635176,'lambertpierre@laposte.net',1,'','','lambertpierre@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:10',0,'2018-07-31 17:07:10'),(26635177,'nicole.kerouredan@free.fr',1,'','','nicole.kerouredan@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:11',0,'2018-07-31 17:07:10'),(26635178,'patogaz59@gmail.com',1,'','','patogaz59@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:11',0,'2018-07-31 17:07:10'),(26635179,'chanel76730@aol.com',1,'','','chanel76730@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:11',0,'2018-07-31 17:07:10'),(26635180,'t.trsek@gmail.com',1,'','','t.trsek@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:11',0,'2018-07-31 17:07:10'),(26635181,'michael.thalheimer@wanadoo.fr',1,'','','michael.thalheimer@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:11',0,'2018-07-31 17:07:10'),(26635182,'aqme246@aol.com',1,'','','aqme246@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:12',0,'2018-07-31 17:07:10'),(26635183,'sperez63@free.fr',1,'','','sperez63@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:12',0,'2018-07-31 17:07:10'),(26635184,'bettybettymai21@gmail.com',1,'','','bettybettymai21@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:12',0,'2018-07-31 17:07:10'),(26635185,'kimpasti@outlook.fr',1,'','','kimpasti@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:12',0,'2018-07-31 17:07:10'),(26635186,'miguy2011@live.fr',1,'','','miguy2011@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:13',0,'2018-07-31 17:07:10'),(26635187,'alloclairon@gmail.com',1,'','','alloclairon@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:13',0,'2018-07-31 17:07:10'),(26635188,'virnouleis@outlook.fr',1,'','','virnouleis@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:13',0,'2018-07-31 17:07:10'),(26635189,'violette.mehaye@wanadoo.fr',1,'','','violette.mehaye@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:13',0,'2018-07-31 17:07:10'),(26635190,'polre@netcourrier.com',1,'','','polre@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:14',0,'2018-07-31 17:07:10'),(26635191,'adegus2@gmail.com',1,'','','adegus2@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:14',0,'2018-07-31 17:07:10'),(26635192,'mokio@netcourrier.com',1,'','','mokio@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:14',0,'2018-07-31 17:07:10'),(26635193,'burnel.stephanie@neuf.fr',1,'','','burnel.stephanie@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:14',0,'2018-07-31 17:07:10'),(26635194,'vatelinvirginie@gmail.com',1,'','','vatelinvirginie@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:15',0,'2018-07-31 17:07:10'),(26635195,'carettevalerie@orange.fr',1,'','','carettevalerie@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:15',0,'2018-07-31 17:07:10'),(26635196,'jnath59@hotmail.fr',1,'','','jnath59@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:15',0,'2018-07-31 17:07:10'),(26635197,'laurence.merceron@outlook.fr',1,'','','laurence.merceron@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:15',0,'2018-07-31 17:07:10'),(26635198,'n1c0.gam9@gmail.com',1,'','','n1c0.gam9@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:15',0,'2018-07-31 17:07:10'),(26635199,'lolablandine_19@hotmail.fr',1,'','','lolablandine_19@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:16',0,'2018-07-31 17:07:10'),(26635200,'michel.sapone@aliceadsl.fr',1,'','','michel.sapone@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:16',0,'2018-07-31 17:07:10'),(26635201,'concoursdomi@laposte.net',1,'','','concoursdomi@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:16',0,'2018-07-31 17:07:10'),(26635202,'sazds@netcourrier.com',1,'','','sazds@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:16',0,'2018-07-31 17:07:10'),(26635203,'catherine.chevallier53@sfr.fr',1,'','','catherine.chevallier53@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:17',0,'2018-07-31 17:07:10'),(26635204,'kais18@hotmail.com',1,'','','kais18@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:17',0,'2018-07-31 17:07:10'),(26635205,'jonabu29@gmail.com',1,'','','jonabu29@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:17',0,'2018-07-31 17:07:10'),(26635206,'mlaverny@yahoo.fr',1,'','','mlaverny@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:18',0,'2018-07-31 17:07:10'),(26635207,'moititi24@hotmail.fr',1,'','','moititi24@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:18',0,'2018-07-31 17:07:10'),(26635208,'Sylviarigoulot@gmail.com',1,'','','Sylviarigoulot@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:18',0,'2018-07-31 17:07:10'),(26635209,'payard@wanadoo.fr',1,'','','payard@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:18',0,'2018-07-31 17:07:10'),(26635210,'cath.rossi1@gmail.com',1,'','','cath.rossi1@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:19',0,'2018-07-31 17:07:10'),(26635211,'celineblot@live.fr',1,'','','celineblot@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:19',0,'2018-07-31 17:07:10'),(26635212,'solenemolene@hotmail.fr',1,'','','solenemolene@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:19',0,'2018-07-31 17:07:10'),(26635213,'delphinemorineau@hotmail.fr',1,'','','delphinemorineau@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:19',0,'2018-07-31 17:07:10'),(26635214,'ophelie.a@wanadoo.fr',1,'','','ophelie.a@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:20',0,'2018-07-31 17:07:10'),(26635215,'nath.cn27@gmail.com',1,'','','nath.cn27@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:20',0,'2018-07-31 17:07:10'),(26635216,'cindy.ferreira7@orange.fr',1,'','','cindy.ferreira7@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:20',0,'2018-07-31 17:07:10'),(26635217,'rroselyne@hotmail.fr',1,'','','rroselyne@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:20',0,'2018-07-31 17:07:10'),(26635218,'arche@ebuyclub.com',1,'','','arche@ebuyclub.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:20',0,'2018-07-31 17:07:10'),(26635219,'bennybee67720@gmail.com',1,'','','bennybee67720@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:21',0,'2018-07-31 17:07:10'),(26635220,'z.fatima5@hotmail.fr',1,'','','z.fatima5@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:21',0,'2018-07-31 17:07:10'),(26635221,'superrachou@orange.fr',1,'','','superrachou@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:21',0,'2018-07-31 17:07:10'),(26635222,'amanda80190@outlook.fr',1,'','','amanda80190@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:21',0,'2018-07-31 17:07:10'),(26635223,'miss-cindy@hotmail.fr',1,'','','miss-cindy@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:22',0,'2018-07-31 17:07:10'),(26635224,'chris.026@hotmail.fr',1,'','','chris.026@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:22',0,'2018-07-31 17:07:10'),(26635225,'aureliedu77230@live.fr',1,'','','aureliedu77230@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:22',0,'2018-07-31 17:07:10'),(26635226,'mamypet98@hotmail.fr',1,'','','mamypet98@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:22',0,'2018-07-31 17:07:10'),(26635227,'moustique_84@yahoo.fr',1,'','','moustique_84@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:23',0,'2018-07-31 17:07:10'),(26635228,'urij10@laposte.net',1,'','','urij10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:23',0,'2018-07-31 17:07:10'),(26635229,'yayaka82@hotmail.fr',1,'','','yayaka82@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:23',0,'2018-07-31 17:07:10'),(26635230,'missaladin@outlook.fr',1,'','','missaladin@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:23',0,'2018-07-31 17:07:10'),(26635231,'kurowski.patrick@free.fr',1,'','','kurowski.patrick@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:24',0,'2018-07-31 17:07:10'),(26635232,'vieiraceline@yahoo.fr',1,'','','vieiraceline@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:24',0,'2018-07-31 17:07:10'),(26635233,'fezat@netcourrier.com',1,'','','fezat@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:24',0,'2018-07-31 17:07:10'),(26635234,'claire.bellanger@cabinet-dehaine.fr',1,'','','claire.bellanger@cabinet-dehaine.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:24',0,'2018-07-31 17:07:10'),(26635235,'moput@netcourrier.com',1,'','','moput@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:24',0,'2018-07-31 17:07:10'),(26635236,'aurecar_5@hotmail.com',1,'','','aurecar_5@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:25',0,'2018-07-31 17:07:10'),(26635237,'delavaultkarine@gmail.com',1,'','','delavaultkarine@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:25',0,'2018-07-31 17:07:10'),(26635238,'monjeux92@hotmail.com',1,'','','monjeux92@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:25',0,'2018-07-31 17:07:10'),(26635239,'thouron57@gmail.com',1,'','','thouron57@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:26',0,'2018-07-31 17:07:10'),(26635240,'pouge@netcourrier.com',1,'','','pouge@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:26',0,'2018-07-31 17:07:10'),(26635241,'flosatis@yahoo.fr',1,'','','flosatis@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:26',0,'2018-07-31 17:07:10'),(26635242,'beaulieu32@orange.fr',1,'','','beaulieu32@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:26',0,'2018-07-31 17:07:10'),(26635243,'buij10@laposte.net',1,'','','buij10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:27',0,'2018-07-31 17:07:10'),(26635244,'evincent@eurocave.com',1,'','','evincent@eurocave.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:27',0,'2018-07-31 17:07:10'),(26635245,'mehayemv@wanadoo.fr',1,'','','mehayemv@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:27',0,'2018-07-31 17:07:10'),(26635246,'royroy62@outlook.fr',1,'','','royroy62@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:27',0,'2018-07-31 17:07:10'),(26635247,'koenigsecker.jessica@bbox.fr',1,'','','koenigsecker.jessica@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:27',0,'2018-07-31 17:07:10'),(26635248,'bernard-blanchard@bbox.fr',1,'','','bernard-blanchard@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:28',0,'2018-07-31 17:07:10'),(26635249,'arielledoggy@laposte.net',1,'','','arielledoggy@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:28',0,'2018-07-31 17:07:10'),(26635250,'stephgauch@gmx.fr',1,'','','stephgauch@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:28',0,'2018-07-31 17:07:10'),(26635251,'angie_teddy0111@yahoo.com',1,'','','angie_teddy0111@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:28',0,'2018-07-31 17:07:10'),(26635252,'emilol0311@yahoo.fr',1,'','','emilol0311@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:29',0,'2018-07-31 17:07:10'),(26635253,'petillante1605@hotmail.fr',1,'','','petillante1605@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:29',0,'2018-07-31 17:07:10'),(26635254,'jacqueline.aude@hotmail.fr',1,'','','jacqueline.aude@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:29',0,'2018-07-31 17:07:10'),(26635255,'a.nougier99@gmail.com',1,'','','a.nougier99@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:29',0,'2018-07-31 17:07:10'),(26635256,'celineconcours@orange.fr',1,'','','celineconcours@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:30',0,'2018-07-31 17:07:10'),(26635257,'jacky.masse@libertysurf.fr',1,'','','jacky.masse@libertysurf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:30',0,'2018-07-31 17:07:10'),(26635258,'mf.autier.66@gmail.com',1,'','','mf.autier.66@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:30',0,'2018-07-31 17:07:10'),(26635259,'annemarieb@akeonet.com',1,'','','annemarieb@akeonet.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:30',0,'2018-07-31 17:07:10'),(26635260,'jagentj@gmail.com',1,'','','jagentj@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:30',0,'2018-07-31 17:07:10'),(26635261,'nathnagui@hotmail.fr',1,'','','nathnagui@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:31',0,'2018-07-31 17:07:10'),(26635262,'almatter@estvideo.fr',1,'','','almatter@estvideo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:31',0,'2018-07-31 17:07:10'),(26635263,'nathalie.pillon03@gmail.com',1,'','','nathalie.pillon03@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:31',0,'2018-07-31 17:07:10'),(26635264,'dadottecrevette@gmail.com',1,'','','dadottecrevette@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:32',0,'2018-07-31 17:07:10'),(26635265,'nbrunetmanquat@yahoo.fr',1,'','','nbrunetmanquat@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:32',0,'2018-07-31 17:07:10'),(26635266,'ptitboude41@hotmail.com',1,'','','ptitboude41@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:32',0,'2018-07-31 17:07:10'),(26635267,'claire.massemin@gmail.com',1,'','','claire.massemin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:32',0,'2018-07-31 17:07:10'),(26635268,'mehayedavid@wanadoo.fr',1,'','','mehayedavid@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:33',0,'2018-07-31 17:07:10'),(26635269,'cottin.sabine@neuf.fr',1,'','','cottin.sabine@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:33',0,'2018-07-31 17:07:10'),(26635270,'janu62@sfr.fr',1,'','','janu62@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:33',0,'2018-07-31 17:07:10'),(26635271,'mcp7710@gmail.com',1,'','','mcp7710@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:33',0,'2018-07-31 17:07:10'),(26635272,'boudineuse@gmail.com',1,'','','boudineuse@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:34',0,'2018-07-31 17:07:10'),(26635273,'greg26483@gmail.com',1,'','','greg26483@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:34',0,'2018-07-31 17:07:10'),(26635274,'christelle.bainville@orange.fr',1,'','','christelle.bainville@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:34',0,'2018-07-31 17:07:10'),(26635275,'milar@orange.fr',1,'','','milar@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:34',0,'2018-07-31 17:07:10'),(26635276,'triandalous@outlook.fr',1,'','','triandalous@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:35',0,'2018-07-31 17:07:10'),(26635277,'cecile02051981@hotmail.fr',1,'','','cecile02051981@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:35',0,'2018-07-31 17:07:10'),(26635278,'jlch37@gmail.com',1,'','','jlch37@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:35',0,'2018-07-31 17:07:10'),(26635279,'martinedoufil@outlook.fr',1,'','','martinedoufil@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:35',0,'2018-07-31 17:07:10'),(26635280,'mitch42@hotmail.fr',1,'','','mitch42@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:36',0,'2018-07-31 17:07:10'),(26635281,'elodie.sigu@sfr.fr',1,'','','elodie.sigu@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:36',0,'2018-07-31 17:07:10'),(26635282,'galasso.andre@wanadoo.fr',1,'','','galasso.andre@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:36',0,'2018-07-31 17:07:10'),(26635283,'upyss10@laposte.net',1,'','','upyss10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:36',0,'2018-07-31 17:07:10'),(26635284,'vabu120@gmail.com',1,'','','vabu120@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:37',0,'2018-07-31 17:07:10'),(26635285,'lyndasg@yahoo.fr',1,'','','lyndasg@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:37',0,'2018-07-31 17:07:10'),(26635286,'mkelll50440@gmail.com',1,'','','mkelll50440@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:37',0,'2018-07-31 17:07:10'),(26635287,'marie.batisse91@hotmail.com',1,'','','marie.batisse91@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:37',0,'2018-07-31 17:07:10'),(26635288,'irof15@laposte.net',1,'','','irof15@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:38',0,'2018-07-31 17:07:10'),(26635289,'belange2ab@gmail.com',1,'','','belange2ab@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:38',0,'2018-07-31 17:07:10'),(26635290,'steph2juvincourt@orange.fr',1,'','','steph2juvincourt@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:38',0,'2018-07-31 17:07:10'),(26635291,'bernard.fabien88@orange.fr',1,'','','bernard.fabien88@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:38',0,'2018-07-31 17:07:10'),(26635292,'nied14@laposte.net',1,'','','nied14@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:39',0,'2018-07-31 17:07:10'),(26635293,'gisele.clause@laposte.net',1,'','','gisele.clause@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:39',0,'2018-07-31 17:07:10'),(26635294,'michelle.cottart@yahoo.fr',1,'','','michelle.cottart@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:39',0,'2018-07-31 17:07:10'),(26635295,'rodika10@laposte.net',1,'','','rodika10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:40',0,'2018-07-31 17:07:10'),(26635296,'marieccccmarie@aol.fr',1,'','','marieccccmarie@aol.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:40',0,'2018-07-31 17:07:10'),(26635297,'caron.cindy@hotmail.fr',1,'','','caron.cindy@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:40',0,'2018-07-31 17:07:10'),(26635298,'missfeemain@gmail.com',1,'','','missfeemain@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:40',0,'2018-07-31 17:07:10'),(26635299,'sylvieguemy@aol.com',1,'','','sylvieguemy@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:40',0,'2018-07-31 17:07:10'),(26635300,'lepol10@laposte.net',1,'','','lepol10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:41',0,'2018-07-31 17:07:10'),(26635301,'dequidtbeatrice@hotmail.fr',1,'','','dequidtbeatrice@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:41',0,'2018-07-31 17:07:10'),(26635302,'atol15@laposte.net',1,'','','atol15@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:41',0,'2018-07-31 17:07:10'),(26635303,'mygibert@neuf.fr',1,'','','mygibert@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:42',0,'2018-07-31 17:07:10'),(26635304,'davorele@gmail.com',1,'','','davorele@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:42',0,'2018-07-31 17:07:10'),(26635305,'lbinet37@yahoo.fr',1,'','','lbinet37@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:42',0,'2018-07-31 17:07:10'),(26635306,'rabet.oceane@gmail.com',1,'','','rabet.oceane@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:42',0,'2018-07-31 17:07:10'),(26635307,'aurelielucas@laposte.net',1,'','','aurelielucas@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:43',0,'2018-07-31 17:07:10'),(26635308,'vjea69@gmail.com',1,'','','vjea69@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:43',0,'2018-07-31 17:07:10'),(26635309,'lolotte260197@hotmail.fr',1,'','','lolotte260197@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:43',0,'2018-07-31 17:07:10'),(26635310,'clematite1961@laposte.net',1,'','','clematite1961@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:43',0,'2018-07-31 17:07:10'),(26635311,'tiffany.comma@laposte.net',1,'','','tiffany.comma@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:44',0,'2018-07-31 17:07:10'),(26635312,'LOLITA.COEUGNEIT@SFR.FR',1,'','','LOLITA.COEUGNEIT@SFR.FR','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:44',0,'2018-07-31 17:07:10'),(26635313,'evelyne.quemener@wanadoo.fr',1,'','','evelyne.quemener@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:44',0,'2018-07-31 17:07:10'),(26635314,'melanie59610@hotmail.fr',1,'','','melanie59610@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:44',0,'2018-07-31 17:07:10'),(26635315,'gydok10@laposte.net',1,'','','gydok10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:45',0,'2018-07-31 17:07:10'),(26635316,'sylvie.leroux22@sfr.fr',1,'','','sylvie.leroux22@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:45',0,'2018-07-31 17:07:10'),(26635317,'nj1999@sfr.fr',1,'','','nj1999@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:45',0,'2018-07-31 17:07:10'),(26635318,'jesssy57730@gmail.com',1,'','','jesssy57730@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:45',0,'2018-07-31 17:07:10'),(26635319,'trelos24@outlook.fr',1,'','','trelos24@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:46',0,'2018-07-31 17:07:10'),(26635320,'sam5084@hotmail.com',1,'','','sam5084@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:46',0,'2018-07-31 17:07:10'),(26635321,'ikol44@laposte.net',1,'','','ikol44@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:46',0,'2018-07-31 17:07:10'),(26635322,'colette.hazera@sfr.fr',1,'','','colette.hazera@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:46',0,'2018-07-31 17:07:10'),(26635323,'joprz63@gmail.com',1,'','','joprz63@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:47',0,'2018-07-31 17:07:10'),(26635324,'martuchette@yahoo.fr',1,'','','martuchette@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:47',0,'2018-07-31 17:07:10'),(26635325,'boujibar.sarah@gmail.com',1,'','','boujibar.sarah@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:47',0,'2018-07-31 17:07:10'),(26635326,'flayac2@gmail.com',1,'','','flayac2@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:47',0,'2018-07-31 17:07:10'),(26635327,'jackyregine@outlook.com',1,'','','jackyregine@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:48',0,'2018-07-31 17:07:10'),(26635328,'cathhou@gmail.com',1,'','','cathhou@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:48',0,'2018-07-31 17:07:10'),(26635329,'haran.jacky@wanadoo.fr',1,'','','haran.jacky@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:48',0,'2018-07-31 17:07:10'),(26635330,'soleifein@live.fr',1,'','','soleifein@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:48',0,'2018-07-31 17:07:10'),(26635331,'delagarde.jacques@orange.fr',1,'','','delagarde.jacques@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:49',0,'2018-07-31 17:07:10'),(26635332,'claframar@aol.com',1,'','','claframar@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:49',0,'2018-07-31 17:07:10'),(26635333,'berger.pascal17@gmail.com',1,'','','berger.pascal17@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:49',0,'2018-07-31 17:07:10'),(26635334,'laurent.cymerman@orange.fr',1,'','','laurent.cymerman@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:49',0,'2018-07-31 17:07:10'),(26635335,'guy.huneau@yahoo.fr',1,'','','guy.huneau@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:49',0,'2018-07-31 17:07:10'),(26635336,'didouetqueenie@live.fr',1,'','','didouetqueenie@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:50',0,'2018-07-31 17:07:10'),(26635337,'fabien.bourdillon@gmx.fr',1,'','','fabien.bourdillon@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:50',0,'2018-07-31 17:07:10'),(26635338,'nompre-nom@hotmail.fr',1,'','','nompre-nom@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:50',0,'2018-07-31 17:07:10'),(26635339,'gilbert.hoh@laposte.net',1,'','','gilbert.hoh@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:50',0,'2018-07-31 17:07:10'),(26635340,'christelle.favre12@orange.fr',1,'','','christelle.favre12@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:51',0,'2018-07-31 17:07:10'),(26635341,'mtn.sirius@gmail.com',1,'','','mtn.sirius@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:51',0,'2018-07-31 17:07:10'),(26635342,'hubjojocedjon68@live.fr',1,'','','hubjojocedjon68@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:51',0,'2018-07-31 17:07:10'),(26635343,'annecoppik@sfr.fr',1,'','','annecoppik@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:52',0,'2018-07-31 17:07:10'),(26635344,'timael76@hotmail.fr',1,'','','timael76@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:52',0,'2018-07-31 17:07:10'),(26635345,'annefaugeres@free.fr',1,'','','annefaugeres@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:52',0,'2018-07-31 17:07:10'),(26635346,'shino3335@laposte.net',1,'','','shino3335@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:52',0,'2018-07-31 17:07:10'),(26635347,'d.lippmann@orange.fr',1,'','','d.lippmann@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:53',0,'2018-07-31 17:07:10'),(26635348,'audrey45999@hotmail.com',1,'','','audrey45999@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:53',0,'2018-07-31 17:07:10'),(26635349,'free1911@free.fr',1,'','','free1911@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:53',0,'2018-07-31 17:07:10'),(26635350,'robindesbois3152@gmail.com',1,'','','robindesbois3152@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:54',0,'2018-07-31 17:07:10'),(26635351,'tofox10@laposte.net',1,'','','tofox10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:54',0,'2018-07-31 17:07:10'),(26635352,'vega159@orange.fr',1,'','','vega159@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:54',0,'2018-07-31 17:07:10'),(26635353,'doublemetre83@aol.com',1,'','','doublemetre83@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:55',0,'2018-07-31 17:07:10'),(26635354,'jc.fumat@free.fr',1,'','','jc.fumat@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:55',0,'2018-07-31 17:07:10'),(26635355,'engel-s@live.fr',1,'','','engel-s@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:55',0,'2018-07-31 17:07:10'),(26635356,'genevievevietnam@orange.fr',1,'','','genevievevietnam@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:55',0,'2018-07-31 17:07:10'),(26635357,'pierre.renaut0538@free.fr',1,'','','pierre.renaut0538@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:55',0,'2018-07-31 17:07:10'),(26635358,'upite10@laposte.net',1,'','','upite10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:56',0,'2018-07-31 17:07:10'),(26635359,'ggallet81@gmail.com',1,'','','ggallet81@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:56',0,'2018-07-31 17:07:10'),(26635360,'famille.komnenovic@laposte.net',1,'','','famille.komnenovic@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:56',0,'2018-07-31 17:07:10'),(26635361,'famille.diard@free.fr',1,'','','famille.diard@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:56',0,'2018-07-31 17:07:10'),(26635362,'unifluence@gmail.com',1,'','','unifluence@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:57',0,'2018-07-31 17:07:10'),(26635363,'margoul@free.fr',1,'','','margoul@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:57',0,'2018-07-31 17:07:10'),(26635364,'sabrina.rozmiarek@gmail.com',1,'','','sabrina.rozmiarek@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:57',0,'2018-07-31 17:07:10'),(26635365,'beatrice.lavande@gmail.com',1,'','','beatrice.lavande@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:58',0,'2018-07-31 17:07:10'),(26635366,'sep10@laposte.net',1,'','','sep10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:58',0,'2018-07-31 17:07:10'),(26635367,'legrand.nicole0087@orange.fr',1,'','','legrand.nicole0087@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:58',0,'2018-07-31 17:07:10'),(26635368,'patrick.beiser@laposte.net',1,'','','patrick.beiser@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:58',0,'2018-07-31 17:07:10'),(26635369,'02nicohind@gmail.com',1,'','','02nicohind@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:59',0,'2018-07-31 17:07:10'),(26635370,'bestjeje@gmail.com',1,'','','bestjeje@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:59',0,'2018-07-31 17:07:10'),(26635371,'audrey.benyahia@orange.fr',1,'','','audrey.benyahia@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:59',0,'2018-07-31 17:07:10'),(26635372,'audreyreydey81@yahoo.fr',1,'','','audreyreydey81@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:27:59',0,'2018-07-31 17:07:10'),(26635373,'patrick.deval@wanadoo.fr',1,'','','patrick.deval@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:00',0,'2018-07-31 17:07:10'),(26635374,'maidivatelin@gmail.com',1,'','','maidivatelin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:00',0,'2018-07-31 17:07:10'),(26635375,'lerrog62@gmail.com',1,'','','lerrog62@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:00',0,'2018-07-31 17:07:10'),(26635376,'blancheval@hotmail.fr',1,'','','blancheval@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:00',0,'2018-07-31 17:07:10'),(26635377,'al.brunet@wanadoo.fr',1,'','','al.brunet@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:00',0,'2018-07-31 17:07:10'),(26635378,'moonrigal@yahoo.fr',1,'','','moonrigal@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:01',0,'2018-07-31 17:07:10'),(26635379,'babydollie59@free.fr',1,'','','babydollie59@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:01',0,'2018-07-31 17:07:10'),(26635380,'elynad55@sfr.fr',1,'','','elynad55@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:01',0,'2018-07-31 17:07:10'),(26635381,'yves.inconstant@club-internet.fr',1,'','','yves.inconstant@club-internet.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:01',0,'2018-07-31 17:07:10'),(26635382,'mimosabzh@yahoo.fr',1,'','','mimosabzh@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:02',0,'2018-07-31 17:07:10'),(26635383,'famisok@ymail.com',1,'','','famisok@ymail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:02',0,'2018-07-31 17:07:10'),(26635384,'anne.leder@free.fr',1,'','','anne.leder@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:02',0,'2018-07-31 17:07:10'),(26635385,'cdn31@laposte.net',1,'','','cdn31@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:03',0,'2018-07-31 17:07:10'),(26635386,'laurentvert@free.fr',1,'','','laurentvert@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:03',0,'2018-07-31 17:07:10'),(26635387,'mayhelen2609@gmail.com',1,'','','mayhelen2609@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:03',0,'2018-07-31 17:07:10'),(26635388,'milynousse@hotmail.fr',1,'','','milynousse@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:03',0,'2018-07-31 17:07:10'),(26635389,'moutiyann@orange.fr',1,'','','moutiyann@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:04',0,'2018-07-31 17:07:10'),(26635390,'Jeanmandin@gmail.com',1,'','','Jeanmandin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:04',0,'2018-07-31 17:07:10'),(26635391,'colombette30@hotmail.fr',1,'','','colombette30@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:04',0,'2018-07-31 17:07:10'),(26635392,'line.panzou@free.fr',1,'','','line.panzou@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:05',0,'2018-07-31 17:07:10'),(26635393,'nicohind63@gmail.com',1,'','','nicohind63@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:05',0,'2018-07-31 17:07:10'),(26635394,'lucray@free.fr',1,'','','lucray@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:05',0,'2018-07-31 17:07:10'),(26635395,'bernard.gellez@dbmail.com',1,'','','bernard.gellez@dbmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:05',0,'2018-07-31 17:07:10'),(26635396,'daniel.mayaux@orange.fr',1,'','','daniel.mayaux@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:06',0,'2018-07-31 17:07:10'),(26635397,'elodielaurent54@gmail.com',1,'','','elodielaurent54@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:06',0,'2018-07-31 17:07:10'),(26635398,'virginielecomte@laposte.net',1,'','','virginielecomte@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:06',0,'2018-07-31 17:07:10'),(26635399,'loulouplouf@gmail.com',1,'','','loulouplouf@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:06',0,'2018-07-31 17:07:10'),(26635400,'claire.libot@yahoo.fr',1,'','','claire.libot@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:06',0,'2018-07-31 17:07:10'),(26635401,'jean-claudemaout@orange.fr',1,'','','jean-claudemaout@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:07',0,'2018-07-31 17:07:10'),(26635402,'ubolt10@laposte.net',1,'','','ubolt10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:07',0,'2018-07-31 17:07:10'),(26635403,'andre-duynslaeger@bbox.fr',1,'','','andre-duynslaeger@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:07',0,'2018-07-31 17:07:10'),(26635404,'gdgouss@gmail.com',1,'','','gdgouss@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:08',0,'2018-07-31 17:07:10'),(26635405,'hinnico39@yahoo.fr',1,'','','hinnico39@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:08',0,'2018-07-31 17:07:10'),(26635406,'nora.nouboua@neuf.fr',1,'','','nora.nouboua@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:08',0,'2018-07-31 17:07:10'),(26635407,'dermag@sfr.fr',1,'','','dermag@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:08',0,'2018-07-31 17:07:10'),(26635408,'chipie14600@outlook.fr',1,'','','chipie14600@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:09',0,'2018-07-31 17:07:10'),(26635409,'hubo10@laposte.net',1,'','','hubo10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:09',0,'2018-07-31 17:07:10'),(26635410,'pxnadine@gmail.com',1,'','','pxnadine@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:09',0,'2018-07-31 17:07:10'),(26635411,'lilikissme@hotmail.fr',1,'','','lilikissme@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:10',0,'2018-07-31 17:07:10'),(26635412,'sandrafusillier@laposte.net',1,'','','sandrafusillier@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:10',0,'2018-07-31 17:07:10'),(26635413,'latrabatiere@wanadoo.fr',1,'','','latrabatiere@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:11',0,'2018-07-31 17:07:10'),(26635414,'frederic.monjou@sfr.fr',1,'','','frederic.monjou@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:11',0,'2018-07-31 17:07:10'),(26635415,'laurent.depaye@wanadoo.fr',1,'','','laurent.depaye@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:11',0,'2018-07-31 17:07:10'),(26635416,'restignac@free.fr',1,'','','restignac@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:12',0,'2018-07-31 17:07:10'),(26635417,'karinehouard@yahoo.fr',1,'','','karinehouard@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:12',0,'2018-07-31 17:07:10'),(26635418,'ma.marbach@laposte.net',1,'','','ma.marbach@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:13',0,'2018-07-31 17:07:10'),(26635419,'davcardor@hotmail.fr',1,'','','davcardor@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:13',0,'2018-07-31 17:07:10'),(26635420,'heuze.veronique@bbox.fr',1,'','','heuze.veronique@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:13',0,'2018-07-31 17:07:10'),(26635421,'michel.guiheneuf@yahoo.fr',1,'','','michel.guiheneuf@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:14',0,'2018-07-31 17:07:10'),(26635422,'etondepatou@yahoo.fr',1,'','','etondepatou@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:14',0,'2018-07-31 17:07:10'),(26635423,'maryou2@hotmail.fr',1,'','','maryou2@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:14',0,'2018-07-31 17:07:10'),(26635424,'celine95100@laposte.net',1,'','','celine95100@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:14',0,'2018-07-31 17:07:10'),(26635425,'veronique.moulin31@hotmail.com',1,'','','veronique.moulin31@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:15',0,'2018-07-31 17:07:10'),(26635426,'trochard92@free.fr',1,'','','trochard92@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:15',0,'2018-07-31 17:07:10'),(26635427,'pathache62@aol.com',1,'','','pathache62@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:15',0,'2018-07-31 17:07:10'),(26635428,'yolande.gillet@hotmail.com',1,'','','yolande.gillet@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:15',0,'2018-07-31 17:07:10'),(26635429,'doudou.tahiti@neuf.fr',1,'','','doudou.tahiti@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:16',0,'2018-07-31 17:07:10'),(26635430,'jeanluc.bourgoin@bbox.fr',1,'','','jeanluc.bourgoin@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:16',0,'2018-07-31 17:07:10'),(26635431,'gregggory73000@hotmail.com',1,'','','gregggory73000@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:16',0,'2018-07-31 17:07:10'),(26635432,'jennyfermary2008@gmail.com',1,'','','jennyfermary2008@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:16',0,'2018-07-31 17:07:10'),(26635433,'forum.mcs@neuf.fr',1,'','','forum.mcs@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:17',0,'2018-07-31 17:07:10'),(26635434,'jdr76@gmx.fr',1,'','','jdr76@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:17',0,'2018-07-31 17:07:10'),(26635435,'patagnes69@gmail.com',1,'','','patagnes69@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:17',0,'2018-07-31 17:07:10'),(26635436,'rouge.renee@orange.fr',1,'','','rouge.renee@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:17',0,'2018-07-31 17:07:10'),(26635437,'melisse-jean-francois.max@orange.fr',1,'','','melisse-jean-francois.max@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:18',0,'2018-07-31 17:07:10'),(26635438,'merveillej84@gmail.com',1,'','','merveillej84@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:18',0,'2018-07-31 17:07:10'),(26635439,'marianibruno10@yahoo.com',1,'','','marianibruno10@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:18',0,'2018-07-31 17:07:10'),(26635440,'ynu10@laposte.net',1,'','','ynu10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:18',0,'2018-07-31 17:07:10'),(26635441,'v62.annie@gmail.com',1,'','','v62.annie@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:19',0,'2018-07-31 17:07:10'),(26635442,'kaoutar.catlab@gmail.com',1,'','','kaoutar.catlab@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:19',0,'2018-07-31 17:07:10'),(26635443,'monique.dekytspotter@me.com',1,'','','monique.dekytspotter@me.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:19',0,'2018-07-31 17:07:10'),(26635444,'didiermarc.loiseau@wanadoo.fr',1,'','','didiermarc.loiseau@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:19',0,'2018-07-31 17:07:10'),(26635445,'kies29@yahoo.fr',1,'','','kies29@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:20',0,'2018-07-31 17:07:10'),(26635446,'serieyssol.philippe@net-c.com',1,'','','serieyssol.philippe@net-c.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:20',0,'2018-07-31 17:07:10'),(26635447,'jutyr@netcourrier.com',1,'','','jutyr@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:20',0,'2018-07-31 17:07:10'),(26635448,'verofs@orange.fr',1,'','','verofs@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:20',0,'2018-07-31 17:07:10'),(26635449,'cotat.joel@orange.fr',1,'','','cotat.joel@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:21',0,'2018-07-31 17:07:10'),(26635450,'utim10@laposte.net',1,'','','utim10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:21',0,'2018-07-31 17:07:10'),(26635451,'dominiquejouanne@sfr.fr',1,'','','dominiquejouanne@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:21',0,'2018-07-31 17:07:10'),(26635452,'bonaventure2.a@orange.fr',1,'','','bonaventure2.a@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:21',0,'2018-07-31 17:07:10'),(26635453,'soso0309@live.fr',1,'','','soso0309@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:22',0,'2018-07-31 17:07:10'),(26635454,'brigitte-jeanne@outlook.fr',1,'','','brigitte-jeanne@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:22',0,'2018-07-31 17:07:10'),(26635455,'pecar12340@gmail.com',1,'','','pecar12340@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:22',0,'2018-07-31 17:07:10'),(26635456,'sabinelyder02@outlook.com',1,'','','sabinelyder02@outlook.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:22',0,'2018-07-31 17:07:10'),(26635457,'gerard.francis@outlook.fr',1,'','','gerard.francis@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:22',0,'2018-07-31 17:07:10'),(26635458,'patrickcallon@bbox.fr',1,'','','patrickcallon@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:23',0,'2018-07-31 17:07:10'),(26635459,'pasdechance44780@gmail.com',1,'','','pasdechance44780@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:23',0,'2018-07-31 17:07:10'),(26635460,'bibinouuu@gmail.com',1,'','','bibinouuu@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:23',0,'2018-07-31 17:07:10'),(26635461,'simplydeep75@hotmail.com',1,'','','simplydeep75@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:23',0,'2018-07-31 17:07:10'),(26635462,'gerce@netcourrier.com',1,'','','gerce@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:24',0,'2018-07-31 17:07:10'),(26635463,'schmidt.helene@cegetel.net',1,'','','schmidt.helene@cegetel.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:24',0,'2018-07-31 17:07:10'),(26635464,'theolucas02@yahoo.fr',1,'','','theolucas02@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:24',0,'2018-07-31 17:07:10'),(26635465,'sonia.viellard@live.fr',1,'','','sonia.viellard@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:24',0,'2018-07-31 17:07:10'),(26635466,'larcie_caroline@yahoo.fr',1,'','','larcie_caroline@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:25',0,'2018-07-31 17:07:10'),(26635467,'antchouezaouilzeinab14@gmail.com',1,'','','antchouezaouilzeinab14@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:25',0,'2018-07-31 17:07:10'),(26635468,'turvil415@gmail.com',1,'','','turvil415@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:25',0,'2018-07-31 17:07:10'),(26635469,'guada60@hotmail.fr',1,'','','guada60@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:25',0,'2018-07-31 17:07:10'),(26635470,'yvette.dumaspasquet@orange.fr',1,'','','yvette.dumaspasquet@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:26',0,'2018-07-31 17:07:10'),(26635471,'porheljpp@aol.com',1,'','','porheljpp@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:26',0,'2018-07-31 17:07:10'),(26635472,'celinoube@gmail.com',1,'','','celinoube@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:26',0,'2018-07-31 17:07:10'),(26635473,'geoand@hotmail.fr',1,'','','geoand@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:26',0,'2018-07-31 17:07:10'),(26635474,'ocjama.mama@yopmail.com',1,'','','ocjama.mama@yopmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:27',0,'2018-07-31 17:07:10'),(26635475,'beaugier.laetitia@orange.fr',1,'','','beaugier.laetitia@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:27',0,'2018-07-31 17:07:10'),(26635476,'boby6killer@live.fr',1,'','','boby6killer@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:27',0,'2018-07-31 17:07:10'),(26635477,'erdez@netcourrier.com',1,'','','erdez@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:27',0,'2018-07-31 17:07:10'),(26635478,'fezai@netcourrier.com',1,'','','fezai@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:28',0,'2018-07-31 17:07:10'),(26635479,'pk.burgos@laposte.net',1,'','','pk.burgos@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:28',0,'2018-07-31 17:07:10'),(26635480,'gurt.delf@yopmail.com',1,'','','gurt.delf@yopmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:28',0,'2018-07-31 17:07:10'),(26635481,'lolo_felix@hotmail.com',1,'','','lolo_felix@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:28',0,'2018-07-31 17:07:10'),(26635482,'sempere.corinne@orange.fr',1,'','','sempere.corinne@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:29',0,'2018-07-31 17:07:10'),(26635483,'mllesoso22@hotmail.com',1,'','','mllesoso22@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:29',0,'2018-07-31 17:07:10'),(26635484,'nathalieristerucci@yahoo.fr',1,'','','nathalieristerucci@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:29',0,'2018-07-31 17:07:10'),(26635485,'hinnico1@gmail.com',1,'','','hinnico1@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:29',0,'2018-07-31 17:07:10'),(26635486,'o2nico@live.fr',1,'','','o2nico@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:30',0,'2018-07-31 17:07:10'),(26635487,'zasou80@gmail.com',1,'','','zasou80@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:30',0,'2018-07-31 17:07:10'),(26635488,'dasece@netcourrier.com',1,'','','dasece@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:30',0,'2018-07-31 17:07:10'),(26635489,'jeremyjulien75000@gmail.com',1,'','','jeremyjulien75000@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:30',0,'2018-07-31 17:07:10'),(26635490,'jukibe10@laposte.net',1,'','','jukibe10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:31',0,'2018-07-31 17:07:10'),(26635491,'bris.sou@hotmail.fr',1,'','','bris.sou@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:31',0,'2018-07-31 17:07:10'),(26635492,'msaragea@yahoo.com',1,'','','msaragea@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:31',0,'2018-07-31 17:07:10'),(26635493,'p.autier.62@gmail.com',1,'','','p.autier.62@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:31',0,'2018-07-31 17:07:10'),(26635494,'k.julet@orange.fr',1,'','','k.julet@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:32',0,'2018-07-31 17:07:10'),(26635495,'gagne.ocjama@yopmail.com',1,'','','gagne.ocjama@yopmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:32',0,'2018-07-31 17:07:10'),(26635496,'claustrespatrick@orange.fr',1,'','','claustrespatrick@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:32',0,'2018-07-31 17:07:10'),(26635497,'missdodo_vip@hotmail.fr',1,'','','missdodo_vip@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:32',0,'2018-07-31 17:07:10'),(26635498,'n.r78@hotmail.fr',1,'','','n.r78@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:32',0,'2018-07-31 17:07:10'),(26635499,'clermontguillaume@hotmail.fr',1,'','','clermontguillaume@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:33',0,'2018-07-31 17:07:10'),(26635500,'murielchevallier@free.fr',1,'','','murielchevallier@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:33',0,'2018-07-31 17:07:10'),(26635501,'yasdicat@orange.fr',1,'','','yasdicat@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:33',0,'2018-07-31 17:07:10'),(26635502,'elvellyn99@yahoo.fr',1,'','','elvellyn99@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:33',0,'2018-07-31 17:07:10'),(26635503,'sailormoon94@hotmail.fr',1,'','','sailormoon94@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:34',0,'2018-07-31 17:07:10'),(26635504,'jean-luc.thouron@wanadoo.fr',1,'','','jean-luc.thouron@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:34',0,'2018-07-31 17:07:10'),(26635505,'redece@netcourrier.com',1,'','','redece@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:34',0,'2018-07-31 17:07:10'),(26635506,'julie.ladune@orange.fr',1,'','','julie.ladune@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:34',0,'2018-07-31 17:07:10'),(26635507,'momobrun@wanadoo.fr',1,'','','momobrun@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:35',0,'2018-07-31 17:07:10'),(26635508,'vedre@netcourrier.com',1,'','','vedre@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:35',0,'2018-07-31 17:07:10'),(26635509,'irto15@laposte.net',1,'','','irto15@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:35',0,'2018-07-31 17:07:10'),(26635510,'tieryel@yahoo.fr',1,'','','tieryel@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:35',0,'2018-07-31 17:07:10'),(26635511,'basmadjianb@aol.com',1,'','','basmadjianb@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:36',0,'2018-07-31 17:07:10'),(26635512,'shecool@live.fr',1,'','','shecool@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:36',0,'2018-07-31 17:07:10'),(26635513,'josiane.mercier69@gmail.com',1,'','','josiane.mercier69@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:36',0,'2018-07-31 17:07:10'),(26635514,'bea971@orange.fr',1,'','','bea971@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:36',0,'2018-07-31 17:07:10'),(26635515,'laetitia.sylva@neuf.fr',1,'','','laetitia.sylva@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:37',0,'2018-07-31 17:07:10'),(26635516,'boennec.delphine@laposte.net',1,'','','boennec.delphine@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:37',0,'2018-07-31 17:07:10'),(26635517,'missiconc@hotmail.com',1,'','','missiconc@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:37',0,'2018-07-31 17:07:10'),(26635518,'vezas@netcourrier.com',1,'','','vezas@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:38',0,'2018-07-31 17:07:10'),(26635519,'mfservais@yahoo.fr',1,'','','mfservais@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:38',0,'2018-07-31 17:07:10'),(26635520,'jm.remy@yahoo.fr',1,'','','jm.remy@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:38',0,'2018-07-31 17:07:10'),(26635521,'soley21000@gmail.com',1,'','','soley21000@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:38',0,'2018-07-31 17:07:10'),(26635522,'ceas@netcourrier.com',1,'','','ceas@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:39',0,'2018-07-31 17:07:10'),(26635523,'philaddy1404@gmail.com',1,'','','philaddy1404@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:39',0,'2018-07-31 17:07:10'),(26635524,'perlesetcoquillages@netc.fr',1,'','','perlesetcoquillages@netc.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:39',0,'2018-07-31 17:07:10'),(26635525,'faer@netcourrier.com',1,'','','faer@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:39',0,'2018-07-31 17:07:10'),(26635526,'tubyk10@laposte.net',1,'','','tubyk10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:40',0,'2018-07-31 17:07:10'),(26635527,'julienmarillat84@hotmail.fr',1,'','','julienmarillat84@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:40',0,'2018-07-31 17:07:10'),(26635528,'hinnico2@gmail.com',1,'','','hinnico2@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:40',0,'2018-07-31 17:07:10'),(26635529,'georges.fouchac@wanadoo.fr',1,'','','georges.fouchac@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:40',0,'2018-07-31 17:07:10'),(26635530,'riso10@laposte.net',1,'','','riso10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:41',0,'2018-07-31 17:07:10'),(26635531,'de-schepper.marc@orange.fr',1,'','','de-schepper.marc@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:41',0,'2018-07-31 17:07:10'),(26635532,'perez.marjoriie@gmail.com',1,'','','perez.marjoriie@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:41',0,'2018-07-31 17:07:10'),(26635533,'alain.bonnaud@educagri.fr',1,'','','alain.bonnaud@educagri.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:41',0,'2018-07-31 17:07:10'),(26635534,'treza@netcourrier.com',1,'','','treza@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:42',0,'2018-07-31 17:07:10'),(26635535,'alice.jullian44@gmail.com',1,'','','alice.jullian44@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:42',0,'2018-07-31 17:07:10'),(26635536,'annyyves@gmail.com',1,'','','annyyves@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:42',0,'2018-07-31 17:07:10'),(26635537,'mcs53@laposte.net',1,'','','mcs53@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:42',0,'2018-07-31 17:07:10'),(26635538,'mfsaintviance@orange.fr',1,'','','mfsaintviance@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:42',0,'2018-07-31 17:07:10'),(26635539,'gezas@netcourrier.com',1,'','','gezas@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:43',0,'2018-07-31 17:07:10'),(26635540,'soflo1023@yahoo.fr',1,'','','soflo1023@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:43',0,'2018-07-31 17:07:10'),(26635541,'yaa.quesnel@free.fr',1,'','','yaa.quesnel@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:43',0,'2018-07-31 17:07:10'),(26635542,'tiope@netcourrier.com',1,'','','tiope@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:43',0,'2018-07-31 17:07:10'),(26635543,'tender-moments@hotmail.fr',1,'','','tender-moments@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:44',0,'2018-07-31 17:07:10'),(26635544,'loiter@netcourrier.com',1,'','','loiter@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:44',0,'2018-07-31 17:07:10'),(26635545,'toilut@netcourrier.com',1,'','','toilut@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:44',0,'2018-07-31 17:07:10'),(26635546,'urifo10@laposte.net',1,'','','urifo10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:44',0,'2018-07-31 17:07:10'),(26635547,'aurore.801@live.fr',1,'','','aurore.801@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:44',0,'2018-07-31 17:07:10'),(26635548,'estebanmatheo88@gmail.com',1,'','','estebanmatheo88@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:45',0,'2018-07-31 17:07:10'),(26635549,'philippedut@free.fr',1,'','','philippedut@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:45',0,'2018-07-31 17:07:10'),(26635550,'bikoko77@hotmail.com',1,'','','bikoko77@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:45',0,'2018-07-31 17:07:10'),(26635551,'delf.franck@laposte.net',1,'','','delf.franck@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:46',0,'2018-07-31 17:07:10'),(26635552,'lylgal@orange.fr',1,'','','lylgal@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:46',0,'2018-07-31 17:07:10'),(26635553,'elyse50@free.fr',1,'','','elyse50@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:46',0,'2018-07-31 17:07:10'),(26635554,'jojo.boron@wanadoo.fr',1,'','','jojo.boron@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:46',0,'2018-07-31 17:07:10'),(26635555,'matt35133@gmail.com',1,'','','matt35133@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:47',0,'2018-07-31 17:07:10'),(26635556,'ponia54230@hotmail.fr',1,'','','ponia54230@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:47',0,'2018-07-31 17:07:10'),(26635557,'kiso10@laposte.net',1,'','','kiso10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:47',0,'2018-07-31 17:07:10'),(26635558,'annieperraudin@aol.com',1,'','','annieperraudin@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:47',0,'2018-07-31 17:07:10'),(26635559,'sauve.jeanlouis@orange.fr',1,'','','sauve.jeanlouis@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:48',0,'2018-07-31 17:07:10'),(26635560,'brigitte.fauchey@gmail.com',1,'','','brigitte.fauchey@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:48',0,'2018-07-31 17:07:10'),(26635561,'michbonvin@gmail.com',1,'','','michbonvin@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:48',0,'2018-07-31 17:07:10'),(26635562,'irenlova2113@gmail.com',1,'','','irenlova2113@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:48',0,'2018-07-31 17:07:10'),(26635563,'daniel.yams@orange.fr',1,'','','daniel.yams@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:48',0,'2018-07-31 17:07:10'),(26635564,'jeankz@wanadoo.fr',1,'','','jeankz@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:49',0,'2018-07-31 17:07:10'),(26635565,'alex.delinot@gmail.com',1,'','','alex.delinot@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:49',0,'2018-07-31 17:07:10'),(26635566,'melissagrunberg.83520@gmail.com',1,'','','melissagrunberg.83520@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:49',0,'2018-07-31 17:07:10'),(26635567,'alexandre.fusilli@wanadoo.fr',1,'','','alexandre.fusilli@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:49',0,'2018-07-31 17:07:10'),(26635568,'sabetienne@gmail.com',1,'','','sabetienne@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:50',0,'2018-07-31 17:07:10'),(26635569,'domih14@orange.fr',1,'','','domih14@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:50',0,'2018-07-31 17:07:10'),(26635570,'lisatacjeux@gmail.com',1,'','','lisatacjeux@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:50',0,'2018-07-31 17:07:10'),(26635571,'ukil14@laposte.net',1,'','','ukil14@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:50',0,'2018-07-31 17:07:10'),(26635572,'patrick.degrancey@orange.fr',1,'','','patrick.degrancey@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:51',0,'2018-07-31 17:07:10'),(26635573,'landreau.ysabel@gmail.com',1,'','','landreau.ysabel@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:51',0,'2018-07-31 17:07:10'),(26635574,'therese.trsek@yahoo.fr',1,'','','therese.trsek@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:51',0,'2018-07-31 17:07:10'),(26635575,'spinettavalou@gmail.com',1,'','','spinettavalou@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:51',0,'2018-07-31 17:07:10'),(26635576,'chivas31@outlook.fr',1,'','','chivas31@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:52',0,'2018-07-31 17:07:10'),(26635577,'danimace@hotmail.fr',1,'','','danimace@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:52',0,'2018-07-31 17:07:10'),(26635578,'corinne.debray3@orange.fr',1,'','','corinne.debray3@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:52',0,'2018-07-31 17:07:10'),(26635579,'sgwagner@free.fr',1,'','','sgwagner@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:52',0,'2018-07-31 17:07:10'),(26635580,'bodyguard.g@orange.fr',1,'','','bodyguard.g@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:52',0,'2018-07-31 17:07:10'),(26635581,'gerard.accart@orange.fr',1,'','','gerard.accart@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:53',0,'2018-07-31 17:07:10'),(26635582,'marietherese.lepioulle@bbox.fr',1,'','','marietherese.lepioulle@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:53',0,'2018-07-31 17:07:10'),(26635583,'globule_0@yahoo.fr',1,'','','globule_0@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:53',0,'2018-07-31 17:07:10'),(26635584,'andremagnier1@bbox.fr',1,'','','andremagnier1@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:54',0,'2018-07-31 17:07:10'),(26635585,'mamanglobule@yahoo.fr',1,'','','mamanglobule@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:54',0,'2018-07-31 17:07:10'),(26635586,'do.recoque@laposte.net',1,'','','do.recoque@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:54',0,'2018-07-31 17:07:10'),(26635587,'alphonse.engelmann@laposte.net',1,'','','alphonse.engelmann@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:54',0,'2018-07-31 17:07:10'),(26635588,'dsantini@dsantini.com',1,'','','dsantini@dsantini.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:55',0,'2018-07-31 17:07:10'),(26635589,'stefanoise76@gmail.com',1,'','','stefanoise76@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:55',0,'2018-07-31 17:07:10'),(26635590,'rogerprieur2@aol.fr',1,'','','rogerprieur2@aol.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:55',0,'2018-07-31 17:07:10'),(26635591,'isabelle.tiramani@free.fr',1,'','','isabelle.tiramani@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:55',0,'2018-07-31 17:07:10'),(26635592,'sab3344@hotmail.com',1,'','','sab3344@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:56',0,'2018-07-31 17:07:10'),(26635593,'sabine_lefebvre@live.fr',1,'','','sabine_lefebvre@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:56',0,'2018-07-31 17:07:10'),(26635594,'fromentin_pascal@bbox.fr',1,'','','fromentin_pascal@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:56',0,'2018-07-31 17:07:10'),(26635595,'framboise.dune@orange.fr',1,'','','framboise.dune@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:57',0,'2018-07-31 17:07:10'),(26635596,'jp@chatill.fr',1,'','','jp@chatill.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:57',0,'2018-07-31 17:07:10'),(26635597,'collet.ludo@sfr.fr',1,'','','collet.ludo@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:57',0,'2018-07-31 17:07:10'),(26635598,'philippelevaillant@sfr.fr',1,'','','philippelevaillant@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:57',0,'2018-07-31 17:07:10'),(26635599,'francis.corroyette@orange.fr',1,'','','francis.corroyette@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:57',0,'2018-07-31 17:07:10'),(26635600,'nines2@free.fr',1,'','','nines2@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:58',0,'2018-07-31 17:07:10'),(26635601,'4ocjama@yopmail.com',1,'','','4ocjama@yopmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:58',0,'2018-07-31 17:07:10'),(26635602,'jean.hennebelle@orange.fr',1,'','','jean.hennebelle@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:58',0,'2018-07-31 17:07:10'),(26635603,'pouty@netcourrier.com',1,'','','pouty@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:58',0,'2018-07-31 17:07:10'),(26635604,'nath11300@live.fr',1,'','','nath11300@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:59',0,'2018-07-31 17:07:10'),(26635605,'tresd@netcourrier.com',1,'','','tresd@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:59',0,'2018-07-31 17:07:10'),(26635606,'dufdav59700@gmail.com',1,'','','dufdav59700@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:59',0,'2018-07-31 17:07:10'),(26635607,'lolomairesse@gmail.com',1,'','','lolomairesse@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:28:59',0,'2018-07-31 17:07:10'),(26635608,'morau.marie@orange.fr',1,'','','morau.marie@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:00',0,'2018-07-31 17:07:10'),(26635609,'ernest.gourmelen@orange.fr',1,'','','ernest.gourmelen@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:00',0,'2018-07-31 17:07:10'),(26635610,'titipic77@gmail.com',1,'','','titipic77@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:00',0,'2018-07-31 17:07:10'),(26635611,'laulaudeparis@laposte.net',1,'','','laulaudeparis@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:00',0,'2018-07-31 17:07:10'),(26635612,'jplecharles@orange.fr',1,'','','jplecharles@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:00',0,'2018-07-31 17:07:10'),(26635613,'j.dias12000@free.fr',1,'','','j.dias12000@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:01',0,'2018-07-31 17:07:10'),(26635614,'massariol.jeanpierre@neuf.fr',1,'','','massariol.jeanpierre@neuf.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:01',0,'2018-07-31 17:07:10'),(26635615,'loife@netcourrier.com',1,'','','loife@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:01',0,'2018-07-31 17:07:10'),(26635616,'claude.lienhart@orange.fr',1,'','','claude.lienhart@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:01',0,'2018-07-31 17:07:10'),(26635617,'francesca42@wanadoo.fr',1,'','','francesca42@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:02',0,'2018-07-31 17:07:10'),(26635618,'servoin70jeu@gmx.fr',1,'','','servoin70jeu@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:02',0,'2018-07-31 17:07:10'),(26635619,'gerard.guerin33@orange.fr',1,'','','gerard.guerin33@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:02',0,'2018-07-31 17:07:10'),(26635620,'joel.martial@hotmail.fr',1,'','','joel.martial@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:02',0,'2018-07-31 17:07:10'),(26635621,'fabinou2@gmx.fr',1,'','','fabinou2@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:03',0,'2018-07-31 17:07:10'),(26635622,'rvrg@free.fr',1,'','','rvrg@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:03',0,'2018-07-31 17:07:10'),(26635623,'lafalou@laposte.net',1,'','','lafalou@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:03',0,'2018-07-31 17:07:10'),(26635624,'orlanelogan@outlook.fr',1,'','','orlanelogan@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:03',0,'2018-07-31 17:07:10'),(26635625,'c_baudot@orange.fr',1,'','','c_baudot@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:04',0,'2018-07-31 17:07:10'),(26635626,'b.chantal@numericable.fr',1,'','','b.chantal@numericable.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:04',0,'2018-07-31 17:07:10'),(26635627,'kalou4646@hotmail.fr',1,'','','kalou4646@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:04',0,'2018-07-31 17:07:10'),(26635628,'ethantapez@gmail.com',1,'','','ethantapez@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:04',0,'2018-07-31 17:07:10'),(26635629,'colibritatoo22@laposte.net',1,'','','colibritatoo22@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:04',0,'2018-07-31 17:07:10'),(26635630,'satiasatia18@outlook.fr',1,'','','satiasatia18@outlook.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:05',0,'2018-07-31 17:07:10'),(26635631,'lionel.leconte@hotmail.fr',1,'','','lionel.leconte@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:05',0,'2018-07-31 17:07:10'),(26635632,'bluejill75@gmail.com',1,'','','bluejill75@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:05',0,'2018-07-31 17:07:10'),(26635633,'audrey.schleg@orange.fr',1,'','','audrey.schleg@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:05',0,'2018-07-31 17:07:10'),(26635634,'80-laurence@live.fr',1,'','','80-laurence@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:06',0,'2018-07-31 17:07:10'),(26635635,'yoko34@gmx.fr',1,'','','yoko34@gmx.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:06',0,'2018-07-31 17:07:10'),(26635636,'lahaye.c03@hotmail.fr',1,'','','lahaye.c03@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:06',0,'2018-07-31 17:07:10'),(26635637,'albac.pierre@aliceadsl.fr',1,'','','albac.pierre@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:06',0,'2018-07-31 17:07:10'),(26635638,'wautersfab@gmail.com',1,'','','wautersfab@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:07',0,'2018-07-31 17:07:10'),(26635639,'sylviecazabat@yahoo.fr',1,'','','sylviecazabat@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:07',0,'2018-07-31 17:07:10'),(26635640,'lutna@free.fr',1,'','','lutna@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:07',0,'2018-07-31 17:07:10'),(26635641,'mo.gavriloiu@free.fr',1,'','','mo.gavriloiu@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:07',0,'2018-07-31 17:07:10'),(26635642,'josefatou1958@gmail.com',1,'','','josefatou1958@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:08',0,'2018-07-31 17:07:10'),(26635643,'angelica.bataller@free.fr',1,'','','angelica.bataller@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:08',0,'2018-07-31 17:07:10'),(26635644,'doudland@live.fr',1,'','','doudland@live.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:08',0,'2018-07-31 17:07:10'),(26635645,'gerate@netcourrier.com',1,'','','gerate@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:08',0,'2018-07-31 17:07:10'),(26635646,'kazan2607@hotmail.fr',1,'','','kazan2607@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:09',0,'2018-07-31 17:07:10'),(26635647,'oberlin.jean-paul@orange.fr',1,'','','oberlin.jean-paul@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:09',0,'2018-07-31 17:07:10'),(26635648,'lagadic.virginie@sfr.fr',1,'','','lagadic.virginie@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:09',0,'2018-07-31 17:07:10'),(26635649,'elisabeth.plywacz@wanadoo.fr',1,'','','elisabeth.plywacz@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:09',0,'2018-07-31 17:07:10'),(26635650,'jonathandufour5@gmail.com',1,'','','jonathandufour5@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:09',0,'2018-07-31 17:07:10'),(26635651,'hope5962@hotmail.fr',1,'','','hope5962@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:10',0,'2018-07-31 17:07:10'),(26635652,'utyk10@laposte.net',1,'','','utyk10@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:10',0,'2018-07-31 17:07:10'),(26635653,'mclaire@netcourrier.com',1,'','','mclaire@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:10',0,'2018-07-31 17:07:10'),(26635654,'lucie.cauquil@yahoo.fr',1,'','','lucie.cauquil@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:10',0,'2018-07-31 17:07:10'),(26635655,'marie83460@hotmail.fr',1,'','','marie83460@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:11',0,'2018-07-31 17:07:10'),(26635656,'alfonsjoosten@hetnet.nl',1,'','','alfonsjoosten@hetnet.nl','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:11',0,'2018-07-31 17:07:10'),(26635657,'latiie@msn.com',1,'','','latiie@msn.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:11',0,'2018-07-31 17:07:10'),(26635658,'hello.collet@yahoo.com',1,'','','hello.collet@yahoo.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:11',0,'2018-07-31 17:07:10'),(26635659,'panon.jean-yves@orange.fr',1,'','','panon.jean-yves@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:12',0,'2018-07-31 17:07:10'),(26635660,'gourmansise123@hotmail.fr',1,'','','gourmansise123@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:12',0,'2018-07-31 17:07:10'),(26635661,'bpierre2@bbox.fr',1,'','','bpierre2@bbox.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:12',0,'2018-07-31 17:07:10'),(26635662,'luc.muller3@wanadoo.fr',1,'','','luc.muller3@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:12',0,'2018-07-31 17:07:10'),(26635663,'patrick.risini@orange.fr',1,'','','patrick.risini@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:13',0,'2018-07-31 17:07:10'),(26635664,'rosario.ragusa@t-online.de',1,'','','rosario.ragusa@t-online.de','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:13',0,'2018-07-31 17:07:10'),(26635665,'claude.poulet3@wanadoo.fr',1,'','','claude.poulet3@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:13',0,'2018-07-31 17:07:10'),(26635666,'jean-pierre.raison0@orange.fr',1,'','','jean-pierre.raison0@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:13',0,'2018-07-31 17:07:10'),(26635667,'jeanchristophe89116@free.fr',1,'','','jeanchristophe89116@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:13',0,'2018-07-31 17:07:10'),(26635668,'melynath@yahoo.fr',1,'','','melynath@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:14',0,'2018-07-31 17:07:10'),(26635669,'furet.pascal@wanadoo.fr',1,'','','furet.pascal@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:14',0,'2018-07-31 17:07:10'),(26635670,'opiuy@netcourrier.com',1,'','','opiuy@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:14',0,'2018-07-31 17:07:10'),(26635671,'francoise.carles@yahoo.fr',1,'','','francoise.carles@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:14',0,'2018-07-31 17:07:10'),(26635672,'bebecricri.franchi@laposte.net',1,'','','bebecricri.franchi@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:15',0,'2018-07-31 17:07:10'),(26635673,'sandrine.silva@orange.fr',1,'','','sandrine.silva@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:15',0,'2018-07-31 17:07:10'),(26635674,'lenasand950@gmail.com',1,'','','lenasand950@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:15',0,'2018-07-31 17:07:10'),(26635675,'beha456@hotmail.fr',1,'','','beha456@hotmail.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:15',0,'2018-07-31 17:07:10'),(26635676,'lardenoiscorinne@gmail.com',1,'','','lardenoiscorinne@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:15',0,'2018-07-31 17:07:10'),(26635677,'patricia.hervieux@nordnet.fr',1,'','','patricia.hervieux@nordnet.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:16',0,'2018-07-31 17:07:10'),(26635678,'marie-jose.kaim@orange.fr',1,'','','marie-jose.kaim@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:16',0,'2018-07-31 17:07:10'),(26635679,'zabeth.mathieu@wanadoo.fr',1,'','','zabeth.mathieu@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:16',0,'2018-07-31 17:07:10'),(26635680,'berlinson.georges@wanadoo.fr',1,'','','berlinson.georges@wanadoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:16',0,'2018-07-31 17:07:10'),(26635681,'armelle.berthelot@aliceadsl.fr',1,'','','armelle.berthelot@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:17',0,'2018-07-31 17:07:10'),(26635682,'cretonpatrick@orange.fr',1,'','','cretonpatrick@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:17',0,'2018-07-31 17:07:10'),(26635683,'petitchat3132@hotmail.com',1,'','','petitchat3132@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:17',0,'2018-07-31 17:07:10'),(26635684,'frtval13@gmail.com',1,'','','frtval13@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:17',0,'2018-07-31 17:07:10'),(26635685,'mariec.b@orange.fr',1,'','','mariec.b@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:18',0,'2018-07-31 17:07:10'),(26635686,'ca@bedivar.pt',1,'','','ca@bedivar.pt','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:18',0,'2018-07-31 17:07:10'),(26635687,'d.hurt@laposte.net',1,'','','d.hurt@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:18',0,'2018-07-31 17:07:10'),(26635688,'rene.perraud@sfr.fr',1,'','','rene.perraud@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:19',0,'2018-07-31 17:07:10'),(26635689,'garbezalain@free.fr',1,'','','garbezalain@free.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:19',0,'2018-07-31 17:07:10'),(26635690,'sabrinaspf09@gmail.com',1,'','','sabrinaspf09@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:19',0,'2018-07-31 17:07:10'),(26635691,'cuenotlaurent@gmail.com',1,'','','cuenotlaurent@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:19',0,'2018-07-31 17:07:10'),(26635692,'thierry.avit@laposte.net',1,'','','thierry.avit@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:19',0,'2018-07-31 17:07:10'),(26635693,'y.garnier17@orange.fr',1,'','','y.garnier17@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:20',0,'2018-07-31 17:07:10'),(26635694,'terremetisse@yahoo.fr',1,'','','terremetisse@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:20',0,'2018-07-31 17:07:10'),(26635695,'dourylecole@aol.com',1,'','','dourylecole@aol.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:20',0,'2018-07-31 17:07:10'),(26635696,'fezsa@netcourrier.com',1,'','','fezsa@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:20',0,'2018-07-31 17:07:10'),(26635697,'jeanclaude.escande06@orange.fr',1,'','','jeanclaude.escande06@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:21',0,'2018-07-31 17:07:10'),(26635698,'aureliebalthasarwanner@sfr.fr',1,'','','aureliebalthasarwanner@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:21',0,'2018-07-31 17:07:10'),(26635699,'cap.19021966@orange.fr',1,'','','cap.19021966@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:21',0,'2018-07-31 17:07:10'),(26635700,'jegou.p@orange.fr',1,'','','jegou.p@orange.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:21',0,'2018-07-31 17:07:10'),(26635701,'vreder@netcourrier.com',1,'','','vreder@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:22',0,'2018-07-31 17:07:10'),(26635702,'martinegr@netcourrier.com',1,'','','martinegr@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:22',0,'2018-07-31 17:07:10'),(26635703,'veropancras@hotmail.com',1,'','','veropancras@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:22',0,'2018-07-31 17:07:10'),(26635704,'opitre@netcourrier.com',1,'','','opitre@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:22',0,'2018-07-31 17:07:10'),(26635705,'moniezpauline@gmail.com',1,'','','moniezpauline@gmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:23',0,'2018-07-31 17:07:10'),(26635706,'florence.compe@aliceadsl.fr',1,'','','florence.compe@aliceadsl.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:23',0,'2018-07-31 17:07:10'),(26635707,'malyl@sfr.fr',1,'','','malyl@sfr.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:24',0,'2018-07-31 17:07:10'),(26635708,'sabrinar3344@laposte.net',1,'','','sabrinar3344@laposte.net','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:24',0,'2018-07-31 17:07:10'),(26635709,'renepautremat@hotmail.com',1,'','','renepautremat@hotmail.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:24',0,'2018-07-31 17:07:10'),(26635710,'valeboudec@yahoo.fr',1,'','','valeboudec@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:24',0,'2018-07-31 17:07:10'),(26635711,'xaser@netcourrier.com',1,'','','xaser@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:25',0,'2018-07-31 17:07:10'),(26635712,'tresda@netcourrier.com',1,'','','tresda@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:25',0,'2018-07-31 17:07:10'),(26635713,'lilianis44100@yahoo.fr',1,'','','lilianis44100@yahoo.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:25',0,'2018-07-31 17:07:10'),(26635714,'gregory.coste@club-internet.fr',1,'','','gregory.coste@club-internet.fr','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:25',0,'2018-07-31 17:07:10'),(26635715,'likut@netcourrier.com',1,'','','likut@netcourrier.com','0',NULL,'1','1',NULL,'en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,76,50,0,'2018-07-31 17:07:10','2018-08-02 22:29:26',0,'2018-07-31 17:07:10'),(26635719,'id98r65',1,'soufiane','bennani','soufianeben95@gmail.com','0','0000-00-00','1','1','FR','FR',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,79,50,0,'2018-08-01 10:02:38','2018-08-06 11:35:34',0,'2018-07-31 17:07:10'),(26636911,'mario.aguiar@orange.fr',1,NULL,NULL,'mario.aguiar@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636912,'J.barraud@seric-group.fr',1,NULL,NULL,'J.barraud@seric-group.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636913,'mlemonen@icloud.com',1,NULL,NULL,'mlemonen@icloud.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636914,'bbodin@maisonronde.com',1,NULL,NULL,'bbodin@maisonronde.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636915,'sarldg@laposte.net',1,NULL,NULL,'sarldg@laposte.net','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636916,'mca@mca-tp.fr',1,NULL,NULL,'mca@mca-tp.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636917,'contact@paysages-floreal.fr',1,NULL,NULL,'contact@paysages-floreal.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636918,'cmaconcept@orange.fr',1,NULL,NULL,'cmaconcept@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636919,'daniel.guillaud1@orange.fr',1,NULL,NULL,'daniel.guillaud1@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636920,'sarlgungor@live.fr',1,NULL,NULL,'sarlgungor@live.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636921,'f.justen@seric-group.fr',1,NULL,NULL,'f.justen@seric-group.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636922,'rhfermetures@bbox.fr',1,NULL,NULL,'rhfermetures@bbox.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636923,'regismollaret@gmail.com',1,NULL,NULL,'regismollaret@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636924,'ferronnerie.lamorou@gmail.com',1,NULL,NULL,'ferronnerie.lamorou@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636925,'creat.terrassement@gmail.com',1,NULL,NULL,'creat.terrassement@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636926,'Cmf-facades@orange.fr',1,NULL,NULL,'Cmf-facades@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636927,'tpcm@groupe-velentin.fr',1,NULL,NULL,'tpcm@groupe-velentin.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636928,'perrier.paysage@sfr.fr',1,NULL,NULL,'perrier.paysage@sfr.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636929,'l.toesca@chazalsas.fr',1,NULL,NULL,'l.toesca@chazalsas.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636930,'JACQUES.viviant@orange.fr',1,NULL,NULL,'JACQUES.viviant@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:53:48',0,'2018-08-03 14:23:10'),(26636931,'set.yacoub@gmail.com',1,NULL,NULL,'set.yacoub@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:54:39',0,'2018-08-03 14:23:10'),(26636932,'maisons-rivoises@wanadoo.fr',1,NULL,NULL,'maisons-rivoises@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636933,'guillotp2a@orange.fr',1,NULL,NULL,'guillotp2a@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636934,'alain.fabre20@wanadoo.fr',1,NULL,NULL,'alain.fabre20@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636935,'commercial@clodis.fr',1,NULL,NULL,'commercial@clodis.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636936,'AEMCLO@FREE.FR',1,NULL,NULL,'AEMCLO@FREE.FR','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636937,'alainforcheron@gmail.com',1,NULL,NULL,'alainforcheron@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636938,'r.danthony1957@orange.fr',1,NULL,NULL,'r.danthony1957@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636939,'mionsportail@orange.fr',1,NULL,NULL,'mionsportail@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636940,'alban.drev@gmail.com',1,NULL,NULL,'alban.drev@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636941,'a.ruby@tecalemit-aerospace.fr',1,NULL,NULL,'a.ruby@tecalemit-aerospace.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636942,'amasson@ducetpreneuf.fr',1,NULL,NULL,'amasson@ducetpreneuf.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636943,'a.roucaute@tarvel.fr',1,NULL,NULL,'a.roucaute@tarvel.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636944,'annelaure.cart@benedetti-guelpa.fr',1,NULL,NULL,'annelaure.cart@benedetti-guelpa.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636945,'cailleux.anthony@yahoo.com',1,NULL,NULL,'cailleux.anthony@yahoo.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636946,'drevet.tp@gmail.com',1,NULL,NULL,'drevet.tp@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636947,'anthonypalle@yahoo.fr',1,NULL,NULL,'anthonypalle@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636948,'aveyret@ets-bernard.com',1,NULL,NULL,'aveyret@ets-bernard.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636949,'actionbat06@free.fr',1,NULL,NULL,'actionbat06@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636950,'arnaud.zaragoza@madic.com',1,NULL,NULL,'arnaud.zaragoza@madic.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636951,'contact@agconcept.net',1,NULL,NULL,'contact@agconcept.net','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636952,'yannickpolicon@gmail.com',1,NULL,NULL,'yannickpolicon@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636953,'acmagnard@sfr.fr',1,NULL,NULL,'acmagnard@sfr.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636954,'boisetpaysage38410@gmail.com',1,NULL,NULL,'boisetpaysage38410@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636955,'benjamin.cjs@orange.fr',1,NULL,NULL,'benjamin.cjs@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636956,'benoit.baltz@gmail.com',1,NULL,NULL,'benoit.baltz@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636957,'badinbertrand28@gmail.com',1,NULL,NULL,'badinbertrand28@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636958,'bertrand.espacesverts@orange.fr',1,NULL,NULL,'bertrand.espacesverts@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636959,'yvette-besson@orange.fr',1,NULL,NULL,'yvette-besson@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636960,'lambert-boris@wanadoo.fr',1,NULL,NULL,'lambert-boris@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636961,'ugo59@orange.fr',1,NULL,NULL,'ugo59@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636962,'bourgeattp@hotmail.fr',1,NULL,NULL,'bourgeattp@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636963,'steven.gallay@prevoir.com',1,NULL,NULL,'steven.gallay@prevoir.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636964,'grosjean.espacesverts@orange.fr',1,NULL,NULL,'grosjean.espacesverts@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636965,'beaulieuconstruction@free.fr',1,NULL,NULL,'beaulieuconstruction@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636966,'cnbbatiment@gmail.com',1,NULL,NULL,'cnbbatiment@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636967,'carlosmatos38@aol.com',1,NULL,NULL,'carlosmatos38@aol.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636968,'capbleupaysages@orange.fr',1,NULL,NULL,'capbleupaysages@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636969,'cedric.lherbette@club-internet.fr',1,NULL,NULL,'cedric.lherbette@club-internet.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636970,'c.rota@umhs.fr',1,NULL,NULL,'c.rota@umhs.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636971,'bcellard@genevray-paysagiste.fr',1,NULL,NULL,'bcellard@genevray-paysagiste.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636972,'info@ludiclotures.ch',1,NULL,NULL,'info@ludiclotures.ch','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636973,'christian@doulat.fr',1,NULL,NULL,'christian@doulat.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636974,'landwave@outlook.com',1,NULL,NULL,'landwave@outlook.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636975,'ltropres@orange.fr',1,NULL,NULL,'ltropres@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636976,'vercorspaysage@free.fr',1,NULL,NULL,'vercorspaysage@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636977,'christophe.maxx@gmail.com',1,NULL,NULL,'christophe.maxx@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636978,'gaedennco@live.fr',1,NULL,NULL,'gaedennco@live.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636979,'liobard84@gmail.com',1,NULL,NULL,'liobard84@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636980,'lesjardinsdubrionnais@outlook.fr',1,NULL,NULL,'lesjardinsdubrionnais@outlook.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636981,'az-paysage@wanadoo.fr',1,NULL,NULL,'az-paysage@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636982,'daniel.jay@rhone-espaces-verts.fr',1,NULL,NULL,'daniel.jay@rhone-espaces-verts.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636983,'davidbourgeat@gmail.com',1,NULL,NULL,'davidbourgeat@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636984,'dfuentes@rjs-eg.fr',1,NULL,NULL,'dfuentes@rjs-eg.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636985,'bressefermetures@gmail.com',1,NULL,NULL,'bressefermetures@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636986,'david.barnarie@orange.fr',1,NULL,NULL,'david.barnarie@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636987,'gil.fermetures@gmail.com',1,NULL,NULL,'gil.fermetures@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636988,'contact@vernaytp.com',1,NULL,NULL,'contact@vernaytp.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636989,'acs.collot@k-net.fr',1,NULL,NULL,'acs.collot@k-net.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636990,'myhomegarden74@gmail.com',1,NULL,NULL,'myhomegarden74@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636991,'didierlugrin@hotmail.fr',1,NULL,NULL,'didierlugrin@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636992,'contact@ci2p.fr',1,NULL,NULL,'contact@ci2p.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636993,'emmanuelsacchi@gmail.com',1,NULL,NULL,'emmanuelsacchi@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636994,'hutherfab@free.fr',1,NULL,NULL,'hutherfab@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636995,'entbrando@hotmail.fr',1,NULL,NULL,'entbrando@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:53:13',0,'2018-08-03 14:23:10'),(26636996,'fabrice.paysages@gmail.com',1,NULL,NULL,'fabrice.paysages@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636997,'ainpaysage@gmail.com',1,NULL,NULL,'ainpaysage@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:54:37',0,'2018-08-03 14:23:10'),(26636998,'fb.1000paysages@gmail.com',1,NULL,NULL,'fb.1000paysages@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26636999,'flopeyraud@gmail.com',1,NULL,NULL,'flopeyraud@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637000,'art-beton@art-beton.fr',1,NULL,NULL,'art-beton@art-beton.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637001,'ruiz27@live.fr',1,NULL,NULL,'ruiz27@live.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637002,'gazon.express38@gmail.com',1,NULL,NULL,'gazon.express38@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637003,'paysages-services@bbox.fr',1,NULL,NULL,'paysages-services@bbox.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637004,'thiebaud.frederic@orange.fr',1,NULL,NULL,'thiebaud.frederic@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637005,'barthassatgary@gmail.com',1,NULL,NULL,'barthassatgary@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637006,'gmagencement@yahoo.fr',1,NULL,NULL,'gmagencement@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637007,'polin.paysage@gmail.com',1,NULL,NULL,'polin.paysage@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637008,'smt38@orange.fr',1,NULL,NULL,'smt38@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637009,'gerard.porchier@sfr.fr',1,NULL,NULL,'gerard.porchier@sfr.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637010,'progeba@orange.fr',1,NULL,NULL,'progeba@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637011,'info@balsigersarl.ch',1,NULL,NULL,'info@balsigersarl.ch','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637012,'guillaumecollavet@gmail.com',1,NULL,NULL,'guillaumecollavet@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637013,'augredutemps.paysage@orange.fr',1,NULL,NULL,'augredutemps.paysage@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637014,'herve.bouniol@laposte.net',1,NULL,NULL,'herve.bouniol@laposte.net','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637015,'thevenet.paysagiste@orange.fr',1,NULL,NULL,'thevenet.paysagiste@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637016,'bg-paysage@orange.fr',1,NULL,NULL,'bg-paysage@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637017,'jacky.durand3@wanadoo.fr',1,NULL,NULL,'jacky.durand3@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637018,'agro.process@wanadoo.fr',1,NULL,NULL,'agro.process@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637019,'jcgalmiche@ducetpreneuf.fr',1,NULL,NULL,'jcgalmiche@ducetpreneuf.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637020,'jcrojon@hotmail.com',1,NULL,NULL,'jcrojon@hotmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637021,'acmi33@hotmail.fr',1,NULL,NULL,'acmi33@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637022,'jfg@lyon-paysagiste.fr',1,NULL,NULL,'jfg@lyon-paysagiste.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637023,'jjsekerger@gmail.com',1,NULL,NULL,'jjsekerger@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637024,'jjv@vaury.com',1,NULL,NULL,'jjv@vaury.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637025,'contact@dvert-paysage.com',1,NULL,NULL,'contact@dvert-paysage.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637026,'jplpaysagiste@sfr.fr',1,NULL,NULL,'jplpaysagiste@sfr.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637027,'baumann6@orange.fr',1,NULL,NULL,'baumann6@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637028,'c.dragon@crom.fr',1,NULL,NULL,'c.dragon@crom.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637029,'ntpaysage@yahoo.fr',1,NULL,NULL,'ntpaysage@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:54:40',0,'2018-08-03 14:23:10'),(26637030,'jbille@arterris.fr',1,NULL,NULL,'jbille@arterris.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637031,'aspect.proprete@free.fr',1,NULL,NULL,'aspect.proprete@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637032,'jhlrenovation73@gmail.com',1,NULL,NULL,'jhlrenovation73@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637033,'j.pouget@agrosylva.eu',1,NULL,NULL,'j.pouget@agrosylva.eu','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637034,'joel.sommacal@orange.fr',1,NULL,NULL,'joel.sommacal@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637035,'jordan.vialette@benedetti-guelpa.fr',1,NULL,NULL,'jordan.vialette@benedetti-guelpa.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637036,'pizzetta.sarl@orange.fr',1,NULL,NULL,'pizzetta.sarl@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637037,'contact@lileojardins.fr',1,NULL,NULL,'contact@lileojardins.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637038,'jedisvert@gmail.com',1,NULL,NULL,'jedisvert@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637039,'sougeyjulien@hotmail.fr',1,NULL,NULL,'sougeyjulien@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637040,'karine.brochet@sixt.com',1,NULL,NULL,'karine.brochet@sixt.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637041,'jardinium@yahoo.fr',1,NULL,NULL,'jardinium@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637042,'cpmducreux@gmail.com',1,NULL,NULL,'cpmducreux@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637043,'Laura.p@jardinblanc.fr',1,NULL,NULL,'Laura.p@jardinblanc.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637044,'laurent.bourgey@bbox.fr',1,NULL,NULL,'laurent.bourgey@bbox.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637045,'barbarachanu@cegetel.net',1,NULL,NULL,'barbarachanu@cegetel.net','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637046,'lferrand@jacquardespacesverts.fr',1,NULL,NULL,'lferrand@jacquardespacesverts.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637047,'mley@ducetpreneuf.fr',1,NULL,NULL,'mley@ducetpreneuf.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637048,'guinetservices@hotmail.fr',1,NULL,NULL,'guinetservices@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637049,'yan.loszach@compagniedesalpes.fr',1,NULL,NULL,'yan.loszach@compagniedesalpes.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637050,'luc.roche@wanadoo.fr',1,NULL,NULL,'luc.roche@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637051,'espacesvertsbruet@orange.fr',1,NULL,NULL,'espacesvertsbruet@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637052,'ls.grandjean@hotmail.fr',1,NULL,NULL,'ls.grandjean@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637053,'lardiere.js@orange.fr',1,NULL,NULL,'lardiere.js@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637054,'manuel.batko@sfr.fr',1,NULL,NULL,'manuel.batko@sfr.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637055,'marsattler@aol.com',1,NULL,NULL,'marsattler@aol.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637056,'Ka.sauer@yahoo.fr',1,NULL,NULL,'Ka.sauer@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637057,'mturbeaux@evmo.fr',1,NULL,NULL,'mturbeaux@evmo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637058,'perrin.paysage.m.m@wanadoo.fr',1,NULL,NULL,'perrin.paysage.m.m@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637059,'vert.services.fournier@orange.fr',1,NULL,NULL,'vert.services.fournier@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637060,'durand.entreprise18@gmail.com',1,NULL,NULL,'durand.entreprise18@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637061,'m2sfermetures@gmail.com',1,NULL,NULL,'m2sfermetures@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637062,'mikepanico@free.fr',1,NULL,NULL,'mikepanico@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637063,'mgespacesverts@orange.fr',1,NULL,NULL,'mgespacesverts@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637064,'michel.castellino@hotmail.fr',1,NULL,NULL,'michel.castellino@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637065,'bruet.paysage@orange.fr',1,NULL,NULL,'bruet.paysage@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637066,'micheldenisautomatisme@orange.fr',1,NULL,NULL,'micheldenisautomatisme@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637067,'mdeyres@hotmail.com',1,NULL,NULL,'mdeyres@hotmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637068,'m.bonhomme@paysageb.fr',1,NULL,NULL,'m.bonhomme@paysageb.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637069,'mickael@creanature.com',1,NULL,NULL,'mickael@creanature.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637070,'cedric.milly@orange.fr',1,NULL,NULL,'cedric.milly@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637071,'morgan.bernadas@gmail.com',1,NULL,NULL,'morgan.bernadas@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637072,'nmorand@rjs-eg.fr',1,NULL,NULL,'nmorand@rjs-eg.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637073,'acepaysagegalle@gmail.com',1,NULL,NULL,'acepaysagegalle@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637074,'nspaysages38@gmail.com',1,NULL,NULL,'nspaysages38@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637075,'n.madzar@free.fr',1,NULL,NULL,'n.madzar@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637076,'olivier.berthet@lyon-paysagiste.fr',1,NULL,NULL,'olivier.berthet@lyon-paysagiste.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637077,'p.brottier@easpacs.fr',1,NULL,NULL,'p.brottier@easpacs.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 11:52:19',0,'2018-08-03 14:23:10'),(26637078,'cpassionjardin@orange.fr',1,NULL,NULL,'cpassionjardin@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637079,'pbarbier@umhs.fr',1,NULL,NULL,'pbarbier@umhs.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637080,'secretariat@espacesvertsdusudest.fr',1,NULL,NULL,'secretariat@espacesvertsdusudest.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637081,'thomas.petitpierre@wanadoo.fr',1,NULL,NULL,'thomas.petitpierre@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637082,'p.chenu@apajh38.org',1,NULL,NULL,'p.chenu@apajh38.org','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637083,'marchandphilippe38@gmail.com',1,NULL,NULL,'marchandphilippe38@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637084,'vincent.espacesverts@orange.fr',1,NULL,NULL,'vincent.espacesverts@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637085,'carreaucconcept.pierregrange@gmail.com',1,NULL,NULL,'carreaucconcept.pierregrange@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637086,'lorenvert@yahoo.fr',1,NULL,NULL,'lorenvert@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637087,'pierre.montbel@orange.fr',1,NULL,NULL,'pierre.montbel@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637088,'pierre.coquatrix@orange.fr',1,NULL,NULL,'pierre.coquatrix@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637089,'vr.paysages@hotmail.fr',1,NULL,NULL,'vr.paysages@hotmail.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637090,'br.menuiserie@yahoo.fr',1,NULL,NULL,'br.menuiserie@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637091,'remy.murcia@wanadoo.fr',1,NULL,NULL,'remy.murcia@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637092,'atp.renaud@gmail.com',1,NULL,NULL,'atp.renaud@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637093,'lesjardinsdetheo@yahoo.fr',1,NULL,NULL,'lesjardinsdetheo@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637094,'romain-lacroix@club-internet.fr',1,NULL,NULL,'romain-lacroix@club-internet.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637095,'pat.chareyre@orange.fr',1,NULL,NULL,'pat.chareyre@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637096,'parcsetjardinsiserois@gmail.com',1,NULL,NULL,'parcsetjardinsiserois@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637097,'stallet@ducetpreneuf.fr',1,NULL,NULL,'stallet@ducetpreneuf.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637098,'bois-cyril@orange.fr',1,NULL,NULL,'bois-cyril@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:51:34',0,'2018-08-03 14:23:10'),(26637099,'citeservicesequipements@wanadoo.fr',1,NULL,NULL,'citeservicesequipements@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637100,'guicherd.paysages@gmail.com',1,NULL,NULL,'guicherd.paysages@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637101,'maconnerie.finet38@orange.fr',1,NULL,NULL,'maconnerie.finet38@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:54:38',0,'2018-08-03 14:23:10'),(26637102,'jordan@travauxpaysagers.fr',1,NULL,NULL,'jordan@travauxpaysagers.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637103,'boisdaisy@gmail.com',1,NULL,NULL,'boisdaisy@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637104,'candyseb@orange.fr',1,NULL,NULL,'candyseb@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637105,'picardpassionpaysage@gmail.com',1,NULL,NULL,'picardpassionpaysage@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637106,'optevoz2@orange.fr',1,NULL,NULL,'optevoz2@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637107,'jmaespacesverts@orange.fr',1,NULL,NULL,'jmaespacesverts@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637108,'pardo-bgpaysage@orange.fr',1,NULL,NULL,'pardo-bgpaysage@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637109,'audinos.paysages@gmail.com',1,NULL,NULL,'audinos.paysages@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637110,'bsm8@orange.fr',1,NULL,NULL,'bsm8@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637111,'fleuryconcept@gmail.com',1,NULL,NULL,'fleuryconcept@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637112,'stef81.martin@yahoo.fr',1,NULL,NULL,'stef81.martin@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637113,'edenpaysage@sfr.fr',1,NULL,NULL,'edenpaysage@sfr.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637114,'sg.renov69@gmail.com',1,NULL,NULL,'sg.renov69@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637115,'admin@tpcm.fr',1,NULL,NULL,'admin@tpcm.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637116,'entreprisesylvainfournier@orange.fr',1,NULL,NULL,'entreprisesylvainfournier@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637117,'sylvainratel.cdd@orange.fr',1,NULL,NULL,'sylvainratel.cdd@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637118,'sylvain_rey@orange.fr',1,NULL,NULL,'sylvain_rey@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637119,'pavillonconcept.sarl@orange.fr',1,NULL,NULL,'pavillonconcept.sarl@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637120,'dutoit_tp@yahoo.fr',1,NULL,NULL,'dutoit_tp@yahoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637121,'tdma@wanadoo.fr',1,NULL,NULL,'tdma@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637122,'t.almy@s-ingenierie.com',1,NULL,NULL,'t.almy@s-ingenierie.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637123,'sauvage.paysages@gmail.com',1,NULL,NULL,'sauvage.paysages@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637124,'t.germiyan@entreprisemjs.fr',1,NULL,NULL,'t.germiyan@entreprisemjs.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637125,'t.mullard@euroludique.com',1,NULL,NULL,'t.mullard@euroludique.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637126,'thomas.lecomte@eurovia.com',1,NULL,NULL,'thomas.lecomte@eurovia.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637127,'guillard.paysage@gmail.com',1,NULL,NULL,'guillard.paysage@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637128,'valeriekhrome@gmail.com',1,NULL,NULL,'valeriekhrome@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637129,'eric.verdon@bluewin.ch',1,NULL,NULL,'eric.verdon@bluewin.ch','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637130,'contact@apisconseil.eu',1,NULL,NULL,'contact@apisconseil.eu','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:53:48',0,'2018-08-03 14:23:10'),(26637131,'vincent.tomassi@conceptom.fr',1,NULL,NULL,'vincent.tomassi@conceptom.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637132,'vincent.dujardin@gmail.com',1,NULL,NULL,'vincent.dujardin@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637133,'charly.nature@wanadoo.fr',1,NULL,NULL,'charly.nature@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637134,'sarl-m2c@orange.fr',1,NULL,NULL,'sarl-m2c@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',0,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-16 16:54:39',0,'2018-08-03 14:23:10'),(26637135,'y.glasson@espacesvertsdusudest.fr',1,NULL,NULL,'y.glasson@espacesvertsdusudest.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637136,'contact@smenagementsauniercreation.fr',1,NULL,NULL,'contact@smenagementsauniercreation.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637137,'arbh.terre@gmail.com',1,NULL,NULL,'arbh.terre@gmail.com','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637138,'ll0367@orange.fr',1,NULL,NULL,'ll0367@orange.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637139,'yves.duperray@free.fr',1,NULL,NULL,'yves.duperray@free.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637140,'paysagistegarnier@wanadoo.fr',1,NULL,NULL,'paysagistegarnier@wanadoo.fr','0',NULL,'1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,16,50,0,'2018-08-03 14:23:10','2018-08-05 01:00:32',0,'2018-08-03 14:23:10'),(26637658,'soufiane@gmail.com',1,NULL,NULL,'soufiane@gmail.com','0','0000-00-00','1','1',NULL,NULL,NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,19,50,0,'2018-08-06 15:06:27','2018-08-06 15:09:57',0,'2018-08-06 15:06:27'),(26637685,'shpm-10013@yopmail.com',1,'','','shpm-10013@yopmail.com','0','0000-00-00','0','1','','fr',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:15',0,'2018-08-07 10:21:10'),(26637687,'shpm-10015@yopmail.com',1,'','','shpm-10015@yopmail.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:16',0,'2018-08-07 10:21:10'),(26637688,'shpm-10016@mailinator.com',1,'','','shpm-10016@mailinator.com','0','0000-00-00','0','1','','en',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,82,50,0,'2018-08-07 10:21:10','2018-08-07 10:21:16',0,'2018-08-07 10:21:10'),(26637738,'rezrez@gmail.com',1,'','','rezrez@gmail.com','0','0000-00-00','','','','',NULL,NULL,'1',2,'0000-00-00 00:00:00','ok','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,68,50,0,'2018-08-14 15:58:23','2018-08-16 15:46:19',0,'2018-08-14 15:58:23');
/*!40000 ALTER TABLE `shops_customers_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_10`
--

DROP TABLE IF EXISTS `shops_customers_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_10` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `perfect_timing` time DEFAULT NULL,
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26637761 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_10`
--

LOCK TABLES `shops_customers_10` WRITE;
/*!40000 ALTER TABLE `shops_customers_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_2`
--

DROP TABLE IF EXISTS `shops_customers_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_2` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633183 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_2`
--

LOCK TABLES `shops_customers_2` WRITE;
/*!40000 ALTER TABLE `shops_customers_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_3`
--

DROP TABLE IF EXISTS `shops_customers_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_3` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633183 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_3`
--

LOCK TABLES `shops_customers_3` WRITE;
/*!40000 ALTER TABLE `shops_customers_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_4`
--

DROP TABLE IF EXISTS `shops_customers_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_4` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633183 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_4`
--

LOCK TABLES `shops_customers_4` WRITE;
/*!40000 ALTER TABLE `shops_customers_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_5`
--

DROP TABLE IF EXISTS `shops_customers_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_5` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633183 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_5`
--

LOCK TABLES `shops_customers_5` WRITE;
/*!40000 ALTER TABLE `shops_customers_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_6`
--

DROP TABLE IF EXISTS `shops_customers_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_6` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633183 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_6`
--

LOCK TABLES `shops_customers_6` WRITE;
/*!40000 ALTER TABLE `shops_customers_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_7`
--

DROP TABLE IF EXISTS `shops_customers_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_7` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633183 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_7`
--

LOCK TABLES `shops_customers_7` WRITE;
/*!40000 ALTER TABLE `shops_customers_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_8`
--

DROP TABLE IF EXISTS `shops_customers_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_8` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633218 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_8`
--

LOCK TABLES `shops_customers_8` WRITE;
/*!40000 ALTER TABLE `shops_customers_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_9`
--

DROP TABLE IF EXISTS `shops_customers_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_9` (
  `id_shop_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_shop` varchar(150) DEFAULT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('0','1','2') NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `opt_in` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter` enum('0','1','2') NOT NULL DEFAULT '0',
  `country` varchar(2) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `type` tinyint(3) NOT NULL,
  `last_check_bounce` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `last_date_reject` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_view` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_click` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_order` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `custom1` text DEFAULT NULL,
  `custom2` text DEFAULT NULL,
  `custom3` text DEFAULT NULL,
  `custom4` text DEFAULT NULL,
  `rating_initial` int(10) unsigned DEFAULT 0,
  `rating_dynamic` int(10) unsigned DEFAULT 45,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `unsub_type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer`),
  UNIQUE KEY `id_shop_customer` (`id_shop_customer`,`date`),
  UNIQUE KEY `email` (`email`,`id_shop`,`date`),
  UNIQUE KEY `id_customer_shop` (`id_customer_shop`,`id_shop`,`date`),
  KEY `gender` (`gender`),
  KEY `birthday` (`birthday`),
  KEY `opt_in` (`opt_in`),
  KEY `newsletter` (`newsletter`),
  KEY `country` (`country`),
  KEY `lang` (`lang`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `active` (`active`),
  KEY `type` (`type`),
  KEY `id_shop` (`id_shop`),
  KEY `last_check_bounce` (`last_check_bounce`,`id_shop_customer`,`email`),
  KEY `id_shop_newsletter_type` (`id_shop`,`newsletter`,`type`),
  KEY `last_date_reject` (`last_date_reject`,`master_reject_type`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `rating_initial` (`rating_initial`),
  KEY `rating_dynamic` (`rating_dynamic`),
  KEY `id_shop_email` (`id_shop`,`email`),
  KEY `id_shop_type_master_reject_type` (`id_shop`,`type`,`master_reject_type`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`),
  KEY `id_shop_first_name_last_name_email` (`id_shop`,`last_name`,`first_name`,`email`),
  KEY `date_modification` (`date_modification`),
  KEY `id_shop_first_name_last_name_email_date_modification` (`id_shop`,`last_name`,`first_name`,`email`,`date_modification`),
  KEY `base` (`active`,`deleted`,`master_reject_type`,`newsletter`,`type`),
  KEY `tmp_gender_type` (`gender`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_date_type` (`date`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_birthday_type` (`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_gender_birthday_type` (`gender`,`birthday`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_lang_type` (`active`,`master_reject_type`,`lang`,`id_shop_customer`,`type`),
  KEY `tmp_type` (`active`,`master_reject_type`,`type`,`id_shop_customer`,`lang`),
  KEY `tmp_reject_type` (`active`,`master_reject_type`,`id_shop_customer`,`type`),
  KEY `tmp_last_view` (`last_view`,`lang`),
  KEY `tmp_last_click` (`last_click`,`lang`),
  KEY `tmp_last_order` (`last_order`,`lang`)
) ENGINE=InnoDB AUTO_INCREMENT=26633218 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_9`
--

LOCK TABLES `shops_customers_9` WRITE;
/*!40000 ALTER TABLE `shops_customers_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_1`
--

DROP TABLE IF EXISTS `shops_customers_addresses_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_1` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075459 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_1`
--

LOCK TABLES `shops_customers_addresses_1` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_1` DISABLE KEYS */;
INSERT INTO `shops_customers_addresses_1` VALUES (74075308,26633181,1,1,'******','******','******','******','******','******','******','******','','******','**',NULL,NULL,'','1','2018-04-04 12:27:16','2018-05-25 16:34:04','2018-04-03 11:34:22'),(74075309,26633181,1,4,'******','******','******','******','******','******','******','******','','******','**',NULL,NULL,'','1','2018-04-04 12:27:16','2018-05-25 16:34:04','2018-04-03 11:34:22'),(74075393,26634526,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:47:00'),(74075408,26637692,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:48:00'),(74075394,26637677,1,NULL,'test1','test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075395,26637678,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075396,26637679,1,NULL,'test1','test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075397,26637680,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075398,26637681,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075399,26637683,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075400,26637684,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075402,26637686,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075405,26637689,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075406,26637690,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075407,26637691,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075409,26637693,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075410,26637694,1,NULL,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075411,26637695,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075412,26637696,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075423,26637707,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-07-24 12:50:00'),(74075310,26633311,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'00',NULL,NULL,'0','1','2018-07-25 09:09:53','2018-07-25 09:09:53','2018-07-25 09:09:53'),(74075311,26633335,1,NULL,'soufiane','bennani',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'fr',NULL,NULL,NULL,'1','2018-07-31 14:37:50','2018-07-31 14:37:50','2018-07-31 14:37:50'),(74075346,26634528,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-05 01:00:22','2018-08-05 01:00:22','2018-07-31 17:07:00'),(74075348,26634530,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-05 01:00:22','2018-08-05 01:00:22','2018-07-31 17:07:00'),(74075351,26634533,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-05 01:00:22','2018-08-05 01:00:22','2018-07-31 17:07:00'),(74075357,26634539,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-05 01:00:22','2018-08-05 01:00:22','2018-07-31 17:07:00'),(74075359,26634541,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-05 01:00:22','2018-08-05 01:00:22','2018-07-31 17:07:00'),(74075380,26634527,1,NULL,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075381,26634529,1,NULL,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075383,26634532,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075384,26634534,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075385,26634535,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075386,26634536,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075387,26634537,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075388,26634538,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075389,26634540,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075390,26634542,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075391,26634543,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075392,26634544,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-07-31 17:07:00'),(74075378,26635719,1,NULL,'soufiane','bennani',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'FR',NULL,NULL,NULL,'1','2018-08-06 11:35:34','2018-08-06 11:35:34','2018-08-01 10:02:00'),(74075382,26634531,1,NULL,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-06 15:24:53','2018-08-06 15:24:53','2018-08-06 15:24:53'),(74075401,26637685,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-08-07 10:21:10'),(74075403,26637687,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-08-07 10:21:10'),(74075404,26637688,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-07 10:21:10','2018-08-07 10:21:10','2018-08-07 10:21:10'),(74075446,26637699,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075447,26637700,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075448,26637701,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075449,26637702,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075450,26637703,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075451,26637704,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075452,26637705,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075453,26637706,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075454,26637738,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'1','2018-08-14 15:58:23','2018-08-14 15:58:23','2018-08-14 15:58:23'),(74075457,26637697,1,NULL,NULL,NULL,'65116526556',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'20',NULL,NULL,NULL,'1','2018-08-16 15:46:19','2018-08-16 15:46:19','2018-08-16 15:46:19'),(74075458,26637698,1,NULL,NULL,NULL,'663658345',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'20',NULL,NULL,NULL,'1','2018-08-16 15:46:19','2018-08-16 15:46:19','2018-08-16 15:46:19');
/*!40000 ALTER TABLE `shops_customers_addresses_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_10`
--

DROP TABLE IF EXISTS `shops_customers_addresses_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_10` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075459 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_10`
--

LOCK TABLES `shops_customers_addresses_10` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_2`
--

DROP TABLE IF EXISTS `shops_customers_addresses_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_2` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_2`
--

LOCK TABLES `shops_customers_addresses_2` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_3`
--

DROP TABLE IF EXISTS `shops_customers_addresses_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_3` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_3`
--

LOCK TABLES `shops_customers_addresses_3` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_4`
--

DROP TABLE IF EXISTS `shops_customers_addresses_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_4` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_4`
--

LOCK TABLES `shops_customers_addresses_4` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_5`
--

DROP TABLE IF EXISTS `shops_customers_addresses_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_5` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_5`
--

LOCK TABLES `shops_customers_addresses_5` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_6`
--

DROP TABLE IF EXISTS `shops_customers_addresses_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_6` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_6`
--

LOCK TABLES `shops_customers_addresses_6` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_7`
--

DROP TABLE IF EXISTS `shops_customers_addresses_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_7` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_7`
--

LOCK TABLES `shops_customers_addresses_7` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_8`
--

DROP TABLE IF EXISTS `shops_customers_addresses_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_8` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_8`
--

LOCK TABLES `shops_customers_addresses_8` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_addresses_9`
--

DROP TABLE IF EXISTS `shops_customers_addresses_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_addresses_9` (
  `id_shop_customer_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone1` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_address`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`,`date`),
  UNIQUE KEY `id_address_shop` (`id_address_shop`,`id_shop`,`date`),
  KEY `postcode` (`postcode`,`country`),
  KEY `country` (`country`),
  KEY `region` (`region`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_customer_id_shop` (`id_shop`,`id_shop_customer`),
  KEY `phone1_phone2_active` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `latitude` (`latitude`,`id_shop_customer`),
  KEY `tmp_ind_addresses` (`id_shop_customer`,`active`,`phone1`,`phone2`),
  KEY `tmp_ind_latlon` (`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=74075310 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_addresses_9`
--

LOCK TABLES `shops_customers_addresses_9` WRITE;
/*!40000 ALTER TABLE `shops_customers_addresses_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_addresses_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_1`
--

DROP TABLE IF EXISTS `shops_customers_groups_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_1` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606062 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_1`
--

LOCK TABLES `shops_customers_groups_1` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_1` DISABLE KEYS */;
INSERT INTO `shops_customers_groups_1` VALUES (19606055,26633181,1,'3','2018-04-04 12:27:16','2018-04-03 11:34:22'),(19606056,26633249,1,'3','2018-07-06 12:19:28','2018-07-06 12:19:08'),(19606057,26633275,1,'3','2018-07-11 19:55:40','2018-07-06 12:19:08'),(19606058,26633298,1,'3','2018-07-17 09:11:44','2018-07-06 12:19:08'),(19606059,26633315,1,'3','2018-07-27 17:22:40','2018-07-27 17:22:39'),(19606060,26633312,1,'3','2018-07-27 17:30:33','2018-07-27 17:30:32'),(19606061,26635716,1,'3','2018-08-01 09:23:11','2018-07-27 17:30:32');
/*!40000 ALTER TABLE `shops_customers_groups_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_10`
--

DROP TABLE IF EXISTS `shops_customers_groups_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_10` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606062 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_10`
--

LOCK TABLES `shops_customers_groups_10` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_2`
--

DROP TABLE IF EXISTS `shops_customers_groups_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_2` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_2`
--

LOCK TABLES `shops_customers_groups_2` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_3`
--

DROP TABLE IF EXISTS `shops_customers_groups_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_3` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_3`
--

LOCK TABLES `shops_customers_groups_3` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_4`
--

DROP TABLE IF EXISTS `shops_customers_groups_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_4` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_4`
--

LOCK TABLES `shops_customers_groups_4` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_5`
--

DROP TABLE IF EXISTS `shops_customers_groups_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_5` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_5`
--

LOCK TABLES `shops_customers_groups_5` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_6`
--

DROP TABLE IF EXISTS `shops_customers_groups_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_6` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_6`
--

LOCK TABLES `shops_customers_groups_6` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_7`
--

DROP TABLE IF EXISTS `shops_customers_groups_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_7` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_7`
--

LOCK TABLES `shops_customers_groups_7` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_8`
--

DROP TABLE IF EXISTS `shops_customers_groups_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_8` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_8`
--

LOCK TABLES `shops_customers_groups_8` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_9`
--

DROP TABLE IF EXISTS `shops_customers_groups_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_9` (
  `id_shop_customer_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group`),
  UNIQUE KEY `id_shop_customer_group` (`id_shop_customer_group`,`date`),
  UNIQUE KEY `id_shop_customer_group_2` (`id_shop_customer`,`id_shop`,`group`,`date`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `group` (`group`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp_ind_groups` (`id_shop_customer`,`group`),
  KEY `tmp_ind_groups_group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=19606056 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_9`
--

LOCK TABLES `shops_customers_groups_9` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_list_1`
--

DROP TABLE IF EXISTS `shops_customers_groups_list_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_list_1` (
  `id_shop_customer_group_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group_list`),
  UNIQUE KEY `id_shop_customer_group_list` (`id_shop_customer_group_list`,`date`),
  UNIQUE KEY `id_group` (`id_group`,`lang`,`id_shop`,`date`),
  KEY `lang` (`lang`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_date_modification` (`id_shop`,`date_modification`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_list_1`
--

LOCK TABLES `shops_customers_groups_list_1` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_list_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_list_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_groups_list_10`
--

DROP TABLE IF EXISTS `shops_customers_groups_list_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_groups_list_10` (
  `id_shop_customer_group_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_customer_group_list`),
  UNIQUE KEY `id_shop_customer_group_list` (`id_shop_customer_group_list`,`date`),
  UNIQUE KEY `id_group` (`id_group`,`lang`,`id_shop`,`date`),
  KEY `lang` (`lang`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_date_modification` (`id_shop`,`date_modification`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_groups_list_10`
--

LOCK TABLES `shops_customers_groups_list_10` WRITE;
/*!40000 ALTER TABLE `shops_customers_groups_list_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_groups_list_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_1`
--

DROP TABLE IF EXISTS `shops_customers_phones_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_1` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040992 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_1`
--

LOCK TABLES `shops_customers_phones_1` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_1` DISABLE KEYS */;
INSERT INTO `shops_customers_phones_1` VALUES (267040977,74075308,26633181,1,NULL,'**','******','******',0,NULL,NULL,'0'),(267040990,74075457,26637697,1,NULL,'201','','',0,NULL,NULL,'1'),(267040991,74075458,26637698,1,NULL,'201','','',0,NULL,NULL,'1');
/*!40000 ALTER TABLE `shops_customers_phones_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_10`
--

DROP TABLE IF EXISTS `shops_customers_phones_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_10` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040992 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_10`
--

LOCK TABLES `shops_customers_phones_10` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_2`
--

DROP TABLE IF EXISTS `shops_customers_phones_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_2` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_2`
--

LOCK TABLES `shops_customers_phones_2` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_3`
--

DROP TABLE IF EXISTS `shops_customers_phones_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_3` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_3`
--

LOCK TABLES `shops_customers_phones_3` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_4`
--

DROP TABLE IF EXISTS `shops_customers_phones_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_4` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_4`
--

LOCK TABLES `shops_customers_phones_4` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_5`
--

DROP TABLE IF EXISTS `shops_customers_phones_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_5` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_5`
--

LOCK TABLES `shops_customers_phones_5` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_6`
--

DROP TABLE IF EXISTS `shops_customers_phones_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_6` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_6`
--

LOCK TABLES `shops_customers_phones_6` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_7`
--

DROP TABLE IF EXISTS `shops_customers_phones_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_7` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_7`
--

LOCK TABLES `shops_customers_phones_7` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_8`
--

DROP TABLE IF EXISTS `shops_customers_phones_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_8` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_8`
--

LOCK TABLES `shops_customers_phones_8` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_customers_phones_9`
--

DROP TABLE IF EXISTS `shops_customers_phones_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_customers_phones_9` (
  `id_shop_customer_phone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_customer_address` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_address_shop` int(10) unsigned DEFAULT NULL,
  `country` char(3) DEFAULT NULL,
  `mobile1` varchar(32) DEFAULT NULL,
  `mobile2` varchar(32) DEFAULT NULL,
  `mobile_count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_shop_customer_phone`),
  UNIQUE KEY `id_shop_customer_address` (`id_shop_customer_address`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_mobile_count` (`id_shop`,`mobile_count`,`active`),
  KEY `id_shop_mobile_count_latitude_longitude_active` (`id_shop`,`mobile_count`,`latitude`,`longitude`,`active`),
  KEY `tmp_phones` (`mobile1`),
  KEY `tmp_ind_phones` (`id_shop_customer`,`mobile_count`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=267040982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_customers_phones_9`
--

LOCK TABLES `shops_customers_phones_9` WRITE;
/*!40000 ALTER TABLE `shops_customers_phones_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_customers_phones_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_1`
--

DROP TABLE IF EXISTS `shops_lists_customers_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_1` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8294139 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_1`
--

LOCK TABLES `shops_lists_customers_1` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_1` DISABLE KEYS */;
INSERT INTO `shops_lists_customers_1` VALUES (8290107,1285,26635717,'ok',NULL,NULL),(8290108,1285,26635719,'ok',NULL,NULL),(8290109,1285,26634526,'ok',NULL,NULL),(8290110,1285,26634527,'ok',NULL,NULL),(8290111,1285,26634528,'ok',NULL,NULL),(8290112,1285,26634529,'ok',NULL,NULL),(8290113,1285,26634530,'ok',NULL,NULL),(8290114,1285,26634531,'ok',NULL,NULL),(8290115,1285,26634532,'ok',NULL,NULL),(8290116,1285,26634533,'ok',NULL,NULL),(8290117,1285,26634534,'ok',NULL,NULL),(8290118,1285,26634535,'ok',NULL,NULL),(8290119,1285,26634536,'ok',NULL,NULL),(8290120,1285,26634537,'ok',NULL,NULL),(8290121,1285,26634538,'ok',NULL,NULL),(8290122,1285,26634539,'ok',NULL,NULL),(8290123,1285,26634540,'ok',NULL,NULL),(8290124,1285,26634541,'ok',NULL,NULL),(8290125,1285,26634542,'ok',NULL,NULL),(8290126,1285,26634543,'ok',NULL,NULL),(8290127,1285,26634544,'ok',NULL,NULL),(8290128,1285,26634545,'ok',NULL,NULL),(8290129,1285,26634546,'ok',NULL,NULL),(8290130,1285,26634547,'ok',NULL,NULL),(8290131,1285,26634548,'ok',NULL,NULL),(8290132,1285,26634549,'ok',NULL,NULL),(8290133,1285,26634550,'ok',NULL,NULL),(8290134,1285,26634551,'ok',NULL,NULL),(8290135,1285,26634552,'ok',NULL,NULL),(8290136,1285,26634553,'ok',NULL,NULL),(8290137,1285,26634554,'ok',NULL,NULL),(8290138,1285,26634555,'ok',NULL,NULL),(8290139,1285,26634556,'ok',NULL,NULL),(8290140,1285,26634557,'ok',NULL,NULL),(8290141,1285,26634558,'ok',NULL,NULL),(8290142,1285,26634559,'ok',NULL,NULL),(8290143,1285,26634560,'ok',NULL,NULL),(8290144,1285,26634561,'ok',NULL,NULL),(8290145,1285,26634562,'ok',NULL,NULL),(8290146,1285,26634563,'ok',NULL,NULL),(8290147,1285,26634564,'ok',NULL,NULL),(8290148,1285,26634565,'ok',NULL,NULL),(8290149,1285,26634566,'ok',NULL,NULL),(8290150,1285,26634567,'ok',NULL,NULL),(8290151,1285,26634568,'ok',NULL,NULL),(8290152,1285,26634569,'ok',NULL,NULL),(8290153,1285,26634570,'ok',NULL,NULL),(8290154,1285,26634571,'ok',NULL,NULL),(8290155,1285,26634572,'ok',NULL,NULL),(8290156,1285,26634573,'ok',NULL,NULL),(8290157,1285,26634574,'ok',NULL,NULL),(8290158,1285,26634575,'ok',NULL,NULL),(8290159,1285,26634576,'ok',NULL,NULL),(8290160,1285,26634577,'ok',NULL,NULL),(8290161,1285,26634578,'ok',NULL,NULL),(8290162,1285,26634579,'ok',NULL,NULL),(8290163,1285,26634580,'ok',NULL,NULL),(8290164,1285,26634581,'ok',NULL,NULL),(8290165,1285,26634582,'ok',NULL,NULL),(8290166,1285,26634583,'ok',NULL,NULL),(8290167,1285,26634584,'ok',NULL,NULL),(8290168,1285,26634585,'ok',NULL,NULL),(8290169,1285,26634586,'ok',NULL,NULL),(8290170,1285,26634587,'ok',NULL,NULL),(8290171,1285,26634588,'ok',NULL,NULL),(8290172,1285,26634589,'ok',NULL,NULL),(8290173,1285,26634590,'ok',NULL,NULL),(8290174,1285,26634591,'ok',NULL,NULL),(8290175,1285,26634592,'ok',NULL,NULL),(8290176,1285,26634593,'ok',NULL,NULL),(8290177,1285,26634594,'ok',NULL,NULL),(8290178,1285,26634595,'ok',NULL,NULL),(8290179,1285,26634596,'ok',NULL,NULL),(8290180,1285,26634597,'ok',NULL,NULL),(8290181,1285,26634598,'ok',NULL,NULL),(8290182,1285,26634599,'ok',NULL,NULL),(8290183,1285,26634600,'ok',NULL,NULL),(8290184,1285,26634601,'ok',NULL,NULL),(8290185,1285,26634602,'ok',NULL,NULL),(8290186,1285,26634603,'ok',NULL,NULL),(8290187,1285,26634604,'ok',NULL,NULL),(8290188,1285,26634605,'ok',NULL,NULL),(8290189,1285,26634606,'ok',NULL,NULL),(8290190,1285,26634607,'ok',NULL,NULL),(8290191,1285,26634608,'ok',NULL,NULL),(8290192,1285,26634609,'ok',NULL,NULL),(8290193,1285,26634610,'ok',NULL,NULL),(8290194,1285,26634611,'ok',NULL,NULL),(8290195,1285,26634612,'ok',NULL,NULL),(8290196,1285,26634613,'ok',NULL,NULL),(8290197,1285,26634614,'ok',NULL,NULL),(8290198,1285,26634615,'ok',NULL,NULL),(8290199,1285,26634616,'ok',NULL,NULL),(8290200,1285,26634617,'ok',NULL,NULL),(8290201,1285,26634618,'ok',NULL,NULL),(8290202,1285,26634619,'ok',NULL,NULL),(8290203,1285,26634620,'ok',NULL,NULL),(8290204,1285,26634621,'ok',NULL,NULL),(8290205,1285,26634622,'ok',NULL,NULL),(8290206,1285,26634623,'ok',NULL,NULL),(8290207,1285,26634624,'ok',NULL,NULL),(8290208,1285,26634625,'ok',NULL,NULL),(8290209,1285,26634626,'ok',NULL,NULL),(8290210,1285,26634627,'ok',NULL,NULL),(8290211,1285,26634628,'ok',NULL,NULL),(8290212,1285,26634629,'ok',NULL,NULL),(8290213,1285,26634630,'ok',NULL,NULL),(8290214,1285,26634631,'ok',NULL,NULL),(8290215,1285,26634632,'ok',NULL,NULL),(8290216,1285,26634633,'ok',NULL,NULL),(8290217,1285,26634634,'ok',NULL,NULL),(8290218,1285,26634635,'ok',NULL,NULL),(8290219,1285,26634636,'ok',NULL,NULL),(8290220,1285,26634637,'ok',NULL,NULL),(8290221,1285,26634638,'ok',NULL,NULL),(8290222,1285,26634639,'ok',NULL,NULL),(8290223,1285,26634640,'ok',NULL,NULL),(8290224,1285,26634641,'ok',NULL,NULL),(8290225,1285,26634642,'ok',NULL,NULL),(8290226,1285,26634643,'ok',NULL,NULL),(8290227,1285,26634644,'ok',NULL,NULL),(8290228,1285,26634645,'ok',NULL,NULL),(8290229,1285,26634646,'ok',NULL,NULL),(8290230,1285,26634647,'ok',NULL,NULL),(8290231,1285,26634648,'ok',NULL,NULL),(8290232,1285,26634649,'ok',NULL,NULL),(8290233,1285,26634650,'ok',NULL,NULL),(8290234,1285,26634651,'ok',NULL,NULL),(8290235,1285,26634652,'ok',NULL,NULL),(8290236,1285,26634653,'ok',NULL,NULL),(8290237,1285,26634654,'ok',NULL,NULL),(8290238,1285,26634655,'ok',NULL,NULL),(8290239,1285,26634656,'ok',NULL,NULL),(8290240,1285,26634657,'ok',NULL,NULL),(8290241,1285,26634658,'ok',NULL,NULL),(8290242,1285,26634659,'ok',NULL,NULL),(8290243,1285,26634660,'ok',NULL,NULL),(8290244,1285,26634661,'ok',NULL,NULL),(8290245,1285,26634662,'ok',NULL,NULL),(8290246,1285,26634663,'ok',NULL,NULL),(8290247,1285,26634664,'ok',NULL,NULL),(8290248,1285,26634665,'ok',NULL,NULL),(8290249,1285,26634666,'ok',NULL,NULL),(8290250,1285,26634667,'ok',NULL,NULL),(8290251,1285,26634668,'ok',NULL,NULL),(8290252,1285,26634669,'ok',NULL,NULL),(8290253,1285,26634670,'ok',NULL,NULL),(8290254,1285,26634671,'ok',NULL,NULL),(8290255,1285,26634672,'ok',NULL,NULL),(8290256,1285,26634673,'ok',NULL,NULL),(8290257,1285,26634674,'ok',NULL,NULL),(8290258,1285,26634675,'ok',NULL,NULL),(8290259,1285,26634676,'ok',NULL,NULL),(8290260,1285,26634677,'ok',NULL,NULL),(8290261,1285,26634678,'ok',NULL,NULL),(8290262,1285,26634679,'ok',NULL,NULL),(8290263,1285,26634680,'ok',NULL,NULL),(8290264,1285,26634681,'ok',NULL,NULL),(8290265,1285,26634682,'ok',NULL,NULL),(8290266,1285,26634683,'ok',NULL,NULL),(8290267,1285,26634684,'ok',NULL,NULL),(8290268,1285,26634685,'ok',NULL,NULL),(8290269,1285,26634686,'ok',NULL,NULL),(8290270,1285,26634687,'ok',NULL,NULL),(8290271,1285,26634688,'ok',NULL,NULL),(8290272,1285,26634689,'ok',NULL,NULL),(8290273,1285,26634690,'ok',NULL,NULL),(8290274,1285,26634691,'ok',NULL,NULL),(8290275,1285,26634692,'ok',NULL,NULL),(8290276,1285,26634693,'ok',NULL,NULL),(8290277,1285,26634694,'ok',NULL,NULL),(8290278,1285,26634695,'ok',NULL,NULL),(8290279,1285,26634696,'ok',NULL,NULL),(8290280,1285,26634697,'ok',NULL,NULL),(8290281,1285,26634698,'ok',NULL,NULL),(8290282,1285,26634699,'ok',NULL,NULL),(8290283,1285,26634700,'ok',NULL,NULL),(8290284,1285,26634701,'ok',NULL,NULL),(8290285,1285,26634702,'ok',NULL,NULL),(8290286,1285,26634703,'ok',NULL,NULL),(8290287,1285,26634704,'ok',NULL,NULL),(8290288,1285,26634705,'ok',NULL,NULL),(8290289,1285,26634706,'ok',NULL,NULL),(8290290,1285,26634707,'ok',NULL,NULL),(8290291,1285,26634708,'ok',NULL,NULL),(8290292,1285,26634709,'ok',NULL,NULL),(8290293,1285,26634710,'ok',NULL,NULL),(8290294,1285,26634711,'ok',NULL,NULL),(8290295,1285,26634712,'ok',NULL,NULL),(8290296,1285,26634713,'ok',NULL,NULL),(8290297,1285,26634714,'ok',NULL,NULL),(8290298,1285,26634715,'ok',NULL,NULL),(8290299,1285,26634716,'ok',NULL,NULL),(8290300,1285,26634717,'ok',NULL,NULL),(8290301,1285,26634718,'ok',NULL,NULL),(8290302,1285,26634719,'ok',NULL,NULL),(8290303,1285,26634720,'ok',NULL,NULL),(8290304,1285,26634721,'ok',NULL,NULL),(8290305,1285,26634722,'ok',NULL,NULL),(8290306,1285,26634723,'ok',NULL,NULL),(8290307,1285,26634724,'ok',NULL,NULL),(8290308,1285,26634725,'ok',NULL,NULL),(8290309,1285,26634726,'ok',NULL,NULL),(8290310,1285,26634727,'ok',NULL,NULL),(8290311,1285,26634728,'ok',NULL,NULL),(8290312,1285,26634729,'ok',NULL,NULL),(8290313,1285,26634730,'ok',NULL,NULL),(8290314,1285,26634731,'ok',NULL,NULL),(8290315,1285,26634732,'ok',NULL,NULL),(8290316,1285,26634733,'ok',NULL,NULL),(8290317,1285,26634734,'ok',NULL,NULL),(8290318,1285,26634735,'ok',NULL,NULL),(8290319,1285,26634736,'ok',NULL,NULL),(8290320,1285,26634737,'ok',NULL,NULL),(8290321,1285,26634738,'ok',NULL,NULL),(8290322,1285,26634739,'ok',NULL,NULL),(8290323,1285,26634740,'ok',NULL,NULL),(8290324,1285,26634741,'ok',NULL,NULL),(8290325,1285,26634742,'ok',NULL,NULL),(8290326,1285,26634743,'ok',NULL,NULL),(8290327,1285,26634744,'ok',NULL,NULL),(8290328,1285,26634745,'ok',NULL,NULL),(8290329,1285,26634746,'ok',NULL,NULL),(8290330,1285,26634747,'ok',NULL,NULL),(8290331,1285,26634748,'ok',NULL,NULL),(8290332,1285,26634749,'ok',NULL,NULL),(8290333,1285,26634750,'ok',NULL,NULL),(8290334,1285,26634751,'ok',NULL,NULL),(8290335,1285,26634752,'ok',NULL,NULL),(8290336,1285,26634753,'ok',NULL,NULL),(8290337,1285,26634754,'ok',NULL,NULL),(8290338,1285,26634755,'ok',NULL,NULL),(8290339,1285,26634756,'ok',NULL,NULL),(8290340,1285,26634757,'ok',NULL,NULL),(8290341,1285,26634758,'ok',NULL,NULL),(8290342,1285,26634759,'ok',NULL,NULL),(8290343,1285,26634760,'ok',NULL,NULL),(8290344,1285,26634761,'ok',NULL,NULL),(8290345,1285,26634762,'ok',NULL,NULL),(8290346,1285,26634763,'ok',NULL,NULL),(8290347,1285,26634764,'ok',NULL,NULL),(8290348,1285,26634765,'ok',NULL,NULL),(8290349,1285,26634766,'ok',NULL,NULL),(8290350,1285,26634767,'ok',NULL,NULL),(8290351,1285,26634768,'ok',NULL,NULL),(8290352,1285,26634769,'ok',NULL,NULL),(8290353,1285,26634770,'ok',NULL,NULL),(8290354,1285,26634771,'ok',NULL,NULL),(8290355,1285,26634772,'ok',NULL,NULL),(8290356,1285,26634773,'ok',NULL,NULL),(8290357,1285,26634774,'ok',NULL,NULL),(8290358,1285,26634775,'ok',NULL,NULL),(8290359,1285,26634776,'ok',NULL,NULL),(8290360,1285,26634777,'ok',NULL,NULL),(8290361,1285,26634778,'ok',NULL,NULL),(8290362,1285,26634779,'ok',NULL,NULL),(8290363,1285,26634780,'ok',NULL,NULL),(8290364,1285,26634781,'ok',NULL,NULL),(8290365,1285,26634782,'ok',NULL,NULL),(8290366,1285,26634783,'ok',NULL,NULL),(8290367,1285,26634784,'ok',NULL,NULL),(8290368,1285,26634785,'ok',NULL,NULL),(8290369,1285,26634786,'ok',NULL,NULL),(8290370,1285,26634787,'ok',NULL,NULL),(8290371,1285,26634788,'ok',NULL,NULL),(8290372,1285,26634789,'ok',NULL,NULL),(8290373,1285,26634790,'ok',NULL,NULL),(8290374,1285,26634791,'ok',NULL,NULL),(8290375,1285,26634792,'ok',NULL,NULL),(8290376,1285,26634793,'ok',NULL,NULL),(8290377,1285,26634794,'ok',NULL,NULL),(8290378,1285,26634795,'ok',NULL,NULL),(8290379,1285,26634796,'ok',NULL,NULL),(8290380,1285,26634797,'ok',NULL,NULL),(8290381,1285,26634798,'ok',NULL,NULL),(8290382,1285,26634799,'ok',NULL,NULL),(8290383,1285,26634800,'ok',NULL,NULL),(8290384,1285,26634801,'ok',NULL,NULL),(8290385,1285,26634802,'ok',NULL,NULL),(8290386,1285,26634803,'ok',NULL,NULL),(8290387,1285,26634804,'ok',NULL,NULL),(8290388,1285,26634805,'ok',NULL,NULL),(8290389,1285,26634806,'ok',NULL,NULL),(8290390,1285,26634807,'ok',NULL,NULL),(8290391,1285,26634808,'ok',NULL,NULL),(8290392,1285,26634809,'ok',NULL,NULL),(8290393,1285,26634810,'ok',NULL,NULL),(8290394,1285,26634811,'ok',NULL,NULL),(8290395,1285,26634812,'ok',NULL,NULL),(8290396,1285,26634813,'ok',NULL,NULL),(8290397,1285,26634814,'ok',NULL,NULL),(8290398,1285,26634815,'ok',NULL,NULL),(8290399,1285,26634816,'ok',NULL,NULL),(8290400,1285,26634817,'ok',NULL,NULL),(8290401,1285,26634818,'ok',NULL,NULL),(8290402,1285,26634819,'ok',NULL,NULL),(8290403,1285,26634820,'ok',NULL,NULL),(8290404,1285,26634821,'ok',NULL,NULL),(8290405,1285,26634822,'ok',NULL,NULL),(8290406,1285,26634823,'ok',NULL,NULL),(8290407,1285,26634824,'ok',NULL,NULL),(8290408,1285,26634825,'ok',NULL,NULL),(8290409,1285,26634826,'ok',NULL,NULL),(8290410,1285,26634827,'ok',NULL,NULL),(8290411,1285,26634828,'ok',NULL,NULL),(8290412,1285,26634829,'ok',NULL,NULL),(8290413,1285,26634830,'ok',NULL,NULL),(8290414,1285,26634831,'ok',NULL,NULL),(8290415,1285,26634832,'ok',NULL,NULL),(8290416,1285,26634833,'ok',NULL,NULL),(8290417,1285,26634834,'ok',NULL,NULL),(8290418,1285,26634835,'ok',NULL,NULL),(8290419,1285,26634836,'ok',NULL,NULL),(8290420,1285,26634837,'ok',NULL,NULL),(8290421,1285,26634838,'ok',NULL,NULL),(8290422,1285,26634839,'ok',NULL,NULL),(8290423,1285,26634840,'ok',NULL,NULL),(8290424,1285,26634841,'ok',NULL,NULL),(8290425,1285,26634842,'ok',NULL,NULL),(8290426,1285,26634843,'ok',NULL,NULL),(8290427,1285,26634844,'ok',NULL,NULL),(8290428,1285,26634845,'ok',NULL,NULL),(8290429,1285,26634846,'ok',NULL,NULL),(8290430,1285,26634847,'ok',NULL,NULL),(8290431,1285,26634848,'ok',NULL,NULL),(8290432,1285,26634849,'ok',NULL,NULL),(8290433,1285,26634850,'ok',NULL,NULL),(8290434,1285,26634851,'ok',NULL,NULL),(8290435,1285,26634852,'ok',NULL,NULL),(8290436,1285,26634853,'ok',NULL,NULL),(8290437,1285,26634854,'ok',NULL,NULL),(8290438,1285,26634855,'ok',NULL,NULL),(8290439,1285,26634856,'ok',NULL,NULL),(8290440,1285,26634857,'ok',NULL,NULL),(8290441,1285,26634858,'ok',NULL,NULL),(8290442,1285,26634859,'ok',NULL,NULL),(8290443,1285,26634860,'ok',NULL,NULL),(8290444,1285,26634861,'ok',NULL,NULL),(8290445,1285,26634862,'ok',NULL,NULL),(8290446,1285,26634863,'ok',NULL,NULL),(8290447,1285,26634864,'ok',NULL,NULL),(8290448,1285,26634865,'ok',NULL,NULL),(8290449,1285,26634866,'ok',NULL,NULL),(8290450,1285,26634867,'ok',NULL,NULL),(8290451,1285,26634868,'ok',NULL,NULL),(8290452,1285,26634869,'ok',NULL,NULL),(8290453,1285,26634870,'ok',NULL,NULL),(8290454,1285,26634871,'ok',NULL,NULL),(8290455,1285,26634872,'ok',NULL,NULL),(8290456,1285,26634873,'ok',NULL,NULL),(8290457,1285,26634874,'ok',NULL,NULL),(8290458,1285,26634875,'ok',NULL,NULL),(8290459,1285,26634876,'ok',NULL,NULL),(8290460,1285,26634877,'ok',NULL,NULL),(8290461,1285,26634878,'ok',NULL,NULL),(8290462,1285,26634879,'ok',NULL,NULL),(8290463,1285,26634880,'ok',NULL,NULL),(8290464,1285,26634881,'ok',NULL,NULL),(8290465,1285,26634882,'ok',NULL,NULL),(8290466,1285,26634883,'ok',NULL,NULL),(8290467,1285,26634884,'ok',NULL,NULL),(8290468,1285,26634885,'ok',NULL,NULL),(8290469,1285,26634886,'ok',NULL,NULL),(8290470,1285,26634887,'ok',NULL,NULL),(8290471,1285,26634888,'ok',NULL,NULL),(8290472,1285,26634889,'ok',NULL,NULL),(8290473,1285,26634890,'ok',NULL,NULL),(8290474,1285,26634891,'ok',NULL,NULL),(8290475,1285,26634892,'ok',NULL,NULL),(8290476,1285,26634893,'ok',NULL,NULL),(8290477,1285,26634894,'ok',NULL,NULL),(8290478,1285,26634895,'ok',NULL,NULL),(8290479,1285,26634896,'ok',NULL,NULL),(8290480,1285,26634897,'ok',NULL,NULL),(8290481,1285,26634898,'ok',NULL,NULL),(8290482,1285,26634899,'ok',NULL,NULL),(8290483,1285,26634900,'ok',NULL,NULL),(8290484,1285,26634901,'ok',NULL,NULL),(8290485,1285,26634902,'ok',NULL,NULL),(8290486,1285,26634903,'ok',NULL,NULL),(8290487,1285,26634904,'ok',NULL,NULL),(8290488,1285,26634905,'ok',NULL,NULL),(8290489,1285,26634906,'ok',NULL,NULL),(8290490,1285,26634907,'ok',NULL,NULL),(8290491,1285,26634908,'ok',NULL,NULL),(8290492,1285,26634909,'ok',NULL,NULL),(8290493,1285,26634910,'ok',NULL,NULL),(8290494,1285,26634911,'ok',NULL,NULL),(8290495,1285,26634912,'ok',NULL,NULL),(8290496,1285,26634913,'ok',NULL,NULL),(8290497,1285,26634914,'ok',NULL,NULL),(8290498,1285,26634915,'ok',NULL,NULL),(8290499,1285,26634916,'ok',NULL,NULL),(8290500,1285,26634917,'ok',NULL,NULL),(8290501,1285,26634918,'ok',NULL,NULL),(8290502,1285,26634919,'ok',NULL,NULL),(8290503,1285,26634920,'ok',NULL,NULL),(8290504,1285,26634921,'ok',NULL,NULL),(8290505,1285,26634922,'ok',NULL,NULL),(8290506,1285,26634923,'ok',NULL,NULL),(8290507,1285,26634924,'ok',NULL,NULL),(8290508,1285,26634925,'ok',NULL,NULL),(8290509,1285,26634926,'ok',NULL,NULL),(8290510,1285,26634927,'ok',NULL,NULL),(8290511,1285,26634928,'ok',NULL,NULL),(8290512,1285,26634929,'ok',NULL,NULL),(8290513,1285,26634930,'ok',NULL,NULL),(8290514,1285,26634931,'ok',NULL,NULL),(8290515,1285,26634932,'ok',NULL,NULL),(8290516,1285,26634933,'ok',NULL,NULL),(8290517,1285,26634934,'ok',NULL,NULL),(8290518,1285,26634935,'ok',NULL,NULL),(8290519,1285,26634936,'ok',NULL,NULL),(8290520,1285,26634937,'ok',NULL,NULL),(8290521,1285,26634938,'ok',NULL,NULL),(8290522,1285,26634939,'ok',NULL,NULL),(8290523,1285,26634940,'ok',NULL,NULL),(8290524,1285,26634941,'ok',NULL,NULL),(8290525,1285,26634942,'ok',NULL,NULL),(8290526,1285,26634943,'ok',NULL,NULL),(8290527,1285,26634944,'ok',NULL,NULL),(8290528,1285,26634945,'ok',NULL,NULL),(8290529,1285,26634946,'ok',NULL,NULL),(8290530,1285,26634947,'ok',NULL,NULL),(8290531,1285,26634948,'ok',NULL,NULL),(8290532,1285,26634949,'ok',NULL,NULL),(8290533,1285,26634950,'ok',NULL,NULL),(8290534,1285,26634951,'ok',NULL,NULL),(8290535,1285,26634952,'ok',NULL,NULL),(8290536,1285,26634953,'ok',NULL,NULL),(8290537,1285,26634954,'ok',NULL,NULL),(8290538,1285,26634955,'ok',NULL,NULL),(8290539,1285,26634956,'ok',NULL,NULL),(8290540,1285,26634957,'ok',NULL,NULL),(8290541,1285,26634958,'ok',NULL,NULL),(8290542,1285,26634959,'ok',NULL,NULL),(8290543,1285,26634960,'ok',NULL,NULL),(8290544,1285,26634961,'ok',NULL,NULL),(8290545,1285,26634962,'ok',NULL,NULL),(8290546,1285,26634963,'ok',NULL,NULL),(8290547,1285,26634964,'ok',NULL,NULL),(8290548,1285,26634965,'ok',NULL,NULL),(8290549,1285,26634966,'ok',NULL,NULL),(8290550,1285,26634967,'ok',NULL,NULL),(8290551,1285,26634968,'ok',NULL,NULL),(8290552,1285,26634969,'ok',NULL,NULL),(8290553,1285,26634970,'ok',NULL,NULL),(8290554,1285,26634971,'ok',NULL,NULL),(8290555,1285,26634972,'ok',NULL,NULL),(8290556,1285,26634973,'ok',NULL,NULL),(8290557,1285,26634974,'ok',NULL,NULL),(8290558,1285,26634975,'ok',NULL,NULL),(8290559,1285,26634976,'ok',NULL,NULL),(8290560,1285,26634977,'ok',NULL,NULL),(8290561,1285,26634978,'ok',NULL,NULL),(8290562,1285,26634979,'ok',NULL,NULL),(8290563,1285,26634980,'ok',NULL,NULL),(8290564,1285,26634981,'ok',NULL,NULL),(8290565,1285,26634982,'ok',NULL,NULL),(8290566,1285,26634983,'ok',NULL,NULL),(8290567,1285,26634984,'ok',NULL,NULL),(8290568,1285,26634985,'ok',NULL,NULL),(8290569,1285,26634986,'ok',NULL,NULL),(8290570,1285,26634987,'ok',NULL,NULL),(8290571,1285,26634988,'ok',NULL,NULL),(8290572,1285,26634989,'ok',NULL,NULL),(8290573,1285,26634990,'ok',NULL,NULL),(8290574,1285,26634991,'ok',NULL,NULL),(8290575,1285,26634992,'ok',NULL,NULL),(8290576,1285,26634993,'ok',NULL,NULL),(8290577,1285,26634994,'ok',NULL,NULL),(8290578,1285,26634995,'ok',NULL,NULL),(8290579,1285,26634996,'ok',NULL,NULL),(8290580,1285,26634997,'ok',NULL,NULL),(8290581,1285,26634998,'ok',NULL,NULL),(8290582,1285,26634999,'ok',NULL,NULL),(8290583,1285,26635000,'ok',NULL,NULL),(8290584,1285,26635001,'ok',NULL,NULL),(8290585,1285,26635002,'ok',NULL,NULL),(8290586,1285,26635003,'ok',NULL,NULL),(8290587,1285,26635004,'ok',NULL,NULL),(8290588,1285,26635005,'ok',NULL,NULL),(8290589,1285,26635006,'ok',NULL,NULL),(8290590,1285,26635007,'ok',NULL,NULL),(8290591,1285,26635008,'ok',NULL,NULL),(8290592,1285,26635009,'ok',NULL,NULL),(8290593,1285,26635010,'ok',NULL,NULL),(8290594,1285,26635011,'ok',NULL,NULL),(8290595,1285,26635012,'ok',NULL,NULL),(8290596,1285,26635013,'ok',NULL,NULL),(8290597,1285,26635014,'ok',NULL,NULL),(8290598,1285,26635015,'ok',NULL,NULL),(8290599,1285,26635016,'ok',NULL,NULL),(8290600,1285,26635017,'ok',NULL,NULL),(8290601,1285,26635018,'ok',NULL,NULL),(8290602,1285,26635019,'ok',NULL,NULL),(8290603,1285,26635020,'ok',NULL,NULL),(8290604,1285,26635021,'ok',NULL,NULL),(8290605,1285,26635022,'ok',NULL,NULL),(8290606,1285,26635023,'ok',NULL,NULL),(8290607,1285,26635024,'ok',NULL,NULL),(8290608,1285,26635025,'ok',NULL,NULL),(8290609,1285,26635026,'ok',NULL,NULL),(8290610,1285,26635027,'ok',NULL,NULL),(8290611,1285,26635028,'ok',NULL,NULL),(8290612,1285,26635029,'ok',NULL,NULL),(8290613,1285,26635030,'ok',NULL,NULL),(8290614,1285,26635031,'ok',NULL,NULL),(8290615,1285,26635032,'ok',NULL,NULL),(8290616,1285,26635033,'ok',NULL,NULL),(8290617,1285,26635034,'ok',NULL,NULL),(8290618,1285,26635035,'ok',NULL,NULL),(8290619,1285,26635036,'ok',NULL,NULL),(8290620,1285,26635037,'ok',NULL,NULL),(8290621,1285,26635038,'ok',NULL,NULL),(8290622,1285,26635039,'ok',NULL,NULL),(8290623,1285,26635040,'ok',NULL,NULL),(8290624,1285,26635041,'ok',NULL,NULL),(8290625,1285,26635042,'ok',NULL,NULL),(8290626,1285,26635043,'ok',NULL,NULL),(8290627,1285,26635044,'ok',NULL,NULL),(8290628,1285,26635045,'ok',NULL,NULL),(8290629,1285,26635046,'ok',NULL,NULL),(8290630,1285,26635047,'ok',NULL,NULL),(8290631,1285,26635048,'ok',NULL,NULL),(8290632,1285,26635049,'ok',NULL,NULL),(8290633,1285,26635050,'ok',NULL,NULL),(8290634,1285,26635051,'ok',NULL,NULL),(8290635,1285,26635052,'ok',NULL,NULL),(8290636,1285,26635053,'ok',NULL,NULL),(8290637,1285,26635054,'ok',NULL,NULL),(8290638,1285,26635055,'ok',NULL,NULL),(8290639,1285,26635056,'ok',NULL,NULL),(8290640,1285,26635057,'ok',NULL,NULL),(8290641,1285,26635058,'ok',NULL,NULL),(8290642,1285,26635059,'ok',NULL,NULL),(8290643,1285,26635060,'ok',NULL,NULL),(8290644,1285,26635061,'ok',NULL,NULL),(8290645,1285,26635062,'ok',NULL,NULL),(8290646,1285,26635063,'ok',NULL,NULL),(8290647,1285,26635064,'ok',NULL,NULL),(8290648,1285,26635065,'ok',NULL,NULL),(8290649,1285,26635066,'ok',NULL,NULL),(8290650,1285,26635067,'ok',NULL,NULL),(8290651,1285,26635068,'ok',NULL,NULL),(8290652,1285,26635069,'ok',NULL,NULL),(8290653,1285,26635070,'ok',NULL,NULL),(8290654,1285,26635071,'ok',NULL,NULL),(8290655,1285,26635072,'ok',NULL,NULL),(8290656,1285,26635073,'ok',NULL,NULL),(8290657,1285,26635074,'ok',NULL,NULL),(8290658,1285,26635075,'ok',NULL,NULL),(8290659,1285,26635076,'ok',NULL,NULL),(8290660,1285,26635077,'ok',NULL,NULL),(8290661,1285,26635078,'ok',NULL,NULL),(8290662,1285,26635079,'ok',NULL,NULL),(8290663,1285,26635080,'ok',NULL,NULL),(8290664,1285,26635081,'ok',NULL,NULL),(8290665,1285,26635082,'ok',NULL,NULL),(8290666,1285,26635083,'ok',NULL,NULL),(8290667,1285,26635084,'ok',NULL,NULL),(8290668,1285,26635085,'ok',NULL,NULL),(8290669,1285,26635086,'ok',NULL,NULL),(8290670,1285,26635087,'ok',NULL,NULL),(8290671,1285,26635088,'ok',NULL,NULL),(8290672,1285,26635089,'ok',NULL,NULL),(8290673,1285,26635090,'ok',NULL,NULL),(8290674,1285,26635091,'ok',NULL,NULL),(8290675,1285,26635092,'ok',NULL,NULL),(8290676,1285,26635093,'ok',NULL,NULL),(8290677,1285,26635094,'ok',NULL,NULL),(8290678,1285,26635095,'ok',NULL,NULL),(8290679,1285,26635096,'ok',NULL,NULL),(8290680,1285,26635097,'ok',NULL,NULL),(8290681,1285,26635098,'ok',NULL,NULL),(8290682,1285,26635099,'ok',NULL,NULL),(8290683,1285,26635100,'ok',NULL,NULL),(8290684,1285,26635101,'ok',NULL,NULL),(8290685,1285,26635102,'ok',NULL,NULL),(8290686,1285,26635103,'ok',NULL,NULL),(8290687,1285,26635104,'ok',NULL,NULL),(8290688,1285,26635105,'ok',NULL,NULL),(8290689,1285,26635106,'ok',NULL,NULL),(8290690,1285,26635107,'ok',NULL,NULL),(8290691,1285,26635108,'ok',NULL,NULL),(8290692,1285,26635109,'ok',NULL,NULL),(8290693,1285,26635110,'ok',NULL,NULL),(8290694,1285,26635111,'ok',NULL,NULL),(8290695,1285,26635112,'ok',NULL,NULL),(8290696,1285,26635113,'ok',NULL,NULL),(8290697,1285,26635114,'ok',NULL,NULL),(8290698,1285,26635115,'ok',NULL,NULL),(8290699,1285,26635116,'ok',NULL,NULL),(8290700,1285,26635117,'ok',NULL,NULL),(8290701,1285,26635118,'ok',NULL,NULL),(8290702,1285,26635119,'ok',NULL,NULL),(8290703,1285,26635120,'ok',NULL,NULL),(8290704,1285,26635121,'ok',NULL,NULL),(8290705,1285,26635122,'ok',NULL,NULL),(8290706,1285,26635123,'ok',NULL,NULL),(8290707,1285,26635124,'ok',NULL,NULL),(8290708,1285,26635125,'ok',NULL,NULL),(8290709,1285,26635126,'ok',NULL,NULL),(8290710,1285,26635127,'ok',NULL,NULL),(8290711,1285,26635128,'ok',NULL,NULL),(8290712,1285,26635129,'ok',NULL,NULL),(8290713,1285,26635130,'ok',NULL,NULL),(8290714,1285,26635131,'ok',NULL,NULL),(8290715,1285,26635132,'ok',NULL,NULL),(8290716,1285,26635133,'ok',NULL,NULL),(8290717,1285,26635134,'ok',NULL,NULL),(8290718,1285,26635135,'ok',NULL,NULL),(8290719,1285,26635136,'ok',NULL,NULL),(8290720,1285,26635137,'ok',NULL,NULL),(8290721,1285,26635138,'ok',NULL,NULL),(8290722,1285,26635139,'ok',NULL,NULL),(8290723,1285,26635140,'ok',NULL,NULL),(8290724,1285,26635141,'ok',NULL,NULL),(8290725,1285,26635142,'ok',NULL,NULL),(8290726,1285,26635143,'ok',NULL,NULL),(8290727,1285,26635144,'ok',NULL,NULL),(8290728,1285,26635145,'ok',NULL,NULL),(8290729,1285,26635146,'ok',NULL,NULL),(8290730,1285,26635147,'ok',NULL,NULL),(8290731,1285,26635148,'ok',NULL,NULL),(8290732,1285,26635149,'ok',NULL,NULL),(8290733,1285,26635150,'ok',NULL,NULL),(8290734,1285,26635151,'ok',NULL,NULL),(8290735,1285,26635152,'ok',NULL,NULL),(8290736,1285,26635153,'ok',NULL,NULL),(8290737,1285,26635154,'ok',NULL,NULL),(8290738,1285,26635155,'ok',NULL,NULL),(8290739,1285,26635156,'ok',NULL,NULL),(8290740,1285,26635157,'ok',NULL,NULL),(8290741,1285,26635158,'ok',NULL,NULL),(8290742,1285,26635159,'ok',NULL,NULL),(8290743,1285,26635160,'ok',NULL,NULL),(8290744,1285,26635161,'ok',NULL,NULL),(8290745,1285,26635162,'ok',NULL,NULL),(8290746,1285,26635163,'ok',NULL,NULL),(8290747,1285,26635164,'ok',NULL,NULL),(8290748,1285,26635165,'ok',NULL,NULL),(8290749,1285,26635166,'ok',NULL,NULL),(8290750,1285,26635167,'ok',NULL,NULL),(8290751,1285,26635168,'ok',NULL,NULL),(8290752,1285,26635169,'ok',NULL,NULL),(8290753,1285,26635170,'ok',NULL,NULL),(8290754,1285,26635171,'ok',NULL,NULL),(8290755,1285,26635172,'ok',NULL,NULL),(8290756,1285,26635173,'ok',NULL,NULL),(8290757,1285,26635174,'ok',NULL,NULL),(8290758,1285,26635175,'ok',NULL,NULL),(8290759,1285,26635176,'ok',NULL,NULL),(8290760,1285,26635177,'ok',NULL,NULL),(8290761,1285,26635178,'ok',NULL,NULL),(8290762,1285,26635179,'ok',NULL,NULL),(8290763,1285,26635180,'ok',NULL,NULL),(8290764,1285,26635181,'ok',NULL,NULL),(8290765,1285,26635182,'ok',NULL,NULL),(8290766,1285,26635183,'ok',NULL,NULL),(8290767,1285,26635184,'ok',NULL,NULL),(8290768,1285,26635185,'ok',NULL,NULL),(8290769,1285,26635186,'ok',NULL,NULL),(8290770,1285,26635187,'ok',NULL,NULL),(8290771,1285,26635188,'ok',NULL,NULL),(8290772,1285,26635189,'ok',NULL,NULL),(8290773,1285,26635190,'ok',NULL,NULL),(8290774,1285,26635191,'ok',NULL,NULL),(8290775,1285,26635192,'ok',NULL,NULL),(8290776,1285,26635193,'ok',NULL,NULL),(8290777,1285,26635194,'ok',NULL,NULL),(8290778,1285,26635195,'ok',NULL,NULL),(8290779,1285,26635196,'ok',NULL,NULL),(8290780,1285,26635197,'ok',NULL,NULL),(8290781,1285,26635198,'ok',NULL,NULL),(8290782,1285,26635199,'ok',NULL,NULL),(8290783,1285,26635200,'ok',NULL,NULL),(8290784,1285,26635201,'ok',NULL,NULL),(8290785,1285,26635202,'ok',NULL,NULL),(8290786,1285,26635203,'ok',NULL,NULL),(8290787,1285,26635204,'ok',NULL,NULL),(8290788,1285,26635205,'ok',NULL,NULL),(8290789,1285,26635206,'ok',NULL,NULL),(8290790,1285,26635207,'ok',NULL,NULL),(8290791,1285,26635208,'ok',NULL,NULL),(8290792,1285,26635209,'ok',NULL,NULL),(8290793,1285,26635210,'ok',NULL,NULL),(8290794,1285,26635211,'ok',NULL,NULL),(8290795,1285,26635212,'ok',NULL,NULL),(8290796,1285,26635213,'ok',NULL,NULL),(8290797,1285,26635214,'ok',NULL,NULL),(8290798,1285,26635215,'ok',NULL,NULL),(8290799,1285,26635216,'ok',NULL,NULL),(8290800,1285,26635217,'ok',NULL,NULL),(8290801,1285,26635218,'ok',NULL,NULL),(8290802,1285,26635219,'ok',NULL,NULL),(8290803,1285,26635220,'ok',NULL,NULL),(8290804,1285,26635221,'ok',NULL,NULL),(8290805,1285,26635222,'ok',NULL,NULL),(8290806,1285,26635223,'ok',NULL,NULL),(8290807,1285,26635224,'ok',NULL,NULL),(8290808,1285,26635225,'ok',NULL,NULL),(8290809,1285,26635226,'ok',NULL,NULL),(8290810,1285,26635227,'ok',NULL,NULL),(8290811,1285,26635228,'ok',NULL,NULL),(8290812,1285,26635229,'ok',NULL,NULL),(8290813,1285,26635230,'ok',NULL,NULL),(8290814,1285,26635231,'ok',NULL,NULL),(8290815,1285,26635232,'ok',NULL,NULL),(8290816,1285,26635233,'ok',NULL,NULL),(8290817,1285,26635234,'ok',NULL,NULL),(8290818,1285,26635235,'ok',NULL,NULL),(8290819,1285,26635236,'ok',NULL,NULL),(8290820,1285,26635237,'ok',NULL,NULL),(8290821,1285,26635238,'ok',NULL,NULL),(8290822,1285,26635239,'ok',NULL,NULL),(8290823,1285,26635240,'ok',NULL,NULL),(8290824,1285,26635241,'ok',NULL,NULL),(8290825,1285,26635242,'ok',NULL,NULL),(8290826,1285,26635243,'ok',NULL,NULL),(8290827,1285,26635244,'ok',NULL,NULL),(8290828,1285,26635245,'ok',NULL,NULL),(8290829,1285,26635246,'ok',NULL,NULL),(8290830,1285,26635247,'ok',NULL,NULL),(8290831,1285,26635248,'ok',NULL,NULL),(8290832,1285,26635249,'ok',NULL,NULL),(8290833,1285,26635250,'ok',NULL,NULL),(8290834,1285,26635251,'ok',NULL,NULL),(8290835,1285,26635252,'ok',NULL,NULL),(8290836,1285,26635253,'ok',NULL,NULL),(8290837,1285,26635254,'ok',NULL,NULL),(8290838,1285,26635255,'ok',NULL,NULL),(8290839,1285,26635256,'ok',NULL,NULL),(8290840,1285,26635257,'ok',NULL,NULL),(8290841,1285,26635258,'ok',NULL,NULL),(8290842,1285,26635259,'ok',NULL,NULL),(8290843,1285,26635260,'ok',NULL,NULL),(8290844,1285,26635261,'ok',NULL,NULL),(8290845,1285,26635262,'ok',NULL,NULL),(8290846,1285,26635263,'ok',NULL,NULL),(8290847,1285,26635264,'ok',NULL,NULL),(8290848,1285,26635265,'ok',NULL,NULL),(8290849,1285,26635266,'ok',NULL,NULL),(8290850,1285,26635267,'ok',NULL,NULL),(8290851,1285,26635268,'ok',NULL,NULL),(8290852,1285,26635269,'ok',NULL,NULL),(8290853,1285,26635270,'ok',NULL,NULL),(8290854,1285,26635271,'ok',NULL,NULL),(8290855,1285,26635272,'ok',NULL,NULL),(8290856,1285,26635273,'ok',NULL,NULL),(8290857,1285,26635274,'ok',NULL,NULL),(8290858,1285,26635275,'ok',NULL,NULL),(8290859,1285,26635276,'ok',NULL,NULL),(8290860,1285,26635277,'ok',NULL,NULL),(8290861,1285,26635278,'ok',NULL,NULL),(8290862,1285,26635279,'ok',NULL,NULL),(8290863,1285,26635280,'ok',NULL,NULL),(8290864,1285,26635281,'ok',NULL,NULL),(8290865,1285,26635282,'ok',NULL,NULL),(8290866,1285,26635283,'ok',NULL,NULL),(8290867,1285,26635284,'ok',NULL,NULL),(8290868,1285,26635285,'ok',NULL,NULL),(8290869,1285,26635286,'ok',NULL,NULL),(8290870,1285,26635287,'ok',NULL,NULL),(8290871,1285,26635288,'ok',NULL,NULL),(8290872,1285,26635289,'ok',NULL,NULL),(8290873,1285,26635290,'ok',NULL,NULL),(8290874,1285,26635291,'ok',NULL,NULL),(8290875,1285,26635292,'ok',NULL,NULL),(8290876,1285,26635293,'ok',NULL,NULL),(8290877,1285,26635294,'ok',NULL,NULL),(8290878,1285,26635295,'ok',NULL,NULL),(8290879,1285,26635296,'ok',NULL,NULL),(8290880,1285,26635297,'ok',NULL,NULL),(8290881,1285,26635298,'ok',NULL,NULL),(8290882,1285,26635299,'ok',NULL,NULL),(8290883,1285,26635300,'ok',NULL,NULL),(8290884,1285,26635301,'ok',NULL,NULL),(8290885,1285,26635302,'ok',NULL,NULL),(8290886,1285,26635303,'ok',NULL,NULL),(8290887,1285,26635304,'ok',NULL,NULL),(8290888,1285,26635305,'ok',NULL,NULL),(8290889,1285,26635306,'ok',NULL,NULL),(8290890,1285,26635307,'ok',NULL,NULL),(8290891,1285,26635308,'ok',NULL,NULL),(8290892,1285,26635309,'ok',NULL,NULL),(8290893,1285,26635310,'ok',NULL,NULL),(8290894,1285,26635311,'ok',NULL,NULL),(8290895,1285,26635312,'ok',NULL,NULL),(8290896,1285,26635313,'ok',NULL,NULL),(8290897,1285,26635314,'ok',NULL,NULL),(8290898,1285,26635315,'ok',NULL,NULL),(8290899,1285,26635316,'ok',NULL,NULL),(8290900,1285,26635317,'ok',NULL,NULL),(8290901,1285,26635318,'ok',NULL,NULL),(8290902,1285,26635319,'ok',NULL,NULL),(8290903,1285,26635320,'ok',NULL,NULL),(8290904,1285,26635321,'ok',NULL,NULL),(8290905,1285,26635322,'ok',NULL,NULL),(8290906,1285,26635323,'ok',NULL,NULL),(8290907,1285,26635324,'ok',NULL,NULL),(8290908,1285,26635325,'ok',NULL,NULL),(8290909,1285,26635326,'ok',NULL,NULL),(8290910,1285,26635327,'ok',NULL,NULL),(8290911,1285,26635328,'ok',NULL,NULL),(8290912,1285,26635329,'ok',NULL,NULL),(8290913,1285,26635330,'ok',NULL,NULL),(8290914,1285,26635331,'ok',NULL,NULL),(8290915,1285,26635332,'ok',NULL,NULL),(8290916,1285,26635333,'ok',NULL,NULL),(8290917,1285,26635334,'ok',NULL,NULL),(8290918,1285,26635335,'ok',NULL,NULL),(8290919,1285,26635336,'ok',NULL,NULL),(8290920,1285,26635337,'ok',NULL,NULL),(8290921,1285,26635338,'ok',NULL,NULL),(8290922,1285,26635339,'ok',NULL,NULL),(8290923,1285,26635340,'ok',NULL,NULL),(8290924,1285,26635341,'ok',NULL,NULL),(8290925,1285,26635342,'ok',NULL,NULL),(8290926,1285,26635343,'ok',NULL,NULL),(8290927,1285,26635344,'ok',NULL,NULL),(8290928,1285,26635345,'ok',NULL,NULL),(8290929,1285,26635346,'ok',NULL,NULL),(8290930,1285,26635347,'ok',NULL,NULL),(8290931,1285,26635348,'ok',NULL,NULL),(8290932,1285,26635349,'ok',NULL,NULL),(8290933,1285,26635350,'ok',NULL,NULL),(8290934,1285,26635351,'ok',NULL,NULL),(8290935,1285,26635352,'ok',NULL,NULL),(8290936,1285,26635353,'ok',NULL,NULL),(8290937,1285,26635354,'ok',NULL,NULL),(8290938,1285,26635355,'ok',NULL,NULL),(8290939,1285,26635356,'ok',NULL,NULL),(8290940,1285,26635357,'ok',NULL,NULL),(8290941,1285,26635358,'ok',NULL,NULL),(8290942,1285,26635359,'ok',NULL,NULL),(8290943,1285,26635360,'ok',NULL,NULL),(8290944,1285,26635361,'ok',NULL,NULL),(8290945,1285,26635362,'ok',NULL,NULL),(8290946,1285,26635363,'ok',NULL,NULL),(8290947,1285,26635364,'ok',NULL,NULL),(8290948,1285,26635365,'ok',NULL,NULL),(8290949,1285,26635366,'ok',NULL,NULL),(8290950,1285,26635367,'ok',NULL,NULL),(8290951,1285,26635368,'ok',NULL,NULL),(8290952,1285,26635369,'ok',NULL,NULL),(8290953,1285,26635370,'ok',NULL,NULL),(8290954,1285,26635371,'ok',NULL,NULL),(8290955,1285,26635372,'ok',NULL,NULL),(8290956,1285,26635373,'ok',NULL,NULL),(8290957,1285,26635374,'ok',NULL,NULL),(8290958,1285,26635375,'ok',NULL,NULL),(8290959,1285,26635376,'ok',NULL,NULL),(8290960,1285,26635377,'ok',NULL,NULL),(8290961,1285,26635378,'ok',NULL,NULL),(8290962,1285,26635379,'ok',NULL,NULL),(8290963,1285,26635380,'ok',NULL,NULL),(8290964,1285,26635381,'ok',NULL,NULL),(8290965,1285,26635382,'ok',NULL,NULL),(8290966,1285,26635383,'ok',NULL,NULL),(8290967,1285,26635384,'ok',NULL,NULL),(8290968,1285,26635385,'ok',NULL,NULL),(8290969,1285,26635386,'ok',NULL,NULL),(8290970,1285,26635387,'ok',NULL,NULL),(8290971,1285,26635388,'ok',NULL,NULL),(8290972,1285,26635389,'ok',NULL,NULL),(8290973,1285,26635390,'ok',NULL,NULL),(8290974,1285,26635391,'ok',NULL,NULL),(8290975,1285,26635392,'ok',NULL,NULL),(8290976,1285,26635393,'ok',NULL,NULL),(8290977,1285,26635394,'ok',NULL,NULL),(8290978,1285,26635395,'ok',NULL,NULL),(8290979,1285,26635396,'ok',NULL,NULL),(8290980,1285,26635397,'ok',NULL,NULL),(8290981,1285,26635398,'ok',NULL,NULL),(8290982,1285,26635399,'ok',NULL,NULL),(8290983,1285,26635400,'ok',NULL,NULL),(8290984,1285,26635401,'ok',NULL,NULL),(8290985,1285,26635402,'ok',NULL,NULL),(8290986,1285,26635403,'ok',NULL,NULL),(8290987,1285,26635404,'ok',NULL,NULL),(8290988,1285,26635405,'ok',NULL,NULL),(8290989,1285,26635406,'ok',NULL,NULL),(8290990,1285,26635407,'ok',NULL,NULL),(8290991,1285,26635408,'ok',NULL,NULL),(8290992,1285,26635409,'ok',NULL,NULL),(8290993,1285,26635410,'ok',NULL,NULL),(8290994,1285,26635411,'ok',NULL,NULL),(8290995,1285,26635412,'ok',NULL,NULL),(8290996,1285,26635413,'ok',NULL,NULL),(8290997,1285,26635414,'ok',NULL,NULL),(8290998,1285,26635415,'ok',NULL,NULL),(8290999,1285,26635416,'ok',NULL,NULL),(8291000,1285,26635417,'ok',NULL,NULL),(8291001,1285,26635418,'ok',NULL,NULL),(8291002,1285,26635419,'ok',NULL,NULL),(8291003,1285,26635420,'ok',NULL,NULL),(8291004,1285,26635421,'ok',NULL,NULL),(8291005,1285,26635422,'ok',NULL,NULL),(8291006,1285,26635423,'ok',NULL,NULL),(8291007,1285,26635424,'ok',NULL,NULL),(8291008,1285,26635425,'ok',NULL,NULL),(8291009,1285,26635426,'ok',NULL,NULL),(8291010,1285,26635427,'ok',NULL,NULL),(8291011,1285,26635428,'ok',NULL,NULL),(8291012,1285,26635429,'ok',NULL,NULL),(8291013,1285,26635430,'ok',NULL,NULL),(8291014,1285,26635431,'ok',NULL,NULL),(8291015,1285,26635432,'ok',NULL,NULL),(8291016,1285,26635433,'ok',NULL,NULL),(8291017,1285,26635434,'ok',NULL,NULL),(8291018,1285,26635435,'ok',NULL,NULL),(8291019,1285,26635436,'ok',NULL,NULL),(8291020,1285,26635437,'ok',NULL,NULL),(8291021,1285,26635438,'ok',NULL,NULL),(8291022,1285,26635439,'ok',NULL,NULL),(8291023,1285,26635440,'ok',NULL,NULL),(8291024,1285,26635441,'ok',NULL,NULL),(8291025,1285,26635442,'ok',NULL,NULL),(8291026,1285,26635443,'ok',NULL,NULL),(8291027,1285,26635444,'ok',NULL,NULL),(8291028,1285,26635445,'ok',NULL,NULL),(8291029,1285,26635446,'ok',NULL,NULL),(8291030,1285,26635447,'ok',NULL,NULL),(8291031,1285,26635448,'ok',NULL,NULL),(8291032,1285,26635449,'ok',NULL,NULL),(8291033,1285,26635450,'ok',NULL,NULL),(8291034,1285,26635451,'ok',NULL,NULL),(8291035,1285,26635452,'ok',NULL,NULL),(8291036,1285,26635453,'ok',NULL,NULL),(8291037,1285,26635454,'ok',NULL,NULL),(8291038,1285,26635455,'ok',NULL,NULL),(8291039,1285,26635456,'ok',NULL,NULL),(8291040,1285,26635457,'ok',NULL,NULL),(8291041,1285,26635458,'ok',NULL,NULL),(8291042,1285,26635459,'ok',NULL,NULL),(8291043,1285,26635460,'ok',NULL,NULL),(8291044,1285,26635461,'ok',NULL,NULL),(8291045,1285,26635462,'ok',NULL,NULL),(8291046,1285,26635463,'ok',NULL,NULL),(8291047,1285,26635464,'ok',NULL,NULL),(8291048,1285,26635465,'ok',NULL,NULL),(8291049,1285,26635466,'ok',NULL,NULL),(8291050,1285,26635467,'ok',NULL,NULL),(8291051,1285,26635468,'ok',NULL,NULL),(8291052,1285,26635469,'ok',NULL,NULL),(8291053,1285,26635470,'ok',NULL,NULL),(8291054,1285,26635471,'ok',NULL,NULL),(8291055,1285,26635472,'ok',NULL,NULL),(8291056,1285,26635473,'ok',NULL,NULL),(8291057,1285,26635474,'ok',NULL,NULL),(8291058,1285,26635475,'ok',NULL,NULL),(8291059,1285,26635476,'ok',NULL,NULL),(8291060,1285,26635477,'ok',NULL,NULL),(8291061,1285,26635478,'ok',NULL,NULL),(8291062,1285,26635479,'ok',NULL,NULL),(8291063,1285,26635480,'ok',NULL,NULL),(8291064,1285,26635481,'ok',NULL,NULL),(8291065,1285,26635482,'ok',NULL,NULL),(8291066,1285,26635483,'ok',NULL,NULL),(8291067,1285,26635484,'ok',NULL,NULL),(8291068,1285,26635485,'ok',NULL,NULL),(8291069,1285,26635486,'ok',NULL,NULL),(8291070,1285,26635487,'ok',NULL,NULL),(8291071,1285,26635488,'ok',NULL,NULL),(8291072,1285,26635489,'ok',NULL,NULL),(8291073,1285,26635490,'ok',NULL,NULL),(8291074,1285,26635491,'ok',NULL,NULL),(8291075,1285,26635492,'ok',NULL,NULL),(8291076,1285,26635493,'ok',NULL,NULL),(8291077,1285,26635494,'ok',NULL,NULL),(8291078,1285,26635495,'ok',NULL,NULL),(8291079,1285,26635496,'ok',NULL,NULL),(8291080,1285,26635497,'ok',NULL,NULL),(8291081,1285,26635498,'ok',NULL,NULL),(8291082,1285,26635499,'ok',NULL,NULL),(8291083,1285,26635500,'ok',NULL,NULL),(8291084,1285,26635501,'ok',NULL,NULL),(8291085,1285,26635502,'ok',NULL,NULL),(8291086,1285,26635503,'ok',NULL,NULL),(8291087,1285,26635504,'ok',NULL,NULL),(8291088,1285,26635505,'ok',NULL,NULL),(8291089,1285,26635506,'ok',NULL,NULL),(8291090,1285,26635507,'ok',NULL,NULL),(8291091,1285,26635508,'ok',NULL,NULL),(8291092,1285,26635509,'ok',NULL,NULL),(8291093,1285,26635510,'ok',NULL,NULL),(8291094,1285,26635511,'ok',NULL,NULL),(8291095,1285,26635512,'ok',NULL,NULL),(8291096,1285,26635513,'ok',NULL,NULL),(8291097,1285,26635514,'ok',NULL,NULL),(8291098,1285,26635515,'ok',NULL,NULL),(8291099,1285,26635516,'ok',NULL,NULL),(8291100,1285,26635517,'ok',NULL,NULL),(8291101,1285,26635518,'ok',NULL,NULL),(8291102,1285,26635519,'ok',NULL,NULL),(8291103,1285,26635520,'ok',NULL,NULL),(8291104,1285,26635521,'ok',NULL,NULL),(8291105,1285,26635522,'ok',NULL,NULL),(8291106,1285,26635523,'ok',NULL,NULL),(8291107,1285,26635524,'ok',NULL,NULL),(8291108,1285,26635525,'ok',NULL,NULL),(8291109,1285,26635526,'ok',NULL,NULL),(8291110,1285,26635527,'ok',NULL,NULL),(8291111,1285,26635528,'ok',NULL,NULL),(8291112,1285,26635529,'ok',NULL,NULL),(8291113,1285,26635530,'ok',NULL,NULL),(8291114,1285,26635531,'ok',NULL,NULL),(8291115,1285,26635532,'ok',NULL,NULL),(8291116,1285,26635533,'ok',NULL,NULL),(8291117,1285,26635534,'ok',NULL,NULL),(8291118,1285,26635535,'ok',NULL,NULL),(8291119,1285,26635536,'ok',NULL,NULL),(8291120,1285,26635537,'ok',NULL,NULL),(8291121,1285,26635538,'ok',NULL,NULL),(8291122,1285,26635539,'ok',NULL,NULL),(8291123,1285,26635540,'ok',NULL,NULL),(8291124,1285,26635541,'ok',NULL,NULL),(8291125,1285,26635542,'ok',NULL,NULL),(8291126,1285,26635543,'ok',NULL,NULL),(8291127,1285,26635544,'ok',NULL,NULL),(8291128,1285,26635545,'ok',NULL,NULL),(8291129,1285,26635546,'ok',NULL,NULL),(8291130,1285,26635547,'ok',NULL,NULL),(8291131,1285,26635548,'ok',NULL,NULL),(8291132,1285,26635549,'ok',NULL,NULL),(8291133,1285,26635550,'ok',NULL,NULL),(8291134,1285,26635551,'ok',NULL,NULL),(8291135,1285,26635552,'ok',NULL,NULL),(8291136,1285,26635553,'ok',NULL,NULL),(8291137,1285,26635554,'ok',NULL,NULL),(8291138,1285,26635555,'ok',NULL,NULL),(8291139,1285,26635556,'ok',NULL,NULL),(8291140,1285,26635557,'ok',NULL,NULL),(8291141,1285,26635558,'ok',NULL,NULL),(8291142,1285,26635559,'ok',NULL,NULL),(8291143,1285,26635560,'ok',NULL,NULL),(8291144,1285,26635561,'ok',NULL,NULL),(8291145,1285,26635562,'ok',NULL,NULL),(8291146,1285,26635563,'ok',NULL,NULL),(8291147,1285,26635564,'ok',NULL,NULL),(8291148,1285,26635565,'ok',NULL,NULL),(8291149,1285,26635566,'ok',NULL,NULL),(8291150,1285,26635567,'ok',NULL,NULL),(8291151,1285,26635568,'ok',NULL,NULL),(8291152,1285,26635569,'ok',NULL,NULL),(8291153,1285,26635570,'ok',NULL,NULL),(8291154,1285,26635571,'ok',NULL,NULL),(8291155,1285,26635572,'ok',NULL,NULL),(8291156,1285,26635573,'ok',NULL,NULL),(8291157,1285,26635574,'ok',NULL,NULL),(8291158,1285,26635575,'ok',NULL,NULL),(8291159,1285,26635576,'ok',NULL,NULL),(8291160,1285,26635577,'ok',NULL,NULL),(8291161,1285,26635578,'ok',NULL,NULL),(8291162,1285,26635579,'ok',NULL,NULL),(8291163,1285,26635580,'ok',NULL,NULL),(8291164,1285,26635581,'ok',NULL,NULL),(8291165,1285,26635582,'ok',NULL,NULL),(8291166,1285,26635583,'ok',NULL,NULL),(8291167,1285,26635584,'ok',NULL,NULL),(8291168,1285,26635585,'ok',NULL,NULL),(8291169,1285,26635586,'ok',NULL,NULL),(8291170,1285,26635587,'ok',NULL,NULL),(8291171,1285,26635588,'ok',NULL,NULL),(8291172,1285,26635589,'ok',NULL,NULL),(8291173,1285,26635590,'ok',NULL,NULL),(8291174,1285,26635591,'ok',NULL,NULL),(8291175,1285,26635592,'ok',NULL,NULL),(8291176,1285,26635593,'ok',NULL,NULL),(8291177,1285,26635594,'ok',NULL,NULL),(8291178,1285,26635595,'ok',NULL,NULL),(8291179,1285,26635596,'ok',NULL,NULL),(8291180,1285,26635597,'ok',NULL,NULL),(8291181,1285,26635598,'ok',NULL,NULL),(8291182,1285,26635599,'ok',NULL,NULL),(8291183,1285,26635600,'ok',NULL,NULL),(8291184,1285,26635601,'ok',NULL,NULL),(8291185,1285,26635602,'ok',NULL,NULL),(8291186,1285,26635603,'ok',NULL,NULL),(8291187,1285,26635604,'ok',NULL,NULL),(8291188,1285,26635605,'ok',NULL,NULL),(8291189,1285,26635606,'ok',NULL,NULL),(8291190,1285,26635607,'ok',NULL,NULL),(8291191,1285,26635608,'ok',NULL,NULL),(8291192,1285,26635609,'ok',NULL,NULL),(8291193,1285,26635610,'ok',NULL,NULL),(8291194,1285,26635611,'ok',NULL,NULL),(8291195,1285,26635612,'ok',NULL,NULL),(8291196,1285,26635613,'ok',NULL,NULL),(8291197,1285,26635614,'ok',NULL,NULL),(8291198,1285,26635615,'ok',NULL,NULL),(8291199,1285,26635616,'ok',NULL,NULL),(8291200,1285,26635617,'ok',NULL,NULL),(8291201,1285,26635618,'ok',NULL,NULL),(8291202,1285,26635619,'ok',NULL,NULL),(8291203,1285,26635620,'ok',NULL,NULL),(8291204,1285,26635621,'ok',NULL,NULL),(8291205,1285,26635622,'ok',NULL,NULL),(8291206,1285,26635623,'ok',NULL,NULL),(8291207,1285,26635624,'ok',NULL,NULL),(8291208,1285,26635625,'ok',NULL,NULL),(8291209,1285,26635626,'ok',NULL,NULL),(8291210,1285,26635627,'ok',NULL,NULL),(8291211,1285,26635628,'ok',NULL,NULL),(8291212,1285,26635629,'ok',NULL,NULL),(8291213,1285,26635630,'ok',NULL,NULL),(8291214,1285,26635631,'ok',NULL,NULL),(8291215,1285,26635632,'ok',NULL,NULL),(8291216,1285,26635633,'ok',NULL,NULL),(8291217,1285,26635634,'ok',NULL,NULL),(8291218,1285,26635635,'ok',NULL,NULL),(8291219,1285,26635636,'ok',NULL,NULL),(8291220,1285,26635637,'ok',NULL,NULL),(8291221,1285,26635638,'ok',NULL,NULL),(8291222,1285,26635639,'ok',NULL,NULL),(8291223,1285,26635640,'ok',NULL,NULL),(8291224,1285,26635641,'ok',NULL,NULL),(8291225,1285,26635642,'ok',NULL,NULL),(8291226,1285,26635643,'ok',NULL,NULL),(8291227,1285,26635644,'ok',NULL,NULL),(8291228,1285,26635645,'ok',NULL,NULL),(8291229,1285,26635646,'ok',NULL,NULL),(8291230,1285,26635647,'ok',NULL,NULL),(8291231,1285,26635648,'ok',NULL,NULL),(8291232,1285,26635649,'ok',NULL,NULL),(8291233,1285,26635650,'ok',NULL,NULL),(8291234,1285,26635651,'ok',NULL,NULL),(8291235,1285,26635652,'ok',NULL,NULL),(8291236,1285,26635653,'ok',NULL,NULL),(8291237,1285,26635654,'ok',NULL,NULL),(8291238,1285,26635655,'ok',NULL,NULL),(8291239,1285,26635656,'ok',NULL,NULL),(8291240,1285,26635657,'ok',NULL,NULL),(8291241,1285,26635658,'ok',NULL,NULL),(8291242,1285,26635659,'ok',NULL,NULL),(8291243,1285,26635660,'ok',NULL,NULL),(8291244,1285,26635661,'ok',NULL,NULL),(8291245,1285,26635662,'ok',NULL,NULL),(8291246,1285,26635663,'ok',NULL,NULL),(8291247,1285,26635664,'ok',NULL,NULL),(8291248,1285,26635665,'ok',NULL,NULL),(8291249,1285,26635666,'ok',NULL,NULL),(8291250,1285,26635667,'ok',NULL,NULL),(8291251,1285,26635668,'ok',NULL,NULL),(8291252,1285,26635669,'ok',NULL,NULL),(8291253,1285,26635670,'ok',NULL,NULL),(8291254,1285,26635671,'ok',NULL,NULL),(8291255,1285,26635672,'ok',NULL,NULL),(8291256,1285,26635673,'ok',NULL,NULL),(8291257,1285,26635674,'ok',NULL,NULL),(8291258,1285,26635675,'ok',NULL,NULL),(8291259,1285,26635676,'ok',NULL,NULL),(8291260,1285,26635677,'ok',NULL,NULL),(8291261,1285,26635678,'ok',NULL,NULL),(8291262,1285,26635679,'ok',NULL,NULL),(8291263,1285,26635680,'ok',NULL,NULL),(8291264,1285,26635681,'ok',NULL,NULL),(8291265,1285,26635682,'ok',NULL,NULL),(8291266,1285,26635683,'ok',NULL,NULL),(8291267,1285,26635684,'ok',NULL,NULL),(8291268,1285,26635685,'ok',NULL,NULL),(8291269,1285,26635686,'ok',NULL,NULL),(8291270,1285,26635687,'ok',NULL,NULL),(8291271,1285,26635688,'ok',NULL,NULL),(8291272,1285,26635689,'ok',NULL,NULL),(8291273,1285,26635690,'ok',NULL,NULL),(8291274,1285,26635691,'ok',NULL,NULL),(8291275,1285,26635692,'ok',NULL,NULL),(8291276,1285,26635693,'ok',NULL,NULL),(8291277,1285,26635694,'ok',NULL,NULL),(8291278,1285,26635695,'ok',NULL,NULL),(8291279,1285,26635696,'ok',NULL,NULL),(8291280,1285,26635697,'ok',NULL,NULL),(8291281,1285,26635698,'ok',NULL,NULL),(8291282,1285,26635699,'ok',NULL,NULL),(8291283,1285,26635700,'ok',NULL,NULL),(8291284,1285,26635701,'ok',NULL,NULL),(8291285,1285,26635702,'ok',NULL,NULL),(8291286,1285,26635703,'ok',NULL,NULL),(8291287,1285,26635704,'ok',NULL,NULL),(8291288,1285,26635705,'ok',NULL,NULL),(8291289,1285,26635706,'ok',NULL,NULL),(8291290,1285,26635707,'ok',NULL,NULL),(8291291,1285,26635708,'ok',NULL,NULL),(8291292,1285,26635709,'ok',NULL,NULL),(8291293,1285,26635710,'ok',NULL,NULL),(8291294,1285,26635711,'ok',NULL,NULL),(8291295,1285,26635712,'ok',NULL,NULL),(8291296,1285,26635713,'ok',NULL,NULL),(8291297,1285,26635714,'ok',NULL,NULL),(8291298,1285,26635715,'ok',NULL,NULL),(8291299,1284,26634526,'ok',NULL,NULL),(8291300,1284,26634527,'ok',NULL,NULL),(8291301,1284,26634528,'ok',NULL,NULL),(8291302,1284,26634529,'ok',NULL,NULL),(8291303,1284,26634530,'ok',NULL,NULL),(8291304,1284,26634531,'ok',NULL,NULL),(8291305,1284,26634532,'ok',NULL,NULL),(8291306,1284,26634533,'ok',NULL,NULL),(8291307,1284,26634534,'ok',NULL,NULL),(8291308,1284,26634535,'ok',NULL,NULL),(8291309,1284,26634536,'ok',NULL,NULL),(8291310,1284,26634537,'ok',NULL,NULL),(8291311,1284,26634538,'ok',NULL,NULL),(8291312,1284,26634539,'ok',NULL,NULL),(8291313,1284,26634540,'ok',NULL,NULL),(8291314,1284,26634541,'ok',NULL,NULL),(8291315,1284,26634542,'ok',NULL,NULL),(8291316,1284,26634543,'ok',NULL,NULL),(8291317,1284,26634544,'ok',NULL,NULL),(8291318,1284,26634545,'ok',NULL,NULL),(8291319,1284,26634546,'ok',NULL,NULL),(8291320,1284,26634547,'ok',NULL,NULL),(8291321,1284,26634548,'ok',NULL,NULL),(8291322,1284,26634549,'ok',NULL,NULL),(8291323,1284,26634550,'ok',NULL,NULL),(8291324,1284,26634551,'ok',NULL,NULL),(8291325,1284,26634552,'ok',NULL,NULL),(8291326,1284,26634553,'ok',NULL,NULL),(8291327,1284,26634554,'ok',NULL,NULL),(8291328,1284,26634555,'ok',NULL,NULL),(8291329,1284,26634556,'ok',NULL,NULL),(8291330,1284,26634557,'ok',NULL,NULL),(8291331,1284,26634558,'ok',NULL,NULL),(8291332,1284,26634559,'ok',NULL,NULL),(8291333,1284,26634560,'ok',NULL,NULL),(8291334,1284,26634561,'ok',NULL,NULL),(8291335,1284,26634562,'ok',NULL,NULL),(8291336,1284,26634563,'ok',NULL,NULL),(8291337,1284,26634564,'ok',NULL,NULL),(8291338,1284,26634565,'ok',NULL,NULL),(8291339,1284,26634566,'ok',NULL,NULL),(8291340,1284,26634567,'ok',NULL,NULL),(8291341,1284,26634568,'ok',NULL,NULL),(8291342,1284,26634569,'ok',NULL,NULL),(8291343,1284,26634570,'ok',NULL,NULL),(8291344,1284,26634571,'ok',NULL,NULL),(8291345,1284,26634572,'ok',NULL,NULL),(8291346,1284,26634573,'ok',NULL,NULL),(8291347,1284,26634574,'ok',NULL,NULL),(8291348,1284,26634575,'ok',NULL,NULL),(8291349,1284,26634576,'ok',NULL,NULL),(8291350,1284,26634577,'ok',NULL,NULL),(8291351,1284,26634578,'ok',NULL,NULL),(8291352,1284,26634579,'ok',NULL,NULL),(8291353,1284,26634580,'ok',NULL,NULL),(8291354,1284,26634581,'ok',NULL,NULL),(8291355,1284,26634582,'ok',NULL,NULL),(8291356,1284,26634583,'ok',NULL,NULL),(8291357,1284,26634584,'ok',NULL,NULL),(8291358,1284,26634585,'ok',NULL,NULL),(8291359,1284,26634586,'ok',NULL,NULL),(8291360,1284,26634587,'ok',NULL,NULL),(8291361,1284,26634588,'ok',NULL,NULL),(8291362,1284,26634589,'ok',NULL,NULL),(8291363,1284,26634590,'ok',NULL,NULL),(8291364,1284,26634591,'ok',NULL,NULL),(8291365,1284,26634592,'ok',NULL,NULL),(8291366,1284,26634593,'ok',NULL,NULL),(8291367,1284,26634594,'ok',NULL,NULL),(8291368,1284,26634595,'ok',NULL,NULL),(8291369,1284,26634596,'ok',NULL,NULL),(8291370,1284,26634597,'ok',NULL,NULL),(8291371,1284,26634598,'ok',NULL,NULL),(8291372,1284,26634599,'ok',NULL,NULL),(8291373,1284,26634600,'ok',NULL,NULL),(8291374,1284,26634601,'ok',NULL,NULL),(8291375,1284,26634602,'ok',NULL,NULL),(8291376,1284,26634603,'ok',NULL,NULL),(8291377,1284,26634604,'ok',NULL,NULL),(8291378,1284,26634605,'ok',NULL,NULL),(8291379,1284,26634606,'ok',NULL,NULL),(8291380,1284,26634607,'ok',NULL,NULL),(8291381,1284,26634608,'ok',NULL,NULL),(8291382,1284,26634609,'ok',NULL,NULL),(8291383,1284,26634610,'ok',NULL,NULL),(8291384,1284,26634611,'ok',NULL,NULL),(8291385,1284,26634612,'ok',NULL,NULL),(8291386,1284,26634613,'ok',NULL,NULL),(8291387,1284,26634614,'ok',NULL,NULL),(8291388,1284,26634615,'ok',NULL,NULL),(8291389,1284,26634616,'ok',NULL,NULL),(8291390,1284,26634617,'ok',NULL,NULL),(8291391,1284,26634618,'ok',NULL,NULL),(8291392,1284,26634619,'ok',NULL,NULL),(8291393,1284,26634620,'ok',NULL,NULL),(8291394,1284,26634621,'ok',NULL,NULL),(8291395,1284,26634622,'ok',NULL,NULL),(8291396,1284,26634623,'ok',NULL,NULL),(8291397,1284,26634624,'ok',NULL,NULL),(8291398,1284,26634625,'ok',NULL,NULL),(8291399,1284,26634626,'ok',NULL,NULL),(8291400,1284,26634627,'ok',NULL,NULL),(8291401,1284,26634628,'ok',NULL,NULL),(8291402,1284,26634629,'ok',NULL,NULL),(8291403,1284,26634630,'ok',NULL,NULL),(8291404,1284,26634631,'ok',NULL,NULL),(8291405,1284,26634632,'ok',NULL,NULL),(8291406,1284,26634633,'ok',NULL,NULL),(8291407,1284,26634634,'ok',NULL,NULL),(8291408,1284,26634635,'ok',NULL,NULL),(8291409,1284,26634636,'ok',NULL,NULL),(8291410,1284,26634637,'ok',NULL,NULL),(8291411,1284,26634638,'ok',NULL,NULL),(8291412,1284,26634639,'ok',NULL,NULL),(8291413,1284,26634640,'ok',NULL,NULL),(8291414,1284,26634641,'ok',NULL,NULL),(8291415,1284,26634642,'ok',NULL,NULL),(8291416,1284,26634643,'ok',NULL,NULL),(8291417,1284,26634644,'ok',NULL,NULL),(8291418,1284,26634645,'ok',NULL,NULL),(8291419,1284,26634646,'ok',NULL,NULL),(8291420,1284,26634647,'ok',NULL,NULL),(8291421,1284,26634648,'ok',NULL,NULL),(8291422,1284,26634649,'ok',NULL,NULL),(8291423,1284,26634650,'ok',NULL,NULL),(8291424,1284,26634651,'ok',NULL,NULL),(8291425,1284,26634652,'ok',NULL,NULL),(8291426,1284,26634653,'ok',NULL,NULL),(8291427,1284,26634654,'ok',NULL,NULL),(8291428,1284,26634655,'ok',NULL,NULL),(8291429,1284,26634656,'ok',NULL,NULL),(8291430,1284,26634657,'ok',NULL,NULL),(8291431,1284,26634658,'ok',NULL,NULL),(8291432,1284,26634659,'ok',NULL,NULL),(8291433,1284,26634660,'ok',NULL,NULL),(8291434,1284,26634661,'ok',NULL,NULL),(8291435,1284,26634662,'ok',NULL,NULL),(8291436,1284,26634663,'ok',NULL,NULL),(8291437,1284,26634664,'ok',NULL,NULL),(8291438,1284,26634665,'ok',NULL,NULL),(8291439,1284,26634666,'ok',NULL,NULL),(8291440,1284,26634667,'ok',NULL,NULL),(8291441,1284,26634668,'ok',NULL,NULL),(8291442,1284,26634669,'ok',NULL,NULL),(8291443,1284,26634670,'ok',NULL,NULL),(8291444,1284,26634671,'ok',NULL,NULL),(8291445,1284,26634672,'ok',NULL,NULL),(8291446,1284,26634673,'ok',NULL,NULL),(8291447,1284,26634674,'ok',NULL,NULL),(8291448,1284,26634675,'ok',NULL,NULL),(8291449,1284,26634676,'ok',NULL,NULL),(8291450,1284,26634677,'ok',NULL,NULL),(8291451,1284,26634678,'ok',NULL,NULL),(8291452,1284,26634679,'ok',NULL,NULL),(8291453,1284,26634680,'ok',NULL,NULL),(8291454,1284,26634681,'ok',NULL,NULL),(8291455,1284,26634682,'ok',NULL,NULL),(8291456,1284,26634683,'ok',NULL,NULL),(8291457,1284,26634684,'ok',NULL,NULL),(8291458,1284,26634685,'ok',NULL,NULL),(8291459,1284,26634686,'ok',NULL,NULL),(8291460,1284,26634687,'ok',NULL,NULL),(8291461,1284,26634688,'ok',NULL,NULL),(8291462,1284,26634689,'ok',NULL,NULL),(8291463,1284,26634690,'ok',NULL,NULL),(8291464,1284,26634691,'ok',NULL,NULL),(8291465,1284,26634692,'ok',NULL,NULL),(8291466,1284,26634693,'ok',NULL,NULL),(8291467,1284,26634694,'ok',NULL,NULL),(8291468,1284,26634695,'ok',NULL,NULL),(8291469,1284,26634696,'ok',NULL,NULL),(8291470,1284,26634697,'ok',NULL,NULL),(8291471,1284,26634698,'ok',NULL,NULL),(8291472,1284,26634699,'ok',NULL,NULL),(8291473,1284,26634700,'ok',NULL,NULL),(8291474,1284,26634701,'ok',NULL,NULL),(8291475,1284,26634702,'ok',NULL,NULL),(8291476,1284,26634703,'ok',NULL,NULL),(8291477,1284,26634704,'ok',NULL,NULL),(8291478,1284,26634705,'ok',NULL,NULL),(8291479,1284,26634706,'ok',NULL,NULL),(8291480,1284,26634707,'ok',NULL,NULL),(8291481,1284,26634708,'ok',NULL,NULL),(8291482,1284,26634709,'ok',NULL,NULL),(8291483,1284,26634710,'ok',NULL,NULL),(8291484,1284,26634711,'ok',NULL,NULL),(8291485,1284,26634712,'ok',NULL,NULL),(8291486,1284,26634713,'ok',NULL,NULL),(8291487,1284,26634714,'ok',NULL,NULL),(8291488,1284,26634715,'ok',NULL,NULL),(8291489,1284,26634716,'ok',NULL,NULL),(8291490,1284,26634717,'ok',NULL,NULL),(8291491,1284,26634718,'ok',NULL,NULL),(8291492,1284,26634719,'ok',NULL,NULL),(8291493,1284,26634720,'ok',NULL,NULL),(8291494,1284,26634721,'ok',NULL,NULL),(8291495,1284,26634722,'ok',NULL,NULL),(8291496,1284,26634723,'ok',NULL,NULL),(8291497,1284,26634724,'ok',NULL,NULL),(8291498,1284,26634725,'ok',NULL,NULL),(8291499,1284,26634726,'ok',NULL,NULL),(8291500,1284,26634727,'ok',NULL,NULL),(8291501,1284,26634728,'ok',NULL,NULL),(8291502,1284,26634729,'ok',NULL,NULL),(8291503,1284,26634730,'ok',NULL,NULL),(8291504,1284,26634731,'ok',NULL,NULL),(8291505,1284,26634732,'ok',NULL,NULL),(8291506,1284,26634733,'ok',NULL,NULL),(8291507,1284,26634734,'ok',NULL,NULL),(8291508,1284,26634735,'ok',NULL,NULL),(8291509,1284,26634736,'ok',NULL,NULL),(8291510,1284,26634737,'ok',NULL,NULL),(8291511,1284,26634738,'ok',NULL,NULL),(8291512,1284,26634739,'ok',NULL,NULL),(8291513,1284,26634740,'ok',NULL,NULL),(8291514,1284,26634741,'ok',NULL,NULL),(8291515,1284,26634742,'ok',NULL,NULL),(8291516,1284,26634743,'ok',NULL,NULL),(8291517,1284,26634744,'ok',NULL,NULL),(8291518,1284,26634745,'ok',NULL,NULL),(8291519,1284,26634746,'ok',NULL,NULL),(8291520,1284,26634747,'ok',NULL,NULL),(8291521,1284,26634748,'ok',NULL,NULL),(8291522,1284,26634749,'ok',NULL,NULL),(8291523,1284,26634750,'ok',NULL,NULL),(8291524,1284,26634751,'ok',NULL,NULL),(8291525,1284,26634752,'ok',NULL,NULL),(8291526,1284,26634753,'ok',NULL,NULL),(8291527,1284,26634754,'ok',NULL,NULL),(8291528,1284,26634755,'ok',NULL,NULL),(8291529,1284,26634756,'ok',NULL,NULL),(8291530,1284,26634757,'ok',NULL,NULL),(8291531,1284,26634758,'ok',NULL,NULL),(8291532,1284,26634759,'ok',NULL,NULL),(8291533,1284,26634760,'ok',NULL,NULL),(8291534,1284,26634761,'ok',NULL,NULL),(8291535,1284,26634762,'ok',NULL,NULL),(8291536,1284,26634763,'ok',NULL,NULL),(8291537,1284,26634764,'ok',NULL,NULL),(8291538,1284,26634765,'ok',NULL,NULL),(8291539,1284,26634766,'ok',NULL,NULL),(8291540,1284,26634767,'ok',NULL,NULL),(8291541,1284,26634768,'ok',NULL,NULL),(8291542,1284,26634769,'ok',NULL,NULL),(8291543,1284,26634770,'ok',NULL,NULL),(8291544,1284,26634771,'ok',NULL,NULL),(8291545,1284,26634772,'ok',NULL,NULL),(8291546,1284,26634773,'ok',NULL,NULL),(8291547,1284,26634774,'ok',NULL,NULL),(8291548,1284,26634775,'ok',NULL,NULL),(8291549,1284,26634776,'ok',NULL,NULL),(8291550,1284,26634777,'ok',NULL,NULL),(8291551,1284,26634778,'ok',NULL,NULL),(8291552,1284,26634779,'ok',NULL,NULL),(8291553,1284,26634780,'ok',NULL,NULL),(8291554,1284,26634781,'ok',NULL,NULL),(8291555,1284,26634782,'ok',NULL,NULL),(8291556,1284,26634783,'ok',NULL,NULL),(8291557,1284,26634784,'ok',NULL,NULL),(8291558,1284,26634785,'ok',NULL,NULL),(8291559,1284,26634786,'ok',NULL,NULL),(8291560,1284,26634787,'ok',NULL,NULL),(8291561,1284,26634788,'ok',NULL,NULL),(8291562,1284,26634789,'ok',NULL,NULL),(8291563,1284,26634790,'ok',NULL,NULL),(8291564,1284,26634791,'ok',NULL,NULL),(8291565,1284,26634792,'ok',NULL,NULL),(8291566,1284,26634793,'ok',NULL,NULL),(8291567,1284,26634794,'ok',NULL,NULL),(8291568,1284,26634795,'ok',NULL,NULL),(8291569,1284,26634796,'ok',NULL,NULL),(8291570,1284,26634797,'ok',NULL,NULL),(8291571,1284,26634798,'ok',NULL,NULL),(8291572,1284,26634799,'ok',NULL,NULL),(8291573,1284,26634800,'ok',NULL,NULL),(8291574,1284,26634801,'ok',NULL,NULL),(8291575,1284,26634802,'ok',NULL,NULL),(8291576,1284,26634803,'ok',NULL,NULL),(8291577,1284,26634804,'ok',NULL,NULL),(8291578,1284,26634805,'ok',NULL,NULL),(8291579,1284,26634806,'ok',NULL,NULL),(8291580,1284,26634807,'ok',NULL,NULL),(8291581,1284,26634808,'ok',NULL,NULL),(8291582,1284,26634809,'ok',NULL,NULL),(8291583,1284,26634810,'ok',NULL,NULL),(8291584,1284,26634811,'ok',NULL,NULL),(8291585,1284,26634812,'ok',NULL,NULL),(8291586,1284,26634813,'ok',NULL,NULL),(8291587,1284,26634814,'ok',NULL,NULL),(8291588,1284,26634815,'ok',NULL,NULL),(8291589,1284,26634816,'ok',NULL,NULL),(8291590,1284,26634817,'ok',NULL,NULL),(8291591,1284,26634818,'ok',NULL,NULL),(8291592,1284,26634819,'ok',NULL,NULL),(8291593,1284,26634820,'ok',NULL,NULL),(8291594,1284,26634821,'ok',NULL,NULL),(8291595,1284,26634822,'ok',NULL,NULL),(8291596,1284,26634823,'ok',NULL,NULL),(8291597,1284,26634824,'ok',NULL,NULL),(8291598,1284,26634825,'ok',NULL,NULL),(8291599,1284,26634826,'ok',NULL,NULL),(8291600,1284,26634827,'ok',NULL,NULL),(8291601,1284,26634828,'ok',NULL,NULL),(8291602,1284,26634829,'ok',NULL,NULL),(8291603,1284,26634830,'ok',NULL,NULL),(8291604,1284,26634831,'ok',NULL,NULL),(8291605,1284,26634832,'ok',NULL,NULL),(8291606,1284,26634833,'ok',NULL,NULL),(8291607,1284,26634834,'ok',NULL,NULL),(8291608,1284,26634835,'ok',NULL,NULL),(8291609,1284,26634836,'ok',NULL,NULL),(8291610,1284,26634837,'ok',NULL,NULL),(8291611,1284,26634838,'ok',NULL,NULL),(8291612,1284,26634839,'ok',NULL,NULL),(8291613,1284,26634840,'ok',NULL,NULL),(8291614,1284,26634841,'ok',NULL,NULL),(8291615,1284,26634842,'ok',NULL,NULL),(8291616,1284,26634843,'ok',NULL,NULL),(8291617,1284,26634844,'ok',NULL,NULL),(8291618,1284,26634845,'ok',NULL,NULL),(8291619,1284,26634846,'ok',NULL,NULL),(8291620,1284,26634847,'ok',NULL,NULL),(8291621,1284,26634848,'ok',NULL,NULL),(8291622,1284,26634849,'ok',NULL,NULL),(8291623,1284,26634850,'ok',NULL,NULL),(8291624,1284,26634851,'ok',NULL,NULL),(8291625,1284,26634852,'ok',NULL,NULL),(8291626,1284,26634853,'ok',NULL,NULL),(8291627,1284,26634854,'ok',NULL,NULL),(8291628,1284,26634855,'ok',NULL,NULL),(8291629,1284,26634856,'ok',NULL,NULL),(8291630,1284,26634857,'ok',NULL,NULL),(8291631,1284,26634858,'ok',NULL,NULL),(8291632,1284,26634859,'ok',NULL,NULL),(8291633,1284,26634860,'ok',NULL,NULL),(8291634,1284,26634861,'ok',NULL,NULL),(8291635,1284,26634862,'ok',NULL,NULL),(8291636,1284,26634863,'ok',NULL,NULL),(8291637,1284,26634864,'ok',NULL,NULL),(8291638,1284,26634865,'ok',NULL,NULL),(8291639,1284,26634866,'ok',NULL,NULL),(8291640,1284,26634867,'ok',NULL,NULL),(8291641,1284,26634868,'ok',NULL,NULL),(8291642,1284,26634869,'ok',NULL,NULL),(8291643,1284,26634870,'ok',NULL,NULL),(8291644,1284,26634871,'ok',NULL,NULL),(8291645,1284,26634872,'ok',NULL,NULL),(8291646,1284,26634873,'ok',NULL,NULL),(8291647,1284,26634874,'ok',NULL,NULL),(8291648,1284,26634875,'ok',NULL,NULL),(8291649,1284,26634876,'ok',NULL,NULL),(8291650,1284,26634877,'ok',NULL,NULL),(8291651,1284,26634878,'ok',NULL,NULL),(8291652,1284,26634879,'ok',NULL,NULL),(8291653,1284,26634880,'ok',NULL,NULL),(8291654,1284,26634881,'ok',NULL,NULL),(8291655,1284,26634882,'ok',NULL,NULL),(8291656,1284,26634883,'ok',NULL,NULL),(8291657,1284,26634884,'ok',NULL,NULL),(8291658,1284,26634885,'ok',NULL,NULL),(8291659,1284,26634886,'ok',NULL,NULL),(8291660,1284,26634887,'ok',NULL,NULL),(8291661,1284,26634888,'ok',NULL,NULL),(8291662,1284,26634889,'ok',NULL,NULL),(8291663,1284,26634890,'ok',NULL,NULL),(8291664,1284,26634891,'ok',NULL,NULL),(8291665,1284,26634892,'ok',NULL,NULL),(8291666,1284,26634893,'ok',NULL,NULL),(8291667,1284,26634894,'ok',NULL,NULL),(8291668,1284,26634895,'ok',NULL,NULL),(8291669,1284,26634896,'ok',NULL,NULL),(8291670,1284,26634897,'ok',NULL,NULL),(8291671,1284,26634898,'ok',NULL,NULL),(8291672,1284,26634899,'ok',NULL,NULL),(8291673,1284,26634900,'ok',NULL,NULL),(8291674,1284,26634901,'ok',NULL,NULL),(8291675,1284,26634902,'ok',NULL,NULL),(8291676,1284,26634903,'ok',NULL,NULL),(8291677,1284,26634904,'ok',NULL,NULL),(8291678,1284,26634905,'ok',NULL,NULL),(8291679,1284,26634906,'ok',NULL,NULL),(8291680,1284,26634907,'ok',NULL,NULL),(8291681,1284,26634908,'ok',NULL,NULL),(8291682,1284,26634909,'ok',NULL,NULL),(8291683,1284,26634910,'ok',NULL,NULL),(8291684,1284,26634911,'ok',NULL,NULL),(8291685,1284,26634912,'ok',NULL,NULL),(8291686,1284,26634913,'ok',NULL,NULL),(8291687,1284,26634914,'ok',NULL,NULL),(8291688,1284,26634915,'ok',NULL,NULL),(8291689,1284,26634916,'ok',NULL,NULL),(8291690,1284,26634917,'ok',NULL,NULL),(8291691,1284,26634918,'ok',NULL,NULL),(8291692,1284,26634919,'ok',NULL,NULL),(8291693,1284,26634920,'ok',NULL,NULL),(8291694,1284,26634921,'ok',NULL,NULL),(8291695,1284,26634922,'ok',NULL,NULL),(8291696,1284,26634923,'ok',NULL,NULL),(8291697,1284,26634924,'ok',NULL,NULL),(8291698,1284,26634925,'ok',NULL,NULL),(8291699,1284,26634926,'ok',NULL,NULL),(8291700,1284,26634927,'ok',NULL,NULL),(8291701,1284,26634928,'ok',NULL,NULL),(8291702,1284,26634929,'ok',NULL,NULL),(8291703,1284,26634930,'ok',NULL,NULL),(8291704,1284,26634931,'ok',NULL,NULL),(8291705,1284,26634932,'ok',NULL,NULL),(8291706,1284,26634933,'ok',NULL,NULL),(8291707,1284,26634934,'ok',NULL,NULL),(8291708,1284,26634935,'ok',NULL,NULL),(8291709,1284,26634936,'ok',NULL,NULL),(8291710,1284,26634937,'ok',NULL,NULL),(8291711,1284,26634938,'ok',NULL,NULL),(8291712,1284,26634939,'ok',NULL,NULL),(8291713,1284,26634940,'ok',NULL,NULL),(8291714,1284,26634941,'ok',NULL,NULL),(8291715,1284,26634942,'ok',NULL,NULL),(8291716,1284,26634943,'ok',NULL,NULL),(8291717,1284,26634944,'ok',NULL,NULL),(8291718,1284,26634945,'ok',NULL,NULL),(8291719,1284,26634946,'ok',NULL,NULL),(8291720,1284,26634947,'ok',NULL,NULL),(8291721,1284,26634948,'ok',NULL,NULL),(8291722,1284,26634949,'ok',NULL,NULL),(8291723,1284,26634950,'ok',NULL,NULL),(8291724,1284,26634951,'ok',NULL,NULL),(8291725,1284,26634952,'ok',NULL,NULL),(8291726,1284,26634953,'ok',NULL,NULL),(8291727,1284,26634954,'ok',NULL,NULL),(8291728,1284,26634955,'ok',NULL,NULL),(8291729,1284,26634956,'ok',NULL,NULL),(8291730,1284,26634957,'ok',NULL,NULL),(8291731,1284,26634958,'ok',NULL,NULL),(8291732,1284,26634959,'ok',NULL,NULL),(8291733,1284,26634960,'ok',NULL,NULL),(8291734,1284,26634961,'ok',NULL,NULL),(8291735,1284,26634962,'ok',NULL,NULL),(8291736,1284,26634963,'ok',NULL,NULL),(8291737,1284,26634964,'ok',NULL,NULL),(8291738,1284,26634965,'ok',NULL,NULL),(8291739,1284,26634966,'ok',NULL,NULL),(8291740,1284,26634967,'ok',NULL,NULL),(8291741,1284,26634968,'ok',NULL,NULL),(8291742,1284,26634969,'ok',NULL,NULL),(8291743,1284,26634970,'ok',NULL,NULL),(8291744,1284,26634971,'ok',NULL,NULL),(8291745,1284,26634972,'ok',NULL,NULL),(8291746,1284,26634973,'ok',NULL,NULL),(8291747,1284,26634974,'ok',NULL,NULL),(8291748,1284,26634975,'ok',NULL,NULL),(8291749,1284,26634976,'ok',NULL,NULL),(8291750,1284,26634977,'ok',NULL,NULL),(8291751,1284,26634978,'ok',NULL,NULL),(8291752,1284,26634979,'ok',NULL,NULL),(8291753,1284,26634980,'ok',NULL,NULL),(8291754,1284,26634981,'ok',NULL,NULL),(8291755,1284,26634982,'ok',NULL,NULL),(8291756,1284,26634983,'ok',NULL,NULL),(8291757,1284,26634984,'ok',NULL,NULL),(8291758,1284,26634985,'ok',NULL,NULL),(8291759,1284,26634986,'ok',NULL,NULL),(8291760,1284,26634987,'ok',NULL,NULL),(8291761,1284,26634988,'ok',NULL,NULL),(8291762,1284,26634989,'ok',NULL,NULL),(8291763,1284,26634990,'ok',NULL,NULL),(8291764,1284,26634991,'ok',NULL,NULL),(8291765,1284,26634992,'ok',NULL,NULL),(8291766,1284,26634993,'ok',NULL,NULL),(8291767,1284,26634994,'ok',NULL,NULL),(8291768,1284,26634995,'ok',NULL,NULL),(8291769,1284,26634996,'ok',NULL,NULL),(8291770,1284,26634997,'ok',NULL,NULL),(8291771,1284,26634998,'ok',NULL,NULL),(8291772,1284,26634999,'ok',NULL,NULL),(8291773,1284,26635000,'ok',NULL,NULL),(8291774,1284,26635001,'ok',NULL,NULL),(8291775,1284,26635002,'ok',NULL,NULL),(8291776,1284,26635003,'ok',NULL,NULL),(8291777,1284,26635004,'ok',NULL,NULL),(8291778,1284,26635005,'ok',NULL,NULL),(8291779,1284,26635006,'ok',NULL,NULL),(8291780,1284,26635007,'ok',NULL,NULL),(8291781,1284,26635008,'ok',NULL,NULL),(8291782,1284,26635009,'ok',NULL,NULL),(8291783,1284,26635010,'ok',NULL,NULL),(8291784,1284,26635011,'ok',NULL,NULL),(8291785,1284,26635012,'ok',NULL,NULL),(8291786,1284,26635013,'ok',NULL,NULL),(8291787,1284,26635014,'ok',NULL,NULL),(8291788,1284,26635015,'ok',NULL,NULL),(8291789,1284,26635016,'ok',NULL,NULL),(8291790,1284,26635017,'ok',NULL,NULL),(8291791,1284,26635018,'ok',NULL,NULL),(8291792,1284,26635019,'ok',NULL,NULL),(8291793,1284,26635020,'ok',NULL,NULL),(8291794,1284,26635021,'ok',NULL,NULL),(8291795,1284,26635022,'ok',NULL,NULL),(8291796,1284,26635023,'ok',NULL,NULL),(8291797,1284,26635024,'ok',NULL,NULL),(8291798,1284,26635025,'ok',NULL,NULL),(8291799,1284,26635026,'ok',NULL,NULL),(8291800,1284,26635027,'ok',NULL,NULL),(8291801,1284,26635028,'ok',NULL,NULL),(8291802,1284,26635029,'ok',NULL,NULL),(8291803,1284,26635030,'ok',NULL,NULL),(8291804,1284,26635031,'ok',NULL,NULL),(8291805,1284,26635032,'ok',NULL,NULL),(8291806,1284,26635033,'ok',NULL,NULL),(8291807,1284,26635034,'ok',NULL,NULL),(8291808,1284,26635035,'ok',NULL,NULL),(8291809,1284,26635036,'ok',NULL,NULL),(8291810,1284,26635037,'ok',NULL,NULL),(8291811,1284,26635038,'ok',NULL,NULL),(8291812,1284,26635039,'ok',NULL,NULL),(8291813,1284,26635040,'ok',NULL,NULL),(8291814,1284,26635041,'ok',NULL,NULL),(8291815,1284,26635042,'ok',NULL,NULL),(8291816,1284,26635043,'ok',NULL,NULL),(8291817,1284,26635044,'ok',NULL,NULL),(8291818,1284,26635045,'ok',NULL,NULL),(8291819,1284,26635046,'ok',NULL,NULL),(8291820,1284,26635047,'ok',NULL,NULL),(8291821,1284,26635048,'ok',NULL,NULL),(8291822,1284,26635049,'ok',NULL,NULL),(8291823,1284,26635050,'ok',NULL,NULL),(8291824,1284,26635051,'ok',NULL,NULL),(8291825,1284,26635052,'ok',NULL,NULL),(8291826,1284,26635053,'ok',NULL,NULL),(8291827,1284,26635054,'ok',NULL,NULL),(8291828,1284,26635055,'ok',NULL,NULL),(8291829,1284,26635056,'ok',NULL,NULL),(8291830,1284,26635057,'ok',NULL,NULL),(8291831,1284,26635058,'ok',NULL,NULL),(8291832,1284,26635059,'ok',NULL,NULL),(8291833,1284,26635060,'ok',NULL,NULL),(8291834,1284,26635061,'ok',NULL,NULL),(8291835,1284,26635062,'ok',NULL,NULL),(8291836,1284,26635063,'ok',NULL,NULL),(8291837,1284,26635064,'ok',NULL,NULL),(8291838,1284,26635065,'ok',NULL,NULL),(8291839,1284,26635066,'ok',NULL,NULL),(8291840,1284,26635067,'ok',NULL,NULL),(8291841,1284,26635068,'ok',NULL,NULL),(8291842,1284,26635069,'ok',NULL,NULL),(8291843,1284,26635070,'ok',NULL,NULL),(8291844,1284,26635071,'ok',NULL,NULL),(8291845,1284,26635072,'ok',NULL,NULL),(8291846,1284,26635073,'ok',NULL,NULL),(8291847,1284,26635074,'ok',NULL,NULL),(8291848,1284,26635075,'ok',NULL,NULL),(8291849,1284,26635076,'ok',NULL,NULL),(8291850,1284,26635077,'ok',NULL,NULL),(8291851,1284,26635078,'ok',NULL,NULL),(8291852,1284,26635079,'ok',NULL,NULL),(8291853,1284,26635080,'ok',NULL,NULL),(8291854,1284,26635081,'ok',NULL,NULL),(8291855,1284,26635082,'ok',NULL,NULL),(8291856,1284,26635083,'ok',NULL,NULL),(8291857,1284,26635084,'ok',NULL,NULL),(8291858,1284,26635085,'ok',NULL,NULL),(8291859,1284,26635086,'ok',NULL,NULL),(8291860,1284,26635087,'ok',NULL,NULL),(8291861,1284,26635088,'ok',NULL,NULL),(8291862,1284,26635089,'ok',NULL,NULL),(8291863,1284,26635090,'ok',NULL,NULL),(8291864,1284,26635091,'ok',NULL,NULL),(8291865,1284,26635092,'ok',NULL,NULL),(8291866,1284,26635093,'ok',NULL,NULL),(8291867,1284,26635094,'ok',NULL,NULL),(8291868,1284,26635095,'ok',NULL,NULL),(8291869,1284,26635096,'ok',NULL,NULL),(8291870,1284,26635097,'ok',NULL,NULL),(8291871,1284,26635098,'ok',NULL,NULL),(8291872,1284,26635099,'ok',NULL,NULL),(8291873,1284,26635100,'ok',NULL,NULL),(8291874,1284,26635101,'ok',NULL,NULL),(8291875,1284,26635102,'ok',NULL,NULL),(8291876,1284,26635103,'ok',NULL,NULL),(8291877,1284,26635104,'ok',NULL,NULL),(8291878,1284,26635105,'ok',NULL,NULL),(8291879,1284,26635106,'ok',NULL,NULL),(8291880,1284,26635107,'ok',NULL,NULL),(8291881,1284,26635108,'ok',NULL,NULL),(8291882,1284,26635109,'ok',NULL,NULL),(8291883,1284,26635110,'ok',NULL,NULL),(8291884,1284,26635111,'ok',NULL,NULL),(8291885,1284,26635112,'ok',NULL,NULL),(8291886,1284,26635113,'ok',NULL,NULL),(8291887,1284,26635114,'ok',NULL,NULL),(8291888,1284,26635115,'ok',NULL,NULL),(8291889,1284,26635116,'ok',NULL,NULL),(8291890,1284,26635117,'ok',NULL,NULL),(8291891,1284,26635118,'ok',NULL,NULL),(8291892,1284,26635119,'ok',NULL,NULL),(8291893,1284,26635120,'ok',NULL,NULL),(8291894,1284,26635121,'ok',NULL,NULL),(8291895,1284,26635122,'ok',NULL,NULL),(8291896,1284,26635123,'ok',NULL,NULL),(8291897,1284,26635124,'ok',NULL,NULL),(8291898,1284,26635125,'ok',NULL,NULL),(8291899,1284,26635126,'ok',NULL,NULL),(8291900,1284,26635127,'ok',NULL,NULL),(8291901,1284,26635128,'ok',NULL,NULL),(8291902,1284,26635129,'ok',NULL,NULL),(8291903,1284,26635130,'ok',NULL,NULL),(8291904,1284,26635131,'ok',NULL,NULL),(8291905,1284,26635132,'ok',NULL,NULL),(8291906,1284,26635133,'ok',NULL,NULL),(8291907,1284,26635134,'ok',NULL,NULL),(8291908,1284,26635135,'ok',NULL,NULL),(8291909,1284,26635136,'ok',NULL,NULL),(8291910,1284,26635137,'ok',NULL,NULL),(8291911,1284,26635138,'ok',NULL,NULL),(8291912,1284,26635139,'ok',NULL,NULL),(8291913,1284,26635140,'ok',NULL,NULL),(8291914,1284,26635141,'ok',NULL,NULL),(8291915,1284,26635142,'ok',NULL,NULL),(8291916,1284,26635143,'ok',NULL,NULL),(8291917,1284,26635144,'ok',NULL,NULL),(8291918,1284,26635145,'ok',NULL,NULL),(8291919,1284,26635146,'ok',NULL,NULL),(8291920,1284,26635147,'ok',NULL,NULL),(8291921,1284,26635148,'ok',NULL,NULL),(8291922,1284,26635149,'ok',NULL,NULL),(8291923,1284,26635150,'ok',NULL,NULL),(8291924,1284,26635151,'ok',NULL,NULL),(8291925,1284,26635152,'ok',NULL,NULL),(8291926,1284,26635153,'ok',NULL,NULL),(8291927,1284,26635154,'ok',NULL,NULL),(8291928,1284,26635155,'ok',NULL,NULL),(8291929,1284,26635156,'ok',NULL,NULL),(8291930,1284,26635157,'ok',NULL,NULL),(8291931,1284,26635158,'ok',NULL,NULL),(8291932,1284,26635159,'ok',NULL,NULL),(8291933,1284,26635160,'ok',NULL,NULL),(8291934,1284,26635161,'ok',NULL,NULL),(8291935,1284,26635162,'ok',NULL,NULL),(8291936,1284,26635163,'ok',NULL,NULL),(8291937,1284,26635164,'ok',NULL,NULL),(8291938,1284,26635165,'ok',NULL,NULL),(8291939,1284,26635166,'ok',NULL,NULL),(8291940,1284,26635167,'ok',NULL,NULL),(8291941,1284,26635168,'ok',NULL,NULL),(8291942,1284,26635169,'ok',NULL,NULL),(8291943,1284,26635170,'ok',NULL,NULL),(8291944,1284,26635171,'ok',NULL,NULL),(8291945,1284,26635172,'ok',NULL,NULL),(8291946,1284,26635173,'ok',NULL,NULL),(8291947,1284,26635174,'ok',NULL,NULL),(8291948,1284,26635175,'ok',NULL,NULL),(8291949,1284,26635176,'ok',NULL,NULL),(8291950,1284,26635177,'ok',NULL,NULL),(8291951,1284,26635178,'ok',NULL,NULL),(8291952,1284,26635179,'ok',NULL,NULL),(8291953,1284,26635180,'ok',NULL,NULL),(8291954,1284,26635181,'ok',NULL,NULL),(8291955,1284,26635182,'ok',NULL,NULL),(8291956,1284,26635183,'ok',NULL,NULL),(8291957,1284,26635184,'ok',NULL,NULL),(8291958,1284,26635185,'ok',NULL,NULL),(8291959,1284,26635186,'ok',NULL,NULL),(8291960,1284,26635187,'ok',NULL,NULL),(8291961,1284,26635188,'ok',NULL,NULL),(8291962,1284,26635189,'ok',NULL,NULL),(8291963,1284,26635190,'ok',NULL,NULL),(8291964,1284,26635191,'ok',NULL,NULL),(8291965,1284,26635192,'ok',NULL,NULL),(8291966,1284,26635193,'ok',NULL,NULL),(8291967,1284,26635194,'ok',NULL,NULL),(8291968,1284,26635195,'ok',NULL,NULL),(8291969,1284,26635196,'ok',NULL,NULL),(8291970,1284,26635197,'ok',NULL,NULL),(8291971,1284,26635198,'ok',NULL,NULL),(8291972,1284,26635199,'ok',NULL,NULL),(8291973,1284,26635200,'ok',NULL,NULL),(8291974,1284,26635201,'ok',NULL,NULL),(8291975,1284,26635202,'ok',NULL,NULL),(8291976,1284,26635203,'ok',NULL,NULL),(8291977,1284,26635204,'ok',NULL,NULL),(8291978,1284,26635205,'ok',NULL,NULL),(8291979,1284,26635206,'ok',NULL,NULL),(8291980,1284,26635207,'ok',NULL,NULL),(8291981,1284,26635208,'ok',NULL,NULL),(8291982,1284,26635209,'ok',NULL,NULL),(8291983,1284,26635210,'ok',NULL,NULL),(8291984,1284,26635211,'ok',NULL,NULL),(8291985,1284,26635212,'ok',NULL,NULL),(8291986,1284,26635213,'ok',NULL,NULL),(8291987,1284,26635214,'ok',NULL,NULL),(8291988,1284,26635215,'ok',NULL,NULL),(8291989,1284,26635216,'ok',NULL,NULL),(8291990,1284,26635217,'ok',NULL,NULL),(8291991,1284,26635218,'ok',NULL,NULL),(8291992,1284,26635219,'ok',NULL,NULL),(8291993,1284,26635220,'ok',NULL,NULL),(8291994,1284,26635221,'ok',NULL,NULL),(8291995,1284,26635222,'ok',NULL,NULL),(8291996,1284,26635223,'ok',NULL,NULL),(8291997,1284,26635224,'ok',NULL,NULL),(8291998,1284,26635225,'ok',NULL,NULL),(8291999,1284,26635226,'ok',NULL,NULL),(8292000,1284,26635227,'ok',NULL,NULL),(8292001,1284,26635228,'ok',NULL,NULL),(8292002,1284,26635229,'ok',NULL,NULL),(8292003,1284,26635230,'ok',NULL,NULL),(8292004,1284,26635231,'ok',NULL,NULL),(8292005,1284,26635232,'ok',NULL,NULL),(8292006,1284,26635233,'ok',NULL,NULL),(8292007,1284,26635234,'ok',NULL,NULL),(8292008,1284,26635235,'ok',NULL,NULL),(8292009,1284,26635236,'ok',NULL,NULL),(8292010,1284,26635237,'ok',NULL,NULL),(8292011,1284,26635238,'ok',NULL,NULL),(8292012,1284,26635239,'ok',NULL,NULL),(8292013,1284,26635240,'ok',NULL,NULL),(8292014,1284,26635241,'ok',NULL,NULL),(8292015,1284,26635242,'ok',NULL,NULL),(8292016,1284,26635243,'ok',NULL,NULL),(8292017,1284,26635244,'ok',NULL,NULL),(8292018,1284,26635245,'ok',NULL,NULL),(8292019,1284,26635246,'ok',NULL,NULL),(8292020,1284,26635247,'ok',NULL,NULL),(8292021,1284,26635248,'ok',NULL,NULL),(8292022,1284,26635249,'ok',NULL,NULL),(8292023,1284,26635250,'ok',NULL,NULL),(8292024,1284,26635251,'ok',NULL,NULL),(8292025,1284,26635252,'ok',NULL,NULL),(8292026,1284,26635253,'ok',NULL,NULL),(8292027,1284,26635254,'ok',NULL,NULL),(8292028,1284,26635255,'ok',NULL,NULL),(8292029,1284,26635256,'ok',NULL,NULL),(8292030,1284,26635257,'ok',NULL,NULL),(8292031,1284,26635258,'ok',NULL,NULL),(8292032,1284,26635259,'ok',NULL,NULL),(8292033,1284,26635260,'ok',NULL,NULL),(8292034,1284,26635261,'ok',NULL,NULL),(8292035,1284,26635262,'ok',NULL,NULL),(8292036,1284,26635263,'ok',NULL,NULL),(8292037,1284,26635264,'ok',NULL,NULL),(8292038,1284,26635265,'ok',NULL,NULL),(8292039,1284,26635266,'ok',NULL,NULL),(8292040,1284,26635267,'ok',NULL,NULL),(8292041,1284,26635268,'ok',NULL,NULL),(8292042,1284,26635269,'ok',NULL,NULL),(8292043,1284,26635270,'ok',NULL,NULL),(8292044,1284,26635271,'ok',NULL,NULL),(8292045,1284,26635272,'ok',NULL,NULL),(8292046,1284,26635273,'ok',NULL,NULL),(8292047,1284,26635274,'ok',NULL,NULL),(8292048,1284,26635275,'ok',NULL,NULL),(8292049,1284,26635276,'ok',NULL,NULL),(8292050,1284,26635277,'ok',NULL,NULL),(8292051,1284,26635278,'ok',NULL,NULL),(8292052,1284,26635279,'ok',NULL,NULL),(8292053,1284,26635280,'ok',NULL,NULL),(8292054,1284,26635281,'ok',NULL,NULL),(8292055,1284,26635282,'ok',NULL,NULL),(8292056,1284,26635283,'ok',NULL,NULL),(8292057,1284,26635284,'ok',NULL,NULL),(8292058,1284,26635285,'ok',NULL,NULL),(8292059,1284,26635286,'ok',NULL,NULL),(8292060,1284,26635287,'ok',NULL,NULL),(8292061,1284,26635288,'ok',NULL,NULL),(8292062,1284,26635289,'ok',NULL,NULL),(8292063,1284,26635290,'ok',NULL,NULL),(8292064,1284,26635291,'ok',NULL,NULL),(8292065,1284,26635292,'ok',NULL,NULL),(8292066,1284,26635293,'ok',NULL,NULL),(8292067,1284,26635294,'ok',NULL,NULL),(8292068,1284,26635295,'ok',NULL,NULL),(8292069,1284,26635296,'ok',NULL,NULL),(8292070,1284,26635297,'ok',NULL,NULL),(8292071,1284,26635298,'ok',NULL,NULL),(8292072,1284,26635299,'ok',NULL,NULL),(8292073,1284,26635300,'ok',NULL,NULL),(8292074,1284,26635301,'ok',NULL,NULL),(8292075,1284,26635302,'ok',NULL,NULL),(8292076,1284,26635303,'ok',NULL,NULL),(8292077,1284,26635304,'ok',NULL,NULL),(8292078,1284,26635305,'ok',NULL,NULL),(8292079,1284,26635306,'ok',NULL,NULL),(8292080,1284,26635307,'ok',NULL,NULL),(8292081,1284,26635308,'ok',NULL,NULL),(8292082,1284,26635309,'ok',NULL,NULL),(8292083,1284,26635310,'ok',NULL,NULL),(8292084,1284,26635311,'ok',NULL,NULL),(8292085,1284,26635312,'ok',NULL,NULL),(8292086,1284,26635313,'ok',NULL,NULL),(8292087,1284,26635314,'ok',NULL,NULL),(8292088,1284,26635315,'ok',NULL,NULL),(8292089,1284,26635316,'ok',NULL,NULL),(8292090,1284,26635317,'ok',NULL,NULL),(8292091,1284,26635318,'ok',NULL,NULL),(8292092,1284,26635319,'ok',NULL,NULL),(8292093,1284,26635320,'ok',NULL,NULL),(8292094,1284,26635321,'ok',NULL,NULL),(8292095,1284,26635322,'ok',NULL,NULL),(8292096,1284,26635323,'ok',NULL,NULL),(8292097,1284,26635324,'ok',NULL,NULL),(8292098,1284,26635325,'ok',NULL,NULL),(8292099,1284,26635326,'ok',NULL,NULL),(8292100,1284,26635327,'ok',NULL,NULL),(8292101,1284,26635328,'ok',NULL,NULL),(8292102,1284,26635329,'ok',NULL,NULL),(8292103,1284,26635330,'ok',NULL,NULL),(8292104,1284,26635331,'ok',NULL,NULL),(8292105,1284,26635332,'ok',NULL,NULL),(8292106,1284,26635333,'ok',NULL,NULL),(8292107,1284,26635334,'ok',NULL,NULL),(8292108,1284,26635335,'ok',NULL,NULL),(8292109,1284,26635336,'ok',NULL,NULL),(8292110,1284,26635337,'ok',NULL,NULL),(8292111,1284,26635338,'ok',NULL,NULL),(8292112,1284,26635339,'ok',NULL,NULL),(8292113,1284,26635340,'ok',NULL,NULL),(8292114,1284,26635341,'ok',NULL,NULL),(8292115,1284,26635342,'ok',NULL,NULL),(8292116,1284,26635343,'ok',NULL,NULL),(8292117,1284,26635344,'ok',NULL,NULL),(8292118,1284,26635345,'ok',NULL,NULL),(8292119,1284,26635346,'ok',NULL,NULL),(8292120,1284,26635347,'ok',NULL,NULL),(8292121,1284,26635348,'ok',NULL,NULL),(8292122,1284,26635349,'ok',NULL,NULL),(8292123,1284,26635350,'ok',NULL,NULL),(8292124,1284,26635351,'ok',NULL,NULL),(8292125,1284,26635352,'ok',NULL,NULL),(8292126,1284,26635353,'ok',NULL,NULL),(8292127,1284,26635354,'ok',NULL,NULL),(8292128,1284,26635355,'ok',NULL,NULL),(8292129,1284,26635356,'ok',NULL,NULL),(8292130,1284,26635357,'ok',NULL,NULL),(8292131,1284,26635358,'ok',NULL,NULL),(8292132,1284,26635359,'ok',NULL,NULL),(8292133,1284,26635360,'ok',NULL,NULL),(8292134,1284,26635361,'ok',NULL,NULL),(8292135,1284,26635362,'ok',NULL,NULL),(8292136,1284,26635363,'ok',NULL,NULL),(8292137,1284,26635364,'ok',NULL,NULL),(8292138,1284,26635365,'ok',NULL,NULL),(8292139,1284,26635366,'ok',NULL,NULL),(8292140,1284,26635367,'ok',NULL,NULL),(8292141,1284,26635368,'ok',NULL,NULL),(8292142,1284,26635369,'ok',NULL,NULL),(8292143,1284,26635370,'ok',NULL,NULL),(8292144,1284,26635371,'ok',NULL,NULL),(8292145,1284,26635372,'ok',NULL,NULL),(8292146,1284,26635373,'ok',NULL,NULL),(8292147,1284,26635374,'ok',NULL,NULL),(8292148,1284,26635375,'ok',NULL,NULL),(8292149,1284,26635376,'ok',NULL,NULL),(8292150,1284,26635377,'ok',NULL,NULL),(8292151,1284,26635378,'ok',NULL,NULL),(8292152,1284,26635379,'ok',NULL,NULL),(8292153,1284,26635380,'ok',NULL,NULL),(8292154,1284,26635381,'ok',NULL,NULL),(8292155,1284,26635382,'ok',NULL,NULL),(8292156,1284,26635383,'ok',NULL,NULL),(8292157,1284,26635384,'ok',NULL,NULL),(8292158,1284,26635385,'ok',NULL,NULL),(8292159,1284,26635386,'ok',NULL,NULL),(8292160,1284,26635387,'ok',NULL,NULL),(8292161,1284,26635388,'ok',NULL,NULL),(8292162,1284,26635389,'ok',NULL,NULL),(8292163,1284,26635390,'ok',NULL,NULL),(8292164,1284,26635391,'ok',NULL,NULL),(8292165,1284,26635392,'ok',NULL,NULL),(8292166,1284,26635393,'ok',NULL,NULL),(8292167,1284,26635394,'ok',NULL,NULL),(8292168,1284,26635395,'ok',NULL,NULL),(8292169,1284,26635396,'ok',NULL,NULL),(8292170,1284,26635397,'ok',NULL,NULL),(8292171,1284,26635398,'ok',NULL,NULL),(8292172,1284,26635399,'ok',NULL,NULL),(8292173,1284,26635400,'ok',NULL,NULL),(8292174,1284,26635401,'ok',NULL,NULL),(8292175,1284,26635402,'ok',NULL,NULL),(8292176,1284,26635403,'ok',NULL,NULL),(8292177,1284,26635404,'ok',NULL,NULL),(8292178,1284,26635405,'ok',NULL,NULL),(8292179,1284,26635406,'ok',NULL,NULL),(8292180,1284,26635407,'ok',NULL,NULL),(8292181,1284,26635408,'ok',NULL,NULL),(8292182,1284,26635409,'ok',NULL,NULL),(8292183,1284,26635410,'ok',NULL,NULL),(8292184,1284,26635411,'ok',NULL,NULL),(8292185,1284,26635412,'ok',NULL,NULL),(8292186,1284,26635413,'ok',NULL,NULL),(8292187,1284,26635414,'ok',NULL,NULL),(8292188,1284,26635415,'ok',NULL,NULL),(8292189,1284,26635416,'ok',NULL,NULL),(8292190,1284,26635417,'ok',NULL,NULL),(8292191,1284,26635418,'ok',NULL,NULL),(8292192,1284,26635419,'ok',NULL,NULL),(8292193,1284,26635420,'ok',NULL,NULL),(8292194,1284,26635421,'ok',NULL,NULL),(8292195,1284,26635422,'ok',NULL,NULL),(8292196,1284,26635423,'ok',NULL,NULL),(8292197,1284,26635424,'ok',NULL,NULL),(8292198,1284,26635425,'ok',NULL,NULL),(8292199,1284,26635426,'ok',NULL,NULL),(8292200,1284,26635427,'ok',NULL,NULL),(8292201,1284,26635428,'ok',NULL,NULL),(8292202,1284,26635429,'ok',NULL,NULL),(8292203,1284,26635430,'ok',NULL,NULL),(8292204,1284,26635431,'ok',NULL,NULL),(8292205,1284,26635432,'ok',NULL,NULL),(8292206,1284,26635433,'ok',NULL,NULL),(8292207,1284,26635434,'ok',NULL,NULL),(8292208,1284,26635435,'ok',NULL,NULL),(8292209,1284,26635436,'ok',NULL,NULL),(8292210,1284,26635437,'ok',NULL,NULL),(8292211,1284,26635438,'ok',NULL,NULL),(8292212,1284,26635439,'ok',NULL,NULL),(8292213,1284,26635440,'ok',NULL,NULL),(8292214,1284,26635441,'ok',NULL,NULL),(8292215,1284,26635442,'ok',NULL,NULL),(8292216,1284,26635443,'ok',NULL,NULL),(8292217,1284,26635444,'ok',NULL,NULL),(8292218,1284,26635445,'ok',NULL,NULL),(8292219,1284,26635446,'ok',NULL,NULL),(8292220,1284,26635447,'ok',NULL,NULL),(8292221,1284,26635448,'ok',NULL,NULL),(8292222,1284,26635449,'ok',NULL,NULL),(8292223,1284,26635450,'ok',NULL,NULL),(8292224,1284,26635451,'ok',NULL,NULL),(8292225,1284,26635452,'ok',NULL,NULL),(8292226,1284,26635453,'ok',NULL,NULL),(8292227,1284,26635454,'ok',NULL,NULL),(8292228,1284,26635455,'ok',NULL,NULL),(8292229,1284,26635456,'ok',NULL,NULL),(8292230,1284,26635457,'ok',NULL,NULL),(8292231,1284,26635458,'ok',NULL,NULL),(8292232,1284,26635459,'ok',NULL,NULL),(8292233,1284,26635460,'ok',NULL,NULL),(8292234,1284,26635461,'ok',NULL,NULL),(8292235,1284,26635462,'ok',NULL,NULL),(8292236,1284,26635463,'ok',NULL,NULL),(8292237,1284,26635464,'ok',NULL,NULL),(8292238,1284,26635465,'ok',NULL,NULL),(8292239,1284,26635466,'ok',NULL,NULL),(8292240,1284,26635467,'ok',NULL,NULL),(8292241,1284,26635468,'ok',NULL,NULL),(8292242,1284,26635469,'ok',NULL,NULL),(8292243,1284,26635470,'ok',NULL,NULL),(8292244,1284,26635471,'ok',NULL,NULL),(8292245,1284,26635472,'ok',NULL,NULL),(8292246,1284,26635473,'ok',NULL,NULL),(8292247,1284,26635474,'ok',NULL,NULL),(8292248,1284,26635475,'ok',NULL,NULL),(8292249,1284,26635476,'ok',NULL,NULL),(8292250,1284,26635477,'ok',NULL,NULL),(8292251,1284,26635478,'ok',NULL,NULL),(8292252,1284,26635479,'ok',NULL,NULL),(8292253,1284,26635480,'ok',NULL,NULL),(8292254,1284,26635481,'ok',NULL,NULL),(8292255,1284,26635482,'ok',NULL,NULL),(8292256,1284,26635483,'ok',NULL,NULL),(8292257,1284,26635484,'ok',NULL,NULL),(8292258,1284,26635485,'ok',NULL,NULL),(8292259,1284,26635486,'ok',NULL,NULL),(8292260,1284,26635487,'ok',NULL,NULL),(8292261,1284,26635488,'ok',NULL,NULL),(8292262,1284,26635489,'ok',NULL,NULL),(8292263,1284,26635490,'ok',NULL,NULL),(8292264,1284,26635491,'ok',NULL,NULL),(8292265,1284,26635492,'ok',NULL,NULL),(8292266,1284,26635493,'ok',NULL,NULL),(8292267,1284,26635494,'ok',NULL,NULL),(8292268,1284,26635495,'ok',NULL,NULL),(8292269,1284,26635496,'ok',NULL,NULL),(8292270,1284,26635497,'ok',NULL,NULL),(8292271,1284,26635498,'ok',NULL,NULL),(8292272,1284,26635499,'ok',NULL,NULL),(8292273,1284,26635500,'ok',NULL,NULL),(8292274,1284,26635501,'ok',NULL,NULL),(8292275,1284,26635502,'ok',NULL,NULL),(8292276,1284,26635503,'ok',NULL,NULL),(8292277,1284,26635504,'ok',NULL,NULL),(8292278,1284,26635505,'ok',NULL,NULL),(8292279,1284,26635506,'ok',NULL,NULL),(8292280,1284,26635507,'ok',NULL,NULL),(8292281,1284,26635508,'ok',NULL,NULL),(8292282,1284,26635509,'ok',NULL,NULL),(8292283,1284,26635510,'ok',NULL,NULL),(8292284,1284,26635511,'ok',NULL,NULL),(8292285,1284,26635512,'ok',NULL,NULL),(8292286,1284,26635513,'ok',NULL,NULL),(8292287,1284,26635514,'ok',NULL,NULL),(8292288,1284,26635515,'ok',NULL,NULL),(8292289,1284,26635516,'ok',NULL,NULL),(8292290,1284,26635517,'ok',NULL,NULL),(8292291,1284,26635518,'ok',NULL,NULL),(8292292,1284,26635519,'ok',NULL,NULL),(8292293,1284,26635520,'ok',NULL,NULL),(8292294,1284,26635521,'ok',NULL,NULL),(8292295,1284,26635522,'ok',NULL,NULL),(8292296,1284,26635523,'ok',NULL,NULL),(8292297,1284,26635524,'ok',NULL,NULL),(8292298,1284,26635525,'ok',NULL,NULL),(8292299,1284,26635526,'ok',NULL,NULL),(8292300,1284,26635527,'ok',NULL,NULL),(8292301,1284,26635528,'ok',NULL,NULL),(8292302,1284,26635529,'ok',NULL,NULL),(8292303,1284,26635530,'ok',NULL,NULL),(8292304,1284,26635531,'ok',NULL,NULL),(8292305,1284,26635532,'ok',NULL,NULL),(8292306,1284,26635533,'ok',NULL,NULL),(8292307,1284,26635534,'ok',NULL,NULL),(8292308,1284,26635535,'ok',NULL,NULL),(8292309,1284,26635536,'ok',NULL,NULL),(8292310,1284,26635537,'ok',NULL,NULL),(8292311,1284,26635538,'ok',NULL,NULL),(8292312,1284,26635539,'ok',NULL,NULL),(8292313,1284,26635540,'ok',NULL,NULL),(8292314,1284,26635541,'ok',NULL,NULL),(8292315,1284,26635542,'ok',NULL,NULL),(8292316,1284,26635543,'ok',NULL,NULL),(8292317,1284,26635544,'ok',NULL,NULL),(8292318,1284,26635545,'ok',NULL,NULL),(8292319,1284,26635546,'ok',NULL,NULL),(8292320,1284,26635547,'ok',NULL,NULL),(8292321,1284,26635548,'ok',NULL,NULL),(8292322,1284,26635549,'ok',NULL,NULL),(8292323,1284,26635550,'ok',NULL,NULL),(8292324,1284,26635551,'ok',NULL,NULL),(8292325,1284,26635552,'ok',NULL,NULL),(8292326,1284,26635553,'ok',NULL,NULL),(8292327,1284,26635554,'ok',NULL,NULL),(8292328,1284,26635555,'ok',NULL,NULL),(8292329,1284,26635556,'ok',NULL,NULL),(8292330,1284,26635557,'ok',NULL,NULL),(8292331,1284,26635558,'ok',NULL,NULL),(8292332,1284,26635559,'ok',NULL,NULL),(8292333,1284,26635560,'ok',NULL,NULL),(8292334,1284,26635561,'ok',NULL,NULL),(8292335,1284,26635562,'ok',NULL,NULL),(8292336,1284,26635563,'ok',NULL,NULL),(8292337,1284,26635564,'ok',NULL,NULL),(8292338,1284,26635565,'ok',NULL,NULL),(8292339,1284,26635566,'ok',NULL,NULL),(8292340,1284,26635567,'ok',NULL,NULL),(8292341,1284,26635568,'ok',NULL,NULL),(8292342,1284,26635569,'ok',NULL,NULL),(8292343,1284,26635570,'ok',NULL,NULL),(8292344,1284,26635571,'ok',NULL,NULL),(8292345,1284,26635572,'ok',NULL,NULL),(8292346,1284,26635573,'ok',NULL,NULL),(8292347,1284,26635574,'ok',NULL,NULL),(8292348,1284,26635575,'ok',NULL,NULL),(8292349,1284,26635576,'ok',NULL,NULL),(8292350,1284,26635577,'ok',NULL,NULL),(8292351,1284,26635578,'ok',NULL,NULL),(8292352,1284,26635579,'ok',NULL,NULL),(8292353,1284,26635580,'ok',NULL,NULL),(8292354,1284,26635581,'ok',NULL,NULL),(8292355,1284,26635582,'ok',NULL,NULL),(8292356,1284,26635583,'ok',NULL,NULL),(8292357,1284,26635584,'ok',NULL,NULL),(8292358,1284,26635585,'ok',NULL,NULL),(8292359,1284,26635586,'ok',NULL,NULL),(8292360,1284,26635587,'ok',NULL,NULL),(8292361,1284,26635588,'ok',NULL,NULL),(8292362,1284,26635589,'ok',NULL,NULL),(8292363,1284,26635590,'ok',NULL,NULL),(8292364,1284,26635591,'ok',NULL,NULL),(8292365,1284,26635592,'ok',NULL,NULL),(8292366,1284,26635593,'ok',NULL,NULL),(8292367,1284,26635594,'ok',NULL,NULL),(8292368,1284,26635595,'ok',NULL,NULL),(8292369,1284,26635596,'ok',NULL,NULL),(8292370,1284,26635597,'ok',NULL,NULL),(8292371,1284,26635598,'ok',NULL,NULL),(8292372,1284,26635599,'ok',NULL,NULL),(8292373,1284,26635600,'ok',NULL,NULL),(8292374,1284,26635601,'ok',NULL,NULL),(8292375,1284,26635602,'ok',NULL,NULL),(8292376,1284,26635603,'ok',NULL,NULL),(8292377,1284,26635604,'ok',NULL,NULL),(8292378,1284,26635605,'ok',NULL,NULL),(8292379,1284,26635606,'ok',NULL,NULL),(8292380,1284,26635607,'ok',NULL,NULL),(8292381,1284,26635608,'ok',NULL,NULL),(8292382,1284,26635609,'ok',NULL,NULL),(8292383,1284,26635610,'ok',NULL,NULL),(8292384,1284,26635611,'ok',NULL,NULL),(8292385,1284,26635612,'ok',NULL,NULL),(8292386,1284,26635613,'ok',NULL,NULL),(8292387,1284,26635614,'ok',NULL,NULL),(8292388,1284,26635615,'ok',NULL,NULL),(8292389,1284,26635616,'ok',NULL,NULL),(8292390,1284,26635617,'ok',NULL,NULL),(8292391,1284,26635618,'ok',NULL,NULL),(8292392,1284,26635619,'ok',NULL,NULL),(8292393,1284,26635620,'ok',NULL,NULL),(8292394,1284,26635621,'ok',NULL,NULL),(8292395,1284,26635622,'ok',NULL,NULL),(8292396,1284,26635623,'ok',NULL,NULL),(8292397,1284,26635624,'ok',NULL,NULL),(8292398,1284,26635625,'ok',NULL,NULL),(8292399,1284,26635626,'ok',NULL,NULL),(8292400,1284,26635627,'ok',NULL,NULL),(8292401,1284,26635628,'ok',NULL,NULL),(8292402,1284,26635629,'ok',NULL,NULL),(8292403,1284,26635630,'ok',NULL,NULL),(8292404,1284,26635631,'ok',NULL,NULL),(8292405,1284,26635632,'ok',NULL,NULL),(8292406,1284,26635633,'ok',NULL,NULL),(8292407,1284,26635634,'ok',NULL,NULL),(8292408,1284,26635635,'ok',NULL,NULL),(8292409,1284,26635636,'ok',NULL,NULL),(8292410,1284,26635637,'ok',NULL,NULL),(8292411,1284,26635638,'ok',NULL,NULL),(8292412,1284,26635639,'ok',NULL,NULL),(8292413,1284,26635640,'ok',NULL,NULL),(8292414,1284,26635641,'ok',NULL,NULL),(8292415,1284,26635642,'ok',NULL,NULL),(8292416,1284,26635643,'ok',NULL,NULL),(8292417,1284,26635644,'ok',NULL,NULL),(8292418,1284,26635645,'ok',NULL,NULL),(8292419,1284,26635646,'ok',NULL,NULL),(8292420,1284,26635647,'ok',NULL,NULL),(8292421,1284,26635648,'ok',NULL,NULL),(8292422,1284,26635649,'ok',NULL,NULL),(8292423,1284,26635650,'ok',NULL,NULL),(8292424,1284,26635651,'ok',NULL,NULL),(8292425,1284,26635652,'ok',NULL,NULL),(8292426,1284,26635653,'ok',NULL,NULL),(8292427,1284,26635654,'ok',NULL,NULL),(8292428,1284,26635655,'ok',NULL,NULL),(8292429,1284,26635656,'ok',NULL,NULL),(8292430,1284,26635657,'ok',NULL,NULL),(8292431,1284,26635658,'ok',NULL,NULL),(8292432,1284,26635659,'ok',NULL,NULL),(8292433,1284,26635660,'ok',NULL,NULL),(8292434,1284,26635661,'ok',NULL,NULL),(8292435,1284,26635662,'ok',NULL,NULL),(8292436,1284,26635663,'ok',NULL,NULL),(8292437,1284,26635664,'ok',NULL,NULL),(8292438,1284,26635665,'ok',NULL,NULL),(8292439,1284,26635666,'ok',NULL,NULL),(8292440,1284,26635667,'ok',NULL,NULL),(8292441,1284,26635668,'ok',NULL,NULL),(8292442,1284,26635669,'ok',NULL,NULL),(8292443,1284,26635670,'ok',NULL,NULL),(8292444,1284,26635671,'ok',NULL,NULL),(8292445,1284,26635672,'ok',NULL,NULL),(8292446,1284,26635673,'ok',NULL,NULL),(8292447,1284,26635674,'ok',NULL,NULL),(8292448,1284,26635675,'ok',NULL,NULL),(8292449,1284,26635676,'ok',NULL,NULL),(8292450,1284,26635677,'ok',NULL,NULL),(8292451,1284,26635678,'ok',NULL,NULL),(8292452,1284,26635679,'ok',NULL,NULL),(8292453,1284,26635680,'ok',NULL,NULL),(8292454,1284,26635681,'ok',NULL,NULL),(8292455,1284,26635682,'ok',NULL,NULL),(8292456,1284,26635683,'ok',NULL,NULL),(8292457,1284,26635684,'ok',NULL,NULL),(8292458,1284,26635685,'ok',NULL,NULL),(8292459,1284,26635686,'ok',NULL,NULL),(8292460,1284,26635687,'ok',NULL,NULL),(8292461,1284,26635688,'ok',NULL,NULL),(8292462,1284,26635689,'ok',NULL,NULL),(8292463,1284,26635690,'ok',NULL,NULL),(8292464,1284,26635691,'ok',NULL,NULL),(8292465,1284,26635692,'ok',NULL,NULL),(8292466,1284,26635693,'ok',NULL,NULL),(8292467,1284,26635694,'ok',NULL,NULL),(8292468,1284,26635695,'ok',NULL,NULL),(8292469,1284,26635696,'ok',NULL,NULL),(8292470,1284,26635697,'ok',NULL,NULL),(8292471,1284,26635698,'ok',NULL,NULL),(8292472,1284,26635699,'ok',NULL,NULL),(8292473,1284,26635700,'ok',NULL,NULL),(8292474,1284,26635701,'ok',NULL,NULL),(8292475,1284,26635702,'ok',NULL,NULL),(8292476,1284,26635703,'ok',NULL,NULL),(8292477,1284,26635704,'ok',NULL,NULL),(8292478,1284,26635705,'ok',NULL,NULL),(8292479,1284,26635706,'ok',NULL,NULL),(8292480,1284,26635707,'ok',NULL,NULL),(8292481,1284,26635708,'ok',NULL,NULL),(8292482,1284,26635709,'ok',NULL,NULL),(8292483,1284,26635710,'ok',NULL,NULL),(8292484,1284,26635711,'ok',NULL,NULL),(8292485,1284,26635712,'ok',NULL,NULL),(8292486,1284,26635713,'ok',NULL,NULL),(8292487,1284,26635714,'ok',NULL,NULL),(8292488,1284,26635715,'ok',NULL,NULL),(8292489,1284,26635719,'ok',NULL,NULL),(8292490,1291,26636911,'ok',NULL,NULL),(8292491,1291,26636912,'ok',NULL,NULL),(8292492,1291,26636913,'ok',NULL,NULL),(8292493,1291,26636914,'ok',NULL,NULL),(8292494,1291,26636915,'ok',NULL,NULL),(8292495,1291,26636916,'ok',NULL,NULL),(8292496,1291,26636917,'ok',NULL,NULL),(8292497,1291,26636918,'ok',NULL,NULL),(8292498,1291,26636919,'ok',NULL,NULL),(8292499,1291,26636920,'ok',NULL,NULL),(8292500,1291,26636921,'ok',NULL,NULL),(8292501,1291,26636922,'ok',NULL,NULL),(8292502,1291,26636923,'ok',NULL,NULL),(8292503,1291,26636924,'ok',NULL,NULL),(8292504,1291,26636925,'ok',NULL,NULL),(8292505,1291,26636926,'ok',NULL,NULL),(8292506,1291,26636927,'ok',NULL,NULL),(8292507,1291,26636928,'ok',NULL,NULL),(8292508,1291,26636929,'ok',NULL,NULL),(8292511,1291,26636932,'ok',NULL,NULL),(8292512,1291,26636933,'ok',NULL,NULL),(8292513,1291,26636934,'ok',NULL,NULL),(8292514,1291,26636935,'ok',NULL,NULL),(8292515,1291,26636936,'ok',NULL,NULL),(8292516,1291,26636937,'ok',NULL,NULL),(8292517,1291,26636938,'ok',NULL,NULL),(8292518,1291,26636939,'ok',NULL,NULL),(8292519,1291,26636940,'ok',NULL,NULL),(8292520,1291,26636941,'ok',NULL,NULL),(8292521,1291,26636942,'ok',NULL,NULL),(8292522,1291,26636943,'ok',NULL,NULL),(8292523,1291,26636944,'ok',NULL,NULL),(8292524,1291,26636945,'ok',NULL,NULL),(8292525,1291,26636946,'ok',NULL,NULL),(8292526,1291,26636947,'ok',NULL,NULL),(8292527,1291,26636948,'ok',NULL,NULL),(8292528,1291,26636949,'ok',NULL,NULL),(8292529,1291,26636950,'ok',NULL,NULL),(8292530,1291,26636951,'ok',NULL,NULL),(8292531,1291,26636952,'ok',NULL,NULL),(8292532,1291,26636953,'ok',NULL,NULL),(8292533,1291,26636954,'ok',NULL,NULL),(8292534,1291,26636955,'ok',NULL,NULL),(8292535,1291,26636956,'ok',NULL,NULL),(8292536,1291,26636957,'ok',NULL,NULL),(8292537,1291,26636958,'ok',NULL,NULL),(8292538,1291,26636959,'ok',NULL,NULL),(8292539,1291,26636960,'ok',NULL,NULL),(8292540,1291,26636961,'ok',NULL,NULL),(8292541,1291,26636962,'ok',NULL,NULL),(8292542,1291,26636963,'ok',NULL,NULL),(8292543,1291,26636964,'ok',NULL,NULL),(8292544,1291,26636965,'ok',NULL,NULL),(8292545,1291,26636966,'ok',NULL,NULL),(8292546,1291,26636967,'ok',NULL,NULL),(8292547,1291,26636968,'ok',NULL,NULL),(8292548,1291,26636969,'ok',NULL,NULL),(8292549,1291,26636970,'ok',NULL,NULL),(8292550,1291,26636971,'ok',NULL,NULL),(8292551,1291,26636972,'ok',NULL,NULL),(8292552,1291,26636973,'ok',NULL,NULL),(8292553,1291,26636974,'ok',NULL,NULL),(8292554,1291,26636975,'ok',NULL,NULL),(8292555,1291,26636976,'ok',NULL,NULL),(8292556,1291,26636977,'ok',NULL,NULL),(8292557,1291,26636978,'ok',NULL,NULL),(8292558,1291,26636979,'ok',NULL,NULL),(8292559,1291,26636980,'ok',NULL,NULL),(8292560,1291,26636981,'ok',NULL,NULL),(8292561,1291,26636982,'ok',NULL,NULL),(8292562,1291,26636983,'ok',NULL,NULL),(8292563,1291,26636984,'ok',NULL,NULL),(8292564,1291,26636985,'ok',NULL,NULL),(8292565,1291,26636986,'ok',NULL,NULL),(8292566,1291,26636987,'ok',NULL,NULL),(8292567,1291,26636988,'ok',NULL,NULL),(8292568,1291,26636989,'ok',NULL,NULL),(8292569,1291,26636990,'ok',NULL,NULL),(8292570,1291,26636991,'ok',NULL,NULL),(8292571,1291,26636992,'ok',NULL,NULL),(8292572,1291,26636993,'ok',NULL,NULL),(8292573,1291,26636994,'ok',NULL,NULL),(8292575,1291,26636996,'ok',NULL,NULL),(8292577,1291,26636998,'ok',NULL,NULL),(8292578,1291,26636999,'ok',NULL,NULL),(8292579,1291,26637000,'ok',NULL,NULL),(8292580,1291,26637001,'ok',NULL,NULL),(8292581,1291,26637002,'ok',NULL,NULL),(8292582,1291,26637003,'ok',NULL,NULL),(8292583,1291,26637004,'ok',NULL,NULL),(8292584,1291,26637005,'ok',NULL,NULL),(8292585,1291,26637006,'ok',NULL,NULL),(8292586,1291,26637007,'ok',NULL,NULL),(8292587,1291,26637008,'ok',NULL,NULL),(8292588,1291,26637009,'ok',NULL,NULL),(8292589,1291,26637010,'ok',NULL,NULL),(8292590,1291,26637011,'ok',NULL,NULL),(8292591,1291,26637012,'ok',NULL,NULL),(8292592,1291,26637013,'ok',NULL,NULL),(8292593,1291,26637014,'ok',NULL,NULL),(8292594,1291,26637015,'ok',NULL,NULL),(8292595,1291,26637016,'ok',NULL,NULL),(8292596,1291,26637017,'ok',NULL,NULL),(8292597,1291,26637018,'ok',NULL,NULL),(8292598,1291,26637019,'ok',NULL,NULL),(8292599,1291,26637020,'ok',NULL,NULL),(8292600,1291,26637021,'ok',NULL,NULL),(8292601,1291,26637022,'ok',NULL,NULL),(8292602,1291,26637023,'ok',NULL,NULL),(8292603,1291,26637024,'ok',NULL,NULL),(8292604,1291,26637025,'ok',NULL,NULL),(8292605,1291,26637026,'ok',NULL,NULL),(8292606,1291,26637027,'ok',NULL,NULL),(8292607,1291,26637028,'ok',NULL,NULL),(8292609,1291,26637030,'ok',NULL,NULL),(8292610,1291,26637031,'ok',NULL,NULL),(8292611,1291,26637032,'ok',NULL,NULL),(8292612,1291,26637033,'ok',NULL,NULL),(8292613,1291,26637034,'ok',NULL,NULL),(8292614,1291,26637035,'ok',NULL,NULL),(8292615,1291,26637036,'ok',NULL,NULL),(8292616,1291,26637037,'ok',NULL,NULL),(8292617,1291,26637038,'ok',NULL,NULL),(8292618,1291,26637039,'ok',NULL,NULL),(8292619,1291,26637040,'ok',NULL,NULL),(8292620,1291,26637041,'ok',NULL,NULL),(8292621,1291,26637042,'ok',NULL,NULL),(8292622,1291,26637043,'ok',NULL,NULL),(8292623,1291,26637044,'ok',NULL,NULL),(8292624,1291,26637045,'ok',NULL,NULL),(8292625,1291,26637046,'ok',NULL,NULL),(8292626,1291,26637047,'ok',NULL,NULL),(8292627,1291,26637048,'ok',NULL,NULL),(8292628,1291,26637049,'ok',NULL,NULL),(8292629,1291,26637050,'ok',NULL,NULL),(8292630,1291,26637051,'ok',NULL,NULL),(8292631,1291,26637052,'ok',NULL,NULL),(8292632,1291,26637053,'ok',NULL,NULL),(8292633,1291,26637054,'ok',NULL,NULL),(8292634,1291,26637055,'ok',NULL,NULL),(8292635,1291,26637056,'ok',NULL,NULL),(8292636,1291,26637057,'ok',NULL,NULL),(8292637,1291,26637058,'ok',NULL,NULL),(8292638,1291,26637059,'ok',NULL,NULL),(8292639,1291,26637060,'ok',NULL,NULL),(8292640,1291,26637061,'ok',NULL,NULL),(8292641,1291,26637062,'ok',NULL,NULL),(8292642,1291,26637063,'ok',NULL,NULL),(8292643,1291,26637064,'ok',NULL,NULL),(8292644,1291,26637065,'ok',NULL,NULL),(8292645,1291,26637066,'ok',NULL,NULL),(8292646,1291,26637067,'ok',NULL,NULL),(8292647,1291,26637068,'ok',NULL,NULL),(8292648,1291,26637069,'ok',NULL,NULL),(8292649,1291,26637070,'ok',NULL,NULL),(8292650,1291,26637071,'ok',NULL,NULL),(8292651,1291,26637072,'ok',NULL,NULL),(8292652,1291,26637073,'ok',NULL,NULL),(8292653,1291,26637074,'ok',NULL,NULL),(8292654,1291,26637075,'ok',NULL,NULL),(8292655,1291,26637076,'ok',NULL,NULL),(8292657,1291,26637078,'ok',NULL,NULL),(8292658,1291,26637079,'ok',NULL,NULL),(8292659,1291,26637080,'ok',NULL,NULL),(8292660,1291,26637081,'ok',NULL,NULL),(8292661,1291,26637082,'ok',NULL,NULL),(8292662,1291,26637083,'ok',NULL,NULL),(8292663,1291,26637084,'ok',NULL,NULL),(8292664,1291,26637085,'ok',NULL,NULL),(8292665,1291,26637086,'ok',NULL,NULL),(8292666,1291,26637087,'ok',NULL,NULL),(8292667,1291,26637088,'ok',NULL,NULL),(8292668,1291,26637089,'ok',NULL,NULL),(8292669,1291,26637090,'ok',NULL,NULL),(8292670,1291,26637091,'ok',NULL,NULL),(8292671,1291,26637092,'ok',NULL,NULL),(8292672,1291,26637093,'ok',NULL,NULL),(8292673,1291,26637094,'ok',NULL,NULL),(8292674,1291,26637095,'ok',NULL,NULL),(8292675,1291,26637096,'ok',NULL,NULL),(8292676,1291,26637097,'ok',NULL,NULL),(8292678,1291,26637099,'ok',NULL,NULL),(8292679,1291,26637100,'ok',NULL,NULL),(8292681,1291,26637102,'ok',NULL,NULL),(8292682,1291,26637103,'ok',NULL,NULL),(8292683,1291,26637104,'ok',NULL,NULL),(8292684,1291,26637105,'ok',NULL,NULL),(8292685,1291,26637106,'ok',NULL,NULL),(8292686,1291,26637107,'ok',NULL,NULL),(8292687,1291,26637108,'ok',NULL,NULL),(8292688,1291,26637109,'ok',NULL,NULL),(8292689,1291,26637110,'ok',NULL,NULL),(8292690,1291,26637111,'ok',NULL,NULL),(8292691,1291,26637112,'ok',NULL,NULL),(8292692,1291,26637113,'ok',NULL,NULL),(8292693,1291,26637114,'ok',NULL,NULL),(8292694,1291,26637115,'ok',NULL,NULL),(8292695,1291,26637116,'ok',NULL,NULL),(8292696,1291,26637117,'ok',NULL,NULL),(8292697,1291,26637118,'ok',NULL,NULL),(8292698,1291,26637119,'ok',NULL,NULL),(8292699,1291,26637120,'ok',NULL,NULL),(8292700,1291,26637121,'ok',NULL,NULL),(8292701,1291,26637122,'ok',NULL,NULL),(8292702,1291,26637123,'ok',NULL,NULL),(8292703,1291,26637124,'ok',NULL,NULL),(8292704,1291,26637125,'ok',NULL,NULL),(8292705,1291,26637126,'ok',NULL,NULL),(8292706,1291,26637127,'ok',NULL,NULL),(8292707,1291,26637128,'ok',NULL,NULL),(8292708,1291,26637129,'ok',NULL,NULL),(8292710,1291,26637131,'ok',NULL,NULL),(8292711,1291,26637132,'ok',NULL,NULL),(8292712,1291,26637133,'ok',NULL,NULL),(8292714,1291,26637135,'ok',NULL,NULL),(8292715,1291,26637136,'ok',NULL,NULL),(8292716,1291,26637137,'ok',NULL,NULL),(8292717,1291,26637138,'ok',NULL,NULL),(8292718,1291,26637139,'ok',NULL,NULL),(8292719,1291,26637140,'ok',NULL,NULL),(8292970,1291,26634526,'ok',NULL,NULL),(8292971,1291,26634527,'ok',NULL,NULL),(8292972,1291,26634528,'ok',NULL,NULL),(8292973,1291,26634529,'ok',NULL,NULL),(8292974,1291,26634530,'ok',NULL,NULL),(8292975,1291,26634531,'ok',NULL,NULL),(8292976,1291,26634532,'ok',NULL,NULL),(8292977,1291,26634533,'ok',NULL,NULL),(8292978,1291,26634534,'ok',NULL,NULL),(8292979,1291,26634535,'ok',NULL,NULL),(8292980,1291,26634536,'ok',NULL,NULL),(8292981,1291,26634537,'ok',NULL,NULL),(8292982,1291,26634538,'ok',NULL,NULL),(8292983,1291,26634539,'ok',NULL,NULL),(8292984,1291,26634540,'ok',NULL,NULL),(8292985,1291,26634541,'ok',NULL,NULL),(8292986,1291,26634542,'ok',NULL,NULL),(8292987,1291,26634543,'ok',NULL,NULL),(8292988,1291,26634544,'ok',NULL,NULL),(8292989,1291,26635719,'ok',NULL,NULL),(8292990,1293,26636911,'ok',NULL,NULL),(8292991,1293,26636912,'ok',NULL,NULL),(8292992,1293,26636913,'ok',NULL,NULL),(8292993,1293,26636914,'ok',NULL,NULL),(8292994,1293,26636915,'ok',NULL,NULL),(8292995,1293,26636916,'ok',NULL,NULL),(8292996,1293,26636917,'ok',NULL,NULL),(8292997,1293,26636918,'ok',NULL,NULL),(8292998,1293,26636919,'ok',NULL,NULL),(8292999,1293,26636920,'ok',NULL,NULL),(8293000,1293,26636921,'ok',NULL,NULL),(8293001,1293,26636922,'ok',NULL,NULL),(8293002,1293,26636923,'ok',NULL,NULL),(8293003,1293,26636924,'ok',NULL,NULL),(8293004,1293,26636925,'ok',NULL,NULL),(8293005,1293,26636926,'ok',NULL,NULL),(8293006,1293,26636927,'ok',NULL,NULL),(8293007,1293,26636928,'ok',NULL,NULL),(8293008,1293,26636929,'ok',NULL,NULL),(8293011,1293,26636932,'ok',NULL,NULL),(8293012,1293,26636933,'ok',NULL,NULL),(8293013,1293,26636934,'ok',NULL,NULL),(8293014,1293,26636935,'ok',NULL,NULL),(8293015,1293,26636936,'ok',NULL,NULL),(8293016,1293,26636937,'ok',NULL,NULL),(8293017,1293,26636938,'ok',NULL,NULL),(8293018,1293,26636939,'ok',NULL,NULL),(8293019,1293,26636940,'ok',NULL,NULL),(8293020,1293,26636941,'ok',NULL,NULL),(8293022,1293,26636943,'ok',NULL,NULL),(8293023,1293,26636944,'ok',NULL,NULL),(8293024,1293,26636945,'ok',NULL,NULL),(8293025,1293,26636946,'ok',NULL,NULL),(8293026,1293,26636947,'ok',NULL,NULL),(8293027,1293,26636948,'ok',NULL,NULL),(8293028,1293,26636949,'ok',NULL,NULL),(8293029,1293,26636950,'ok',NULL,NULL),(8293030,1293,26636951,'ok',NULL,NULL),(8293031,1293,26636952,'ok',NULL,NULL),(8293032,1293,26636953,'ok',NULL,NULL),(8293033,1293,26636954,'ok',NULL,NULL),(8293034,1293,26636955,'ok',NULL,NULL),(8293035,1293,26636956,'ok',NULL,NULL),(8293036,1293,26636957,'ok',NULL,NULL),(8293037,1293,26636958,'ok',NULL,NULL),(8293038,1293,26636959,'ok',NULL,NULL),(8293039,1293,26636960,'ok',NULL,NULL),(8293040,1293,26636961,'ok',NULL,NULL),(8293041,1293,26636962,'ok',NULL,NULL),(8293042,1293,26636963,'ok',NULL,NULL),(8293043,1293,26636964,'ok',NULL,NULL),(8293044,1293,26636965,'ok',NULL,NULL),(8293045,1293,26636966,'ok',NULL,NULL),(8293046,1293,26636967,'ok',NULL,NULL),(8293047,1293,26636968,'ok',NULL,NULL),(8293048,1293,26636969,'ok',NULL,NULL),(8293049,1293,26636970,'ok',NULL,NULL),(8293050,1293,26636971,'ok',NULL,NULL),(8293051,1293,26636972,'ok',NULL,NULL),(8293052,1293,26636973,'ok',NULL,NULL),(8293053,1293,26636974,'ok',NULL,NULL),(8293054,1293,26636975,'ok',NULL,NULL),(8293055,1293,26636976,'ok',NULL,NULL),(8293056,1293,26636977,'ok',NULL,NULL),(8293057,1293,26636978,'ok',NULL,NULL),(8293058,1293,26636979,'ok',NULL,NULL),(8293059,1293,26636980,'ok',NULL,NULL),(8293060,1293,26636981,'ok',NULL,NULL),(8293061,1293,26636982,'ok',NULL,NULL),(8293062,1293,26636983,'ok',NULL,NULL),(8293063,1293,26636984,'ok',NULL,NULL),(8293064,1293,26636985,'ok',NULL,NULL),(8293065,1293,26636986,'ok',NULL,NULL),(8293066,1293,26636987,'ok',NULL,NULL),(8293067,1293,26636988,'ok',NULL,NULL),(8293068,1293,26636989,'ok',NULL,NULL),(8293069,1293,26636990,'ok',NULL,NULL),(8293070,1293,26636991,'ok',NULL,NULL),(8293071,1293,26636992,'ok',NULL,NULL),(8293072,1293,26636993,'ok',NULL,NULL),(8293073,1293,26636994,'ok',NULL,NULL),(8293075,1293,26636996,'ok',NULL,NULL),(8293077,1293,26636998,'ok',NULL,NULL),(8293078,1293,26636999,'ok',NULL,NULL),(8293079,1293,26637000,'ok',NULL,NULL),(8293080,1293,26637001,'ok',NULL,NULL),(8293081,1293,26637002,'ok',NULL,NULL),(8293082,1293,26637003,'ok',NULL,NULL),(8293083,1293,26637004,'ok',NULL,NULL),(8293084,1293,26637005,'ok',NULL,NULL),(8293085,1293,26637006,'ok',NULL,NULL),(8293086,1293,26637007,'ok',NULL,NULL),(8293087,1293,26637008,'ok',NULL,NULL),(8293088,1293,26637009,'ok',NULL,NULL),(8293089,1293,26637010,'ok',NULL,NULL),(8293090,1293,26637011,'ok',NULL,NULL),(8293091,1293,26637012,'ok',NULL,NULL),(8293092,1293,26637013,'ok',NULL,NULL),(8293093,1293,26637014,'ok',NULL,NULL),(8293094,1293,26637015,'ok',NULL,NULL),(8293095,1293,26637016,'ok',NULL,NULL),(8293096,1293,26637017,'ok',NULL,NULL),(8293097,1293,26637018,'ok',NULL,NULL),(8293098,1293,26637019,'ok',NULL,NULL),(8293099,1293,26637020,'ok',NULL,NULL),(8293100,1293,26637021,'ok',NULL,NULL),(8293101,1293,26637022,'ok',NULL,NULL),(8293102,1293,26637023,'ok',NULL,NULL),(8293103,1293,26637024,'ok',NULL,NULL),(8293104,1293,26637025,'ok',NULL,NULL),(8293105,1293,26637026,'ok',NULL,NULL),(8293106,1293,26637027,'ok',NULL,NULL),(8293107,1293,26637028,'ok',NULL,NULL),(8293109,1293,26637030,'ok',NULL,NULL),(8293110,1293,26637031,'ok',NULL,NULL),(8293111,1293,26637032,'ok',NULL,NULL),(8293112,1293,26637033,'ok',NULL,NULL),(8293113,1293,26637034,'ok',NULL,NULL),(8293114,1293,26637035,'ok',NULL,NULL),(8293115,1293,26637036,'ok',NULL,NULL),(8293116,1293,26637037,'ok',NULL,NULL),(8293117,1293,26637038,'ok',NULL,NULL),(8293118,1293,26637039,'ok',NULL,NULL),(8293119,1293,26637040,'ok',NULL,NULL),(8293120,1293,26637041,'ok',NULL,NULL),(8293121,1293,26637042,'ok',NULL,NULL),(8293122,1293,26637043,'ok',NULL,NULL),(8293123,1293,26637044,'ok',NULL,NULL),(8293124,1293,26637045,'ok',NULL,NULL),(8293125,1293,26637046,'ok',NULL,NULL),(8293126,1293,26637047,'ok',NULL,NULL),(8293127,1293,26637048,'ok',NULL,NULL),(8293128,1293,26637049,'ok',NULL,NULL),(8293129,1293,26637050,'ok',NULL,NULL),(8293130,1293,26637051,'ok',NULL,NULL),(8293131,1293,26637052,'ok',NULL,NULL),(8293132,1293,26637053,'ok',NULL,NULL),(8293133,1293,26637054,'ok',NULL,NULL),(8293134,1293,26637055,'ok',NULL,NULL),(8293135,1293,26637056,'ok',NULL,NULL),(8293136,1293,26637057,'ok',NULL,NULL),(8293137,1293,26637058,'ok',NULL,NULL),(8293138,1293,26637059,'ok',NULL,NULL),(8293139,1293,26637060,'ok',NULL,NULL),(8293140,1293,26637061,'ok',NULL,NULL),(8293141,1293,26637062,'ok',NULL,NULL),(8293142,1293,26637063,'ok',NULL,NULL),(8293143,1293,26637064,'ok',NULL,NULL),(8293144,1293,26637065,'ok',NULL,NULL),(8293145,1293,26637066,'ok',NULL,NULL),(8293146,1293,26637067,'ok',NULL,NULL),(8293147,1293,26637068,'ok',NULL,NULL),(8293148,1293,26637069,'ok',NULL,NULL),(8293149,1293,26637070,'ok',NULL,NULL),(8293150,1293,26637071,'ok',NULL,NULL),(8293151,1293,26637072,'ok',NULL,NULL),(8293152,1293,26637073,'ok',NULL,NULL),(8293153,1293,26637074,'ok',NULL,NULL),(8293154,1293,26637075,'ok',NULL,NULL),(8293155,1293,26637076,'ok',NULL,NULL),(8293157,1293,26637078,'ok',NULL,NULL),(8293158,1293,26637079,'ok',NULL,NULL),(8293159,1293,26637080,'ok',NULL,NULL),(8293160,1293,26637081,'ok',NULL,NULL),(8293161,1293,26637082,'ok',NULL,NULL),(8293162,1293,26637083,'ok',NULL,NULL),(8293163,1293,26637084,'ok',NULL,NULL),(8293164,1293,26637085,'ok',NULL,NULL),(8293165,1293,26637086,'ok',NULL,NULL),(8293166,1293,26637087,'ok',NULL,NULL),(8293167,1293,26637088,'ok',NULL,NULL),(8293168,1293,26637089,'ok',NULL,NULL),(8293169,1293,26637090,'ok',NULL,NULL),(8293170,1293,26637091,'ok',NULL,NULL),(8293171,1293,26637092,'ok',NULL,NULL),(8293172,1293,26637093,'ok',NULL,NULL),(8293173,1293,26637094,'ok',NULL,NULL),(8293174,1293,26637095,'ok',NULL,NULL),(8293175,1293,26637096,'ok',NULL,NULL),(8293176,1293,26637097,'ok',NULL,NULL),(8293178,1293,26637099,'ok',NULL,NULL),(8293179,1293,26637100,'ok',NULL,NULL),(8293181,1293,26637102,'ok',NULL,NULL),(8293182,1293,26637103,'ok',NULL,NULL),(8293183,1293,26637104,'ok',NULL,NULL),(8293184,1293,26637105,'ok',NULL,NULL),(8293185,1293,26637106,'ok',NULL,NULL),(8293186,1293,26637107,'ok',NULL,NULL),(8293187,1293,26637108,'ok',NULL,NULL),(8293188,1293,26637109,'ok',NULL,NULL),(8293189,1293,26637110,'ok',NULL,NULL),(8293190,1293,26637111,'ok',NULL,NULL),(8293191,1293,26637112,'ok',NULL,NULL),(8293192,1293,26637113,'ok',NULL,NULL),(8293193,1293,26637114,'ok',NULL,NULL),(8293194,1293,26637115,'ok',NULL,NULL),(8293195,1293,26637116,'ok',NULL,NULL),(8293196,1293,26637117,'ok',NULL,NULL),(8293197,1293,26637118,'ok',NULL,NULL),(8293198,1293,26637119,'ok',NULL,NULL),(8293199,1293,26637120,'ok',NULL,NULL),(8293200,1293,26637121,'ok',NULL,NULL),(8293201,1293,26637122,'ok',NULL,NULL),(8293202,1293,26637123,'ok',NULL,NULL),(8293203,1293,26637124,'ok',NULL,NULL),(8293204,1293,26637125,'ok',NULL,NULL),(8293205,1293,26637126,'ok',NULL,NULL),(8293206,1293,26637127,'ok',NULL,NULL),(8293207,1293,26637128,'ok',NULL,NULL),(8293208,1293,26637129,'ok',NULL,NULL),(8293210,1293,26637131,'ok',NULL,NULL),(8293211,1293,26637132,'ok',NULL,NULL),(8293212,1293,26637133,'ok',NULL,NULL),(8293214,1293,26637135,'ok',NULL,NULL),(8293215,1293,26637136,'ok',NULL,NULL),(8293216,1293,26637137,'ok',NULL,NULL),(8293217,1293,26637138,'ok',NULL,NULL),(8293218,1293,26637139,'ok',NULL,NULL),(8293219,1293,26637140,'ok',NULL,NULL),(8293235,1292,26634544,'ok',NULL,NULL),(8293236,1292,26635719,'ok',NULL,NULL),(8293238,1285,26637658,'ok',NULL,NULL),(8293239,1292,26634526,'ok',NULL,NULL),(8293240,1292,26634527,'ok',NULL,NULL),(8293241,1292,26634528,'ok',NULL,NULL),(8293242,1292,26634529,'ok',NULL,NULL),(8293243,1292,26634530,'ok',NULL,NULL),(8293244,1292,26634531,'ok',NULL,NULL),(8293245,1292,26634532,'ok',NULL,NULL),(8293246,1292,26634534,'ok',NULL,NULL),(8293247,1292,26634535,'ok',NULL,NULL),(8293248,1292,26634536,'ok',NULL,NULL),(8293249,1292,26634537,'ok',NULL,NULL),(8293250,1292,26634538,'ok',NULL,NULL),(8293251,1292,26634540,'ok',NULL,NULL),(8293252,1292,26634542,'ok',NULL,NULL),(8293253,1292,26634543,'ok',NULL,NULL),(8293263,1294,26634526,'ok',NULL,NULL),(8293264,1294,26637677,'ok',NULL,NULL),(8293265,1294,26637678,'ok',NULL,NULL),(8293266,1294,26637679,'ok',NULL,NULL),(8293267,1294,26637680,'ok',NULL,NULL),(8293268,1294,26637681,'ok',NULL,NULL),(8293269,1294,26637682,'ok',NULL,NULL),(8293270,1294,26637683,'ok',NULL,NULL),(8293271,1294,26637684,'ok',NULL,NULL),(8293272,1294,26637685,'ok',NULL,NULL),(8293273,1294,26637686,'ok',NULL,NULL),(8293274,1294,26637687,'ok',NULL,NULL),(8293275,1294,26637688,'ok',NULL,NULL),(8293276,1294,26637689,'ok',NULL,NULL),(8293277,1294,26637690,'ok',NULL,NULL),(8293278,1294,26637691,'ok',NULL,NULL),(8293279,1294,26637692,'ok',NULL,NULL),(8293280,1294,26637693,'ok',NULL,NULL),(8293281,1294,26637694,'ok',NULL,NULL),(8293282,1294,26637695,'ok',NULL,NULL),(8293283,1294,26637696,'ok',NULL,NULL),(8293284,1294,26637697,'ok',NULL,NULL),(8293285,1294,26637698,'ok',NULL,NULL),(8293286,1294,26637699,'ok',NULL,NULL),(8293287,1294,26637700,'ok',NULL,NULL),(8293288,1294,26637701,'ok',NULL,NULL),(8293289,1294,26637702,'ok',NULL,NULL),(8293290,1294,26637703,'ok',NULL,NULL),(8293291,1294,26637704,'ok',NULL,NULL),(8293292,1294,26637705,'ok',NULL,NULL),(8293293,1294,26637706,'ok',NULL,NULL),(8293294,1294,26637707,'ok',NULL,NULL),(8293298,1284,26637702,'ok',NULL,NULL),(8293299,1284,26637679,'ok',NULL,NULL),(8293300,1284,26637692,'ok',NULL,NULL),(8293301,1284,26637681,'ok',NULL,NULL),(8293302,1284,26637677,'ok',NULL,NULL),(8293303,1284,26637680,'ok',NULL,NULL),(8293304,1284,26637678,'ok',NULL,NULL),(8293305,1284,26637683,'ok',NULL,NULL),(8293306,1284,26637682,'ok',NULL,NULL),(8293307,1284,26637684,'ok',NULL,NULL),(8293308,1284,26637686,'ok',NULL,NULL),(8293309,1284,26637689,'ok',NULL,NULL),(8293310,1284,26637691,'ok',NULL,NULL),(8293311,1284,26637690,'ok',NULL,NULL),(8293312,1284,26637695,'ok',NULL,NULL),(8293313,1284,26637693,'ok',NULL,NULL),(8293314,1284,26637694,'ok',NULL,NULL),(8293315,1284,26637696,'ok',NULL,NULL),(8293316,1284,26637697,'ok',NULL,NULL),(8293317,1284,26637698,'ok',NULL,NULL),(8293318,1284,26637699,'ok',NULL,NULL),(8293319,1284,26637700,'ok',NULL,NULL),(8293320,1284,26637703,'ok',NULL,NULL),(8293321,1284,26637701,'ok',NULL,NULL),(8293322,1284,26637704,'ok',NULL,NULL),(8293323,1284,26637705,'ok',NULL,NULL),(8293324,1284,26637707,'ok',NULL,NULL),(8293325,1284,26637706,'ok',NULL,NULL),(8293326,1284,26637685,'ok',NULL,NULL),(8293327,1284,26637687,'ok',NULL,NULL),(8293328,1284,26637688,'ok',NULL,NULL),(8293336,1284,26637136,'ok','2018-08-14 14:21:33','2018-08-14 14:21:33'),(8293337,1284,26637136,'ok','2018-08-14 14:22:18','2018-08-14 14:22:18'),(8293338,1284,26637136,'ok','2018-08-14 14:22:30','2018-08-14 14:22:30'),(8293339,1284,26636976,'ok','2018-08-14 14:22:35','2018-08-14 14:22:35'),(8293340,1284,26637026,'ok','2018-08-14 14:22:58','2018-08-14 14:22:58'),(8293344,1284,26636942,'ok','2018-08-14 14:51:40','2018-08-14 14:52:03'),(8293377,1296,26637692,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293378,1296,26637677,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293379,1296,26637678,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293380,1296,26637679,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293381,1296,26637680,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293382,1296,26637681,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293383,1296,26637682,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293384,1296,26637683,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293385,1296,26637684,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293386,1296,26637686,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293387,1296,26637689,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293388,1296,26637690,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293389,1296,26637691,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293390,1296,26637693,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293391,1296,26637694,'ok','2018-08-16 09:54:27','2018-08-16 09:55:25'),(8293392,1296,26637695,'ok','2018-08-16 09:54:28','2018-08-16 09:55:25'),(8293393,1296,26637696,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293394,1296,26637707,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293395,1296,26635716,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293396,1296,26634526,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293397,1296,26634527,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293398,1296,26634528,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293399,1296,26634529,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293400,1296,26634530,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293401,1296,26634531,'ok','2018-08-16 09:54:28','2018-08-16 09:55:26'),(8293402,1296,26634532,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293403,1296,26634533,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293404,1296,26634534,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293405,1296,26634535,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293406,1296,26634536,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293407,1296,26634537,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293408,1296,26634538,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293409,1296,26634539,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293410,1296,26634540,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293411,1296,26634541,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293412,1296,26634542,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293413,1296,26634543,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293414,1296,26634544,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293415,1296,26634545,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293416,1296,26634546,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293417,1296,26634547,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293418,1296,26634548,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293419,1296,26634549,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293420,1296,26634550,'ok','2018-08-16 09:54:29','2018-08-16 09:55:27'),(8293421,1296,26634551,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293422,1296,26634552,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293423,1296,26634553,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293424,1296,26634554,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293425,1296,26634555,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293426,1296,26634556,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293427,1296,26634557,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293428,1296,26634558,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293429,1296,26634559,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293430,1296,26634560,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293431,1296,26634561,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293432,1296,26634562,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293433,1296,26634563,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293434,1296,26634564,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293435,1296,26634565,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293436,1296,26634566,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293437,1296,26634567,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293438,1296,26634568,'ok','2018-08-16 09:54:30','2018-08-16 09:55:28'),(8293439,1296,26634569,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293440,1296,26634570,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293441,1296,26634571,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293442,1296,26634572,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293443,1296,26634573,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293444,1296,26634574,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293445,1296,26634575,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293446,1296,26634576,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293447,1296,26634577,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293448,1296,26634578,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293449,1296,26634579,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293450,1296,26634580,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293451,1296,26634581,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293452,1296,26634582,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293453,1296,26634583,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293454,1296,26634584,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293455,1296,26634585,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293456,1296,26634586,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293457,1296,26634587,'ok','2018-08-16 09:54:31','2018-08-16 09:55:29'),(8293458,1296,26634588,'ok','2018-08-16 09:54:31','2018-08-16 09:55:30'),(8293459,1296,26634589,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293460,1296,26634590,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293461,1296,26634591,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293462,1296,26634592,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293463,1296,26634593,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293464,1296,26634594,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293465,1296,26634595,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293466,1296,26634596,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293467,1296,26634597,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293468,1296,26634598,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293469,1296,26634599,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293470,1296,26634600,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293471,1296,26634601,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293472,1296,26634602,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293473,1296,26634603,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293474,1296,26634604,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293475,1296,26634605,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293476,1296,26634606,'ok','2018-08-16 09:54:32','2018-08-16 09:55:30'),(8293477,1296,26634607,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293478,1296,26634608,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293479,1296,26634609,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293480,1296,26634610,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293481,1296,26634611,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293482,1296,26634612,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293483,1296,26634613,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293484,1296,26634614,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293485,1296,26634615,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293486,1296,26634616,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293487,1296,26634617,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293488,1296,26634618,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293489,1296,26634619,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293490,1296,26634620,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293491,1296,26634621,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293492,1296,26634622,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293493,1296,26634623,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293494,1296,26634624,'ok','2018-08-16 09:54:33','2018-08-16 09:55:31'),(8293495,1296,26634625,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293496,1296,26634626,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293497,1296,26634627,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293498,1296,26634628,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293499,1296,26634629,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293500,1296,26634630,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293501,1296,26634631,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293502,1296,26634632,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293503,1296,26634633,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293504,1296,26634634,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293505,1296,26634635,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293506,1296,26634636,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293507,1296,26634637,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293508,1296,26634638,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293509,1296,26634639,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293510,1296,26634640,'ok','2018-08-16 09:54:34','2018-08-16 09:55:32'),(8293511,1296,26634641,'ok','2018-08-16 09:54:35','2018-08-16 09:55:32'),(8293512,1296,26634642,'ok','2018-08-16 09:54:35','2018-08-16 09:55:32'),(8293513,1296,26634643,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293514,1296,26634644,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293515,1296,26634645,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293516,1296,26634646,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293517,1296,26634647,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293518,1296,26634648,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293519,1296,26634649,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293520,1296,26634650,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293521,1296,26634651,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293522,1296,26634652,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293523,1296,26634653,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293524,1296,26634654,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293525,1296,26634655,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293526,1296,26634656,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293527,1296,26634657,'ok','2018-08-16 09:54:35','2018-08-16 09:55:33'),(8293528,1296,26634658,'ok','2018-08-16 09:54:36','2018-08-16 09:55:33'),(8293529,1296,26634659,'ok','2018-08-16 09:54:36','2018-08-16 09:55:33'),(8293530,1296,26634660,'ok','2018-08-16 09:54:36','2018-08-16 09:55:33'),(8293531,1296,26634661,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293532,1296,26634662,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293533,1296,26634663,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293534,1296,26634664,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293535,1296,26634665,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293536,1296,26634666,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293537,1296,26634667,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293538,1296,26634668,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293539,1296,26634669,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293540,1296,26634670,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293541,1296,26634671,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293542,1296,26634672,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293543,1296,26634673,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293544,1296,26634674,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293545,1296,26634675,'ok','2018-08-16 09:54:36','2018-08-16 09:55:34'),(8293546,1296,26634676,'ok','2018-08-16 09:54:37','2018-08-16 09:55:34'),(8293547,1296,26634677,'ok','2018-08-16 09:54:37','2018-08-16 09:55:34'),(8293548,1296,26634678,'ok','2018-08-16 09:54:37','2018-08-16 09:55:34'),(8293549,1296,26634679,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293550,1296,26634680,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293551,1296,26634681,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293552,1296,26634682,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293553,1296,26634683,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293554,1296,26634684,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293555,1296,26634685,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293556,1296,26634686,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293557,1296,26634687,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293558,1296,26634688,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293559,1296,26634689,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293560,1296,26634690,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293561,1296,26634691,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293562,1296,26634692,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293563,1296,26634693,'ok','2018-08-16 09:54:37','2018-08-16 09:55:35'),(8293564,1296,26634694,'ok','2018-08-16 09:54:38','2018-08-16 09:55:35'),(8293565,1296,26634695,'ok','2018-08-16 09:54:38','2018-08-16 09:55:35'),(8293566,1296,26634696,'ok','2018-08-16 09:54:38','2018-08-16 09:55:35'),(8293567,1296,26634697,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293568,1296,26634698,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293569,1296,26634699,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293570,1296,26634700,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293571,1296,26634701,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293572,1296,26634702,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293573,1296,26634703,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293574,1296,26634704,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293575,1296,26634705,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293576,1296,26634706,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293577,1296,26634707,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293578,1296,26634708,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293579,1296,26634709,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293580,1296,26634710,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293581,1296,26634711,'ok','2018-08-16 09:54:38','2018-08-16 09:55:36'),(8293582,1296,26634712,'ok','2018-08-16 09:54:39','2018-08-16 09:55:36'),(8293583,1296,26634713,'ok','2018-08-16 09:54:39','2018-08-16 09:55:36'),(8293584,1296,26634714,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293585,1296,26634715,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293586,1296,26634716,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293587,1296,26634717,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293588,1296,26634718,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293589,1296,26634719,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293590,1296,26634720,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293591,1296,26634721,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293592,1296,26634722,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293593,1296,26634723,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293594,1296,26634724,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293595,1296,26634725,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293596,1296,26634726,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293597,1296,26634727,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293598,1296,26634728,'ok','2018-08-16 09:54:39','2018-08-16 09:55:37'),(8293599,1296,26634729,'ok','2018-08-16 09:54:40','2018-08-16 09:55:37'),(8293600,1296,26634730,'ok','2018-08-16 09:54:40','2018-08-16 09:55:37'),(8293601,1296,26634731,'ok','2018-08-16 09:54:40','2018-08-16 09:55:37'),(8293602,1296,26634732,'ok','2018-08-16 09:54:40','2018-08-16 09:55:37'),(8293603,1296,26634733,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293604,1296,26634734,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293605,1296,26634735,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293606,1296,26634736,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293607,1296,26634737,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293608,1296,26634738,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293609,1296,26634739,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293610,1296,26634740,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293611,1296,26634741,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293612,1296,26634742,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293613,1296,26634743,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293614,1296,26634744,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293615,1296,26634745,'ok','2018-08-16 09:54:40','2018-08-16 09:55:38'),(8293616,1296,26634746,'ok','2018-08-16 09:54:41','2018-08-16 09:55:38'),(8293617,1296,26634747,'ok','2018-08-16 09:54:41','2018-08-16 09:55:38'),(8293618,1296,26634748,'ok','2018-08-16 09:54:41','2018-08-16 09:55:38'),(8293619,1296,26634749,'ok','2018-08-16 09:54:41','2018-08-16 09:55:38'),(8293620,1296,26634750,'ok','2018-08-16 09:54:41','2018-08-16 09:55:38'),(8293621,1296,26634751,'ok','2018-08-16 09:54:41','2018-08-16 09:55:38'),(8293622,1296,26634752,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293623,1296,26634753,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293624,1296,26634754,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293625,1296,26634755,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293626,1296,26634756,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293627,1296,26634757,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293628,1296,26634758,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293629,1296,26634759,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293630,1296,26634760,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293631,1296,26634761,'ok','2018-08-16 09:54:41','2018-08-16 09:55:39'),(8293632,1296,26634762,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293633,1296,26634763,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293634,1296,26634764,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293635,1296,26634765,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293636,1296,26634766,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293637,1296,26634767,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293638,1296,26634768,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293639,1296,26634769,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293640,1296,26634770,'ok','2018-08-16 09:54:42','2018-08-16 09:55:39'),(8293641,1296,26634771,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293642,1296,26634772,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293643,1296,26634773,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293644,1296,26634774,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293645,1296,26634775,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293646,1296,26634776,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293647,1296,26634777,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293648,1296,26634778,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293649,1296,26634779,'ok','2018-08-16 09:54:42','2018-08-16 09:55:40'),(8293650,1296,26634780,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293651,1296,26634781,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293652,1296,26634782,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293653,1296,26634783,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293654,1296,26634784,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293655,1296,26634785,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293656,1296,26634786,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293657,1296,26634787,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293658,1296,26634788,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293659,1296,26634789,'ok','2018-08-16 09:54:43','2018-08-16 09:55:40'),(8293660,1296,26634790,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293661,1296,26634791,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293662,1296,26634792,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293663,1296,26634793,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293664,1296,26634794,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293665,1296,26634795,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293666,1296,26634796,'ok','2018-08-16 09:54:43','2018-08-16 09:55:41'),(8293667,1296,26634797,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293668,1296,26634798,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293669,1296,26634799,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293670,1296,26634800,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293671,1296,26634801,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293672,1296,26634802,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293673,1296,26634803,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293674,1296,26634804,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293675,1296,26634805,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293676,1296,26634806,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293677,1296,26634807,'ok','2018-08-16 09:54:44','2018-08-16 09:55:41'),(8293678,1296,26634808,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293679,1296,26634809,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293680,1296,26634810,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293681,1296,26634811,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293682,1296,26634812,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293683,1296,26634813,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293684,1296,26634814,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293685,1296,26634815,'ok','2018-08-16 09:54:44','2018-08-16 09:55:42'),(8293686,1296,26634816,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293687,1296,26634817,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293688,1296,26634818,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293689,1296,26634819,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293690,1296,26634820,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293691,1296,26634821,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293692,1296,26634822,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293693,1296,26634823,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293694,1296,26634824,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293695,1296,26634825,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293696,1296,26634826,'ok','2018-08-16 09:54:45','2018-08-16 09:55:42'),(8293697,1296,26634827,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293698,1296,26634828,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293699,1296,26634829,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293700,1296,26634830,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293701,1296,26634831,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293702,1296,26634832,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293703,1296,26634833,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293704,1296,26634834,'ok','2018-08-16 09:54:45','2018-08-16 09:55:43'),(8293705,1296,26634835,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293706,1296,26634836,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293707,1296,26634837,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293708,1296,26634838,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293709,1296,26634839,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293710,1296,26634840,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293711,1296,26634841,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293712,1296,26634842,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293713,1296,26634843,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293714,1296,26634844,'ok','2018-08-16 09:54:46','2018-08-16 09:55:43'),(8293715,1296,26634845,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293716,1296,26634846,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293717,1296,26634847,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293718,1296,26634848,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293719,1296,26634849,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293720,1296,26634850,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293721,1296,26634851,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293722,1296,26634852,'ok','2018-08-16 09:54:46','2018-08-16 09:55:44'),(8293723,1296,26634853,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293724,1296,26634854,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293725,1296,26634855,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293726,1296,26634856,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293727,1296,26634857,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293728,1296,26634858,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293729,1296,26634859,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293730,1296,26634860,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293731,1296,26634861,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293732,1296,26634862,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293733,1296,26634863,'ok','2018-08-16 09:54:47','2018-08-16 09:55:44'),(8293734,1296,26634864,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293735,1296,26634865,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293736,1296,26634866,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293737,1296,26634867,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293738,1296,26634868,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293739,1296,26634869,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293740,1296,26634870,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293741,1296,26634871,'ok','2018-08-16 09:54:47','2018-08-16 09:55:45'),(8293742,1296,26634872,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293743,1296,26634873,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293744,1296,26634874,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293745,1296,26634875,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293746,1296,26634876,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293747,1296,26634877,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293748,1296,26634878,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293749,1296,26634879,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293750,1296,26634880,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293751,1296,26634881,'ok','2018-08-16 09:54:48','2018-08-16 09:55:45'),(8293752,1296,26634882,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293753,1296,26634883,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293754,1296,26634884,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293755,1296,26634885,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293756,1296,26634886,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293757,1296,26634887,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293758,1296,26634888,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293759,1296,26634889,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293760,1296,26634890,'ok','2018-08-16 09:54:48','2018-08-16 09:55:46'),(8293761,1296,26634891,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293762,1296,26634892,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293763,1296,26634893,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293764,1296,26634894,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293765,1296,26634895,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293766,1296,26634896,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293767,1296,26634897,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293768,1296,26634898,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293769,1296,26634899,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293770,1296,26634900,'ok','2018-08-16 09:54:49','2018-08-16 09:55:46'),(8293771,1296,26634901,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293772,1296,26634902,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293773,1296,26634903,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293774,1296,26634904,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293775,1296,26634905,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293776,1296,26634906,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293777,1296,26634907,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293778,1296,26634908,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293779,1296,26634909,'ok','2018-08-16 09:54:49','2018-08-16 09:55:47'),(8293780,1296,26634910,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293781,1296,26634911,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293782,1296,26634912,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293783,1296,26634913,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293784,1296,26634914,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293785,1296,26634915,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293786,1296,26634916,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293787,1296,26634917,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293788,1296,26634918,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293789,1296,26634919,'ok','2018-08-16 09:54:50','2018-08-16 09:55:47'),(8293790,1296,26634920,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293791,1296,26634921,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293792,1296,26634922,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293793,1296,26634923,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293794,1296,26634924,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293795,1296,26634925,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293796,1296,26634926,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293797,1296,26634927,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293798,1296,26634928,'ok','2018-08-16 09:54:50','2018-08-16 09:55:48'),(8293799,1296,26634929,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293800,1296,26634930,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293801,1296,26634931,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293802,1296,26634932,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293803,1296,26634933,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293804,1296,26634934,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293805,1296,26634935,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293806,1296,26634936,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293807,1296,26634937,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293808,1296,26634938,'ok','2018-08-16 09:54:51','2018-08-16 09:55:48'),(8293809,1296,26634939,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293810,1296,26634940,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293811,1296,26634941,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293812,1296,26634942,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293813,1296,26634943,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293814,1296,26634944,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293815,1296,26634945,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293816,1296,26634946,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293817,1296,26634947,'ok','2018-08-16 09:54:51','2018-08-16 09:55:49'),(8293818,1296,26634948,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293819,1296,26634949,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293820,1296,26634950,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293821,1296,26634951,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293822,1296,26634952,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293823,1296,26634953,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293824,1296,26634954,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293825,1296,26634955,'ok','2018-08-16 09:54:52','2018-08-16 09:55:49'),(8293826,1296,26634956,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293827,1296,26634957,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293828,1296,26634958,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293829,1296,26634959,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293830,1296,26634960,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293831,1296,26634961,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293832,1296,26634962,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293833,1296,26634963,'ok','2018-08-16 09:54:52','2018-08-16 09:55:50'),(8293834,1296,26634964,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293835,1296,26634965,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293836,1296,26634966,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293837,1296,26634967,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293838,1296,26634968,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293839,1296,26634969,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293840,1296,26634970,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293841,1296,26634971,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293842,1296,26634972,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293843,1296,26634973,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293844,1296,26634974,'ok','2018-08-16 09:55:50','2018-08-16 09:55:50'),(8293845,1296,26634975,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293846,1296,26634976,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293847,1296,26634977,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293848,1296,26634978,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293849,1296,26634979,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293850,1296,26634980,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293851,1296,26634981,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293852,1296,26634982,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293853,1296,26634983,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293854,1296,26634984,'ok','2018-08-16 09:55:51','2018-08-16 09:55:51'),(8293856,1296,26636911,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293857,1296,26636912,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293858,1296,26636913,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293859,1296,26636914,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293860,1296,26636915,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293861,1296,26636916,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293862,1296,26636917,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293863,1296,26636918,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293864,1296,26636919,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293865,1296,26636920,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293866,1296,26636921,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293867,1296,26636922,'ok','2018-08-16 10:40:48','2018-08-16 14:09:46'),(8293868,1296,26636923,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293869,1296,26636924,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293870,1296,26636925,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293871,1296,26636926,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293872,1296,26636927,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293873,1296,26636928,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293874,1296,26636929,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293877,1296,26636932,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293878,1296,26636933,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293879,1296,26636934,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293880,1296,26636935,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293881,1296,26636936,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293882,1296,26636937,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293883,1296,26636938,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293884,1296,26636939,'ok','2018-08-16 10:40:49','2018-08-16 14:09:47'),(8293885,1296,26636940,'ok','2018-08-16 10:40:49','2018-08-16 14:09:48'),(8293886,1296,26636941,'ok','2018-08-16 10:40:49','2018-08-16 14:09:48'),(8293887,1296,26636943,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293888,1296,26636944,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293889,1296,26636945,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293890,1296,26636946,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293891,1296,26636947,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293892,1296,26636948,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293893,1296,26636949,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293894,1296,26636950,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293895,1296,26636951,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293896,1296,26636952,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293897,1296,26636953,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293898,1296,26636954,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293899,1296,26636955,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293900,1296,26636956,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293901,1296,26636957,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293902,1296,26636958,'ok','2018-08-16 10:40:50','2018-08-16 14:09:48'),(8293903,1296,26636959,'ok','2018-08-16 10:40:50','2018-08-16 14:09:49'),(8293904,1296,26636960,'ok','2018-08-16 10:40:50','2018-08-16 14:09:49'),(8293905,1296,26636961,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293906,1296,26636962,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293907,1296,26636963,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293908,1296,26636964,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293909,1296,26636965,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293910,1296,26636966,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293911,1296,26636967,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293912,1296,26636968,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293913,1296,26636969,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293914,1296,26636970,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293915,1296,26636971,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293916,1296,26636972,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293917,1296,26636973,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293918,1296,26636974,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293919,1296,26636975,'ok','2018-08-16 10:40:51','2018-08-16 14:09:49'),(8293920,1296,26636976,'ok','2018-08-16 10:40:51','2018-08-16 14:09:50'),(8293921,1296,26636977,'ok','2018-08-16 10:40:51','2018-08-16 14:09:50'),(8293922,1296,26636978,'ok','2018-08-16 10:40:51','2018-08-16 14:09:50'),(8293923,1296,26636979,'ok','2018-08-16 10:40:51','2018-08-16 14:09:50'),(8293924,1296,26636980,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293925,1296,26636981,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293926,1296,26636982,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293927,1296,26636983,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293928,1296,26636984,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293929,1296,26636985,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293930,1296,26636986,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293931,1296,26636987,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293932,1296,26636988,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293933,1296,26636989,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293934,1296,26636990,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293935,1296,26636991,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293936,1296,26636992,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293937,1296,26636993,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293938,1296,26636994,'ok','2018-08-16 10:40:52','2018-08-16 14:09:50'),(8293940,1296,26636996,'ok','2018-08-16 10:40:52','2018-08-16 14:09:51'),(8293942,1296,26636998,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293943,1296,26636999,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293944,1296,26637000,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293945,1296,26637001,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293946,1296,26637002,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293947,1296,26637003,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293948,1296,26637004,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293949,1296,26637005,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293950,1296,26637006,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293951,1296,26637007,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293952,1296,26637008,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293953,1296,26637009,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293954,1296,26637010,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293955,1296,26637011,'ok','2018-08-16 10:40:53','2018-08-16 14:09:51'),(8293956,1296,26637012,'ok','2018-08-16 10:40:53','2018-08-16 14:09:52'),(8293957,1296,26637013,'ok','2018-08-16 10:40:53','2018-08-16 14:09:52'),(8293958,1296,26637014,'ok','2018-08-16 10:40:53','2018-08-16 14:09:52'),(8293959,1296,26637015,'ok','2018-08-16 10:40:53','2018-08-16 14:09:52'),(8293960,1296,26637016,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293961,1296,26637017,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293962,1296,26637018,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293963,1296,26637019,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293964,1296,26637020,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293965,1296,26637021,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293966,1296,26637022,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293967,1296,26637023,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293968,1296,26637024,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293969,1296,26637025,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293970,1296,26637026,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293971,1296,26637027,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293972,1296,26637028,'ok','2018-08-16 10:40:54','2018-08-16 14:09:52'),(8293974,1296,26637030,'ok','2018-08-16 10:40:54','2018-08-16 14:09:53'),(8293975,1296,26637031,'ok','2018-08-16 10:40:54','2018-08-16 14:09:53'),(8293976,1296,26637032,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293977,1296,26637033,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293978,1296,26637034,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293979,1296,26637035,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293980,1296,26637036,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293981,1296,26637037,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293982,1296,26637038,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293983,1296,26637039,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293984,1296,26637040,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293985,1296,26637041,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293986,1296,26637042,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293987,1296,26637043,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293988,1296,26637044,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293989,1296,26637045,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293990,1296,26637046,'ok','2018-08-16 10:40:55','2018-08-16 14:09:53'),(8293991,1296,26637047,'ok','2018-08-16 10:40:55','2018-08-16 14:09:54'),(8293992,1296,26637048,'ok','2018-08-16 10:40:55','2018-08-16 14:09:54'),(8293993,1296,26637049,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8293994,1296,26637050,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8293995,1296,26637051,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8293996,1296,26637052,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8293997,1296,26637053,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8293998,1296,26637054,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8293999,1296,26637055,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294000,1296,26637056,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294001,1296,26637057,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294002,1296,26637058,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294003,1296,26637059,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294004,1296,26637060,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294005,1296,26637061,'ok','2018-08-16 10:40:56','2018-08-16 14:09:54'),(8294006,1296,26637062,'ok','2018-08-16 10:40:56','2018-08-16 14:09:55'),(8294007,1296,26637063,'ok','2018-08-16 10:40:56','2018-08-16 14:09:55'),(8294008,1296,26637064,'ok','2018-08-16 10:40:56','2018-08-16 14:09:55'),(8294009,1296,26637065,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294010,1296,26637066,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294011,1296,26637067,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294012,1296,26637068,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294013,1296,26637069,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294014,1296,26637070,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294015,1296,26637071,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294016,1296,26637072,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294017,1296,26637073,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294018,1296,26637074,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294019,1296,26637075,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294020,1296,26637076,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294022,1296,26637078,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294023,1296,26637079,'ok','2018-08-16 10:40:57','2018-08-16 14:09:55'),(8294024,1296,26637080,'ok','2018-08-16 10:40:57','2018-08-16 14:09:56'),(8294025,1296,26637081,'ok','2018-08-16 10:40:57','2018-08-16 14:09:56'),(8294026,1296,26637082,'ok','2018-08-16 10:40:57','2018-08-16 14:09:56'),(8294027,1296,26637083,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294028,1296,26637084,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294029,1296,26637085,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294030,1296,26637086,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294031,1296,26637087,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294032,1296,26637088,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294033,1296,26637089,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294034,1296,26637090,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294035,1296,26637091,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294036,1296,26637092,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294037,1296,26637093,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294038,1296,26637094,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294039,1296,26637095,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294040,1296,26637096,'ok','2018-08-16 10:40:58','2018-08-16 14:09:56'),(8294041,1296,26637097,'ok','2018-08-16 10:40:58','2018-08-16 14:09:57'),(8294043,1296,26637099,'ok','2018-08-16 10:40:58','2018-08-16 14:09:57'),(8294044,1296,26637100,'ok','2018-08-16 10:40:58','2018-08-16 14:09:57'),(8294046,1296,26637102,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294047,1296,26637103,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294048,1296,26637104,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294049,1296,26637105,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294050,1296,26637106,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294051,1296,26637107,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294052,1296,26637108,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294053,1296,26637109,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294054,1296,26637110,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294055,1296,26637111,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294056,1296,26637112,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294057,1296,26637113,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294058,1296,26637114,'ok','2018-08-16 10:40:59','2018-08-16 14:09:57'),(8294059,1296,26637115,'ok','2018-08-16 10:40:59','2018-08-16 14:09:58'),(8294060,1296,26637116,'ok','2018-08-16 10:40:59','2018-08-16 14:09:58'),(8294061,1296,26637117,'ok','2018-08-16 10:40:59','2018-08-16 14:09:58'),(8294062,1296,26637118,'ok','2018-08-16 10:40:59','2018-08-16 14:09:58'),(8294063,1296,26637119,'ok','2018-08-16 10:40:59','2018-08-16 14:09:58'),(8294064,1296,26637120,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294065,1296,26637121,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294066,1296,26637122,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294067,1296,26637123,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294068,1296,26637124,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294069,1296,26637125,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294070,1296,26637126,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294071,1296,26637127,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294072,1296,26637128,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294073,1296,26637129,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294075,1296,26637131,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294076,1296,26637132,'ok','2018-08-16 10:41:00','2018-08-16 14:09:58'),(8294077,1296,26637133,'ok','2018-08-16 10:41:00','2018-08-16 14:09:59'),(8294079,1296,26637135,'ok','2018-08-16 10:41:00','2018-08-16 14:09:59'),(8294080,1296,26637136,'ok','2018-08-16 10:41:00','2018-08-16 14:09:59'),(8294081,1296,26637137,'ok','2018-08-16 10:41:00','2018-08-16 14:09:59'),(8294082,1296,26637138,'ok','2018-08-16 10:41:00','2018-08-16 14:09:59'),(8294083,1296,26637139,'ok','2018-08-16 10:41:01','2018-08-16 14:09:59'),(8294084,1296,26637140,'ok','2018-08-16 10:41:01','2018-08-16 14:09:59'),(8294086,1296,26637697,'ok','2018-08-14 15:50:35','2018-08-16 15:44:34'),(8294087,1296,26637698,'ok','2018-08-14 15:50:36','2018-08-16 15:44:34'),(8294088,1296,26637699,'ok','2018-08-14 15:50:36','2018-08-16 15:44:34'),(8294089,1296,26637700,'ok','2018-08-14 15:50:36','2018-08-16 15:44:34'),(8294090,1296,26637701,'ok','2018-08-14 15:50:37','2018-08-16 15:44:35'),(8294091,1296,26637702,'ok','2018-08-14 15:50:37','2018-08-16 15:44:35'),(8294092,1296,26637703,'ok','2018-08-14 15:50:37','2018-08-16 15:44:35'),(8294093,1296,26637704,'ok','2018-08-14 15:50:38','2018-08-16 15:44:35'),(8294094,1296,26637705,'ok','2018-08-14 15:50:38','2018-08-16 15:44:36'),(8294095,1296,26637706,'ok','2018-08-14 15:50:38','2018-08-16 15:44:36'),(8294096,1296,26637738,'ok','2018-08-14 15:58:27','2018-08-16 15:44:36'),(8294097,1297,26637697,'ok','2018-08-16 15:46:20','2018-08-16 15:46:20'),(8294098,1297,26637698,'ok','2018-08-16 15:46:20','2018-08-16 15:46:20'),(8294099,1297,26637699,'ok','2018-08-16 15:46:21','2018-08-16 15:46:21'),(8294100,1297,26637700,'ok','2018-08-16 15:46:21','2018-08-16 15:46:21'),(8294101,1297,26637701,'ok','2018-08-16 15:46:21','2018-08-16 15:46:21'),(8294102,1297,26637702,'ok','2018-08-16 15:46:21','2018-08-16 15:46:21'),(8294103,1297,26637703,'ok','2018-08-16 15:46:21','2018-08-16 15:46:21'),(8294104,1297,26637704,'ok','2018-08-16 15:46:21','2018-08-16 15:46:21'),(8294105,1297,26637705,'ok','2018-08-16 15:46:22','2018-08-16 15:46:22'),(8294106,1297,26637706,'ok','2018-08-16 15:46:22','2018-08-16 15:46:22'),(8294107,1297,26637738,'ok','2018-08-16 15:46:22','2018-08-16 15:46:22'),(8294138,1284,26635716,'ok',NULL,NULL);
/*!40000 ALTER TABLE `shops_lists_customers_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_10`
--

DROP TABLE IF EXISTS `shops_lists_customers_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_10` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8294139 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_10`
--

LOCK TABLES `shops_lists_customers_10` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_2`
--

DROP TABLE IF EXISTS `shops_lists_customers_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_2` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8285866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_2`
--

LOCK TABLES `shops_lists_customers_2` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_3`
--

DROP TABLE IF EXISTS `shops_lists_customers_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_3` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8285866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_3`
--

LOCK TABLES `shops_lists_customers_3` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_4`
--

DROP TABLE IF EXISTS `shops_lists_customers_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_4` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8285866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_4`
--

LOCK TABLES `shops_lists_customers_4` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_5`
--

DROP TABLE IF EXISTS `shops_lists_customers_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_5` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8285866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_5`
--

LOCK TABLES `shops_lists_customers_5` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_6`
--

DROP TABLE IF EXISTS `shops_lists_customers_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_6` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8285866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_6`
--

LOCK TABLES `shops_lists_customers_6` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_7`
--

DROP TABLE IF EXISTS `shops_lists_customers_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_7` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8285866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_7`
--

LOCK TABLES `shops_lists_customers_7` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_8`
--

DROP TABLE IF EXISTS `shops_lists_customers_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_8` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8286034 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_8`
--

LOCK TABLES `shops_lists_customers_8` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_lists_customers_9`
--

DROP TABLE IF EXISTS `shops_lists_customers_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_lists_customers_9` (
  `id_shop_list_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_list` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `master_reject_type` enum('hard-bounce','unsub','spam','soft-bounce-1','soft-bounce-2','soft-bounce-3','ok') NOT NULL DEFAULT 'ok',
  `date_creation` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  PRIMARY KEY (`id_shop_list_customer`),
  KEY `id_shop_list_id_shop_customer` (`id_shop_list`,`id_shop_customer`),
  KEY `master_reject_type` (`master_reject_type`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_shop_list` (`id_shop_list`),
  KEY `id_shop_list_master_reject_type` (`id_shop_list`,`master_reject_type`),
  KEY `id_shop_list_date_modification` (`id_shop_list`,`date_modification`),
  KEY `date_creation` (`date_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=8286197 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_lists_customers_9`
--

LOCK TABLES `shops_lists_customers_9` WRITE;
/*!40000 ALTER TABLE `shops_lists_customers_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_lists_customers_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_manufacturers_1`
--

DROP TABLE IF EXISTS `shops_manufacturers_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_manufacturers_1` (
  `id_shop_manufacturer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_manufacturer` varchar(45) NOT NULL,
  `name` varchar(128) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`id_shop_manufacturer`),
  UNIQUE KEY `id_shop_id_manufacturer` (`id_shop`,`id_manufacturer`),
  KEY `name` (`name`),
  KEY `date` (`date`),
  KEY `date_modification` (`date_modification`),
  KEY `date_creation` (`date_creation`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=27980646 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_manufacturers_1`
--

LOCK TABLES `shops_manufacturers_1` WRITE;
/*!40000 ALTER TABLE `shops_manufacturers_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_manufacturers_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_manufacturers_10`
--

DROP TABLE IF EXISTS `shops_manufacturers_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_manufacturers_10` (
  `id_shop_manufacturer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_manufacturer` varchar(45) NOT NULL,
  `name` varchar(128) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`id_shop_manufacturer`),
  UNIQUE KEY `id_shop_id_manufacturer` (`id_shop`,`id_manufacturer`),
  KEY `name` (`name`),
  KEY `date` (`date`),
  KEY `date_modification` (`date_modification`),
  KEY `date_creation` (`date_creation`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=27980646 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_manufacturers_10`
--

LOCK TABLES `shops_manufacturers_10` WRITE;
/*!40000 ALTER TABLE `shops_manufacturers_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_manufacturers_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_1`
--

DROP TABLE IF EXISTS `shops_orders_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_1` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_1`
--

LOCK TABLES `shops_orders_1` WRITE;
/*!40000 ALTER TABLE `shops_orders_1` DISABLE KEYS */;
INSERT INTO `shops_orders_1` VALUES (17301840,1,26633181,'1','XKBKNABJK','6','fr',55,53,0,'EUR','',NULL,1,1,'1','2018-04-03 11:34:22','0',NULL,'2018-04-04 12:27:17','2018-04-04 12:27:17','2018-04-03 11:34:22'),(17301841,1,26633181,'2','OHSATSERP','1','fr',75.9,73.9,0,'EUR','',NULL,1,1,'2','2018-04-03 11:34:22','0',NULL,'2018-04-04 12:27:17','2018-04-04 12:27:17','2018-04-03 11:34:22'),(17301842,1,26633181,'3','UOYEVOLI','8','fr',76.01,74.01,0,'EUR','',NULL,1,1,'3','2018-04-03 11:34:22','0',NULL,'2018-04-04 12:27:17','2018-04-04 12:27:17','2018-04-03 11:34:22'),(17301843,1,26633181,'4','FFATNOMMJ','1','fr',89.89,87.89,0,'EUR','',NULL,1,1,'4','2018-04-03 11:34:22','0',NULL,'2018-04-04 12:27:17','2018-04-04 12:27:17','2018-04-03 11:34:22'),(17301844,1,26633181,'5','KHWLILZLL','10','fr',71.51,69.51,0,'EUR','',NULL,1,1,'5','2018-04-03 11:34:22','0',NULL,'2018-04-04 12:27:17','2018-04-04 12:27:17','2018-04-03 11:34:22');
/*!40000 ALTER TABLE `shops_orders_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_10`
--

DROP TABLE IF EXISTS `shops_orders_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_10` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_10`
--

LOCK TABLES `shops_orders_10` WRITE;
/*!40000 ALTER TABLE `shops_orders_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_2`
--

DROP TABLE IF EXISTS `shops_orders_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_2` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_2`
--

LOCK TABLES `shops_orders_2` WRITE;
/*!40000 ALTER TABLE `shops_orders_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_3`
--

DROP TABLE IF EXISTS `shops_orders_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_3` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_3`
--

LOCK TABLES `shops_orders_3` WRITE;
/*!40000 ALTER TABLE `shops_orders_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_4`
--

DROP TABLE IF EXISTS `shops_orders_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_4` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_4`
--

LOCK TABLES `shops_orders_4` WRITE;
/*!40000 ALTER TABLE `shops_orders_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_5`
--

DROP TABLE IF EXISTS `shops_orders_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_5` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_5`
--

LOCK TABLES `shops_orders_5` WRITE;
/*!40000 ALTER TABLE `shops_orders_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_6`
--

DROP TABLE IF EXISTS `shops_orders_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_6` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_6`
--

LOCK TABLES `shops_orders_6` WRITE;
/*!40000 ALTER TABLE `shops_orders_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_7`
--

DROP TABLE IF EXISTS `shops_orders_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_7` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_7`
--

LOCK TABLES `shops_orders_7` WRITE;
/*!40000 ALTER TABLE `shops_orders_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_8`
--

DROP TABLE IF EXISTS `shops_orders_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_8` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_8`
--

LOCK TABLES `shops_orders_8` WRITE;
/*!40000 ALTER TABLE `shops_orders_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_9`
--

DROP TABLE IF EXISTS `shops_orders_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_9` (
  `id_shop_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `order_reference` varchar(48) DEFAULT NULL,
  `status` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `amount` double NOT NULL COMMENT 'Montant avec tax',
  `amount_without_tax` double NOT NULL COMMENT 'Montant sans tax',
  `voucher_amount` double DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `spm_voucher_used` enum('0','1') NOT NULL DEFAULT '0',
  `voucher` varchar(256) DEFAULT NULL,
  `currency_rate` double DEFAULT NULL,
  `currency_rate_to_euro` double NOT NULL DEFAULT 1,
  `id_cart` varchar(45) NOT NULL,
  `date_cart` datetime NOT NULL,
  `is_valid` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_number` varchar(128) DEFAULT NULL,
  `date_creation` datetime NOT NULL COMMENT 'Date creation  selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order`),
  UNIQUE KEY `id_shop_order` (`id_shop_order`,`date`),
  UNIQUE KEY `id_order` (`id_order`,`id_shop`,`date`),
  KEY `id_shop` (`id_shop`),
  KEY `order_reference` (`order_reference`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `is_valid` (`is_valid`),
  KEY `lang` (`lang`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `voucher` (`voucher`(255)),
  KEY `spm_voucher_used` (`spm_voucher_used`),
  KEY `tmp_ids_valid_date_id` (`id_shop_customer`,`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_id_order_valid_date` (`id_shop_order`,`is_valid`,`date`),
  KEY `tmp_valid_date_id` (`is_valid`,`date`,`id_shop_order`),
  KEY `tmp_date_id` (`date`,`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17301845 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_9`
--

LOCK TABLES `shops_orders_9` WRITE;
/*!40000 ALTER TABLE `shops_orders_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_1`
--

DROP TABLE IF EXISTS `shops_orders_products_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_1` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_1`
--

LOCK TABLES `shops_orders_products_1` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_1` DISABLE KEYS */;
INSERT INTO `shops_orders_products_1` VALUES (40566590,17301840,12970423,35854717,1,'2','10','1',26.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566591,17301840,12970425,35854726,1,'3','13','1',25.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566592,17301841,12970423,35854717,1,'2','10','1',26.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566593,17301841,12970431,35854769,1,'6','32','1',30.502569,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566594,17301841,12970433,35854780,1,'7','34','1',20.501236,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566595,17301842,12970421,35854702,1,'1','1','1',16.51,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566596,17301842,12970423,35854717,1,'2','10','1',26.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566597,17301842,12970431,35854769,1,'6','32','1',30.502569,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566598,17301843,12970421,35854702,1,'1','1','1',16.51,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566599,17301843,12970425,35854726,1,'3','13','1',25.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566600,17301843,12970429,35854744,1,'5','19','1',30.506321,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566601,17301843,12970433,35854780,1,'7','34','1',20.501236,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566602,17301844,12970421,35854702,1,'1','1','1',16.51,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566603,17301844,12970423,35854714,1,'2','7','1',26.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22'),(40566604,17301844,12970425,35854726,1,'3','13','1',25.999852,1,'2018-04-04 12:27:17','2018-04-03 11:34:22');
/*!40000 ALTER TABLE `shops_orders_products_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_10`
--

DROP TABLE IF EXISTS `shops_orders_products_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_10` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_10`
--

LOCK TABLES `shops_orders_products_10` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_2`
--

DROP TABLE IF EXISTS `shops_orders_products_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_2` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_2`
--

LOCK TABLES `shops_orders_products_2` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_3`
--

DROP TABLE IF EXISTS `shops_orders_products_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_3` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_3`
--

LOCK TABLES `shops_orders_products_3` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_4`
--

DROP TABLE IF EXISTS `shops_orders_products_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_4` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_4`
--

LOCK TABLES `shops_orders_products_4` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_5`
--

DROP TABLE IF EXISTS `shops_orders_products_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_5` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_5`
--

LOCK TABLES `shops_orders_products_5` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_6`
--

DROP TABLE IF EXISTS `shops_orders_products_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_6` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_6`
--

LOCK TABLES `shops_orders_products_6` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_7`
--

DROP TABLE IF EXISTS `shops_orders_products_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_7` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_7`
--

LOCK TABLES `shops_orders_products_7` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_8`
--

DROP TABLE IF EXISTS `shops_orders_products_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_8` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_8`
--

LOCK TABLES `shops_orders_products_8` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_products_9`
--

DROP TABLE IF EXISTS `shops_orders_products_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_products_9` (
  `id_shop_order_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop_product_attribute` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` varchar(45) DEFAULT NULL,
  `id_combination` varchar(45) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int(10) unsigned DEFAULT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_order_product`),
  UNIQUE KEY `id_shop_order_product` (`id_shop_order_product`,`date`),
  UNIQUE KEY `id_shop_order_id_shop_product_id_shop_date` (`id_shop_order`,`id_shop_product`,`id_shop_product_attribute`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_shop_order` (`id_shop_order`),
  KEY `id_product` (`id_product`),
  KEY `id_combination` (`id_combination`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `qty` (`qty`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product_attribute` (`id_shop_product_attribute`),
  KEY `tmp1` (`id_shop_order`)
) ENGINE=InnoDB AUTO_INCREMENT=40566605 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_products_9`
--

LOCK TABLES `shops_orders_products_9` WRITE;
/*!40000 ALTER TABLE `shops_orders_products_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_products_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_status_1`
--

DROP TABLE IF EXISTS `shops_orders_status_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_status_1` (
  `id_shop_order_status` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_status` varchar(45) NOT NULL,
  `name` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_creation` datetime NOT NULL COMMENT 'Date creation selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`id_shop_order_status`),
  UNIQUE KEY `id_shop_id_status` (`id_shop`,`id_status`,`lang`),
  KEY `name` (`name`),
  KEY `date` (`date`),
  KEY `date_modification` (`date_modification`),
  KEY `date_creation` (`date_creation`),
  KEY `deleted` (`deleted`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_status_1`
--

LOCK TABLES `shops_orders_status_1` WRITE;
/*!40000 ALTER TABLE `shops_orders_status_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_status_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_orders_status_10`
--

DROP TABLE IF EXISTS `shops_orders_status_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_orders_status_10` (
  `id_shop_order_status` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_status` varchar(45) NOT NULL,
  `name` varchar(128) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_creation` datetime NOT NULL COMMENT 'Date creation selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`id_shop_order_status`),
  UNIQUE KEY `id_shop_id_status` (`id_shop`,`id_status`,`lang`),
  KEY `name` (`name`),
  KEY `date` (`date`),
  KEY `date_modification` (`date_modification`),
  KEY `date_creation` (`date_creation`),
  KEY `deleted` (`deleted`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_orders_status_10`
--

LOCK TABLES `shops_orders_status_10` WRITE;
/*!40000 ALTER TABLE `shops_orders_status_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_orders_status_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_1`
--

DROP TABLE IF EXISTS `shops_products_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_1` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_1`
--

LOCK TABLES `shops_products_1` WRITE;
/*!40000 ALTER TABLE `shops_products_1` DISABLE KEYS */;
INSERT INTO `shops_products_1` VALUES (12970421,1,1,'demo_1','fr','T-shirt délavé à manches courtes','T-shirt délavé à manches courtes et col rond. Matière douce et extensible pour un confort inégalé. Pour un look estival, portez-le avec un chapeau de paille !','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','1','EUR',19.812,19.812,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970422,1,1,'demo_1','gb','T-shirt délavé à manches courtes','T-shirt délavé à manches courtes et col rond. Matière douce et extensible pour un confort inégalé. Pour un look estival, portez-le avec un chapeau de paille !','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','1','EUR',19.812,19.812,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970423,1,2,'demo_2','fr','Chemisier','Chemisier à manches courtes drapées, pour un style féminin et élégant.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','1','EUR',32.399822,32.399822,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970424,1,2,'demo_2','gb','Chemisier','Chemisier à manches courtes drapées, pour un style féminin et élégant.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','1','EUR',32.399822,32.399822,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970425,1,3,'demo_3','fr','Robe imprimée','Robe imprimée 100 % coton. Haut rayé noir et blanc et bas composé d\'une jupe patineuse taille haute.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','1','EUR',31.199822,31.199822,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970426,1,3,'demo_3','gb','Robe imprimée','Robe imprimée 100 % coton. Haut rayé noir et blanc et bas composé d\'une jupe patineuse taille haute.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','1','EUR',31.199822,31.199822,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970427,1,4,'demo_4','fr','Robe imprimée','Robe de soirée imprimée à manches droites et volants, avec fine ceinture noire à la taille.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','1','EUR',61.192984,61.192984,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970428,1,4,'demo_4','gb','Robe imprimée','Robe de soirée imprimée à manches droites et volants, avec fine ceinture noire à la taille.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','1','EUR',61.192984,61.192984,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970429,1,5,'demo_5','fr','Robe d\'été imprimée','Longue robe imprimée à fines bretelles réglables. Décolleté en V et armature sous la poitrine. Volants au bas de la robe.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','1','EUR',36.607585,34.777206,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970430,1,5,'demo_5','gb','Robe d\'été imprimée','Longue robe imprimée à fines bretelles réglables. Décolleté en V et armature sous la poitrine. Volants au bas de la robe.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','1','EUR',36.607585,34.777206,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970431,1,6,'demo_6','fr','Robe d\'été imprimée','Robe en mousseline sans manches, longueur genoux. Décolleté en V avec élastique sous la poitrine.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','1','EUR',36.603083,36.603083,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970432,1,6,'demo_6','gb','Robe d\'été imprimée','Robe en mousseline sans manches, longueur genoux. Décolleté en V avec élastique sous la poitrine.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','1','EUR',36.603083,36.603083,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970433,1,7,'demo_7','fr','Robe en mousseline imprimée','Robe en mousseline imprimée à bretelles, longueur genoux. Profond décolleté en V.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','1','EUR',24.601483,19.681187,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(12970434,1,7,'demo_7','gb','Robe en mousseline imprimée','Robe en mousseline imprimée à bretelles, longueur genoux. Profond décolleté en V.','Fashion propose des vêtements de qualité depuis 2010. La marque propose une gamme féminine composée d\'élégants vêtements à coordonner et de robes originales et offre désormais une collection complète de prêt-à-porter, regroupant toutes les pièces qu\'une femme doit avoir dans sa garde-robe. Fashion se distingue avec des looks à la fois cool, simples et rafraîchissants, alliant élégance et chic, pour un style reconnaissable entre mille. Chacune des magnifiques pièces de la collection est fabriquée avec le plus grand soin en Italie. Fashion enrichit son offre avec une gamme d\'accessoires incluant chaussures, chapeaux, ceintures et bien plus encore !','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','1','EUR',24.601483,19.681187,0,'','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22');
/*!40000 ALTER TABLE `shops_products_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_10`
--

DROP TABLE IF EXISTS `shops_products_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_10` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_10`
--

LOCK TABLES `shops_products_10` WRITE;
/*!40000 ALTER TABLE `shops_products_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_2`
--

DROP TABLE IF EXISTS `shops_products_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_2` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_2`
--

LOCK TABLES `shops_products_2` WRITE;
/*!40000 ALTER TABLE `shops_products_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_3`
--

DROP TABLE IF EXISTS `shops_products_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_3` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_3`
--

LOCK TABLES `shops_products_3` WRITE;
/*!40000 ALTER TABLE `shops_products_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_4`
--

DROP TABLE IF EXISTS `shops_products_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_4` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_4`
--

LOCK TABLES `shops_products_4` WRITE;
/*!40000 ALTER TABLE `shops_products_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_5`
--

DROP TABLE IF EXISTS `shops_products_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_5` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_5`
--

LOCK TABLES `shops_products_5` WRITE;
/*!40000 ALTER TABLE `shops_products_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_6`
--

DROP TABLE IF EXISTS `shops_products_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_6` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_6`
--

LOCK TABLES `shops_products_6` WRITE;
/*!40000 ALTER TABLE `shops_products_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_7`
--

DROP TABLE IF EXISTS `shops_products_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_7` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_7`
--

LOCK TABLES `shops_products_7` WRITE;
/*!40000 ALTER TABLE `shops_products_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_8`
--

DROP TABLE IF EXISTS `shops_products_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_8` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_8`
--

LOCK TABLES `shops_products_8` WRITE;
/*!40000 ALTER TABLE `shops_products_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_9`
--

DROP TABLE IF EXISTS `shops_products_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_9` (
  `id_shop_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description_short` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `id_manufacturer` varchar(45) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `price` double NOT NULL DEFAULT 0,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT 0,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product`),
  UNIQUE KEY `id_shop_product` (`id_shop_product`,`date`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`lang`,`date`),
  KEY `reference` (`reference`),
  KEY `lang` (`lang`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `currency` (`currency`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_link` (`id_shop`,`link`)
) ENGINE=InnoDB AUTO_INCREMENT=12970435 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_9`
--

LOCK TABLES `shops_products_9` WRITE;
/*!40000 ALTER TABLE `shops_products_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_1`
--

DROP TABLE IF EXISTS `shops_products_attributes_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_1` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_1`
--

LOCK TABLES `shops_products_attributes_1` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_1` DISABLE KEYS */;
INSERT INTO `shops_products_attributes_1` VALUES (35854702,12970421,1,1,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','Taille S Couleur Orange',19.812,19.812,299,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854703,12970421,1,2,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/3/3-home_default.jpg','Taille S Couleur Bleu',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854704,12970421,1,3,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','Taille M Couleur Orange',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854705,12970421,1,4,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/3/3-home_default.jpg','Taille M Couleur Bleu',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854706,12970421,1,5,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','Taille L Couleur Orange',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854707,12970421,1,6,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/3/3-home_default.jpg','Taille L Couleur Bleu',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854708,12970422,1,1,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','Taille S Couleur Orange',19.812,19.812,299,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854709,12970422,1,2,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/3/3-home_default.jpg','Taille S Couleur Bleu',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854710,12970422,1,3,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','Taille M Couleur Orange',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854711,12970422,1,4,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/3/3-home_default.jpg','Taille M Couleur Bleu',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854712,12970422,1,5,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1-home_default.jpg','Taille L Couleur Orange',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854713,12970422,1,6,'','http://neopresta.shopimind.com/index.php?id_product=1&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/3/3-home_default.jpg','Taille L Couleur Bleu',19.812,19.812,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854714,12970423,1,7,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','Taille S Couleur Noir',32.399822,32.399822,299,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854715,12970423,1,8,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/5/5-home_default.jpg','Taille S Couleur Blanc',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854716,12970423,1,9,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','Taille M Couleur Noir',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854717,12970423,1,10,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/5/5-home_default.jpg','Taille M Couleur Blanc',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854718,12970423,1,11,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','Taille L Couleur Noir',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854719,12970423,1,12,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/5/5-home_default.jpg','Taille L Couleur Blanc',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854720,12970424,1,7,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','Taille S Couleur Noir',32.399822,32.399822,299,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854721,12970424,1,8,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/5/5-home_default.jpg','Taille S Couleur Blanc',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854722,12970424,1,9,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','Taille M Couleur Noir',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854723,12970424,1,10,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/5/5-home_default.jpg','Taille M Couleur Blanc',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854724,12970424,1,11,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/7/7-home_default.jpg','Taille L Couleur Noir',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854725,12970424,1,12,'','http://neopresta.shopimind.com/index.php?id_product=2&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/5/5-home_default.jpg','Taille L Couleur Blanc',32.399822,32.399822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854726,12970425,1,13,'','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','Taille S Couleur Orange',31.199822,31.199822,299,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854727,12970425,1,14,'','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','Taille M Couleur Orange',31.199822,31.199822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854728,12970425,1,15,'','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','Taille L Couleur Orange',31.199822,31.199822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854729,12970426,1,13,'','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','Taille S Couleur Orange',31.199822,31.199822,299,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854730,12970426,1,14,'','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','Taille M Couleur Orange',31.199822,31.199822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854731,12970426,1,15,'','http://neopresta.shopimind.com/index.php?id_product=3&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/8/8-home_default.jpg','Taille L Couleur Orange',31.199822,31.199822,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854732,12970427,1,16,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','Taille S Couleur Beige',61.192984,61.192984,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854733,12970427,1,17,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','Taille M Couleur Beige',61.192984,61.192984,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854734,12970427,1,18,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','Taille L Couleur Beige',61.192984,61.192984,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854735,12970427,1,43,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1/11-home_default.jpg','Taille S Couleur Rose',61.192984,61.192984,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854736,12970427,1,44,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1/11-home_default.jpg','Taille M Couleur Rose',61.192984,61.192984,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854737,12970427,1,45,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/1/11-home_default.jpg','Taille L Couleur Rose',61.192984,61.192984,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854738,12970428,1,16,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','Taille S Couleur Beige',61.192984,61.192984,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854739,12970428,1,17,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','Taille M Couleur Beige',61.192984,61.192984,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854740,12970428,1,18,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/0/10-home_default.jpg','Taille L Couleur Beige',61.192984,61.192984,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854741,12970428,1,43,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1/11-home_default.jpg','Taille S Couleur Rose',61.192984,61.192984,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854742,12970428,1,44,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1/11-home_default.jpg','Taille M Couleur Rose',61.192984,61.192984,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854743,12970428,1,45,'','http://neopresta.shopimind.com/index.php?id_product=4&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/1/11-home_default.jpg','Taille L Couleur Rose',61.192984,61.192984,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854744,12970429,1,19,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','Taille S Couleur Jaune',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854745,12970429,1,20,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/3/13-home_default.jpg','Taille S Couleur Bleu',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854746,12970429,1,21,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/4/14-home_default.jpg','Taille S Couleur Orange',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854747,12970429,1,22,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/5/15-home_default.jpg','Taille S Couleur Noir',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854748,12970429,1,23,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','Taille M Couleur Jaune',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854749,12970429,1,24,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/3/13-home_default.jpg','Taille M Couleur Bleu',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854750,12970429,1,25,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/4/14-home_default.jpg','Taille M Couleur Orange',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854751,12970429,1,26,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/5/15-home_default.jpg','Taille M Couleur Noir',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854752,12970429,1,27,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','Taille L Couleur Jaune',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854753,12970429,1,28,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/3/13-home_default.jpg','Taille L Couleur Bleu',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854754,12970429,1,29,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/4/14-home_default.jpg','Taille L Couleur Orange',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854755,12970429,1,30,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/5/15-home_default.jpg','Taille L Couleur Noir',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854756,12970430,1,19,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','Taille S Couleur Jaune',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854757,12970430,1,20,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/3/13-home_default.jpg','Taille S Couleur Bleu',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854758,12970430,1,21,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/4/14-home_default.jpg','Taille S Couleur Orange',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854759,12970430,1,22,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/5/15-home_default.jpg','Taille S Couleur Noir',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854760,12970430,1,23,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','Taille M Couleur Jaune',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854761,12970430,1,24,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/3/13-home_default.jpg','Taille M Couleur Bleu',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854762,12970430,1,25,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/4/14-home_default.jpg','Taille M Couleur Orange',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854763,12970430,1,26,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/5/15-home_default.jpg','Taille M Couleur Noir',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854764,12970430,1,27,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/2/12-home_default.jpg','Taille L Couleur Jaune',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854765,12970430,1,28,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/3/13-home_default.jpg','Taille L Couleur Bleu',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854766,12970430,1,29,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/4/14-home_default.jpg','Taille L Couleur Orange',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854767,12970430,1,30,'','http://neopresta.shopimind.com/index.php?id_product=5&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/5/15-home_default.jpg','Taille L Couleur Noir',36.607585,34.777206,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854768,12970431,1,31,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','Taille S Couleur Jaune',36.603083,36.603083,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854769,12970431,1,32,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','Taille M Couleur Jaune',36.603083,36.603083,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854770,12970431,1,33,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','Taille L Couleur Jaune',36.603083,36.603083,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854771,12970431,1,40,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/8/18-home_default.jpg','Taille S Couleur Blanc',36.603083,36.603083,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854772,12970431,1,41,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/8/18-home_default.jpg','Taille M Couleur Blanc',36.603083,36.603083,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854773,12970431,1,42,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/1/8/18-home_default.jpg','Taille L Couleur Blanc',36.603083,36.603083,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854774,12970432,1,31,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','Taille S Couleur Jaune',36.603083,36.603083,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854775,12970432,1,32,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','Taille M Couleur Jaune',36.603083,36.603083,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854776,12970432,1,33,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/6/16-home_default.jpg','Taille L Couleur Jaune',36.603083,36.603083,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854777,12970432,1,40,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/8/18-home_default.jpg','Taille S Couleur Blanc',36.603083,36.603083,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854778,12970432,1,41,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/8/18-home_default.jpg','Taille M Couleur Blanc',36.603083,36.603083,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854779,12970432,1,42,'','http://neopresta.shopimind.com/index.php?id_product=6&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/1/8/18-home_default.jpg','Taille L Couleur Blanc',36.603083,36.603083,0,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854780,12970433,1,34,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','Taille S Couleur Jaune',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854781,12970433,1,35,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','Taille M Couleur Jaune',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854782,12970433,1,36,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','Taille L Couleur Jaune',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854783,12970433,1,37,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/2/22-home_default.jpg','Taille S Couleur Vert',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854784,12970433,1,38,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/2/22-home_default.jpg','Taille M Couleur Vert',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854785,12970433,1,39,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=1','http://neopresta.shopimind.com/img/p/2/2/22-home_default.jpg','Taille L Couleur Vert',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854786,12970434,1,34,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','Taille S Couleur Jaune',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854787,12970434,1,35,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','Taille M Couleur Jaune',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854788,12970434,1,36,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/0/20-home_default.jpg','Taille L Couleur Jaune',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854789,12970434,1,37,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/2/22-home_default.jpg','Taille S Couleur Vert',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854790,12970434,1,38,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/2/22-home_default.jpg','Taille M Couleur Vert',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(35854791,12970434,1,39,'','http://neopresta.shopimind.com/index.php?id_product=7&controller=product&id_lang=2','http://neopresta.shopimind.com/img/p/2/2/22-home_default.jpg','Taille L Couleur Vert',24.601483,19.681187,300,'2018-04-04 12:27:16','2018-04-03 11:34:22');
/*!40000 ALTER TABLE `shops_products_attributes_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_10`
--

DROP TABLE IF EXISTS `shops_products_attributes_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_10` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_10`
--

LOCK TABLES `shops_products_attributes_10` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_2`
--

DROP TABLE IF EXISTS `shops_products_attributes_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_2` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_2`
--

LOCK TABLES `shops_products_attributes_2` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_3`
--

DROP TABLE IF EXISTS `shops_products_attributes_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_3` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_3`
--

LOCK TABLES `shops_products_attributes_3` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_4`
--

DROP TABLE IF EXISTS `shops_products_attributes_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_4` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_4`
--

LOCK TABLES `shops_products_attributes_4` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_5`
--

DROP TABLE IF EXISTS `shops_products_attributes_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_5` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_5`
--

LOCK TABLES `shops_products_attributes_5` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_6`
--

DROP TABLE IF EXISTS `shops_products_attributes_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_6` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_6`
--

LOCK TABLES `shops_products_attributes_6` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_7`
--

DROP TABLE IF EXISTS `shops_products_attributes_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_7` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_7`
--

LOCK TABLES `shops_products_attributes_7` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_8`
--

DROP TABLE IF EXISTS `shops_products_attributes_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_8` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_8`
--

LOCK TABLES `shops_products_attributes_8` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_attributes_9`
--

DROP TABLE IF EXISTS `shops_products_attributes_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_attributes_9` (
  `id_shop_product_attribute` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop_product` int(10) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_combination` int(10) unsigned NOT NULL,
  `reference` varchar(128) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `image_link` varchar(256) DEFAULT NULL,
  `combination_name` varchar(256) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_attribute`),
  UNIQUE KEY `id_shop_product_attribute` (`id_shop_product_attribute`,`date`),
  UNIQUE KEY `id_shop_product_id_shop_id_combination_date` (`id_shop_product`,`id_shop`,`id_combination`,`date`),
  KEY `id_combination` (`id_combination`),
  KEY `reference` (`reference`),
  KEY `quantity` (`quantity`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_product` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=35854792 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_attributes_9`
--

LOCK TABLES `shops_products_attributes_9` WRITE;
/*!40000 ALTER TABLE `shops_products_attributes_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_attributes_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_1`
--

DROP TABLE IF EXISTS `shops_products_categories_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_1` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_1`
--

LOCK TABLES `shops_products_categories_1` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_1` DISABLE KEYS */;
INSERT INTO `shops_products_categories_1` VALUES (88267107,817926,12970421,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267108,817942,12970421,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267109,817940,12970421,1,4,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267110,817938,12970421,1,5,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267111,817927,12970422,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267112,817943,12970422,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267113,817941,12970422,1,4,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267114,817939,12970422,1,5,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267115,817926,12970423,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267116,817942,12970423,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267117,817940,12970423,1,4,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267118,817934,12970423,1,7,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267119,817927,12970424,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267120,817943,12970424,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267121,817941,12970424,1,4,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267122,817935,12970424,1,7,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267123,817926,12970425,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267124,817942,12970425,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267125,817932,12970425,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267126,817930,12970425,1,9,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267127,817927,12970426,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267128,817943,12970426,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267129,817933,12970426,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267130,817931,12970426,1,9,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267131,817926,12970427,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267132,817942,12970427,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267133,817932,12970427,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267134,817928,12970427,1,10,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267135,817927,12970428,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267136,817943,12970428,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267137,817933,12970428,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267138,817929,12970428,1,10,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267139,817926,12970429,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267140,817942,12970429,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267141,817932,12970429,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267142,817944,12970429,1,11,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267143,817927,12970430,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267144,817943,12970430,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267145,817933,12970430,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267146,817945,12970430,1,11,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267147,817926,12970431,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267148,817942,12970431,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267149,817932,12970431,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267150,817944,12970431,1,11,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267151,817927,12970432,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267152,817943,12970432,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267153,817933,12970432,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267154,817945,12970432,1,11,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267155,817926,12970433,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267156,817942,12970433,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267157,817932,12970433,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267158,817944,12970433,1,11,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267159,817927,12970434,1,2,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267160,817943,12970434,1,3,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267161,817933,12970434,1,8,'2018-04-04 12:27:16','2018-04-03 11:34:22'),(88267162,817945,12970434,1,11,'2018-04-04 12:27:16','2018-04-03 11:34:22');
/*!40000 ALTER TABLE `shops_products_categories_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_10`
--

DROP TABLE IF EXISTS `shops_products_categories_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_10` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_10`
--

LOCK TABLES `shops_products_categories_10` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_2`
--

DROP TABLE IF EXISTS `shops_products_categories_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_2` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_2`
--

LOCK TABLES `shops_products_categories_2` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_3`
--

DROP TABLE IF EXISTS `shops_products_categories_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_3` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_3`
--

LOCK TABLES `shops_products_categories_3` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_4`
--

DROP TABLE IF EXISTS `shops_products_categories_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_4` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_4`
--

LOCK TABLES `shops_products_categories_4` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_5`
--

DROP TABLE IF EXISTS `shops_products_categories_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_5` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_5`
--

LOCK TABLES `shops_products_categories_5` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_6`
--

DROP TABLE IF EXISTS `shops_products_categories_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_6` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_6`
--

LOCK TABLES `shops_products_categories_6` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_7`
--

DROP TABLE IF EXISTS `shops_products_categories_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_7` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_7`
--

LOCK TABLES `shops_products_categories_7` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_8`
--

DROP TABLE IF EXISTS `shops_products_categories_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_8` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_8`
--

LOCK TABLES `shops_products_categories_8` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_9`
--

DROP TABLE IF EXISTS `shops_products_categories_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_9` (
  `id_shop_product_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_product_category_list` int(10) unsigned DEFAULT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category`),
  UNIQUE KEY `id_shop_product_category` (`id_shop_product_category`,`date`),
  UNIQUE KEY `id_shop_product_category_list_id_shop_product_id_shop_date` (`id_shop_product_category_list`,`id_shop_product`,`id_shop`,`date`),
  KEY `id_shop_product` (`id_shop_product`),
  KEY `id_category` (`id_category`),
  KEY `id_shop` (`id_shop`),
  KEY `tmp1` (`id_shop_product`)
) ENGINE=InnoDB AUTO_INCREMENT=88267163 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_9`
--

LOCK TABLES `shops_products_categories_9` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_1`
--

DROP TABLE IF EXISTS `shops_products_categories_list_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_1` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_1`
--

LOCK TABLES `shops_products_categories_list_1` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_1` DISABLE KEYS */;
INSERT INTO `shops_products_categories_list_1` VALUES (817924,1,1,0,'fr','Racine','','http://neopresta.shopimind.com/index.php?id_category=1&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:21'),(817925,1,1,0,'gb','Racine','','http://neopresta.shopimind.com/index.php?id_category=1&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:21'),(817926,1,2,1,'fr','Accueil','','http://neopresta.shopimind.com/index.php?id_category=2&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:21'),(817927,1,2,1,'gb','Accueil','','http://neopresta.shopimind.com/index.php?id_category=2&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:21'),(817928,1,10,8,'fr','Robes de soirée','Trouvez la robe parfaite pour une soirée inoubliable !','http://neopresta.shopimind.com/index.php?id_category=10&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817929,1,10,8,'gb','Robes de soirée','Trouvez la robe parfaite pour une soirée inoubliable !','http://neopresta.shopimind.com/index.php?id_category=10&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817930,1,9,8,'fr','Robes décontractées','Vous cherchez une robe pour la vie de tous les jours ? Découvrez\nnotre sélection de robes et trouvez celle qui vous convient.','http://neopresta.shopimind.com/index.php?id_category=9&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817931,1,9,8,'gb','Robes décontractées','Vous cherchez une robe pour la vie de tous les jours ? Découvrez\nnotre sélection de robes et trouvez celle qui vous convient.','http://neopresta.shopimind.com/index.php?id_category=9&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817932,1,8,3,'fr','Robes','Trouvez votre nouvelle pièce préférée parmi une large sélection de robes décontractées, d\'été et de soirée !\nNous avons des robes pour tous les styles et toutes les occasions.','http://neopresta.shopimind.com/index.php?id_category=8&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817933,1,8,3,'gb','Robes','Trouvez votre nouvelle pièce préférée parmi une large sélection de robes décontractées, d\'été et de soirée !\nNous avons des robes pour tous les styles et toutes les occasions.','http://neopresta.shopimind.com/index.php?id_category=8&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817934,1,7,4,'fr','Chemisiers','Coordonnez vos accessoires à vos chemisiers préférés, pour un look parfait.','http://neopresta.shopimind.com/index.php?id_category=7&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817935,1,7,4,'gb','Chemisiers','Coordonnez vos accessoires à vos chemisiers préférés, pour un look parfait.','http://neopresta.shopimind.com/index.php?id_category=7&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817936,1,6,4,'fr','Tops','Choisissez le top qui vous va le mieux, parmi une large sélection.','http://neopresta.shopimind.com/index.php?id_category=6&controller=category&id_lang=1','0','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817937,1,6,4,'gb','Tops','Choisissez le top qui vous va le mieux, parmi une large sélection.','http://neopresta.shopimind.com/index.php?id_category=6&controller=category&id_lang=2','0','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817938,1,5,4,'fr','T-shirts','Les must have de votre garde-robe : découvrez les divers modèles ainsi que les différentes\ncoupes et couleurs de notre collection !','http://neopresta.shopimind.com/index.php?id_category=5&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817939,1,5,4,'gb','T-shirts','Les must have de votre garde-robe : découvrez les divers modèles ainsi que les différentes\ncoupes et couleurs de notre collection !','http://neopresta.shopimind.com/index.php?id_category=5&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817940,1,4,3,'fr','Tops','Choisissez parmi une large sélection de T-shirts à manches courtes, longues ou 3/4, de tops, de débardeurs, de chemisiers et bien plus encore.\nTrouvez la coupe qui vous va le mieux !','http://neopresta.shopimind.com/index.php?id_category=4&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817941,1,4,3,'gb','Tops','Choisissez parmi une large sélection de T-shirts à manches courtes, longues ou 3/4, de tops, de débardeurs, de chemisiers et bien plus encore.\nTrouvez la coupe qui vous va le mieux !','http://neopresta.shopimind.com/index.php?id_category=4&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817942,1,3,2,'fr','Femmes','Vous trouverez ici toutes les collections mode pour femmes.\nCette catégorie regroupe tous les basiques de votre garde-robe et bien plus encore :\nchaussures, accessoires, T-shirts imprimés, robes élégantes et jeans pour femmes !','http://neopresta.shopimind.com/index.php?id_category=3&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817943,1,3,2,'gb','Femmes','Vous trouverez ici toutes les collections mode pour femmes.\nCette catégorie regroupe tous les basiques de votre garde-robe et bien plus encore :\nchaussures, accessoires, T-shirts imprimés, robes élégantes et jeans pour femmes !','http://neopresta.shopimind.com/index.php?id_category=3&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817944,1,11,8,'fr','Robes d\'été','Courte, longue, en soie ou imprimée, trouvez votre robe d\'été idéale !','http://neopresta.shopimind.com/index.php?id_category=11&controller=category&id_lang=1','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22'),(817945,1,11,8,'gb','Robes d\'été','Courte, longue, en soie ou imprimée, trouvez votre robe d\'été idéale !','http://neopresta.shopimind.com/index.php?id_category=11&controller=category&id_lang=2','1','2018-04-04 12:27:16','2018-04-04 12:27:16','2018-04-03 11:34:22');
/*!40000 ALTER TABLE `shops_products_categories_list_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_10`
--

DROP TABLE IF EXISTS `shops_products_categories_list_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_10` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_10`
--

LOCK TABLES `shops_products_categories_list_10` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_2`
--

DROP TABLE IF EXISTS `shops_products_categories_list_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_2` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_2`
--

LOCK TABLES `shops_products_categories_list_2` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_3`
--

DROP TABLE IF EXISTS `shops_products_categories_list_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_3` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_3`
--

LOCK TABLES `shops_products_categories_list_3` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_4`
--

DROP TABLE IF EXISTS `shops_products_categories_list_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_4` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_4`
--

LOCK TABLES `shops_products_categories_list_4` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_5`
--

DROP TABLE IF EXISTS `shops_products_categories_list_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_5` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_5`
--

LOCK TABLES `shops_products_categories_list_5` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_6`
--

DROP TABLE IF EXISTS `shops_products_categories_list_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_6` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_6`
--

LOCK TABLES `shops_products_categories_list_6` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_7`
--

DROP TABLE IF EXISTS `shops_products_categories_list_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_7` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_7`
--

LOCK TABLES `shops_products_categories_list_7` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_8`
--

DROP TABLE IF EXISTS `shops_products_categories_list_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_8` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_8`
--

LOCK TABLES `shops_products_categories_list_8` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops_products_categories_list_9`
--

DROP TABLE IF EXISTS `shops_products_categories_list_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops_products_categories_list_9` (
  `id_shop_product_category_list` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_category_parent` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `date_creation` datetime NOT NULL COMMENT 'Date creation commande selon heure du serveur client',
  `date_modification` datetime NOT NULL COMMENT 'Date modification commande selon heure du serveur client',
  `date` datetime NOT NULL COMMENT 'Date creation selon heure de notre serveur. Décalage entre le serveur du client et notre serveur.',
  PRIMARY KEY (`date`,`id_shop_product_category_list`),
  UNIQUE KEY `id_shop_product_category_list` (`id_shop_product_category_list`,`date`),
  UNIQUE KEY `id_category` (`id_category`,`lang`,`id_shop`,`date`),
  KEY `id_category_parent` (`id_category_parent`),
  KEY `lang` (`lang`),
  KEY `active` (`active`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=817946 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops_products_categories_list_9`
--

LOCK TABLES `shops_products_categories_list_9` WRITE;
/*!40000 ALTER TABLE `shops_products_categories_list_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops_products_categories_list_9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_1`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_1` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_1`
--

LOCK TABLES `sms_sends_unsubscribed_1` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_10`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_10` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_10`
--

LOCK TABLES `sms_sends_unsubscribed_10` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_10` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_2`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_2` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_2`
--

LOCK TABLES `sms_sends_unsubscribed_2` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_3`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_3` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_3`
--

LOCK TABLES `sms_sends_unsubscribed_3` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_3` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_4`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_4` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_4`
--

LOCK TABLES `sms_sends_unsubscribed_4` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_4` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_5`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_5` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_5`
--

LOCK TABLES `sms_sends_unsubscribed_5` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_5` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_6`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_6` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_6`
--

LOCK TABLES `sms_sends_unsubscribed_6` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_6` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_7`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_7` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_7`
--

LOCK TABLES `sms_sends_unsubscribed_7` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_7` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_8`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_8` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_8`
--

LOCK TABLES `sms_sends_unsubscribed_8` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_8` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sends_unsubscribed_9`
--

DROP TABLE IF EXISTS `sms_sends_unsubscribed_9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sends_unsubscribed_9` (
  `id_campaign` int(10) unsigned NOT NULL,
  `type` enum('bulk','relaunch') NOT NULL DEFAULT 'bulk',
  `id_shop` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_campaign`,`id_shop`,`phone`,`date`,`id_shop_customer`,`type`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_id_shop_customer` (`id_shop`,`id_shop_customer`),
  KEY `phone` (`phone`),
  KEY `type_id_shop_date` (`id_shop`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sends_unsubscribed_9`
--

LOCK TABLES `sms_sends_unsubscribed_9` WRITE;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_9` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sends_unsubscribed_9` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-11  9:06:42
