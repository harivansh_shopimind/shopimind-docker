-- MySQL dump 10.17  Distrib 10.3.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: neosoft.shopimind.com    Database: shopymindstats
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB-10.2.13+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `data_content`
--

DROP TABLE IF EXISTS `data_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_content` (
  `id_data_content` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_marketing_workflow` int(10) unsigned NOT NULL DEFAULT 0,
  `id_marketing_workflow_step` int(10) unsigned NOT NULL DEFAULT 0,
  `lang` varchar(2) NOT NULL,
  `content` blob DEFAULT NULL,
  PRIMARY KEY (`id_data_content`),
  UNIQUE KEY `id_marketing_workflow_id_marketing_workflow_step` (`id_marketing_workflow`,`id_marketing_workflow_step`,`lang`),
  KEY `id_marketing_workflow` (`id_marketing_workflow`),
  KEY `id_marketing_workflow_step` (`id_marketing_workflow_step`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_content`
--

LOCK TABLES `data_content` WRITE;
/*!40000 ALTER TABLE `data_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_crossselling`
--

DROP TABLE IF EXISTS `data_crossselling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_crossselling` (
  `id_data_crossselling` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `crossselling` blob NOT NULL,
  PRIMARY KEY (`id_data_crossselling`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_crossselling`
--

LOCK TABLES `data_crossselling` WRITE;
/*!40000 ALTER TABLE `data_crossselling` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_crossselling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_parameters`
--

DROP TABLE IF EXISTS `data_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_parameters` (
  `id_data_parameters` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parameters` blob NOT NULL,
  PRIMARY KEY (`id_data_parameters`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_parameters`
--

LOCK TABLES `data_parameters` WRITE;
/*!40000 ALTER TABLE `data_parameters` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_subject`
--

DROP TABLE IF EXISTS `data_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_subject` (
  `id_data_subject` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_marketing_workflow` int(10) unsigned NOT NULL DEFAULT 0,
  `id_marketing_workflow_step` int(10) unsigned NOT NULL DEFAULT 0,
  `lang` varchar(2) NOT NULL,
  `subject` blob DEFAULT NULL,
  PRIMARY KEY (`id_data_subject`),
  UNIQUE KEY `id_marketing_workflow_id_marketing_workflow_step` (`id_marketing_workflow`,`id_marketing_workflow_step`,`lang`),
  KEY `id_marketing_workflow` (`id_marketing_workflow`),
  KEY `id_marketing_workflow_step` (`id_marketing_workflow_step`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_subject`
--

LOCK TABLES `data_subject` WRITE;
/*!40000 ALTER TABLE `data_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_bounce`
--

DROP TABLE IF EXISTS `stats_bounce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_bounce` (
  `id_stats_bounce` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_stats_send` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(512) NOT NULL COMMENT 'nom, prénom, email',
  `bounce_type` enum('hard-bounce','soft-bounce','soft-bounce-1','soft-bounce-2','soft-bounce-3','spam','unsub') NOT NULL,
  `bounce_msg` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  PRIMARY KEY (`id_stats_bounce`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`bounce_type`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`bounce_type`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_bounce`
--

LOCK TABLES `stats_bounce` WRITE;
/*!40000 ALTER TABLE `stats_bounce` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_bounce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_bounce_1`
--

DROP TABLE IF EXISTS `stats_bounce_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_bounce_1` (
  `id_stats_bounce` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_stats_send` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(512) NOT NULL COMMENT 'nom, prénom, email',
  `bounce_type` enum('hard-bounce','soft-bounce','soft-bounce-1','soft-bounce-2','soft-bounce-3','spam','unsub') NOT NULL,
  `bounce_msg` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  PRIMARY KEY (`id_stats_bounce`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`bounce_type`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`bounce_type`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_bounce_1`
--

LOCK TABLES `stats_bounce_1` WRITE;
/*!40000 ALTER TABLE `stats_bounce_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_bounce_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_click`
--

DROP TABLE IF EXISTS `stats_click`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_click` (
  `id_stats_click` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stats_send` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned NOT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `id_shop_url` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `shops_visitors_fp_id` int(10) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_open` datetime NOT NULL,
  `date_send` datetime NOT NULL COMMENT 'date de mise à jour',
  PRIMARY KEY (`id_stats_click`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_click`
--

LOCK TABLES `stats_click` WRITE;
/*!40000 ALTER TABLE `stats_click` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_click` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_click_1`
--

DROP TABLE IF EXISTS `stats_click_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_click_1` (
  `id_stats_click` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stats_send` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned NOT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `id_shop_url` int(10) unsigned NOT NULL,
  `id_shop_product` int(10) unsigned NOT NULL,
  `shops_visitors_fp_id` int(10) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_open` datetime NOT NULL,
  `date_send` datetime NOT NULL COMMENT 'date de mise à jour',
  PRIMARY KEY (`id_stats_click`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_click_1`
--

LOCK TABLES `stats_click_1` WRITE;
/*!40000 ALTER TABLE `stats_click_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_click_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_open`
--

DROP TABLE IF EXISTS `stats_open`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_open` (
  `id_stats_open` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stats_send` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned NOT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `shops_visitors_fp_id` bigint(20) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_send` datetime NOT NULL COMMENT 'date de mise à jour',
  PRIMARY KEY (`id_stats_open`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_open`
--

LOCK TABLES `stats_open` WRITE;
/*!40000 ALTER TABLE `stats_open` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_open` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_open_1`
--

DROP TABLE IF EXISTS `stats_open_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_open_1` (
  `id_stats_open` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stats_send` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned NOT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `shops_visitors_fp_id` bigint(20) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_send` datetime NOT NULL COMMENT 'date de mise à jour',
  PRIMARY KEY (`id_stats_open`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_open_1`
--

LOCK TABLES `stats_open_1` WRITE;
/*!40000 ALTER TABLE `stats_open_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_open_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_registration`
--

DROP TABLE IF EXISTS `stats_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_registration` (
  `id_stats_registration` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_stats_send` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `shops_visitors_fp_id` bigint(20) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  PRIMARY KEY (`id_stats_registration`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`channel`,`id_marketing_workflow`,`id_marketing_workflows_step`,`id_shop_customer`,`date_creation`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_registration`
--

LOCK TABLES `stats_registration` WRITE;
/*!40000 ALTER TABLE `stats_registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_registration_1`
--

DROP TABLE IF EXISTS `stats_registration_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_registration_1` (
  `id_stats_registration` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_stats_send` int(10) unsigned NOT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `shops_visitors_fp_id` bigint(20) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  PRIMARY KEY (`id_stats_registration`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`channel`,`id_marketing_workflow`,`id_marketing_workflows_step`,`id_shop_customer`,`date_creation`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_registration_1`
--

LOCK TABLES `stats_registration_1` WRITE;
/*!40000 ALTER TABLE `stats_registration_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_registration_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_sell`
--

DROP TABLE IF EXISTS `stats_sell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_sell` (
  `id_stats_sell` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stats_send` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned NOT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `shops_visitors_fp_id` int(10) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `voucher_used` tinyint(1) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `id_customer` varchar(45) NOT NULL,
  `amount` double unsigned NOT NULL,
  `amount_without_tax` double unsigned NOT NULL,
  `currency_rate` double unsigned NOT NULL,
  `currency_rate_to_euro` double unsigned NOT NULL,
  `currency` varchar(3) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_send` datetime NOT NULL,
  `date_open` datetime NOT NULL,
  `date_click` datetime NOT NULL,
  PRIMARY KEY (`id_stats_sell`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `i_3` (`id_marketing_workflow`,`id_marketing_workflows_step`,`date_creation`),
  KEY `i_4` (`id_marketing_workflow`,`id_marketing_workflows_step`,`id_shop_customer`,`date_creation`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_sell`
--

LOCK TABLES `stats_sell` WRITE;
/*!40000 ALTER TABLE `stats_sell` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_sell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_sell_1`
--

DROP TABLE IF EXISTS `stats_sell_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_sell_1` (
  `id_stats_sell` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stats_send` int(10) unsigned NOT NULL DEFAULT 0,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned NOT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(256) NOT NULL COMMENT 'nom, prénom, email',
  `shops_visitors_fp_id` int(10) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `voucher_used` tinyint(1) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `id_shop_order` int(10) unsigned NOT NULL,
  `id_order` varchar(45) NOT NULL,
  `id_customer` varchar(45) NOT NULL,
  `amount` double unsigned NOT NULL,
  `amount_without_tax` double unsigned NOT NULL,
  `currency_rate` double unsigned NOT NULL,
  `currency_rate_to_euro` double unsigned NOT NULL,
  `currency` varchar(3) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_send` datetime NOT NULL,
  `date_open` datetime NOT NULL,
  `date_click` datetime NOT NULL,
  PRIMARY KEY (`id_stats_sell`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `id_stats_send` (`id_stats_send`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `i_3` (`id_marketing_workflow`,`id_marketing_workflows_step`,`date_creation`),
  KEY `i_4` (`id_marketing_workflow`,`id_marketing_workflows_step`,`id_shop_customer`,`date_creation`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_sell_1`
--

LOCK TABLES `stats_sell_1` WRITE;
/*!40000 ALTER TABLE `stats_sell_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_sell_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_send`
--

DROP TABLE IF EXISTS `stats_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_send` (
  `id_stats_send` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned DEFAULT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(512) NOT NULL COMMENT 'nom, prénom, email',
  `shops_visitors_fp_id` int(10) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `open_count` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `click_count` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `order_count` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_modification` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'date de mise à jour',
  `last_date_open` datetime DEFAULT NULL,
  `last_date_click` datetime DEFAULT NULL,
  `last_date_order` datetime DEFAULT NULL,
  PRIMARY KEY (`id_stats_send`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_send`
--

LOCK TABLES `stats_send` WRITE;
/*!40000 ALTER TABLE `stats_send` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_send` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats_send_1`
--

DROP TABLE IF EXISTS `stats_send_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_send_1` (
  `id_stats_send` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT 0,
  `send_key` varchar(40) NOT NULL,
  `channel` enum('email','sms','display') NOT NULL,
  `id_marketing_workflow` int(10) unsigned NOT NULL,
  `id_marketing_workflows_step` int(10) unsigned NOT NULL,
  `id_data_subject` int(10) unsigned DEFAULT NULL,
  `id_data_content` int(10) unsigned NOT NULL,
  `id_data_parameters` int(10) unsigned DEFAULT NULL,
  `id_data_crossselling` int(10) unsigned DEFAULT NULL,
  `id_shop_customer` int(10) unsigned NOT NULL,
  `customer` varchar(512) NOT NULL COMMENT 'nom, prénom, email',
  `shops_visitors_fp_id` int(10) unsigned NOT NULL,
  `voucher` varchar(256) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `open_count` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `click_count` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `order_count` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date de création fixe',
  `date_modification` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'date de mise à jour',
  `last_date_open` datetime DEFAULT NULL,
  `last_date_click` datetime DEFAULT NULL,
  `last_date_order` datetime DEFAULT NULL,
  PRIMARY KEY (`id_stats_send`,`date_creation`),
  KEY `i_1` (`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `id_shop_customer` (`id_shop_customer`),
  KEY `i_2` (`id_shop_customer`,`channel`,`date_creation`,`id_marketing_workflow`,`id_marketing_workflows_step`),
  KEY `send_key` (`send_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 PARTITION BY RANGE (year(`date_creation`))
SUBPARTITION BY HASH (month(`date_creation`))
(PARTITION `p2016` VALUES LESS THAN (2017)
 (SUBPARTITION `dec_2016` ENGINE = InnoDB,
  SUBPARTITION `jan_2016` ENGINE = InnoDB,
  SUBPARTITION `feb_2016` ENGINE = InnoDB,
  SUBPARTITION `mar_2016` ENGINE = InnoDB,
  SUBPARTITION `apr_2016` ENGINE = InnoDB,
  SUBPARTITION `may_2016` ENGINE = InnoDB,
  SUBPARTITION `jun_2016` ENGINE = InnoDB,
  SUBPARTITION `jul_2016` ENGINE = InnoDB,
  SUBPARTITION `aug_2016` ENGINE = InnoDB,
  SUBPARTITION `sep_2016` ENGINE = InnoDB,
  SUBPARTITION `oct_2016` ENGINE = InnoDB,
  SUBPARTITION `nov_2016` ENGINE = InnoDB),
 PARTITION `p2017` VALUES LESS THAN (2018)
 (SUBPARTITION `dec_2017` ENGINE = InnoDB,
  SUBPARTITION `jan_2017` ENGINE = InnoDB,
  SUBPARTITION `feb_2017` ENGINE = InnoDB,
  SUBPARTITION `mar_2017` ENGINE = InnoDB,
  SUBPARTITION `apr_2017` ENGINE = InnoDB,
  SUBPARTITION `may_2017` ENGINE = InnoDB,
  SUBPARTITION `jun_2017` ENGINE = InnoDB,
  SUBPARTITION `jul_2017` ENGINE = InnoDB,
  SUBPARTITION `aug_2017` ENGINE = InnoDB,
  SUBPARTITION `sep_2017` ENGINE = InnoDB,
  SUBPARTITION `oct_2017` ENGINE = InnoDB,
  SUBPARTITION `nov_2017` ENGINE = InnoDB),
 PARTITION `p2018` VALUES LESS THAN (2019)
 (SUBPARTITION `dec_2018` ENGINE = InnoDB,
  SUBPARTITION `jan_2018` ENGINE = InnoDB,
  SUBPARTITION `feb_2018` ENGINE = InnoDB,
  SUBPARTITION `mar_2018` ENGINE = InnoDB,
  SUBPARTITION `apr_2018` ENGINE = InnoDB,
  SUBPARTITION `may_2018` ENGINE = InnoDB,
  SUBPARTITION `jun_2018` ENGINE = InnoDB,
  SUBPARTITION `jul_2018` ENGINE = InnoDB,
  SUBPARTITION `aug_2018` ENGINE = InnoDB,
  SUBPARTITION `sep_2018` ENGINE = InnoDB,
  SUBPARTITION `oct_2018` ENGINE = InnoDB,
  SUBPARTITION `nov_2018` ENGINE = InnoDB),
 PARTITION `p2019` VALUES LESS THAN MAXVALUE
 (SUBPARTITION `dec_2019` ENGINE = InnoDB,
  SUBPARTITION `jan_2019` ENGINE = InnoDB,
  SUBPARTITION `feb_2019` ENGINE = InnoDB,
  SUBPARTITION `mar_2019` ENGINE = InnoDB,
  SUBPARTITION `apr_2019` ENGINE = InnoDB,
  SUBPARTITION `may_2019` ENGINE = InnoDB,
  SUBPARTITION `jun_2019` ENGINE = InnoDB,
  SUBPARTITION `jul_2019` ENGINE = InnoDB,
  SUBPARTITION `aug_2019` ENGINE = InnoDB,
  SUBPARTITION `sep_2019` ENGINE = InnoDB,
  SUBPARTITION `oct_2019` ENGINE = InnoDB,
  SUBPARTITION `nov_2019` ENGINE = InnoDB));
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats_send_1`
--

LOCK TABLES `stats_send_1` WRITE;
/*!40000 ALTER TABLE `stats_send_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats_send_1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-11  9:21:41
